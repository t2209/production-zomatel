<?php

namespace App\Providers;

use App\Models\Article;
use App\Models\Articleservice;
use App\Models\Eventarticle;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class ArticleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public static function sumArticleServiceQte($array)
    {
        $sumArtQte = array_reduce(
            $array,
            function (array $sum, array $item) {
                $article = $item['ArticleArticleID'];
                if (array_key_exists($article, $sum)) {
                    $sum[$article]['ArticleServiceQty'] += $item['ArticleServiceQty'];
                } else {
                    $sum[$article] = $item;
                }
                return $sum;
            },
            array()
        );
        $sumArtQte = array_values($sumArtQte);
        return $sumArtQte;
    }

    public static function sumArticleEventQte($array)
    {
        $sumArtQte = array_reduce(
            $array,
            function (array $sum, array $item) {
                $article = $item['ArticleArticleID'];
                if (array_key_exists($article, $sum)) {
                    $sum[$article]['EventArticleQty'] += $item['EventArticleQty'];
                } else {
                    $sum[$article] = $item;
                }
                return $sum;
            },
            array()
        );
        $sumArtQte = array_values($sumArtQte);
        return $sumArtQte;
    }

    public static function sumArticleQte($array)
    {
        $sumArtQte = array_reduce(
            $array,
            function (array $sum, array $item) {
                $article = $item['ArticleID'];
                if (array_key_exists($article, $sum)) {
                    $sum[$article]['ArticleQty'] += $item['ArticleQty'];
                } else {
                    $sum[$article] = $item;
                }
                return $sum;
            },
            array()
        );
        $sumArtQte = array_values($sumArtQte);
        return $sumArtQte;
    }

    public static function getArticles($searchValue,$serviceID, $eventID, $articleClassID,$isArchived = false,$week = null){
        // dd($week);
        $weekView = $week != null ? explode("-W",$week) : null;
        // dd($weekView[1]);
        $weekBefore = $week != null ?
        Carbon::now()
        ->setISODate(
            $weekView[0],
            $weekView[1]
            )
        ->subWeek()
        : null;


        // dd($weekBefore->endOfWeek(Carbon::SUNDAY));
        $articlesService = $serviceID != null ? Articleservice::
            when($serviceID != '*', function($query) use ($serviceID) {
                return $query->where('ServiceServiceID', '=', $serviceID);
            })
            ->whereHas('article', function ($query) use ($isArchived,$articleClassID, $weekBefore){
                $query->where('ArticleIsArchived',$isArchived)
                ->when($weekBefore != null , function($query) use ($weekBefore) {
                    return $query->whereDate('created_at', '=', $weekBefore->endOfWeek(Carbon::SUNDAY)->format('Y-m-d'));
                })
                ->when($articleClassID != '*' && $articleClassID != null, function($query) use ($articleClassID) {
                    return $query->where('ArticleClassArticleClassID', '=', $articleClassID);
                });
            })
            ->whereHas('article', function ($query) use ($searchValue) {
                $query->where('ArticleName','like', '%' . $searchValue . '%');
            })
            ->with('service')
            ->with('article.articleclass')

            //  ->with([
            //     'article.articleclass' => function ($query) use ($articleClassID, $isArchived) {
            //         $query->when($articleClassID != '*' && $articleClassID != null, function($query) use ($articleClassID) {
            //             return $query->where('ArticleClassID', '=', $articleClassID);
            //         });
            //     },
            //     // 'article' => function ($query) use ($isArchived) {
            //     //     $query->where('article.ArticleIsArchived','=', $isArchived);
            //     // },
            // ])
            ->groupBy(["ArticleArticleID",'ServiceServiceID'])
            // ->sum('ArticleServiceQty')
            // ->groupBy()
            ->selectRaw('*,ArticleServiceQty as ArticleQtyGen,SUM(ArticleServiceArticlePrice * ArticleServiceQty) as TotalPrice')
            ->latest()
            // ->distinct()
            ->get()
            // ->unique('ArticleArticleID','ServiceServiceID')
            ->unique(function ($item){
                return $item['ArticleArticleID'] . $item['ServiceServiceID'];
            })
            ->toArray()
            :[];



        foreach ($articlesService as $key => $article) {
            if($article['TotalPrice'] == null){
                // array_slice($articlesService,$key);
                $articlesService = [];
            }
        }
        // dd("qty articleService",$articlesService
        // );
        $articlesEvent = $eventID != null ? Eventarticle::
            whereHas('article', function ($query) use ($isArchived,$articleClassID,$weekBefore){
                $query->where('ArticleIsArchived',$isArchived)
                ->when($weekBefore != null , function($query) use ($weekBefore) {
                    return $query->whereDate('created_at', '=', $weekBefore->endOfWeek(Carbon::SUNDAY)->format('Y-m-d'));
                })
                ->when($articleClassID != '*' && $articleClassID != null, function($query) use ($articleClassID) {
                    return $query->where('ArticleClassArticleClassID', '=', $articleClassID);
                });;
            })
            ->whereHas('event', function ($query) use ($eventID){
                $query
                ->when($eventID  != "*" && $eventID != null, function($query) use ($eventID) {
                    return $query->where('EventTypeEventTypeID', '=', $eventID);
                });;
            })
            ->whereHas('article', function ($query) use ($searchValue) {
                $query->where('ArticleName','like', '%' . $searchValue . '%');
            })
            ->with('event')
            ->with('article.articleclass')
            ->groupBy(["ArticleArticleID",'EventEventID'])
            ->selectRaw('*, EventArticleQty as ArticleQtyGen, SUM(EventArticleArticlePrice * EventArticleQty) as TotalPrice')
            ->latest()
            ->get()
            ->unique(function ($item){
                return $item['ArticleArticleID'] . $item['EventEventID'];
            })
            ->toArray()
            :[];
            //    return $articlesService->merge($articlesEvent);
        // dd("articles event", $articlesEvent);

        foreach ($articlesEvent as $key => $article) {
            if($article['TotalPrice'] == null){
                // array_slice($articlesService,$key);
                $articlesEvent = [];
            }
        }


        // get sum of article qte
        // $qteService = [];
        // if(!empty($articlesService)){
        //     $qteArticleService = static::sumArticleServiceQte($articlesService);
        //     for ($i=0; $i < count($qteArticleService); $i++) {
        //         $qteService[$i]["ArticleID"] = $qteArticleService[$i]["ArticleArticleID"];
        //         $qteService[$i]["ArticleQty"] = $qteArticleService[$i]["ArticleServiceQty"];
        //     }
        // }

        // $qteEvent = [];
        // if (!empty($articlesEvent)) {
        //     $qteArticleEvent = static::sumArticleEventQte($articlesEvent);
        //     for ($i=0; $i < count($qteArticleEvent); $i++) {
        //         $qteEvent[$i]["ArticleID"] = $qteArticleEvent[$i]["ArticleArticleID"];
        //         $qteEvent[$i]["ArticleQty"] = $qteArticleEvent[$i]["EventArticleQty"];
        //     }
        // }

        // $ArticleMerge = array_merge($articlesService,$articlesEvent);

        // $qteArticle = static::sumArticleQte($qteArticleMerge);

        // dd($articlesService);
        // if (!empty($articlesEvent)) {
        //     $articlesEvent = array_values($articlesEvent);
        //     for ($i=0; $i < count($articlesEvent); $i++) {
        //         for ($j=0; $j < count($qteArticle); $j++) {
        //             if ($qteArticle[$j]["ArticleID"] == $articlesEvent[$i]["ArticleArticleID"]) {
        //                 $articlesEvent[$i]["ArticleQtyGen"] = $qteArticle[$j]["ArticleQty"];
        //             }
        //         }
        //     }
        // }

        // dd($articlesService);
        // if (!empty($articlesService)) {
        //     $articlesService = array_values($articlesService);
        //     for ($i=0; $i < count($articlesService); $i++) {
        //         for ($j=0; $j < count($qteArticle); $j++) {
        //             if ($qteArticle[$j]["ArticleID"] == $articlesService[$i]["ArticleArticleID"]) {
        //                 $articlesService[$i]["ArticleQtyGen"] = $qteArticle[$j]["ArticleQty"];
        //             }
        //         }
        //     }
        // }
        // dd(array_merge($articlesEvent, $articlesService));
        // dd(array_merge($articlesEvent, $articlesService));
        return array_merge($articlesEvent, $articlesService);
    // ->orderBy('eventarticle.EventArticleID','DESC')
    // ->groupBy('eventarticle.ArticleArticleID')
    // ->rightJoin('articleservice','article.ArticleID','=','articleservice.ArticleArticleID')
    // ->orderBy('articleservice.ArticleServiceID','DESC')
    // ->unique('ArticleArticleID')
    // ->with('services.rawmatarticles.rawmaterialprovider','events.rawmatarticles.rawmaterialprovider','articleclass')
    // ->with('event.articles.articleclass','service.articles.articleclass',
    // 'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial.rawmatfamily','article.articleclass')
    // ->with(['services' => function ($query) {
    //     $query->when($serviceID != '*', function($query) use ($serviceID) {
    //         return $query->where('ServiceServiceID', '=', $serviceID);
    //       })
    // },
    // 'championships.settings' => function ($query) {
    //     $query->where('something', 'like', '%this%');
    // },
    // ...])
    // ->when($serviceID != '*', function($query) use ($serviceID) {
    //     return $query->where('articleservice.ServiceServiceID', '=', $serviceID);
    //   })
    // ->when($eventID != '*', function($query) use ($eventID) {
    //     return $query->where('eventarticle.EventEventID', '=', $eventID);
    // })
    // ->get()
    // ->unique('ArticleArticleID')
    ;
    }

    public static function getArticlesValue($serviceID, $eventID, $articleClassID,$isArchived = false,$week = null){
        // dd($week);
        $weekView = $week != null ? explode("-W",$week) : null;
        // dd($weekView[1]);
        $weekBefore = $week != null ?
        Carbon::now()
        ->setISODate(
            $weekView[0],
            $weekView[1]
            )
        ->subWeek()
        : null;


        // dd($weekBefore->endOfWeek(Carbon::SUNDAY));
        $articlesService = $serviceID != null ? Articleservice::
            when($serviceID != '*', function($query) use ($serviceID) {
                return $query->where('ServiceServiceID', '=', $serviceID);
            })
            ->whereHas('article', function ($query) use ($isArchived,$articleClassID, $weekBefore){
                $query->where('ArticleIsArchived',$isArchived)
                ->when($weekBefore != null , function($query) use ($weekBefore) {
                    return $query->whereDate('created_at', '=', $weekBefore->endOfWeek(Carbon::SUNDAY)->format('Y-m-d'));
                })
                ->when($articleClassID != '*' && $articleClassID != null, function($query) use ($articleClassID) {
                    return $query->where('ArticleClassArticleClassID', '=', $articleClassID);
                });
            })
            ->with('service')
            ->with('article.articleclass')
            //  ->with([
            //     'article.articleclass' => function ($query) use ($articleClassID, $isArchived) {
            //         $query->when($articleClassID != '*' && $articleClassID != null, function($query) use ($articleClassID) {
            //             return $query->where('ArticleClassID', '=', $articleClassID);
            //         });
            //     },
            //     // 'article' => function ($query) use ($isArchived) {
            //     //     $query->where('article.ArticleIsArchived','=', $isArchived);
            //     // },
            // ])
            // ->groupBy(["ArticleArticleID",'ServiceServiceID'])
            ->join('')
            ->selectRaw('*,SUM(ArticleServiceArticlePrice * ArticleServiceQty) as TotalPrice')
            ->groupBy()
            ->latest()
            // ->distinct()
            ->get()
            // ->unique('ArticleArticleID','ServiceServiceID')
            ->unique(function ($item){
                return $item['ArticleArticleID'] . $item['ServiceServiceID'];
            })
            ->toArray()
            :[];

            // dd($articlesService);

        $articlesEvent = $eventID != null ? Eventarticle::
            whereHas('article', function ($query) use ($isArchived,$articleClassID,$weekBefore){
                $query->where('ArticleIsArchived',$isArchived)
                ->when($weekBefore != null , function($query) use ($weekBefore) {
                    return $query->whereDate('created_at', '=', $weekBefore->endOfWeek(Carbon::SUNDAY)->format('Y-m-d'));
                })
                ->when($articleClassID != '*' && $articleClassID != null, function($query) use ($articleClassID) {
                    return $query->where('ArticleClassArticleClassID', '=', $articleClassID);
                });;
            })
            ->whereHas('event', function ($query) use ($eventID){
                $query
                ->when($eventID  != "*" && $eventID != null, function($query) use ($eventID) {
                    return $query->where('EventTypeEventTypeID', '=', $eventID);
                });;
            })
            ->with('event')
            ->with('article.articleclass')

        //     ->with([
        //         'article.articleclass' => function ($query) use ($articleClassID) {
        //            $query->when($articleClassID != '*' && $articleClassID != null, function($query) use ($articleClassID) {
        //                return $query->where('ArticleClassArticleClassID', '=', $articleClassID);
        //            });
        //        },
        //     //    'article' => function ($query) use ($isArchived) {
        //     //     $query->where('article.ArticleIsArchived','=', $isArchived);
        //     //     },
        //    ])
                  ->latest()
            ->get()
            ->unique(function ($item){
                return $item['ArticleArticleID'] . $item['EventEventID'];
            })
            ->toArray()
            :[];
            //    return $articlesService->merge($articlesEvent);

            // get sum of article qte
            $qteArticleService = static::sumArticleServiceQte($articlesService);
            $qteService = [];
            for ($i=0; $i < count($qteArticleService); $i++) {
                $qteService[$i]["ArticleID"] = $qteArticleService[$i]["ArticleArticleID"];
                $qteService[$i]["ArticleQty"] = $qteArticleService[$i]["ArticleServiceQty"];
            }

            $qteArticleEvent = static::sumArticleEventQte($articlesEvent);
            $qteEvent = [];
            for ($i=0; $i < count($qteArticleEvent); $i++) {
                $qteEvent[$i]["ArticleID"] = $qteArticleEvent[$i]["ArticleArticleID"];
                $qteEvent[$i]["ArticleQty"] = $qteArticleEvent[$i]["EventArticleQty"];
            }
            $qteArticleMerge = array_merge($qteService,$qteEvent);

            $qteArticle = static::sumArticleQte($qteArticleMerge);

            // dd($qteArticle);
            if (!empty($articlesEvent)) {
                $articlesEvent = array_values($articlesEvent);
                for ($i=0; $i < count($articlesEvent); $i++) {
                    for ($j=0; $j < count($qteArticle); $j++) {
                        if ($qteArticle[$j]["ArticleID"] == $articlesEvent[$i]["ArticleArticleID"]) {
                            $articlesEvent[$i]["ArticleQtyGen"] = $qteArticle[$j]["ArticleQty"];
                        }
                    }
                }
            }

            // dd($articlesService);
            if (!empty($articlesService)) {
                $articlesService = array_values($articlesService);
                for ($i=0; $i < count($articlesService); $i++) {
                    for ($j=0; $j < count($qteArticle); $j++) {
                        if ($qteArticle[$j]["ArticleID"] == $articlesService[$i]["ArticleArticleID"]) {
                            $articlesService[$i]["ArticleQtyGen"] = $qteArticle[$j]["ArticleQty"];
                        }
                    }
                }
            }
            // dd(array_merge($articlesEvent, $articlesService));
            // dd(array_merge($articlesEvent, $articlesService));
            return array_merge($articlesEvent, $articlesService);
        // ->orderBy('eventarticle.EventArticleID','DESC')
        // ->groupBy('eventarticle.ArticleArticleID')
        // ->rightJoin('articleservice','article.ArticleID','=','articleservice.ArticleArticleID')
        // ->orderBy('articleservice.ArticleServiceID','DESC')
        // ->unique('ArticleArticleID')
        // ->with('services.rawmatarticles.rawmaterialprovider','events.rawmatarticles.rawmaterialprovider','articleclass')
        // ->with('event.articles.articleclass','service.articles.articleclass',
        // 'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial.rawmatfamily','article.articleclass')
        // ->with(['services' => function ($query) {
        //     $query->when($serviceID != '*', function($query) use ($serviceID) {
        //         return $query->where('ServiceServiceID', '=', $serviceID);
        //       })
        // },
        // 'championships.settings' => function ($query) {
        //     $query->where('something', 'like', '%this%');
        // },
        // ...])
        // ->when($serviceID != '*', function($query) use ($serviceID) {
        //     return $query->where('articleservice.ServiceServiceID', '=', $serviceID);
        //   })
        // ->when($eventID != '*', function($query) use ($eventID) {
        //     return $query->where('eventarticle.EventEventID', '=', $eventID);
        // })
        // ->get()
        // ->unique('ArticleArticleID')
        ;
    }

    public static function getArticlesQty($searchValue,$serviceID, $eventID, $articleClassID,$isArchived = false){
        $articlesService = $serviceID != null ? Articleservice::
            when($serviceID != '*', function($query) use ($serviceID) {
                return $query->where('ServiceServiceID', '=', $serviceID);
            })
            ->whereHas('article', function ($query) use ($isArchived,$articleClassID){
                $query->where('ArticleIsArchived',$isArchived)
                ->when($articleClassID != '*' && $articleClassID != null, function($query) use ($articleClassID) {
                    return $query->where('ArticleClassArticleClassID', '=', $articleClassID);
                });
            })
            ->whereHas('article', function ($query) use ($searchValue) {
                $query->where('ArticleName','like', '%' . $searchValue . '%');
            })
            ->with('service')
            ->with('article.articleclass')
            ->latest()
            ->selectRaw('*, SUM(ArticleServiceArticlePrice * ArticleServiceQty) as TotalPrice')
            ->get()
            ->unique(function ($item){
                return $item['ArticleArticleID'] . $item['ServiceServiceID'];
            })
            ->toArray()
            :[];

        $articlesEvent = $eventID != null ? Eventarticle::
            whereHas('article', function ($query) use ($isArchived,$articleClassID){
                $query->where('ArticleIsArchived',$isArchived)
                ->when($articleClassID != '*' && $articleClassID != null, function($query) use ($articleClassID) {
                    return $query->where('ArticleClassArticleClassID', '=', $articleClassID);
                });;
            })
            ->whereHas('event', function ($query) use ($eventID){
                $query
                ->when($eventID  != "*" && $eventID != null, function($query) use ($eventID) {
                    return $query->where('EventTypeEventTypeID', '=', $eventID);
                });;
            })
            ->whereHas('article', function ($query) use ($searchValue) {
                $query->where('ArticleName','like', '%' . $searchValue . '%');
            })
            ->with('event')
            ->with('article.articleclass')
            ->latest()
            ->get()
            ->unique(function ($item){
                return $item['ArticleArticleID'] . $item['EventEventID'];
            })
            ->toArray()
            :[];

            // get sum of article qte
            $qteArticleService = static::sumArticleServiceQte($articlesService);
            $qteService = [];
            for ($i=0; $i < count($qteArticleService); $i++) {
                $qteService[$i]["ArticleID"] = $qteArticleService[$i]["ArticleArticleID"];
                $qteService[$i]["ArticleQty"] = $qteArticleService[$i]["ArticleServiceQty"];
            }

            $qteArticleEvent = static::sumArticleEventQte($articlesEvent);
            $qteEvent = [];
            for ($i=0; $i < count($qteArticleEvent); $i++) {
                $qteEvent[$i]["ArticleID"] = $qteArticleEvent[$i]["ArticleArticleID"];
                $qteEvent[$i]["ArticleQty"] = $qteArticleEvent[$i]["EventArticleQty"];
            }
            $qteArticleMerge = array_merge($qteService,$qteEvent);

            $qteArticle = static::sumArticleQte($qteArticleMerge);

            // dd($qteArticle);
            if (!empty($articlesEvent)) {
                $articlesEvent = array_values($articlesEvent);
                for ($i=0; $i < count($articlesEvent); $i++) {
                    for ($j=0; $j < count($qteArticle); $j++) {
                        if ($qteArticle[$j]["ArticleID"] == $articlesEvent[$i]["ArticleArticleID"]) {
                            $articlesEvent[$i]["ArticleQtyGen"] = $qteArticle[$j]["ArticleQty"];
                        }
                    }
                }
            }

            // dd($articlesService);
            if (!empty($articlesService)) {
                $articlesService = array_values($articlesService);
                for ($i=0; $i < count($articlesService); $i++) {
                    for ($j=0; $j < count($qteArticle); $j++) {
                        if ($qteArticle[$j]["ArticleID"] == $articlesService[$i]["ArticleArticleID"]) {
                            $articlesService[$i]["ArticleQtyGen"] = $qteArticle[$j]["ArticleQty"];
                        }
                    }
                }
            }

            return array_merge($articlesEvent, $articlesService);

    }




    public static function getServiceArticlePrice($articleID, $serviceID){
        $article =  Articleservice::select('ArticleServiceArticlePrice')
        ->where('ArticleArticleID',$articleID)
        ->where('ServiceServiceID',$serviceID)
        // ->latest()
        ->first();
        return $article != null ? $article->ArticleServiceArticlePrice : 0;
    }

    public static function getEventArticlePrice($articleID, $eventID){
        $article =  Eventarticle::select('EventArticleArticlePrice as ArticleServiceArticlePrice')
        ->where('ArticleArticleID',$articleID)
        ->where('EventEventID',$eventID)
        // ->latest()
        ->first();
        return $article != null ? $article->ArticleServiceArticlePrice : 0;
    }

    public static function getArticle($articleID, $serviceID, $eventID){
        $articlesService = $serviceID != null ? Articleservice::
            when($serviceID != '*', function($query) use ($serviceID) {
                return $query->where('ServiceServiceID', '=', $serviceID);
            })
            ->where('ArticleArticleID','=',$articleID)
            ->with('service')
            ->with('article.articleclass')
            ->latest()
            ->get()
            ->unique(function ($item){
                return $item['ArticleArticleID'] . $item['ServiceServiceID'];
            })
            ->toArray()
            :[];

        $articlesEvent = $eventID != null ? Eventarticle::
            whereHas('event', function ($query) use ($eventID){
                $query
                ->when($eventID  != "*" && $eventID != null, function($query) use ($eventID) {
                    return $query->where('EventTypeEventTypeID', '=', $eventID);
                });;
            })
            ->where('ArticleArticleID','=',$articleID)
            ->with('event')
            ->with('article.articleclass')
            ->latest()
            ->get()
            ->unique(function ($item){
                return $item['ArticleArticleID'] . $item['EventEventID'];
            })
            ->toArray()
            :[];
            return array_merge($articlesEvent, $articlesService);
    }

}
