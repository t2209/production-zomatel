<?php

namespace App\Providers;

use App\Models\DailySale;
use App\Models\Purchaseorderprovider;
use App\Models\Rawmatarticle;
use App\Models\Rawmateconomat;
use App\Models\Rawmatrequire;
use App\Models\Rawmatservice;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class RawMatServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public static function getRawMats($articleID, $serviceID, $eventID, $isArchived = false){
        // return Articleservice::
        // with(['article.rawmatarticles.rawmaterial','article.articleclass','service'])
        // ->when($serviceID != '*', function($query) use ($serviceID) {
        //     return $query->where('ServiceServiceID', '=', $serviceID);
        //   })
        //   ->get();
        // dd($articleID);

        return  Rawmatarticle::with('event.articles.articleclass','service.articles.articleclass',
            'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial.rawmatfamily','rawmaterialprovider.rawmaterial.services','article.articleclass','article')
            // ->groupBy('RawMatArticleCode')
            // ->where('article.ArticleIsArchived',false)
            // ->with(["article" => function($q) use ($articleID){
            //     $q->where('article.ArticleIsArchived', false)
            //     ;
            // }])
            // ->whereHas('article', function ($query) use ($isArchived){
            //     $query->where('article.ArticleIsArchived',$isArchived);
            // })

            ->when($articleID != '*' , function($query) use ($articleID) {
                return $query->where('ArticleArticleID', '=', $articleID);
              })
            ->when($articleID == '*', function($query) use ($isArchived){
                $query->whereHas('article', function ($query) use ($isArchived){
                    $query->where('ArticleIsArchived',$isArchived);
                });
            })
            // ->where('ServiceServiceID',$serviceID)
            ->when($serviceID != '*', function($query) use ($serviceID, $isArchived) {
                return $query->where('ServiceServiceID', '=', $serviceID);
              })
            ->when($eventID != '*', function($query) use ($eventID,$isArchived) {
                return $query->where('EventEventID', '=', $eventID);
            })
            // ->whereHas('event', function ($query) use ($eventID){
            //     $query
            //     ->when($eventID  != "*" && $eventID != null, function($query) use ($eventID) {
            //         return $query->where('EventTypeEventTypeID', '=', $eventID);
            //     });;
            // })
            // ->where('EventEventID',$eventID)
            ->latest()
            ->get()
            ->unique(function ($item){
                return $item['ArticleArticleID'] . $item['EventEventID'] . $item['ServiceServiceID'] . $item['RawMaterialRawMaterialID'];
            });
            // ->unique('ArticleArticleID');

    }

    // ?get initial stock in value of rawmat at economat (group by family)
    public static function getRawMatsInitial($week = null){
        $weekView = $week != null ? explode("-W",$week) : null;
        // dd($weekView[1]);
        $weekBefore = $week != null ?
        Carbon::now()
        ->setISODate(
            $weekView[0],
            $weekView[1]
            )
        ->subWeek()
        : null;
        // dd($weekBefore->endOfWeek(Carbon::SUNDAY));
        return  Rawmateconomat::
            whereDate('RawMatEconomatDate', '<=', $weekBefore->endOfWeek(Carbon::SUNDAY)->format('Y-m-d'))
            ->orderBy('RawMatEconomatDate','DESC')
            // ->whereHas('rawmatprovider',function($query){
            //     $query->selectRaw('rawmatprovider.RawMatProviderUnitPrice * RawMatEconomatQty as TotalPrice');
            // })
            ->join('rawmatprovider','rawmatprovider.RawMatProviderID','=','RawMatProviderRawMatProviderID')
            ->join('rawmaterial','rawmaterial.RawMaterialID','=','rawmatprovider.RawMaterialRawMaterialID')
            ->join('rawmatfamily','rawmatfamily.RawMatFamilyID','=','RawMatFamilyRawMatFamilyID')
            ->groupBy('RawMatFamilyRawMatFamilyID')
            ->selectRaw("rawmateconomat.RawMatEconomatID,RawMatProviderRawMatProviderID,RawMatWording
            ,RawMatFamilyID,SUM(rawmatprovider.RawMatProviderUnitPrice * rawmateconomat.RawMatEconomatQty) as TotalPrice")
            ->get()
            ->unique(function ($item){
                return $item['RawMatProviderRawMatProviderID'];
            })
            ->toArray();
            // ->unique('ArticleArticleID');

    }

     // ?get initial stock in value of rawmat at economat (group by family)
     public static function getRawMatQty($week = null){
        $weekView = $week != null ? explode("-W",$week) : null;
        // dd($weekView[1]);
        $weekBefore = $week != null ?
        Carbon::now()
        ->setISODate(
            $weekView[0],
            $weekView[1]
            )
        ->subWeek()
        : null;
        // dd($weekBefore->endOfWeek(Carbon::SUNDAY));
        return  Rawmateconomat::
            whereDate('RawMatEconomatDate', '<=', $weekBefore->endOfWeek(Carbon::SUNDAY)->format('Y-m-d'))
            ->orderBy('RawMatEconomatDate','DESC')
            // ->whereHas('rawmatprovider',function($query){
            //     $query->selectRaw('rawmatprovider.RawMatProviderUnitPrice * RawMatEconomatQty as TotalPrice');
            // })
            ->join('rawmatprovider','rawmatprovider.RawMatProviderID','=','RawMatProviderRawMatProviderID')
            ->join('rawmaterial','rawmaterial.RawMaterialID','=','rawmatprovider.RawMaterialRawMaterialID')
            ->join('rawmatfamily','rawmatfamily.RawMatFamilyID','=','RawMatFamilyRawMatFamilyID')
            ->groupBy('RawMatFamilyRawMatFamilyID')
            ->selectRaw("rawmateconomat.RawMatEconomatID,RawMatProviderRawMatProviderID,RawMatWording
            ,RawMatFamilyID,SUM(rawmatprovider.RawMatProviderUnitPrice * rawmateconomat.RawMatEconomatQty) as TotalPrice")
            ->get()
            ->unique(function ($item){
                return $item['RawMatProviderRawMatProviderID'];
            })
            ->toArray();
            // ->unique('ArticleArticleID');

    }

    public static function getRawMat($rawMatID,$articleID, $serviceID, $eventID, $isArchived = false){
        return  Rawmatarticle::with('event.articles.articleclass','service.articles.articleclass',
            'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial.rawmatfamily','rawmaterialprovider.rawmaterial.services','article.articleclass','article')

            ->when($articleID != '*' , function($query) use ($articleID) {
                return $query->where('ArticleArticleID', '=', $articleID);
              })
            ->when($articleID == '*', function($query) use ($isArchived){
                $query->whereHas('article', function ($query) use ($isArchived){
                    $query->where('ArticleIsArchived',$isArchived);
                });
            })
            ->where('RawMaterialRawMaterialID','=',$rawMatID)
            ->when($serviceID != '*', function($query) use ($serviceID, $isArchived) {
                return $query->where('ServiceServiceID', '=', $serviceID);
              })
            ->when($eventID != '*', function($query) use ($eventID,$isArchived) {
                return $query->where('EventEventID', '=', $eventID);
            })
            ->latest()
            ->get()
            ->unique(function ($item){
                return $item['ArticleArticleID'] . $item['EventEventID'] . $item['ServiceServiceID'] . $item['RawMaterialRawMaterialID'];
            });
    }

    public static function getRawMatforProvider($week ,$searchValue = null,$filterFamily = null,$adressfilter = null, $paid = null)
    {
        $startOfWeek = Carbon::parse($week->startOfWeek())->format("Y-m-d");
        $endOfWeek = Carbon::parse($week->endOfWeek())->format("Y-m-d");
        // dd($endOfWeek);
        $Rawmatprod = Rawmatrequire::with('purchaseorderservice','rawmatprovider',
        'rawmatprovider.provider','rawmatprovider.rawmaterial.rawmatfamily')

        ->whereHas('purchaseorderservice', function ($query) use ($startOfWeek,$endOfWeek){
            $query->whereBetween('POServDateSend',[$startOfWeek,$endOfWeek]);
        })
        ->whereHas('rawmatprovider.rawmaterial', function ($query) use ($searchValue) {
            $query->where('RawMaterialName','like', '%' . $searchValue . '%');
        })
        ->whereHas('rawmatprovider.rawmaterial.rawmatfamily', function ($query) use ($filterFamily) {
            $query->where('RawMatWording','like', '%' . $filterFamily . '%');
        })
        ->whereHas('rawmatprovider.provider', function ($query) use ($adressfilter) {
            $query->where('ProviderAdress','like', '%' . $adressfilter . '%');
        })
        ->when($paid != null,function ($query) use ($paid){
            $query->where('PaymentRef',$paid == false ? '=' : '!=',null);
        })
        // ->groupBy('RawMaterialRawMaterialID')
        // ->groupBy('RawMatProviderRawMatProviderID')

        // ->whereHas('rawmatprovider', function ($query) {
        //     $query->groupBy('RawMatProviderID');
        // })

        ->join('rawmatprovider','rawmatprovider.RawMatProviderID','=','rawmatrequire.RawMatProviderRawMatProviderID')
        ->groupBy('rawmatprovider.ProviderProviderID')

        ->selectRaw('*, SUM(RawMatRequireQty) as RawMatRequireQtySum, SUM(RawMatProviderUnitPrice * RawMatRequireQty) as TotalPrice')

        // ->selectRaw('*, SUM(RawMatProviderUnitPrice) as RawMatRequireTotal')
        // ->orderBy('RawMaterialRawMaterialID','ASC')
        ->get();

        // dd($Rawmatprod);

        // foreach ($Rawmatprod as $key => $value) {
        //     $Rawmatprod[$key]['RawMatRequirePrice'] = $Rawmatprod[$key]['rawmaterial']['rawmatproviders'][0]['RawMatProviderUnitPrice'];
        //     $Rawmatprod[$key]['RawMatRequirePurchasePrice'] = $Rawmatprod[$key]['RawMatRequireQtySum'] * $Rawmatprod[$key]['rawmaterial']['rawmatproviders'][0]['RawMatProviderUnitPrice'];
        // }
        return $Rawmatprod;
    }

    public static function getRawMatAvg($week ,$paid = null)
    {
        $startOfWeek = Carbon::parse($week->startOfWeek())->format("Y-m-d");
        $endOfWeek = Carbon::parse($week->endOfWeek())->format("Y-m-d");
        // dd($endOfWeek);
        $dataArrangeSum =  Rawmatrequire::with('purchaseorderservice','rawmatprovider',
        'rawmatprovider.provider','rawmatprovider.rawmaterial.rawmatfamily')

        ->whereHas('purchaseorderservice', function ($query) use ($startOfWeek,$endOfWeek){
            $query->whereBetween('POServDateSend',[$startOfWeek,$endOfWeek]);
        })

        ->when($paid != null,function ($query) use ($paid){
            $query->where('PaymentRef',$paid == false ? '=' : '!=',null);
        })
        ->join('rawmatprovider','rawmatprovider.RawMatProviderID','=','rawmatrequire.RawMatProviderRawMatProviderID')
        // ->groupBy('rawmatprovider.ProviderProviderID')
        ->join('rawmaterial','rawmaterial.RawMaterialID','=','rawmatprovider.RawMaterialRawMaterialID')
        ->join('rawmatfamily','rawmatfamily.RawMatFamilyID','=','rawmaterial.RawMatFamilyRawMatFamilyID')
        ->groupBy('rawmatfamily.RawMatFamilyID')
        ->selectRaw('*, SUM(RawMatRequireQty) as RawMatRequireQtySum, SUM(RawMatProviderUnitPrice * RawMatRequireQty) as TotalPrice')
        ->get()->toArray();
        $PurchaseTotalPrice = array_sum(array_column($dataArrangeSum,'TotalPrice'));

        foreach ($dataArrangeSum as $key => $valueByFamily) {
            $dataArrangeSum[$key]["PurchaseTotalPrice"] = $PurchaseTotalPrice;
            $dataArrangeSum[$key]['Avg'] = round((($valueByFamily['TotalPrice']/$PurchaseTotalPrice) * 100) , 2) ;
        }
        return $dataArrangeSum;
    }

    public static function getRawMatByProvider($week,$ProviderID,$searchValue = null,$filterFamily = null,$adressfilter = null)
    {
        $startOfWeek = Carbon::parse($week->startOfWeek())->format("Y-m-d");
        $endOfWeek = Carbon::parse($week->endOfWeek())->format("Y-m-d");

        $Rawmats = Rawmatrequire::with('purchaseorderservice','rawmatprovider',
        'rawmatprovider.provider','rawmatprovider.rawmaterial.rawmatfamily')

        ->whereHas('purchaseorderservice', function ($query) use ($startOfWeek,$endOfWeek){
            $query->whereBetween('POServDateSend',[$startOfWeek,$endOfWeek]);
        })
        ->whereHas('rawmatprovider.rawmaterial', function ($query) use ($searchValue) {
            $query->where('RawMaterialName','like', '%' . $searchValue . '%');
        })
        ->whereHas('rawmatprovider.rawmaterial.rawmatfamily', function ($query) use ($filterFamily) {
            $query->where('RawMatWording','like', '%' . $filterFamily . '%');
        })
        ->whereHas('rawmatprovider.provider', function ($query) use ($adressfilter) {
            $query->where('ProviderAdress','like', '%' . $adressfilter . '%');
        })
        ->whereHas('rawmatprovider',function($query) use ($ProviderID){
            $query->where('ProviderProviderID','=',$ProviderID);
        })
        ->join('rawmatprovider','rawmatprovider.RawMatProviderID','=','rawmatrequire.RawMatProviderRawMatProviderID')

        ->groupBy('rawmatprovider.RawMaterialRawMaterialID')
        ->selectRaw('*, SUM(RawMatRequireQty) as TotalQty, SUM(RawMatProviderUnitPrice * RawMatRequireQty) as TotalPrice')
        ->get()
        ;

        return $Rawmats;
    }

    public static function getRawMatByProviderDetail($week,$ProviderID)
    {
        $startOfWeek = Carbon::parse($week->startOfWeek())->format("Y-m-d");
        $endOfWeek = Carbon::parse($week->endOfWeek())->format("Y-m-d");
       return Rawmatrequire::with('purchaseorderservice','rawmatprovider',
        'rawmatprovider.provider')

        ->whereHas('purchaseorderservice', function ($query) use ($startOfWeek,$endOfWeek){
            $query->whereBetween('POServDateSend',[$startOfWeek,$endOfWeek]);
        })
        ->whereHas('rawmatprovider',function($query) use ($ProviderID){
            $query->where('ProviderProviderID','=',$ProviderID);
        })
        ->get()
        ;
    }

    public static function getRawMatPerProd($week)
    {
        $data = RawMatServiceProvider::getRawMatforProvider($week);
        $RawMatPerProd = [];

        foreach ($data as $key => $value) {
            $RawMatPerProd[$key]["RawMatRequireID"] = $value["RawMatRequireID"];
            $RawMatPerProd[$key]["RawMatRequirePurchasePrice"] = $value["RawMatRequirePurchasePrice"];
            $RawMatPerProd[$key]["ProviderID"] = $value["rawmaterial"]["rawmatproviders"][0]["provider"]["ProviderID"];
            $RawMatPerProd[$key]["ProviderName"] = $value["rawmaterial"]["rawmatproviders"][0]["provider"]["ProviderName"];
            $RawMatPerProd[$key]["RawMaterialName"] = $value["rawmaterial"]["RawMaterialName"];
        }

        // dd($data,$RawMatPerProd);

        return $RawMatPerProd;
    }

    public static function getReceivedRwMatProd($startWeek,$endWeek)
    {
        $data = Purchaseorderprovider::with('purchaseorderservices','purchaseorderservices.rawmatrequires',
        'purchaseorderservices.service','purchaseorderservices.rawmatrequires.rawmaterial')
        ->whereBetween('POProvDateSend',[$startWeek,$endWeek])
        ->get();

        $POServs = [];
        foreach ($data as $key => $value) {
            $POServs = $value['purchaseorderservices'];
        }

        $RwMatRequ = [];
        foreach ($POServs as $key => $value) {
            $RwMatRequ[$key] = $value->rawmatrequires;
        }

        return $RwMatRequ;
    }

    public static function getRawMatEconomatQte($rawMatID)
    {
        return  Rawmateconomat::
            select('RawMatEconomatQty')
            ->with('rawmatprovider', function ($query) use ($rawMatID){
               return $query->where('RawMaterialRawMaterialID',$rawMatID);
            })
            // ->where('RawMaterialRawMaterialID',$rawMatID)
            ->orderBy('RawMatEconomatDate','DESC')
            ->limit(1)->get()[0]->RawMatEconomatQty;
        // $economatQte = 0;
        // foreach ($data as $value) {
        //     $economatQte = $value->RawMatEconomatQty;
        // }

        // return $economatQte;
    }

    public static function getLastRawMatQteService($rawMatID,$serviceID)
    {
        $data = Rawmatservice::where('RawMaterialRawMaterialID',$rawMatID)
                            ->where('ServiceServiceID',$serviceID)
                            ->orderBy('RawMatServiceDate','DESC')
                            ->limit(1)
                            ->get();
        $serviceQte = 0;

        if (!empty($data)) {
            foreach ($data as $value) {
                $serviceQte = $value->RawMatServiceQty;
            }
        }

        return $serviceQte;
    }

    public static function getWeekSale($week)
    {
        $startOfWeek = Carbon::parse($week->startOfWeek())->format("Y-m-d");
        $endOfWeek = Carbon::parse($week->endOfWeek())->format("Y-m-d");

        $data = DailySale::whereBetween('Date',[$startOfWeek,$endOfWeek])->get()->toArray();
        $weekSaleValue = array_sum(array_column($data,'Value'));

        return $weekSaleValue;
    }

    public static function getDetailsWeekSale($week)
    {
        $startOfWeek = Carbon::parse($week->startOfWeek())->format("Y-m-d");
        $endOfWeek = Carbon::parse($week->endOfWeek())->format("Y-m-d");

        $data = DailySale::whereBetween('Date',[$startOfWeek,$endOfWeek])->orderBy("Date",("ASC"))->get()->toArray();

        return $data;
    }


}
