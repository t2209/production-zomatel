<?php

namespace App\Providers;

use App\Models\Article;
use App\Models\ArticleOrder;
use App\Models\OrderTable;
use Illuminate\Support\ServiceProvider;

class OrderProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public static function getCookerOrder($type,$date,$filterStatu,$filterOrder,$filterTable,$filterService)
    {
        $data = OrderTable::select('OrderTableID','OrderID','OrderNum','TableNum','ServiceName','ServiceID','name as Username')
        ->join('order','order.OrderID','=','ordertable.OrderOrderID')
        ->join('table','table.TableID','=','ordertable.TableTableID')
        ->join('service','service.ServiceID','=','table.ServiceServiceID')
        ->join('users','users.UserID','=','service.UserUserID')
        ->whereHas('order',function ($query) use ($date){
            return $query->where('created_at','like','%'.$date.'%');
        })
        ->where('OrderNum','like','%'.$filterOrder.'%')
        ->where('TableNum','like','%'.$filterTable.'%')
        ->where('ServiceName','like','%'.$filterService.'%')
        ->get()
        ->groupBy('OrderNum');

        foreach ($data as $key => $value) {
            if (
                count(ArticleOrder::select('ArticleOrderStatus as Status')->where('OrderOrderID',$value[0]->OrderID)->groupBy('ArticleOrderStatus')->get()->toArray()) > 1
            ) {
                $data[$key]["Status"] = "pendingAction";
            } else {
                $data[$key]["Status"] = ArticleOrder::select('ArticleOrderStatus as Status')->where('OrderOrderID',$value[0]->OrderID)->groupBy('ArticleOrderStatus')->get()->toArray()[0]["Status"];
            }
        }

        if ($type == "dailyHistory") {
            $dataKey = [];
            $dailyData = [];
            foreach ($data as $key => $value) {
                if ($value["Status"] == "done") {
                    $dataKey[] = $key;
                }
            }
            for ($i=0; $i < count($dataKey); $i++) { 
                $dailyData[$dataKey[$i]] = $data[$dataKey[$i]];
            }

            return $dailyData;
        }

        if ($filterStatu != "All") {
            $dataKey = [];
            $dataFilterStatu = [];
            foreach ($data as $key => $value) {
                if ($value["Status"] == $filterStatu) {
                    $dataKey[] = $key;
                }
            }
            for ($i=0; $i < count($dataKey); $i++) { 
                $dataFilterStatu[$dataKey[$i]] = $data[$dataKey[$i]];
            }

            return $dataFilterStatu;
        }

        // dd($dataFilterStatu , $dataKey);

        return $data;
    }

    public static function getTotalAllOrder($type,$ServiceName,$date,$dateBegin = null,$dateEnd = null)
    {
        $ArticleOrder = ArticleOrder::with('articleService')
        ->whereHas('order',function($query) use ($date,$type,$dateBegin,$dateEnd){
            if ($type == "simple") {
                return $query->where('created_at','like','%'.$date.'%');
            } elseif ($type == "filter") {
                return $query->whereBetween('created_at',[date_format(date_create($dateBegin),'Y-m-d 00:00:00'),date_format(date_create($dateEnd),'Y-m-d 23:59:59')]);
            } 
            
        })
        ->whereHas('order.ordertables.table.service',function($query) use ($type,$ServiceName){
            if ($type == "filter" && $ServiceName != "*") {
                return $query->where('ServiceName',$ServiceName);
            }
        })
        ->where('ArticleOrderStatus','done')
        ->get()->toArray();

        // dd($dateBegin,$dateEnd,$ArticleOrder);

        $data = [];

        for ($i=0; $i < count($ArticleOrder); $i++) { 
            $data[$i]["ArticleOrderID"] = $ArticleOrder[$i]["ArticleOrderID"];
            $data[$i]["Price"] = $ArticleOrder[$i]["article_service"]["ArticleServiceArticlePrice"] * $ArticleOrder[$i]["ArticleOrderQty"];
        }
        // dd($data);
        return $data;
    }

    public static function getArticleOrders($type,$OrderOrderID)
    {
        switch ($type) {
            case 'history':
                $data = ArticleOrder::with('articleService','articleService.article')
                ->where('OrderOrderID',$OrderOrderID)
                ->where('ArticleOrderStatus','done')
                ->get();
                break;
            case 'daily':
                $data = ArticleOrder::with('articleService','articleService.article')
                ->where('OrderOrderID',$OrderOrderID)
                ->get();
                break;
            
            default:
                # code...
                break;
        }

        return $data;
    }

    public static function getArticleOrderStatuCount($OrderOrderID)
    {
        $data = ArticleOrder::selectRaw('ArticleOrderStatus as Statu,COUNT(ArticleOrderID) as CountStatus')
                            ->where('OrderOrderID',$OrderOrderID)
                            ->groupBy('ArticleOrderStatus')
                            ->get()->toArray();
        $data = array_combine(array_column($data, 'Statu'), $data);
    
        $newArray = array(
            "" => (array_key_exists("",$data)) ? $data[""]["CountStatus"] : 0,
            "pending" => (array_key_exists("pending",$data)) ? $data["pending"]["CountStatus"] : 0,
            "canceled" => (array_key_exists("canceled",$data)) ? $data["canceled"]["CountStatus"] : 0,
            "done" => (array_key_exists("done",$data)) ? $data["done"]["CountStatus"] : 0,
            "return" => (array_key_exists("return",$data)) ? $data["return"]["CountStatus"] : 0
        );

        // dd($data,$newArray);
        return $newArray;
    }
}
