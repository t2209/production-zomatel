<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProviderController extends Controller
{
    //
    public function index()
    {
        return view('pages.providers');
    }

    public function payment()
    {
        return view('pages.paymentProvider');
    }

    public function Balance()
    {
        return view('pages.balance');
    }

    public function crudPayment()
    {
        return view('pages.crudPayment');
    }

    public function grandBook()
    {
        return view('pages.grand-book');
    }
}
