<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EventArticleController extends Controller
{
    //
    public function index()
    {
        return view("pages.article-events");
    }
}
