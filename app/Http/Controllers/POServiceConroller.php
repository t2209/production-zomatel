<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class POServiceConroller extends Controller
{
    public function index()
    {
        return view('pages.poservice');
    }

    public function POProdRedirection()
    {
        return view('pages.purchase_order_provider');
    }

    public function RwMatReceive()
    {
        return view('pages.raw_mat_received');
    }

    public function ServiceRequest()
    {
        return view('pages.service-request');
    }
}
