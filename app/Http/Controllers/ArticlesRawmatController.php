<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Articleservice;
use App\Models\Eventarticle;
use App\Models\Rawmatarticle;
use App\Models\Rawmaterial;
use App\Providers\RawMatServiceProvider;
use Illuminate\Http\Request;

class ArticlesRawmatController extends Controller
{
    public function boot(Request $request){
        dd($request);
    }

    public function index()
    {
        return view('pages.articles_rawmat');
    }

    // public function loadService($id=null)
    // {
    //     // dd($id);
    //     return view('pages.articles_rawmat_service');
    // }

    // public function loadEvent()
    // {
    //     return view('pages.articles_rawmat_event');
    // }

    // public function loadClass()
    // {
    //     return view('pages.articles_rawmat_class');
    // }

    public function avgFamily($Articles,$forValues=false,$tempArticleRawMats = null, $tempArticleQty = 0)
    {
        $arrayFamilyPerCent = [];

        // dd($Articles);

        foreach ($Articles as $key => $value) {
            // $this->arrayFt[$key]["ID"] = Rawmaterial::with('rawmatproviders')->where('RawMaterialID',$value->rawmaterialprovider->RawMaterialRawMaterialID)->get();

            $rawMats= null;
            $articleQty = 0;
            // dd(Articleservice::class);
            if (getClassInstance($value) === Articleservice::class){
                $rawMats= RawMatServiceProvider::getRawMats($value['article']['ArticleID'],getCode($value),null);
                $articleQty= $value['ArticleServiceQty'];

            }else if(getClassInstance($value) === Eventarticle::class){
                $rawMats= RawMatServiceProvider::getRawMats($value['article']['ArticleID'],null,getCode($value));
                $articleQty = $value['EventArticleQty'];
                // if ($tempArticleRawMats != null) {
                //     // dd($tempArticleRawMats);
                //     $rawMats = array_merge($rawMats->toArray(),$tempArticleRawMats);
                // }
            }
            // dd($rawMats);
            // $arrayArticles[$key]["CostPrice"] = 0;
            $this->getStat($rawMats, $forValues, $articleQty, $arrayFamilyPerCent);
        }
        // stat of the temp article begin
        if ($tempArticleRawMats != null ) {
            $this->getStat($tempArticleRawMats, $forValues, $tempArticleQty, $arrayFamilyPerCent);
        }
        // stat of the temp article end
        $total = array_sum(array_column($arrayFamilyPerCent,'value'));

        // dd($arrayFamilyPerCent);
        // dd($total);

        $valuePerFamily = array_reduce(
            $arrayFamilyPerCent,
            function (array $families, array $item) use ($total) {

                $item['percentage']= round (($item['value'] * 100) / $total, 2);
                array_push($families,$item);
                return $families;
            },
            array()
        );
        return $valuePerFamily;
    }

    public function getStat($rawMats, $forValues, $articleQty, &$arrayFamilyPerCent){
        foreach ($rawMats as $rawMat) {
            $familyID = $rawMat['rawmaterialprovider']['rawmaterial']['rawmatfamily']['RawMatFamilyID'];
            $rawmatLastPrice = getRawMatLastPrice($rawMat['rawmaterialprovider']['RawMaterialRawMaterialID'])[0]->RawMatProviderUnitPrice;

            if (!array_key_exists($familyID,$arrayFamilyPerCent)) {
                $arrayFamilyPerCent[$familyID]['name'] = $rawMat['rawmaterialprovider']['rawmaterial']['rawmatfamily']['RawMatWording'];
                $forValues ?
                $arrayFamilyPerCent[$familyID]['value'] = $rawMat['rawmaterialprovider']['RawMatProviderUnitPrice'] * $rawMat['RawMatArticleQty']  * $articleQty
                // !update| delete articleQty multiplicator
                // dd(getRawMatLastPrice($rawMat['rawmaterialprovider']->RawMaterialRawMaterialID));
                :
                $arrayFamilyPerCent[$familyID]['value'] = $rawmatLastPrice * $rawMat->RawMatArticleQty  ;


            }else{
                $forValues ?
                $arrayFamilyPerCent[$familyID]['value'] += $rawMat['rawmaterialprovider']['RawMatProviderUnitPrice'] * $rawMat['RawMatArticleQty']  * $articleQty
                // !update| delete articleQty multiplicator
                :
                $arrayFamilyPerCent[$familyID]['value'] += $rawmatLastPrice * $rawMat['RawMatArticleQty'] ;

            }
        }
    }

    public function statArticles($Articles)
    {
        // dd($Articles);
        $arrayArticles = [];
        foreach ($Articles as $key => $value) {
            $arrayArticles[$key]["ArticleName"] = $value['article']['ArticleName'];
            // $arrayArticles[$key]["RawMatArticleCode"] = $value->RawMatArticleCode;
            $arrayArticles[$key]["PriceHT"] = round(floatval(getPrice($value)) * (100/120) , 2);
            $rawMats= null;
            // dd(Articleservice::class);
            if (getClassInstance($value) === Articleservice::class){
                $rawMats = RawMatServiceProvider::getRawMats($value['article']['ArticleID'],getCode($value),null);
            }else if(getClassInstance($value) === Eventarticle::class){
                $rawMats= RawMatServiceProvider::getRawMats($value['article']['ArticleID'],null,getCode($value));
            }
            $arrayArticles[$key]["CostPrice"] = 0;
            foreach ($rawMats as $rawMat) {
                $arrayArticles[$key]["CostPrice"] += $rawMat->rawmaterialprovider->RawMatProviderUnitPrice * $rawMat->RawMatArticleQty;
            }

        }
        // dd($arrayArticles);

        // $arrayRawmat = [];
        // foreach ($RawMats as $key => $value) {
        //     // $this->arrayFt[$key]["ID"] = Rawmaterial::with('rawmatproviders')->where('RawMaterialID',$value->rawmaterialprovider->RawMaterialRawMaterialID)->get();
        //     // $arrayRawmat[$key]["RawMatArticleCode"] = $value->RawMatArticleCode;
        //     // calcul prix de revient
        //     // $prixAchat = $this->arrayFt[$key]["ID"][0]->rawmatproviders[0]->RawMatProviderUnitPrice;
        //     $prixAchat = $value->rawmaterialprovider->RawMatProviderUnitPrice;
        //     // $prixRevient = floatval($prixAchat) * floatval($value->RawMatArticleQty);
        //     $prixRevient = $prixAchat * $value->RawMatArticleQty;

        //     $arrayRawmat[$key]["PrixRevient"] = $prixRevient;
        // }

        // $arraySumRwmat = array_reduce(
        //     $arrayRawmat,
        //     function (array $carry, array $item) {
        //         $city = $item['RawMatArticleCode'];
        //         if (array_key_exists($city, $carry)) {
        //             $carry[$city]['PrixRevient'] += $item['PrixRevient'];
        //         } else {
        //             $carry[$city] = $item;
        //         }
        //         return $carry;
        //     },
        //     array()
        // );
        // dd($arrayArticles);

        $stat = array_reduce(
            $arrayArticles,
            function (array $carry, array $item) {
                $tempCoeff =round(floatval($item["PriceHT"]) / floatval($item["CostPrice"]) , 2);
                $tempCoutMat = round((floatval($item["CostPrice"]) / floatval($item["PriceHT"]) * 100) , 2);;
                if (array_key_exists('Coeff', $carry) && array_key_exists('CoutMat', $carry)) {
                    $carry['Coeff'] += $tempCoeff;
                    $carry['CoutMat'] += $tempCoutMat;
                }else{
                    $carry['Coeff'] = $tempCoeff;
                    $carry['CoutMat'] = $tempCoutMat;
                }
                return $carry;
            },
            array()
        );
        // dd($stat);
        // $arraySumRwmat = array_values($arraySumRwmat);

        // $arrayMerge = [];
        // // dd($arrayArticles);
        // for ($i=0; $i < count($arrayArticles); $i++) {
        //     for ($j=0; $j < count($arraySumRwmat); $j++) {
        //         if ($arrayArticles[$i]["RawMatArticleCode"] == $arraySumRwmat[$j]["RawMatArticleCode"]) {
        //             $arrayMerge[$i]["RawMatArticleCode"] = $arrayArticles[$i]["RawMatArticleCode"];
        //             $arrayMerge[$i]["CoutMat"] = round(floatval($arraySumRwmat[$j]["PrixRevient"]) / floatval($arrayArticles[$i]["PriceHT"]) * 100 , 2);
        //             $arrayMerge[$i]["Coeff"] = round(floatval($arrayArticles[$i]["PriceHT"]) / floatval($arraySumRwmat[$j]["PrixRevient"]) , 2);
        //         }
        //     }
        // }

        $arrayStat = [];
        if (count($stat) == 0) {
            $arrayStat["CoutMat"] = 0;
            $arrayStat["Coeff"] = 0;
        } else {
            $arrayStat["CoutMat"] = round($stat['CoutMat'] / count($arrayArticles) , 2);
            $arrayStat["Coeff"] = round($stat['Coeff'] / count($arrayArticles) , 2);
        }


        // dd($arrayStat);

        return $arrayStat;

    }

    // !not used anymore
    // !replaced by getRawMatAvg in RawMatServiceProvider
    //! begin
    public function avgRawmatProviderPurchase($items)
    {
        $PurchaseTotalPrice = array_sum(array_column($items,'TotalPrice'));
        $dataArrange = [];
        // ?iteration of all provider
        foreach ($items as $key => $value) {
            dd($value);
            $dataArrange[$key]["RawmatFamily"] = $value['rawmatprovider']['rawmaterial']['rawmatfamily']['RawMatWording'];
            $dataArrange[$key]["RawMatRequirePurchasePrice"] = $value['TotalPrice'];
        }

        $dataArrangeSum = array_reduce(
            $dataArrange,
            function (array $data, array $item) {
                $city = $item['RawmatFamily'];
                if (array_key_exists($city, $data)) {
                    $data[$city]['RawMatRequirePurchasePrice'] += $item['RawMatRequirePurchasePrice'];
                } else {
                    $data[$city] = $item;
                }
                return $data;
            },
            array()
        );
        $dataArrangeSum = array_values($dataArrangeSum);

        foreach ($dataArrangeSum as $key => $value) {
            $dataArrangeSum[$key]["PurchaseTotalPrice"] = $PurchaseTotalPrice;
            $dataArrangeSum[$key]["Avg"] = round((($dataArrangeSum[$key]['RawMatRequirePurchasePrice']/$PurchaseTotalPrice) * 100) , 2) ;
        }

        return $dataArrangeSum;
    }
    // !end

}
