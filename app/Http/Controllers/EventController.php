<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EventController extends Controller
{
    //
    public function index()
    {
        return view('pages.events');
    }

    public function typeEvent()
    {
        return view('pages.type-events');
    }
}
