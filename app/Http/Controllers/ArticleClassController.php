<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArticleClassController extends Controller
{
    //
    public function index()
    {
        return view('pages.articles-class');
    }
}
