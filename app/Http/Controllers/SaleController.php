<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SaleController extends Controller
{
    public function index()
    {
        return view('pages.sale');
    }

    public function dailySaleIndex()
    {
        return view('pages.dailySale');
    }
    public function caIndex()
    {
        return view('pages.ca');
    }
}
