<?php

namespace App\Http\Controllers;

use App\Events\OrderCreatedEvent;
use Illuminate\Http\Request;

class TableController extends Controller
{
    public function index()
    {
        return view('pages.table');
    }

    public function waiterIndex()
    {
        return view('pages.waiterUI');
    }

    public function cookerIndex()
    {
        return view('pages.cookerUI');
    }

    public function test(){
        $event = new OrderCreatedEvent();
        event($event);
        dd();
    }
}
