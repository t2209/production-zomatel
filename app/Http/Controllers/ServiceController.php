<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ServiceController extends Controller
{
    //
    public function index()
    {
        return view('pages.services');
    }
    public function ServiceStock()
    {
        return view('pages.service-stock');
    }
    public function ServiceStockRawMat()
    {
        return view('pages.service-stock-rawmat');
    }
}
