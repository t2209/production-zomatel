<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProdController extends Controller
{
    //
    public function serviceProd()
    {
        return view("pages.production");
    }
    
    public function eventProd()
    {
        return view("pages.eventProduction");
    }
}
