<?php

namespace App\Http\Livewire\Services;

use App\Models\Rawmatrequireservice;
use Carbon\Carbon;
use Livewire\Component;

class ServiceRequestList extends Component
{
    public $selectedWeek;
    public $weekShow;
    public $servicesID = null;
    public $orderData;
    public $startOfWeek , $endOfWeek;

    protected $queryString = [
        'selectedWeek' => ['except' => '*'],
        'weekShow' => ['except' => '*']
    ];

    public function getServiceOrder()
    {
        // set week view
        $weekView = explode("-W",$this->selectedWeek);
        $this->weekShow = "Semaine " . $weekView[1] . " , " . $weekView[0];

        // get list of order service
        $date = Carbon::now();
        $date->setISODate($weekView[0],$weekView[1]);

        $this->startOfWeek = Carbon::parse($date->startOfWeek())->format("Y-m-d");
        $this->endOfWeek = Carbon::parse($date->endOfWeek())->format("Y-m-d");

        $this->servicesID = Rawmatrequireservice::select('RawMatRequireServiceDateConfirm','ServiceServiceID','RawMatRequireServiceDate')
                            ->with('service')
                            ->whereBetween('RawMatRequireServiceDate', [$this->startOfWeek,$this->endOfWeek])
                            ->groupBy('ServiceServiceID')
                            ->groupBy('RawMatRequireServiceDate')
                            ->orderBy('RawMatRequireServiceDate','DESC')
                            ->orderBy('RawMatRequireServiceDateConfirm','DESC')
                            ->get();
        // dd($this->servicesID);

    }

    public function getServiceOrderRwmat($ID,$date)
    {
        // dd($ID,$date);
        $this->servicesID = Rawmatrequireservice::select('ServiceServiceID','RawMatRequireServiceDate')
                            ->with('service')
                            ->whereBetween('RawMatRequireServiceDate', [$this->startOfWeek,$this->endOfWeek])
                            ->groupBy('ServiceServiceID')
                            ->groupBy('RawMatRequireServiceDate')
                            ->orderBy('RawMatRequireServiceDate','DESC')
                            ->get();
        
        $this->orderData = Rawmatrequireservice::with('service','rawmaterial')
                                            ->where('ServiceServiceID',$ID)
                                            ->where('RawMatRequireServiceDate',$date)->get();
        // dd($this->orderData);
    }

    public function confirmOrder($servicesID,$date)
    {
        // dd('confirm ' . $servicesID . " " . $date);
        Rawmatrequireservice::where("ServiceServiceID",$servicesID)
                            ->where("RawMatRequireServiceDate",$date)
                            ->update([
                                "RawMatRequireServiceDateConfirm" => date("Y-m-d")
                            ]);
        $this->getServiceOrder();

    }

    public function unsetVars()
    {
        $this->servicesID = null;
    }

    public function render()
    {
        return view('livewire.services.service-request-list');
    }
}
