<?php

namespace App\Http\Livewire\Services;

use App\Models\Service;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class ServiceForm extends Component
{
    public $modelID;
    public $ServiceName;
    protected $listeners = [
        'getSelectedService'
    ];

    public function getSelectedService($ServiceID)
    {
        $this->modelID = $ServiceID;
        $Service = Service::find($this->modelID);
        $this->ServiceName = $Service->ServiceName;
    }

    public function clearVars(){
        $this->ServiceName = null;
    }

    public function save(){
        $validateData = [
            'ServiceName' => 'required',
        ];

        $this->validate($validateData);

        $Service = new Service;

        if ($this->modelID) {

            DB::transaction(function () use ($Service) {
                $Service->where("ServiceID",$this->modelID)
                ->update([
                    'ServiceName' => $this->ServiceName,
                ]);
            });

        } else {

            $Service->ServiceName = $this->ServiceName;

            DB::transaction(function () use ($Service) {
                $Service->save();
            });
        }

        $this->clearVars();
        $this->dispatchBrowserEvent('closeServiceModal');
        $this->emit("refreshServiceList");
    }

    public function render()
    {
        return view('livewire.services.service-form');
    }
}
