<?php

namespace App\Http\Livewire\Services;

use App\Models\Articleservice;
use Carbon\Carbon;
use Livewire\Component;

class ServiceStockList extends Component
{
    public $selectedWeek;
    public $weekShow;
    public $servicesID = null;
    public $startOfWeek , $endOfWeek;

    protected $queryString = [
        'selectedWeek' => ['except' => '*'],
        'weekShow' => ['except' => '*']
    ];


    public function getStockService()
    {
        $validateData = [
            'selectedWeek' => 'required',
        ];

        $this->validate($validateData);
        // set week view
        $weekView = explode("-W",$this->selectedWeek);
        $this->weekShow = "Semaine " . $weekView[1] . " , " . $weekView[0];

        // get list of stock service
        $date = Carbon::now();
        $date->setISODate($weekView[0],$weekView[1]);

        $this->startOfWeek = Carbon::parse($date->startOfWeek())->format("Y-m-d");
        $this->endOfWeek = Carbon::parse($date->endOfWeek())->format("Y-m-d");

        $this->servicesID = Articleservice::with('service')
                            // select('ServiceServiceID','ArticleServiceDate','ArticleArticleID',)->
                            ->whereBetween('ArticleServiceDate', [$this->startOfWeek,$this->endOfWeek])
                            ->get();

        // dd($this->servicesID);

    }

    public function unsetVars()
    {
        $this->servicesID = null;
    }

    public function render()
    {
        return view('livewire.services.service-stock-list');
    }
}
