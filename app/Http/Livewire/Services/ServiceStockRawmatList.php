<?php

namespace App\Http\Livewire\Services;

use App\Models\Rawmatservice;
use App\Models\Service;
use Carbon\Carbon;
use Livewire\Component;

class ServiceStockRawmatList extends Component
{
    public $selectedWeek;
    public $searchValue;
    public $weekRendu;
    public $weekShow;
    public $ServiceIDActive = null;
    public $ClassIDActive = "*";
    public $startOfWeek , $endOfWeek;

    protected $queryString = [
        'selectedWeek','startOfWeek','endOfWeek'
    ];

    public function getStockServiceRawMat()
    {
        $validateData = [
            'selectedWeek' => 'required',
        ];

        $this->validate($validateData);
        $this->weekRendu = $this->selectedWeek;
        // set week view
        $weekView = explode("-W",$this->weekRendu);
        $this->weekShow = "Semaine " . $weekView[1] . " , " . $weekView[0];

        // get list of stock service rawmat
        $date = Carbon::now();
        $date->setISODate($weekView[0],$weekView[1]);

        $this->startOfWeek = Carbon::parse($date->startOfWeek())->format("Y-m-d");
        $this->endOfWeek = Carbon::parse($date->endOfWeek())->format("Y-m-d");

        // dd($this->endOfWeek);
    }

    public function unsetVars()
    {
        $this->ServiceIDActive = null;
    }

    public function setServiceID($serviceID)
    {
        $this->ServiceIDActive = $serviceID;
        // dd($this->ServiceIDActive);
    }

    public function unsetServiceID()
    {
        $this->ServiceIDActive = null;
    }

    public function render()
    {
        return view('livewire.services.service-stock-rawmat-list', [
            'servicesID' => Rawmatservice::
            // select('ServiceServiceID','RawMatServiceDate','RawMaterialRawMaterialID',)
            with('service','rawmaterial')
            ->whereHas('rawmaterial', function ($query){
                $query->where('RawMaterialName','like', '%' . $this->searchValue . '%');
            })
            ->where('ServiceServiceID', 'like', '%' . $this->ServiceIDActive . '%')
            ->whereBetween('RawMatServiceDate', [$this->startOfWeek,$this->endOfWeek])
            ->get(),
            'servicelists' => Service::get(),
        ]);
    }
}
