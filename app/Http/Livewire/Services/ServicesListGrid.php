<?php

namespace App\Http\Livewire\Services;

use App\Models\Rawmateconomat;
use App\Models\Rawmatservice;
use App\Providers\RawMatServiceProvider;
use Livewire\Component;

class ServicesListGrid extends Component
{
    public $key = 0;
    public $rawmat = null;
    public $economatQte;

    public function mount()
    {
        // dd($this->rawmat->RawMaterialRawMaterialID);
        $this->economatQte = RawMatServiceProvider::getRawMatEconomatQte($this->rawmat->RawMaterialRawMaterialID);

        // dd($this->economatQte);
    }

    public function validateServiceOrder()
    {
        // dd($this->rawmat->RawMatRequireServiceQtyNeeded,$this->rawmat->ServiceServiceID);
        if ($this->rawmat->RawMatRequireServiceQtyNeeded > $this->economatQte ) {
            dd("ts ampy");
        } else {
            $serviceLastQte = RawMatServiceProvider::getLastRawMatQteService($this->rawmat->RawMaterialRawMaterialID,$this->rawmat->ServiceServiceID);
            // create service rwmat
            Rawmatservice::create([
                "ServiceServiceID" => $this->rawmat->ServiceServiceID,
                "RawMaterialRawMaterialID" => $this->rawmat->RawMaterialRawMaterialID,
                "RawMatServiceQty" => floatval($this->rawmat->RawMatRequireServiceQtyNeeded) + floatval($serviceLastQte),
                "RawMatServiceDate" => date("Y-m-d")
            ]);

            // up rwmat economat
            Rawmateconomat::create([
                "RawMaterialRawMaterialID" => $this->rawmat->RawMaterialRawMaterialID,
                "RawMatEconomatQty" => floatval($this->economatQte) - floatval($this->rawmat->RawMatRequireServiceQtyNeeded),
                "RawMatEconomatDate" => date("Y-m-d")
            ]);
            $this->economatQte = RawMatServiceProvider::getRawMatEconomatQte($this->rawmat->RawMaterialRawMaterialID);
            // dd($this->economatQte);
            
            // dd($serviceLastQte);
        }
        
    }
    
    public function render()
    {
        return view('livewire.services.services-list-grid');
    }
}
