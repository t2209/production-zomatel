<?php

namespace App\Http\Livewire\Services;

use App\Models\Service;
use Livewire\Component;

class ServiceList extends Component
{
    public $selectID;
    public $listeners = [
        'refreshServiceList' => '$refresh'
    ];

    public function selectService($ServiceID , $action)
    {
        $this->selectID = $ServiceID;

        if ($action == "update") {
            $this->emit('getSelectedService' , $this->selectID);
            $this->dispatchBrowserEvent('openServiceModal');
        }else {
            $this->dispatchBrowserEvent('openServiceDeleteModal');
        }
    }

    public function deleteService()
    {
        Service::destroy($this->selectID);
        $this->dispatchBrowserEvent('closeServiceDeleteModal');
    }


    public function render()
    {
        return view('livewire.services.service-list' , [
            'services' => Service::get()
        ]);
    }
}
