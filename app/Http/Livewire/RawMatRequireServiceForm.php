<?php

namespace App\Http\Livewire;

use App\Models\Rawmatrequire;
use App\Models\Rawmatrequireservice;
use Livewire\Component;

class RawMatRequireServiceForm extends Component
{

    public $qtyRawMatRequireServ = 0;
    public $rawMat = null;
    public int $key = 0;
    public $serviceID = null;
    public $eventID = null;
    public $classGrid = "flex items-center h-10 justify-center text-center bg-white border border-slate-800 rounded";

    public $listeners = [
        "getQtyRwmatreqserv",
    ];

    public function clearVars(){
        $this->qtyRawMatRequireServ = 0;
    }

    public function getQtyRwmatreqserv($daty){
        $validateData = [
            'qtyRawMatRequireServ' => 'required'
        ];
        $this->validate($validateData);
        // dd($this->qtyRawMatRequireServ);
        Rawmatrequireservice::create([
            "RawMatRequireServiceQtyNeeded" => floatval($this->qtyRawMatRequireServ),
            "RawMatRequireServiceDate" => $daty,
            "ServiceServiceID" => $this->rawMat->service->ServiceID,
            "RawMaterialRawMaterialID" => $this->rawMat->RawMaterialRawMaterialID
        ]);
        $this->clearVars();
    }

    public function render()
    {
        return view('livewire.raw-mat-require-service-form');
    }
}
