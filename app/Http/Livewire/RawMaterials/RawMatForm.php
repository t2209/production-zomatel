<?php

namespace App\Http\Livewire\RawMaterials;

use App\Models\Provider;
use App\Models\Rawmaterial;
use App\Models\Rawmatfamily;
use App\Models\Rawmatprovider;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class RawMatForm extends Component
{
    public $RawMatFamilyRawMatFamilyID;
    public $RawMaterialAccountNum;
    public $RawMaterialName;
    public $RawMaterialUnity;
    public $RawMaterialForProd = true;
    public $inputs = [];
    public $i = 1;
    public $ProviderID;
    public $RawMatProviderUnitPrice;

    // multiple input form
    public function add($i)
    {
        $i = $i + 1;
        $this->i = $i;
        array_push($this->inputs ,$i);
    }
 
    public function remove($i)
    {
        unset($this->inputs[$i]);
    }

    public $listeners = [
        'refreshFamily' => '$refresh',
    ];

    public function refeshPage()
    {
        $this->emit('refreshFamily');
    }

    public function clearInputRawMat()
    {
        $this->RawMatFamilyRawMatFamilyID = null;
        $this->RawMaterialAccountNum = null;
        $this->RawMaterialName = null;
        $this->RawMaterialUnity = null;
    }

    public function saveRawMat()
    {
        $validateData = [
            "RawMaterialAccountNum" => "required|numeric",
            "RawMaterialName" => "required",
            "RawMatFamilyRawMatFamilyID" => "required",
            // "RawMatProviderUnitPrice" => "required",
            "RawMaterialUnity" => "required"
        ];
        $this->validate($validateData);

        // $integerIDs = str_replace(" " , "" , "10 010");
        // dd($this->RawMatProviderUnitPrice,$integerIDs);

        // raw mat
        $RawMaterial = new Rawmaterial();
        $RawMaterial->RawMatFamilyRawMatFamilyID = $this->RawMatFamilyRawMatFamilyID;
        $RawMaterial->RawMaterialAccountNum = $this->RawMaterialAccountNum;
        $RawMaterial->RawMaterialName = $this->RawMaterialName;
        $RawMaterial->RawMaterialUnity = $this->RawMaterialUnity;

        
        if ($this->RawMaterialForProd == true) {
            $RawMaterial->RawMaterialForProd = 1;
        }elseif ($this->RawMaterialForProd == false) {
            $RawMaterial->RawMaterialForProd = 0;
        }
        
        DB::transaction(function () use ($RawMaterial) {
            $RawMaterial->save();
        });

        // RawMatProvider
        $arrayResult = array_map(function($a,$b){ return array($a,$b);} , $this->ProviderID , $this->RawMatProviderUnitPrice);
        
        for ($i=0; $i < count($arrayResult); $i++) { 
            $RawMatProvider = new Rawmatprovider;
            $RawMatProvider->RawMatProviderUnitPrice = str_replace(" " , "" ,$arrayResult[$i][1]) ;
            $RawMatProvider->RawMaterialRawMaterialID = $RawMaterial->RawMaterialID;
            $RawMatProvider->ProviderProviderID = $arrayResult[$i][0];
            $RawMatProvider->save();
        }
        $this->emit('refreshFamily');
        $this->clearInputRawMat();
    }
    
    public function render()
    {
        return view('livewire.raw-materials.raw-mat-form' ,[
            'rawFamilies' => Rawmatfamily::get(),
            'providers' => Provider::get()
        ] );
    }
}
