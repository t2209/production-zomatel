<?php

namespace App\Http\Livewire\RawMaterials;

use App\Models\Rawmaterial;
use App\Models\Rawmatfamily;
use Livewire\Component;

class RawMatList extends Component
{
    public $RawMaterials;
    public $RwmatFamID = "All";
    public $listeners = [
        'refreshFamily' => '$refresh',
    ];

    public function setFamilyFilter($Id,$filter)
    {
        if ($filter == "perFamily") {
            $this->RawMaterials = Rawmaterial::
            join("rawmatfamily","rawmatfamily.RawMatFamilyID","=","rawmaterial.RawMatFamilyRawMatFamilyID")
            ->orderBy('RawMatFamilyRawMatFamilyID',"ASC")
            ->orderBy('RawMatWording',"ASC")
            ->where('RawMatFamilyID',$Id)
            ->get();

            $this->RwmatFamID = $Id;
        } else {
            $this->RawMaterials = Rawmaterial::join("rawmatfamily","rawmatfamily.RawMatFamilyID","=","rawmaterial.RawMatFamilyRawMatFamilyID")
            ->orderBy('RawMatFamilyRawMatFamilyID',"ASC")
            ->orderBy('RawMatWording',"ASC")
            ->get();

            $this->RwmatFamID = "All";
        }

    }

    public function mount()
    {
        $this->RawMaterials = Rawmaterial::
        join("rawmatfamily","rawmatfamily.RawMatFamilyID","=","rawmaterial.RawMatFamilyRawMatFamilyID")
        ->orderBy('RawMatFamilyRawMatFamilyID',"ASC")
        ->orderBy('RawMatWording',"ASC")
        ->get();
    }

    public function render()
    {
        return view('livewire.raw-materials.raw-mat-list',[
            "RawMatFamilies" => Rawmatfamily::get()
        ]);
    }
}
