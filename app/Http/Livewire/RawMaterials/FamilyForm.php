<?php

namespace App\Http\Livewire\RawMaterials;

use App\Models\Rawmatfamily;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class FamilyForm extends Component
{
    public Rawmatfamily $item;
    public $RawMatFamilyRawMatFamilyID;

    protected $rules = [
        "item.RawMatWording" => "required"
    ]; 
    
    public function delete($ID)
    {
        $Rawmatfamily = new Rawmatfamily;
        DB::transaction(function () use ($Rawmatfamily,$ID) {
            $Rawmatfamily->where("RawMatFamilyID",$ID)
            ->delete();
        });
        $this->emit('refreshFamily');
    }

    public function update($rawMatFamilyID)
    {
        // $this->test = $rawMatFamilyID;
        $Rawmatfamily = new Rawmatfamily;
        DB::transaction(function () use ($Rawmatfamily,$rawMatFamilyID) {
            $Rawmatfamily->where("RawMatFamilyID",$rawMatFamilyID)
            ->update([
                'RawMatWording' => $this->item->RawMatWording
            ]);
        });
    }
    
    public function render()
    {
        return view('livewire.raw-materials.family-form');
    }
}
