<?php

namespace App\Http\Livewire\RawMaterials;

use App\Models\Rawmatfamily;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class RawFamilyMaterialForm extends Component
{
    public $RawMatWording;
    public $RawMatFamilyColor;

    // create family
    public function save()
    {
        $validateData = [
            'RawMatWording' => 'required',
            'RawMatFamilyColor' => 'required'
        ];
        $this->validate($validateData);

        // dd($this->RawMatFamilyColor);

        $RawMatFamily = new Rawmatfamily();
        $RawMatFamily->RawMatWording = $this->RawMatWording;
        $RawMatFamily->RawMatFamilyColor = $this->RawMatFamilyColor;
        DB::transaction(function () use ($RawMatFamily) {
            $RawMatFamily->save();
        });
        $this->emit('refreshFamily');
        $this->RawMatWording = null;

    }
    
    public function render()
    {
        return view('livewire.raw-materials.raw-family-material-form');
    }
}
