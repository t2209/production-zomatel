<?php

namespace App\Http\Livewire\RawMaterials;

use Livewire\Component;

class RawMaterial extends Component
{
    public function render()
    {
        return view('livewire.raw-materials.raw-material');
    }
}
