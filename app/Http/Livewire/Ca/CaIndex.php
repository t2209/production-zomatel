<?php

namespace App\Http\Livewire\Ca;

use App\Providers\OrderProvider;
use Carbon\Carbon;
use Livewire\Component;

class CaIndex extends Component
{
    public $years;

    public $selectedWeek;
    public $selectedMonth;
    public $selectedYear;
    public $selectedService = "*";

    public $activeBtn = "week";
    public $filterView;

    public $dateBegin;
    public $dateEnd;


    public function boot()
    {
        $this->selectedWeek = date("Y") . "-W" . date("W");
        $this->placeholderYear = date("Y");
        
        $date = Carbon::now();
        $date->setISODate(date("Y"),date("W"));

        $this->dateBegin = Carbon::parse($date->startOfWeek())->format("Y-m-d");
        $this->dateEnd = Carbon::parse($date->endOfWeek())->format("Y-m-d");

        // ? set array of year
        $this->years = range(2020,date("Y"));
        rsort($this->years);

        $this->filterView = "Semaine " . date("W") . " , " . date("Y");
        // dd($this->years);

    }

    public function serviceFilter()
    {
        dd($this->selectedService);
    }

    public function weekFilter($week)
    {
        $this->activeBtn = $week;
        $weekSplit = explode("-W",$this->selectedWeek);
        $this->filterView = "Semaine " . $weekSplit[1] . " , " . $weekSplit[0];

        $date = Carbon::now();
        $date->setISODate($weekSplit[0],$weekSplit[1]);

        $this->dateBegin = Carbon::parse($date->startOfWeek())->format("Y-m-d");
        $this->dateEnd = Carbon::parse($date->endOfWeek())->format("Y-m-d");

    }

    public function monthFilter($month)
    {
        $this->validate([
            "selectedMonth" => "required"
        ]);

        setlocale(LC_TIME, "fr_FR");
        $this->filterView = "mois de " . strftime("%B",strtotime($this->selectedMonth)) . " , " . date("Y",strtotime($this->selectedMonth));

        $this->activeBtn = $month;

        $this->dateBegin = Carbon::createFromFormat("Y-m",$this->selectedMonth)->startOfMonth();
        $this->dateEnd = Carbon::createFromFormat("Y-m",$this->selectedMonth)->endOfMonth();
        // dd($this->dateBegin,$this->dateEnd);
    }

    public function yearFilter($year)
    {
        $this->validate([
            "selectedYear" => "required"
        ]);

        $this->filterView = $this->selectedYear;

        $this->activeBtn = $year;

        $date = Carbon::createFromDate($this->selectedYear);
        $this->dateBegin = $date->copy()->startOfYear();
        $this->dateEnd = $date->copy()->endOfYear();

        // dd($this->selectedYear,$this->dateBegin,$this->dateEnd);
    }

    public function render()
    {
        return view('livewire.ca.ca-index',[
            "ca" => array_sum(array_column(OrderProvider::getTotalAllOrder("filter",$this->selectedService,null,$this->dateBegin,$this->dateEnd),"Price")),
            "services" => getAllServices()
        ]);
    }
}
