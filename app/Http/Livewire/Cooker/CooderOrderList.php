<?php

namespace App\Http\Livewire\Cooker;

use App\Models\ArticleOrder;
use App\Providers\OrderProvider;
use Livewire\Component;

class CooderOrderList extends Component
{
    public $orderID = null;
    public $orderNum = null;
    public $articleOrders;
    public $totalOrder;
    public $articleOrderStatus;
    public $dataFilterStatus = "All";
    public $dataFilterService;
    public $dataFilterOrder;
    public $dataFilterTable;

    public $notifyNewOrder =false;

    protected $queryString = ['dataFilterStatus','dataFilterService'];
    protected $listeners = ['echo:chan-order,OrderCreatedEvent' => 'notifyNewOrder'];

    public function notifyNewOrder()
    {
        $this->notifyNewOrder = true;
    }

    public function calculOrderTotal($arrayOrders)
    {
        $array_article_service = $arrayOrders->toArray();
        for ($i=0; $i < count($array_article_service); $i++) {
            $array_article_service[$i]["Price"] = $array_article_service[$i]["article_service"]["ArticleServiceArticlePrice"] * $array_article_service[$i]["ArticleOrderQty"];
        }

        $this->totalOrder = array_sum(array_column($array_article_service,"Price"));
        $this->articleOrderStatus = OrderProvider::getArticleOrderStatuCount($this->orderID);
    }

    public function orderDetails($orderID,$orderNum)
    {
        $this->orderID = $orderID;
        $this->orderNum = $orderNum;

        // ? get articleOrder
        $this->articleOrders = OrderProvider::getArticleOrders('daily',$this->orderID);

        // ? get total of order
        $this->calculOrderTotal($this->articleOrders);

    }

    public function operatorArticleOrder($type,$articleOrderId)
    {
        switch ($type) {
            case 'cancel':
                ArticleOrder::where("ArticleOrderID",$articleOrderId)->update([
                                "ArticleOrderStatus" => "canceled"
                            ]);
                $this->articleOrders = OrderProvider::getArticleOrders('daily',$this->orderID);
                $this->calculOrderTotal($this->articleOrders);
                break;
            case 'pending':
                ArticleOrder::where("ArticleOrderID",$articleOrderId)->update([
                                "ArticleOrderStatus" => "pending"
                            ]);
                $this->articleOrders = OrderProvider::getArticleOrders('daily',$this->orderID);
                $this->calculOrderTotal($this->articleOrders);
                break;

            case 'done':
                ArticleOrder::where("ArticleOrderID",$articleOrderId)->update([
                                "ArticleOrderStatus" => "done"
                            ]);
                $this->articleOrders = OrderProvider::getArticleOrders('daily',$this->orderID);
                $this->calculOrderTotal($this->articleOrders);
                break;

            case 'return':
                ArticleOrder::where("ArticleOrderID",$articleOrderId)->update([
                                "ArticleOrderStatus" => "return"
                            ]);
                $this->articleOrders = OrderProvider::getArticleOrders('daily',$this->orderID);
                $this->calculOrderTotal($this->articleOrders);
                break;

            default:
                dd("Erreur");
                break;
        }
    }

    public function resetVars()
    {
        $this->orderID = null;
    }

    public function filterStatu($statu)
    {
        $this->dataFilterStatus = $statu;
    }

    public function filterService($serviceName)
    {
        $this->dataFilterService = $serviceName;
    }

    public function render()
    {
        return view('livewire.cooker.cooder-order-list',[
            "orders" => OrderProvider::getCookerOrder(null,date("Y-m-d"),$this->dataFilterStatus,$this->dataFilterOrder,$this->dataFilterTable,$this->dataFilterService),
            "services" => getAllServices(),
            "status" => array([
                "libelle" => "Tout",
                "data" => "All",
                "color" => "btn-info"
            ],[
                "libelle" => "En prépartion",
                "data" => "pendingAction",
                "color" => "btn-default"
                ],[
                "libelle" => "Fait",
                "data" => "done",
                "color" => "btn-success"
                ],[
                "libelle" => "Annuler",
                "data" => "canceled",
                "color" => "btn-warning"
                ])
        ]);
    }
}
