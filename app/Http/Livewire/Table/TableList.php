<?php

namespace App\Http\Livewire\Table;

use App\Models\Table;
use Livewire\Component;

class TableList extends Component
{
    public $TableID;
    public $TableNum;
    public $ServiceServiceID;
    public $action = "add";

    protected $listeners = ['refreshTable' => '$refresh'];

    public function saveTable()
    {
        $this->validate([
            "TableNum" => "required",
            "ServiceServiceID" => "required"
        ]);

        if ($this->action == "add") {
            $table = new Table();
            $table->TableNum = $this->TableNum;
            $table->ServiceServiceID = $this->ServiceServiceID;
            if ($table->save()) {
                $this->clrVars();
                $this->emit("refreshTable");
            }
        } else {
            Table::where("TableID" , $this->TableID)->update([
                "TableNum" => $this->TableNum,
                "ServiceServiceID" => $this->ServiceServiceID,
            ]);
            $this->clrVars();
            $this->emit("refreshTable");
        }

    }

    public function deteleTable($tableId)
    {
        if (Table::destroy($tableId)) {
            $this->emit('refreshTable');
        }
    }

    public function setUpdate($action , $tableId , $num , $serviceId)
    {
        // dd($num,$serviceId);
        $this->TableID = $tableId;
        $this->TableNum = $num;
        $this->ServiceServiceID = $serviceId;
        $this->action = $action;
    }
    
    public function clrVars()
    {
        $this->TableNum = null;
        $this->ServiceServiceID = null;
        $this->action = "add";
    }

    public function render()
    {
        return view('livewire.table.table-list',[
            "services" => getAllServices(),
            "tables" => Table::with('service')->get()
        ]);
    }
}
