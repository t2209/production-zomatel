<?php

namespace App\Http\Livewire\Waiter;

use App\Models\ArticleOrder;
use App\Models\Order;
use App\Models\OrderTable;
use App\Models\Table;
use App\Providers\ArticleServiceProvider;
use Livewire\Component;

class WaiterIndex extends Component
{
    public $inputs = [];
    public $i = 0;

    public $newOrder = true;
    public $existingOrder = false;

    public $newOrderTable = true;
    public $existingOrderTable = false;

    public $existingOrderID;

    public $OrderID;
    public $OrderNum;
    public $TableID;
    public $ArticleServiceID;
    public $ArticleOrderQty;

    // multiple input form
    public function add($i)
    {
        $i = $i + 1;
        $this->i = $i;
        array_push($this->inputs ,$i);
    }
    
    public function remove($i)
    {
        unset($this->inputs[$i]);        
    }

    // ? Existing && New order behavior
    public function setNewOrder()
    {
        $this->newOrder = true;
        $this->existingOrder = false;
    }

    public function setExistingOrder()
    {
        $this->newOrder = false;
        $this->existingOrder = true;
        $this->OrderNum = null;
    }

    public function setNewOrderTable()
    {
        $this->newOrderTable = true;
        $this->existingOrderTable = false;
        $this->existingOrderID = null;
    }

    public function setExistingOrderTable()
    {
        $this->newOrderTable = false;
        $this->existingOrderTable = true;
        // $this->OrderNum = null;
    }

    // do an order task
    public function save()
    {
        if ($this->ArticleServiceID == null || $this->ArticleOrderQty == null) {
            dd("Selectionner une articles");
        }
        $arrayArticleOrder = array_map(function($a,$b){ return array($a,$b);} , $this->ArticleServiceID , $this->ArticleOrderQty);
        // dd($arrayArticleOrder);
        
        if ($this->newOrderTable == true) {
            if ($this->newOrder == true) {
                $this->validate([
                    "OrderNum" => "required",
                    "TableID" => "required"
                ]);
                // ? create new order
                $order = Order::create([
                    "OrderNum" => $this->OrderNum
                ]);
                // ? create OrderTable
                OrderTable::create([
                    "TableTableID" => $this->TableID,
                    "OrderOrderID" => $order->OrderID
                ]);
                // ? create articleOrder
                for ($i=0; $i < count($arrayArticleOrder); $i++) { 
                    ArticleOrder::create([
                        "ArticleServiceArticleServiceID" => $arrayArticleOrder[$i][0],
                        "OrderOrderID" => $order->OrderID,
                        "ArticleOrderQty" => $arrayArticleOrder[$i][1]
                    ]);
                }
            }elseif ($this->existingOrder == true) {
                $this->validate([
                    "OrderID" => "required",
                    "TableID" => "required"
                ]);
                // ? create OrderTable
                OrderTable::create([
                    "TableTableID" => $this->TableID,
                    "OrderOrderID" => $this->OrderID
                ]);
                // ? create articleOrder
                for ($i=0; $i < count($arrayArticleOrder); $i++) { 
                    ArticleOrder::create([
                        "ArticleServiceArticleServiceID" => $arrayArticleOrder[$i][0],
                        "OrderOrderID" => $this->OrderID,
                        "ArticleOrderQty" => $arrayArticleOrder[$i][1]
                    ]);
                }
            }
        } elseif ($this->existingOrderTable == true) {
            $this->validate([
                "existingOrderID" => "required"
            ]);

            // ? create articleOrder
            for ($i=0; $i < count($arrayArticleOrder); $i++) { 
                ArticleOrder::create([
                    "ArticleServiceArticleServiceID" => $arrayArticleOrder[$i][0],
                    "OrderOrderID" => $this->existingOrderID,
                    "ArticleOrderQty" => $arrayArticleOrder[$i][1]
                ]);
            }
        }
        
    }

    // reset ui vars
    public function resetUI()
    {
        $this->inputs = [];
        $this->OrderNum = null;
        $this->OrderID = null;
        $this->TableID = null;
    }

    public function render()
    {
        return view('livewire.waiter.waiter-index',[
            "tables" => Table::with('service')->get(),
            "articles" => ArticleServiceProvider::getArticles(null,"*",null,"*"),
            "orders" => Order::where('created_at','like','%'.date("Y-m-d").'%')
                            ->get(),
            "orderTables" => OrderTable::with('order','table')
                                        ->whereHas('order',function($query){
                                            $query->where('created_at','like','%'.date("Y-m-d").'%');
                                        })
                                        ->get()
        ]);
    }
}
