<?php
namespace App\Http\Livewire\Productions;

use App\Models\Articlebox;
use App\Models\Articleservice;
use App\Models\Service;
use App\Providers\ArticleServiceProvider;
use Livewire\Component;

class ProdList extends Component
{
    public $selectID;
    public $ServiceIDActive = "*";
    public $ClassIDActive = "*";
    public $EventTypeIDActive = null;

    public $searchValue;
    public $listeners = [
        'refreshProdList' => '$refresh'
    ];

    protected $queryString = ['ServiceIDActive'];

    public function selectProd($Articlebox , $action)
    {
        $this->selectID = $Articlebox;

        if ($action == "update") {
            $this->emit('getSelectedProd' , $this->selectID);
            $this->dispatchBrowserEvent('openProdModal');
        }else {
            $this->dispatchBrowserEvent('openProdDeleteModal');
        }
    }

    public function deleteProd()
    {
        Articlebox::destroy($this->selectID);
        $this->dispatchBrowserEvent('closeProdDeleteModal');
    }

    public function setServiceID($serviceID)
    {
        $this->ServiceIDActive = $serviceID;
    }

    public function unsetServiceID()
    {
        $this->ServiceIDActive = "*";
    }

    public function render()
    {
        return view('livewire.productions.prod-list' , [
            'articleservice' => ArticleServiceProvider::getArticles($this->searchValue,$this->ServiceIDActive,$this->EventTypeIDActive,$this->ClassIDActive),
            'services' => Service::get()
        ]);
    }
}
