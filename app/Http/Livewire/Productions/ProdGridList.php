<?php

namespace App\Http\Livewire\Productions;

use App\Models\Articlebox;
use App\Models\Articleservice;
use App\Models\Box;
use App\Models\Rawmatservice;
use App\Providers\RawMatServiceProvider;
use Livewire\Component;

class ProdGridList extends Component
{
    public $item;
    public $modalCheck = "";
    public $BoxBoxID;
    public $Rawmats;
    public $newQte = 0;

    public $classGrid = "flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded";

    public $rules = [
        "item.ArticleServiceQty" => "required"
    ];

    public function getRawMats($code){
        if (getClassInstance($this->item) === Articleservice::class){
            return  RawMatServiceProvider::getRawMats($this->item['article']['ArticleID'],$code,null);
        }else if(getClassInstance($this->item) === Eventarticle::class){
            return  RawMatServiceProvider::getRawMats($this->item['article']['ArticleID'],null,$code);
        }
    }

    public function product($code)
    {
        // verife qty rawmat
        $this->Rawmats = $this->getRawMats($code);

        $rawmatservices = [];
        foreach ($this->Rawmats as $key => $value) {
            $RawMatServiceID = Rawmatservice::select("RawMatServiceQty")
                                            ->where('ServiceServiceID',$value->ServiceServiceID)
                                            ->where('RawMaterialRawMaterialID',$value->rawmaterialprovider->rawmaterial->RawMaterialID)
                                            ->orderBy("RawMatServiceID","DESC")
                                            ->limit(1)
                                            ->get()->toArray();

            $rawmatservices[$key]["RawMaterialRawMaterialID"] = $value->rawmaterialprovider->rawmaterial->RawMaterialID;
            $rawmatservices[$key]["QteDispo"] = (!empty($RawMatServiceID)) ? ($RawMatServiceID[0]["RawMatServiceQty"])  : 0;
            // $rawmatservices[$key]["ServiceServiceID"] = $value->ServiceServiceID;
        }

        $rawmatNeeded = [];
        foreach ($this->Rawmats as $key => $value) {
            $rawmatNeeded[$key]["RawMaterialRawMaterialID"] = $value->rawmaterialprovider->rawmaterial->RawMaterialID;
            $rawmatNeeded[$key]["RawMaterialName"] = $value->rawmaterialprovider->rawmaterial->RawMaterialName;
            $rawmatNeeded[$key]["QteNeeded"] = $value->RawMatArticleQty * $this->newQte;
            $rawmatNeeded[$key]["ServiceServiceID"] = $value->ServiceServiceID;
        }

        $rawmats = [];
        foreach ($rawmatservices as $serv) {
            foreach ($rawmatNeeded as $need) {
                if ($serv["RawMaterialRawMaterialID"] == $need["RawMaterialRawMaterialID"]) {
                    // $rawmats[] = array_merge($serv,$need);
                    $rawmats[] = (($serv['QteDispo'] - $need['QteNeeded']) < 0) ? 0 : ($need['QteNeeded']);
                }
            }
        }

        if (in_array(0,$rawmats)) {
            $rawmats = [];
            foreach ($rawmatservices as $serv) {
                foreach ($rawmatNeeded as $need) {
                    if ($serv["RawMaterialRawMaterialID"] == $need["RawMaterialRawMaterialID"]) {
                        $rawmats[] = array_merge($serv,$need);
                    }
                }
            }
            // dd($rawmats);
            session()->flash('msg', 'Quantité de matières première insuffisante');
            session()->flash('msgData', $rawmats);
            $this->modalCheck = "checked";
        }else {
            $this->validate([
                "BoxBoxID" => "required"
            ]);

            // up rawmat serv
            $rawmats = [];
            foreach ($rawmatservices as $serv) {
                foreach ($rawmatNeeded as $need) {
                    if ($serv["RawMaterialRawMaterialID"] == $need["RawMaterialRawMaterialID"]) {
                        $rawmats[] = array_merge($serv,$need);
                    }
                }
            }

            foreach ($rawmats as $value) {
                Rawmatservice::create([
                    "ServiceServiceID" => $value["ServiceServiceID"],
                    "RawMaterialRawMaterialID" => $value["RawMaterialRawMaterialID"],
                    "RawMatServiceQty" => $value["QteDispo"] - $value["QteNeeded"],
                    "RawMatServiceDate" => date("Y-m-d")
                ]);
            }
            // dd($rawmats);

            // up articelService
            Articleservice::create([
                "ServiceServiceID" => $this->item["ServiceServiceID"],
                "ArticleArticleID" => $this->item["ArticleArticleID"],
                "ArticleServiceQty" => $this->item["ArticleServiceQty"] + $this->newQte,
                "ArticleServiceMax" => $this->item["ArticleServiceMax"],
                "ArticleServiceArticlePrice" => $this->item["ArticleServiceArticlePrice"],
                "ArticleServiceDate" => date("Y-m-d")
            ]);
    
            // save to boxe
            Articlebox::create([
                "BoxBoxID" => $this->BoxBoxID,
                "ArticleBoxQty" => $this->item["ArticleServiceQty"] + $this->newQte,
                "ArticleArticleID" => $this->item["article"]["ArticleID"]
            ]);
        }


    }

    public function exitWarning()
    {
        session()->forget('msgData');
        session()->forget('msg');
        $this->modalCheck = "";
    }

    public function render()
    {
        return view('livewire.productions.prod-grid-list',[
            'boxes' => Box::with('freezer')->get()
        ]);
    }
}
