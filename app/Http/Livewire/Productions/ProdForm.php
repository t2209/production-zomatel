<?php

namespace App\Http\Livewire\Productions;

use App\Models\Box;
use App\Models\ArticleBox;
use App\Models\Articleservice;
use App\Models\Rawmatarticle;
use App\Models\Rawmatservice;
use App\Providers\ArticleServiceProvider;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class ProdForm extends Component
{
    public $modelID;
    public $ArticleArticleID;
    public $ArticleBoxQty;
    public $BoxBoxID;
    public $RawMatServiceDate;
    public $ServiceIDActive = "*";
    public $ClassIDActive = "*";
    // public $EventIDActive = "*";
    public $EventTypeIDActive = null;
    protected $listeners = [
        'getSelectedProd'
    ];

    public function getSelectedProd($ArticleBoxID)
    {
        $this->modelID = $ArticleBoxID;
        $ArticleBox = ArticleBox::find($this->modelID);
        $this->ArticleArticleID = $ArticleBox->ArticleArticleID;
        $this->ArticleBoxQty = $ArticleBox->ArticleBoxQty;
        $this->BoxBoxID = $ArticleBox->BoxBoxID;
    }

    public function clearVars(){
        $this->ArticleBoxQty = null;
        $this->RawMatServiceDate = null;
    }

    public function save(){
        $validateData = [
            'ArticleArticleID' => 'required',
            'ArticleBoxQty' => 'required|numeric',
            'BoxBoxID' => 'required',
            'RawMatServiceDate' => 'required',
        ];
        $this->validate($validateData);

        $explodeArticleArticleID = explode('||',$this->ArticleArticleID);
        $expldArticleID = intval($explodeArticleArticleID[0]);

        $expldServiceID = intval($explodeArticleArticleID[1]);


        // set a validation of qte in rawmat economa
        // get rawmat of articles
        $arrayIdRwmat = Rawmatarticle::select('RawMaterialRawMaterialID','ServiceServiceID')->where('ArticleArticleID',$this->ArticleArticleID)->get()->toArray();

         // verifier le qte dans la table rawmateconomat

        $ArticleService = new Articleservice;
        $ArticleService->ArticleArticleID = $expldArticleID;
        $ArticleService->ServiceServiceID = $expldServiceID;
        $ArticleService->ArticleServiceQty = $this->ArticleBoxQty;
        $ArticleService->ArticleServiceMax = $this->ArticleBoxQty;
        $ArticleService->save();

        for ($i=0; $i < count($arrayIdRwmat); $i++) {
            $RawMatService = new Rawmatservice;
            $RawMatService->RawMaterialRawMaterialID = $arrayIdRwmat[$i]["RawMaterialRawMaterialID"];
            $RawMatService->ServiceServiceID = $arrayIdRwmat[$i]["ServiceServiceID"];
            $RawMatService->RawMatServiceQty = $this->ArticleBoxQty;
            $RawMatService->RawMatServiceDate = $this->RawMatServiceDate;
            $RawMatService->save();

        }

        $this->clearVars();
        $this->dispatchBrowserEvent('closeProdModal');
        $this->emit("refreshProdList");
    }

    public function render()
    {
        return view('livewire.productions.prod-form', [
            'articles' => ArticleServiceProvider::getArticles(null,$this->ServiceIDActive,$this->EventTypeIDActive,$this->ClassIDActive),
            'boxes' => Box::get(),
        ]);
    }
}
