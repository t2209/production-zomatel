<?php

namespace App\Http\Livewire\RawMatReceive;

use App\Models\Purchaseorderprovider;
use App\Providers\RawMatServiceProvider;
use Carbon\Carbon;
use Livewire\Component;

class RawMatReceiveList extends Component
{
    public $week = null;
    public $POProd;
    public $POProdDate;
    public $POProdDateRecev;
    public $weekView;
    public $startWeek , $endWeek;
    public $classGrid = "flex h-10 items-center font-mono bg-marron_focus text-white rounded-sm";

    protected $queryString = [
        'week' => ['except' => '*'],
        'startWeek' => ['except' => '*'],
        'endWeek' => ['except' => '*'],
        'weekView' => ['except' => '*']
    ];

    public function getRawMatList()
    {
        $validateData = [
            'week' => 'required',
        ];

        $this->validate($validateData);
        $weekSplit = explode("-W",$this->week);

        $date = Carbon::now();
        $date->setISODate($weekSplit[0],$weekSplit[1]);

        // vars for view
        $this->weekView = "Semaine " . $weekSplit[1] . " , " . $weekSplit[0];
        $this->startWeek = Carbon::parse($date->startOfWeek())->format("Y-m-d");
        $this->endWeek = Carbon::parse($date->endOfWeek())->format("Y-m-d");

        // dd(Carbon::parse($date->startOfWeek())->format("Y-m-d"),Carbon::parse($date->endOfWeek())->format("Y-m-d"));

        $this->POProd = RawMatServiceProvider::getReceivedRwMatProd(Carbon::parse($date->startOfWeek())->format("Y-m-d"),Carbon::parse($date->endOfWeek())->format("Y-m-d"));

        // check done
        $this->POProdDateRecev = Purchaseorderprovider::select('PurchaseOrderProviderID')
        ->where('POProvDateReceive','!=',null)
        ->whereBetween('POProvDateSend',[$this->startWeek,$this->endWeek])
        ->get()->toArray();
        // dd($this->POProdDateRecev);
    }

    public function upQteRecev()
    {
        // up qte receiv
        $this->emit('getQteRecev');


        // up date recev POProd

        $this->POProdDate = Purchaseorderprovider::select('PurchaseOrderProviderID')
        ->whereBetween('POProvDateSend',[$this->startWeek,$this->endWeek])
        ->get();

        foreach ($this->POProdDate as $value) {
            Purchaseorderprovider::find($value->PurchaseOrderProviderID)->update([
                "POProvDateReceive" => date("Y-m-d")
            ]);
        }

    }

    public function render()
    {
        return view('livewire.raw-mat-receive.raw-mat-receive-list');
    }
}
