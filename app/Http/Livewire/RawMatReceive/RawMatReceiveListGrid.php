<?php

namespace App\Http\Livewire\RawMatReceive;

use App\Models\Rawmateconomat;
use App\Models\Rawmatrequire;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class RawMatReceiveListGrid extends Component
{
    public $key = 0;
    public $rwmat = null;
    public $qteReceived = 0;

    public $listeners = [
        "getQteRecev"
    ];

    public function getQteRecev()
    {
        Rawmatrequire::find($this->key)->update([
            "RawMatReceiveQty" => $this->qteReceived
        ]);

        // // stock
            $dataRawmateconomat = [
                "RawMaterialRawMaterialID" => $this->rwmat->rawmaterial->RawMaterialID,
                "RawMatEconomatQty" => $this->qteReceived,
                "RawMatEconomatDate" => date("Y-m-d")
            ];
            Rawmateconomat::create($dataRawmateconomat);

        // dd($this->key);
        // dd($this->rwmat->rawmaterial->RawMaterialID);
    }

    public function mount()
    {
        // dd($this->rwmat);
        $this->qteReceived = $this->rwmat->RawMatReceiveQty;
    }

    public function render()
    {
        return view('livewire.raw-mat-receive.raw-mat-receive-list-grid');
    }
}
