<?php

namespace App\Http\Livewire\Events;

use App\Models\EventType;
use Livewire\Component;

class EventTypeList extends Component
{
    public $selectID;
    public $listeners = [
        'refreshEventTypeList' => '$refresh'
    ];

    public function selectEventType($EventTypeID , $action)
    {
        $this->selectID = $EventTypeID;

        if ($action == "update") {
            $this->emit('getSelectedEventType' , $this->selectID);
            $this->dispatchBrowserEvent('openEventTypeModal');
        }else {
            $this->dispatchBrowserEvent('openEventTypeDeleteModal');
        }
    }

    public function deleteEventType()
    {
        EventType::destroy($this->selectID);
        $this->dispatchBrowserEvent('closeEventTypeDeleteModal');
    }
    public function render()
    {
        return view('livewire.events.event-type-list' ,
        [
            'eventtypes' => EventType::get(),
        ]
    );
    }
}
