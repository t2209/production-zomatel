<?php

namespace App\Http\Livewire\Events;

use App\Models\Event;
use Livewire\Component;

class EventsList extends Component
{
    public $selectID;
    public $listeners = [
        'refreshEventList' => '$refresh'
    ];

    public function selectEvent($EventID , $action)
    {
        $this->selectID = $EventID;
        switch ($action) {
            case 'update':
                $this->emit('getSelectedEvent' , $this->selectID);
                $this->dispatchBrowserEvent('openEventModal');
                break;
            case 'delete':
                $this->dispatchBrowserEvent('openEventDeleteModal');
                break;    
            case 'details':
                $this->emit('getSelectedEvent' , $this->selectID);
                $this->dispatchBrowserEvent('openEventDetailModal');
                break;    
            default:
                # code...
                break;
        }

        // if ($action == "update") {
        //     $this->emit('getSelectedEvent' , $this->selectID);
        //     $this->dispatchBrowserEvent('openEventModal');
        // }if($action == "details") {
        //     $this->emit('getSelectedEvent' , $this->selectID);
        //     $this->dispatchBrowserEvent('openEventDetailModal');
        // }else{
        //     $this->dispatchBrowserEvent('openEventDeleteModal');
        // }
    }

    public function deleteEvent()
    {
        Event::destroy($this->selectID);
        $this->dispatchBrowserEvent('closeEventDeleteModal');
    }

    public function render()
    {
        return view('livewire.events.events-list' , [
            'events' => Event::
            with('eventtype')->
            get()
        ]);
    }
}
