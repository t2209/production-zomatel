<?php

namespace App\Http\Livewire\Events;

use App\Models\EventType;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class EventTypeForm extends Component
{
    public $modelID;
    public $EventTypeID;
    public $EventTypeName;
    protected $listeners = [
        'getSelectedEventType'
    ];

    public function getSelectedEventType($EventTypeID)
    {
        $this->modelID = $EventTypeID;
        $EventType = EventType::find($this->modelID);
        $this->EventTypeName = $EventType->EventName;
    }

    public function clearVars(){
        $this->EventTypeName = null;
    }

    public function save(){
        $validateData = [
            'EventTypeName' => 'required',
        ];
        $this->validate($validateData);

        $EvenType = new EventType;

        if ($this->modelID) {

            DB::transaction(function () use ($EvenType) {
                $EvenType->where("EventTypeID",$this->modelID)
                ->update([
                    'EventTypeName' => $this->EventTypeName,
                ]);
            });

        } else {

            $EvenType->EventTypeName = $this->EventTypeName;

            DB::transaction(function () use ($EvenType) {
                $EvenType->save();
            });

        }

        $this->clearVars();
        $this->dispatchBrowserEvent('closeEventTypeModal');
        $this->emit("refreshEventTypeList");
    }

    public function render()
    {
        return view('livewire.events.event-type-form');
    }
}
