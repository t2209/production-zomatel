<?php

namespace App\Http\Livewire\Events;

use App\Http\Controllers\ArticlesRawmatController;
use App\Models\Event;
use App\Models\Eventarticle;
use App\Providers\ArticleServiceProvider;
use Livewire\Component;

class EventDetail extends Component
{
    public $modelID;
    public $articles = [];
    public Event $event;
    public  $tempArticle = [];
    // for view only use in articleRawMt creation or duplication
    public $forEventView = true;
    public $tempArticleRawMats = null;

    public $familyStat = [];
    public $total =0;
    protected $listeners = [
        'getSelectedEvent',
        'setTempArticle',
        'setTempArticleRawMats'
    ];

    // public function boot(){
    //     $this->tempArticle['EventArticleQty'] = 0;
    //     $this->tempArticle['article']['ArticleName'] = "Temporaire";
    //     $this->tempArticle['EventArticleArticlePrice'] = 0;
    //     $this->tempArticle['TotalPrice'] = 0;
    // }

    public function getSelectedEvent($EventID)
    {
        $this->modelID = $EventID;
        $this->articles = ArticleServiceProvider::getArticles(null,null,$EventID,'*');
        $this->event = Event::find($EventID);
        $this->setStat();
    }

    // ?set statistic and total value
    public function setStat(){
        $articleRawMatController = new ArticlesRawmatController;
        $this->familyStat = $articleRawMatController->avgFamily($this->articles, true, $this->tempArticleRawMats, array_key_exists('EventArticleQty', $this->tempArticle) ? $this->tempArticle['EventArticleQty'] : 0);
        $this->total = array_sum(array_column($this->articles,'TotalPrice')) + $this->event['ExpenseSupp'] + (
            array_key_exists('TotalPrice',$this->tempArticle) && $this->tempArticle['TotalPrice'] != null ?$this->tempArticle['TotalPrice'] : 0);
    }

    // ?for article creation or duplication begin
    public function setTempArticle($qty, $unitPrice, $tempArticleRawMats){
        $this->tempArticle['EventArticleQty'] = $qty;
        $this->tempArticle['article']['ArticleName'] = "Temporaire";
        $this->tempArticle['EventArticleArticlePrice'] = $unitPrice;
        $this->tempArticle['TotalPrice'] = intval($unitPrice) * intval($qty);
        //? update the totality of the event
        $this->tempArticleRawMats = $tempArticleRawMats;
        // dd($tempArticleRawMats);
        // array_push($this->articles, $this->tempArticle);
        $this->setStat();
    }

    public function setTempArticleRawMats($tempArticleRawMats){
        $this->tempArticleRawMats = $tempArticleRawMats;
        $this->setStat();
    }


    public function save(){
        $this->dispatchBrowserEvent('closeEventDetailModal');
    }

    public function render()
    {
        return view('livewire.events.event-detail');
    }
}
