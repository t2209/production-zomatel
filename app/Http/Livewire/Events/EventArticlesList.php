<?php

namespace App\Http\Livewire\Events;

use Livewire\Component;

class EventArticlesList extends Component
{
    public function render()
    {
        return view('livewire.events.event-articles-list');
    }
}
