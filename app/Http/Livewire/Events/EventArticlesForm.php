<?php

namespace App\Http\Livewire\Events;

use App\Models\ArticleClass;
use App\Models\Eventarticle;
use App\Models\Rawmatarticle;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class EventArticlesForm extends Component
{
    public $modelID;
    public $EventEventID;
    public $ArticleArticleID;
    public $EventArticleQty;
    protected $listeners = [
        'getSelectedArtEvent'
    ];

    public function getSelectedArtEvent($EventArticleID)
    {
        $this->modelID = $EventArticleID;
        $Eventarticle = Eventarticle::find($this->modelID);
        $this->EventEventID = $Eventarticle->EventEventID;
        $this->ArticleArticleID = $Eventarticle->ArticleArticleID;
        $this->EventArticleQty = $Eventarticle->EventArticleQty;
    }

    public function clearVars(){
        $this->EventArticleQty = null;
    }

    public function save(){
        $validateData = [
            'EventEventID' => 'required',
            'ArticleArticleID' => 'required',
            'EventArticleQty' => 'required|numeric',
        ];

        $this->validate($validateData);

        $Eventarticle = new Eventarticle;

        if ($this->modelID) {

            DB::transaction(function () use ($Eventarticle) {
                $Eventarticle->where("EventArticleID",$this->modelID)
                ->update([
                    'EventEventID' => $this->EventEventID,
                    'EventArticleQty' => $this->EventArticleQty,
                    'ArticleArticleID' => $this->ArticleArticleID,
                ]);
            });

        } else {

            $Eventarticle->EventEventID = $this->EventEventID;
            $Eventarticle->ArticleArticleID = $this->ArticleArticleID;
            $Eventarticle->EventArticleQty = $this->EventArticleQty;

            DB::transaction(function () use ($Eventarticle) {
                $Eventarticle->save();
            });
        }

        $this->clearVars();
        $this->dispatchBrowserEvent('closeArtEventModal');
        $this->emit("refreshArtEventList");
    }

    public function render()
    {
        return view('livewire.events.event-articles-form', [
            'events' => Rawmatarticle::with('event','service','article','rawmaterialprovider',
            'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial',
            'rawmaterialprovider.rawmaterial.rawmatfamily','articleClass')
            ->groupBy('RawMatArticleCode')
            ->where('EventEventID' , '!=' , null)
            ->get(),
        ]);
    }
}
