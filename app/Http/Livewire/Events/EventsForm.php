<?php

namespace App\Http\Livewire\Events;

use App\Models\Event;
use App\Models\EventType;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class EventsForm extends Component
{
    public $modelID;
    public $EventTypeEventTypeID;
    public $EventName;
    public $EventBeginDate;
    public $EventEndDate;
    public $EventDetail;
    public $EventGuest;
    public $ExpenseSupp;
    protected $listeners = [
        'getSelectedEvent'
    ];

    public function getSelectedEvent($EventID)
    {
        $this->modelID = $EventID;
        $Event = Event::find($this->modelID);
        $this->EventName = $Event->EventName;
        $this->EventBeginDate = Carbon::parse($Event->EventBeginDate)->format('Y-m-d');
        $this->EventEndDate =  Carbon::parse($Event->EventEndDate)->format('Y-m-d');
        $this->EventDetail = $Event->EventDetail;
        $this->EventGuest = $Event->EventGuest;
        $this->ExpenseSupp = $Event->ExpenseSupp;
    }

    public function clearVars(){
        $this->EventName = null;
        $this->EventBeginDate = null;
        $this->EventEndDate = null;
        $this->EventDetail = null;
        $this->EventGuest = null;
        $this->ExpenseSupp = null;
    }

    public function save(){
        $validateData = [
            'EventName' => 'required',
            'EventTypeEventTypeID' => 'required',
            'EventBeginDate' => 'required',
            'EventEndDate' => 'required | after_or_equal:EventBeginDate',
            'EventDetail' => 'required',
            'EventGuest' => 'required'
        ];
        $this->validate($validateData);

        $Event = new Event;

        if ($this->modelID) {

            DB::transaction(function () use ($Event) {
                $Event->where("EventID",$this->modelID)
                ->update([
                    'EventTypeEventTypeID' => $this->EventTypeEventTypeID,
                    'EventName' => $this->EventName,
                    'EventBeginDate' => $this->EventBeginDate,
                    'EventEndDate' => $this->EventEndDate,
                    'EventDetail' => $this->EventDetail,
                    'EventGuest' => $this->EventGuest,
                    'ExpenseSupp' => $this->ExpenseSupp
                ]);
            });

        } else {

            $Event->EventTypeEventTypeID = $this->EventTypeEventTypeID;
            $Event->EventName = $this->EventName;
            $Event->EventBeginDate = $this->EventBeginDate;
            $Event->EventEndDate = $this->EventEndDate;
            $Event->EventDetail = $this->EventDetail;
            $Event->EventGuest = $this->EventGuest;
            $Event->ExpenseSupp = $this->ExpenseSupp;

            DB::transaction(function () use ($Event) {
                $Event->save();
            });

        }

        $this->clearVars();
        $this->dispatchBrowserEvent('closeEventModal');
        $this->emit("refreshEventList");
    }

    public function render()
    {
        return view('livewire.events.events-form' ,
            [
                'eventtypes' => EventType::get(),
            ]
        );
    }
}
