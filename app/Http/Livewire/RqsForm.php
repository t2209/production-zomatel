<?php

namespace App\Http\Livewire;

use App\Models\Articleservice;
use App\Models\Purchaseorderservice;
use App\Models\Rawmatarticle;
use App\Models\Rawmatrequireservice;
use App\Models\Service;
use App\Providers\ArticleServiceProvider;
use App\Providers\RawMatServiceProvider;
use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;

class RqsForm extends Component
{
    public $serviceID=null;
    public $eventID=null;
    public $articles = [];
    public $rawMats = [];
    public $classGrid = "bg-marron_focus p-2 font-semibold text-white rounded-md flex items-center";
    public $RawMatRequireServiceDate;
    public $test = 0;

    public function mount(){
        $this->articles = ArticleServiceProvider::getArticles(null,$this->serviceID,null,'*');
        $this->rawMats = RawMatServiceProvider::getRawMats('*',$this->serviceID,$this->eventID);
    }

    public function clearVars(){
        $this->RawMatRequireServiceDate = null;
    }

    public function saveRQService()
    {
        $validateData = [
            'RawMatRequireServiceDate' => 'required'
        ];
        $this->validate($validateData);
        $this->emit("getQtyRwmatreqserv",$this->RawMatRequireServiceDate);
        $this->clearVars();
    }

    public function render()
    {
        return view('livewire.rqs-form');
    }
}
