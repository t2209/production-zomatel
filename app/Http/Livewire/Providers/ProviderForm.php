<?php

namespace App\Http\Livewire\Providers;

use App\Models\Provider;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class ProviderForm extends Component
{
    public $modelID;
    public $ProviderCode;
    public $ProviderName;
    public $ProviderAdress;
    public $ProviderPhone;
    protected $listeners = [
        'getSelectedProvider'
    ];

    public function getSelectedProvider($ProviderID)
    {
        $this->modelID = $ProviderID;
        $Provider = Provider::find($this->modelID);
        $this->ProviderName = $Provider->ProviderName;
        $this->ProviderAdress = $Provider->ProviderAdress;
        $this->ProviderPhone =  $Provider->ProviderPhone;
        $this->ProviderCode = $Provider->ProviderCode;
    }

    public function clearVars(){
        $this->ProviderCode = null;
        $this->ProviderName = null;
        $this->ProviderAdress = null;
        $this->ProviderPhone = null;
    }

    public function save(){
        $validateData = [
            'ProviderCode' => 'required',
            'ProviderName' => 'required',
            'ProviderAdress' => 'required',
            'ProviderPhone' => 'required'
        ];

        $this->validate($validateData);

        $Provider = new Provider;

        if ($this->modelID) {

            DB::transaction(function () use ($Provider) {
                $Provider->where("ProviderID",$this->modelID)
                ->update([
                    'ProviderName' => $this->ProviderName,
                    'ProviderAdress' => $this->ProviderAdress,
                    'ProviderPhone' => $this->ProviderPhone,
                    'ProviderCode' => $this->ProviderCode,
                ]);
            });

        } else {

            $Provider->ProviderCode = $this->ProviderCode;
            $Provider->ProviderName = $this->ProviderName;
            $Provider->ProviderAdress = $this->ProviderAdress;
            $Provider->ProviderPhone = $this->ProviderPhone;

            DB::transaction(function () use ($Provider) {
                $Provider->save();
            });
        }

        $this->clearVars();
        $this->dispatchBrowserEvent('closeProviderModal');
        $this->emit("refreshProviderList");
    }
    public function render()
    {
        return view('livewire.providers.provider-form');
    }
}
