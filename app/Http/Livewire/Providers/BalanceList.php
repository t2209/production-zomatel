<?php

namespace App\Http\Livewire\Providers;

use App\Models\Provider;
use Livewire\Component;

class BalanceList extends Component
{
    public $selectedDateStart;
    public $selectedDateEnd;
    
    protected $queryString = [
        'selectedDateStart','selectedDateEnd'
    ];

    // ->whereBetween('RawMatEconomatDate', [$this->startOfWeek,$this->endOfWeek])

    public function render()
    {
        return view('livewire.providers.balance-list', [
            'providers' => Provider::get()
        ]);
    }
}
