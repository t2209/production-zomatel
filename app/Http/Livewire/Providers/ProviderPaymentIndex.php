<?php

namespace App\Http\Livewire\Providers;

use App\Models\Rawmatprovider;
use App\Providers\RawMatServiceProvider;
use Carbon\Carbon;
use Livewire\Component;

class ProviderPaymentIndex extends Component
{
    public $weekBlade , $weekDate;
    public $RawmatProd;

    protected $queryString = ['weekBlade'];

    public function getDataOfWeek()
    {
        $this->validate([
            "weekBlade" => "required"
        ]);
        $this->weekDate = Carbon::now();
        $weekView = explode("-W",$this->weekBlade);
        $this->weekDate->setISODate($weekView[0],$weekView[1]);

        $this->RawmatProd = RawMatServiceProvider::getRawMatforProvider($this->weekDate,null,null,null,null);
        // dd($this->RawmatProd);

        // dd(RawMatServiceProvider::getRawMatByProvider($this->weekDate,3));
    }

    // public function getDataOfWeekPerProd()
    // {
    //     $this->validate([
    //         "weekBlade" => "required"
    //     ]);
    //     $this->weekDate = Carbon::now();
    //     $weekView = explode("-W",$this->weekBlade);
    //     $this->weekDate->setISODate($weekView[0],$weekView[1]);

    //     $this->RawmatProd = RawMatServiceProvider::getRawMatPerProd($this->weekDate);
    //     dd($this->RawmatProd);
    // }

    public function render()
    {
        return view('livewire.providers.provider-payment-index');
    }
}
