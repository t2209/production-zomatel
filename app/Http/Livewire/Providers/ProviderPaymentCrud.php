<?php

namespace App\Http\Livewire\Providers;

use App\Models\PaymentMethod;
use Livewire\Component;

class ProviderPaymentCrud extends Component
{
    public $PaymentMethWording;
    public $PaymentMethID;
    public $type = "save";

    protected $listeners = ['refreshPayMeths' => '$refresh'];

    protected $queryString = ['PaymentMethWording','PaymentMethID','type'];

    public function save($type)
    {
        $this->validate([
            'PaymentMethWording' => 'required'
        ]);
        if ($type == "save") {
            PaymentMethod::create([
                "PaymentMethWording" => $this->PaymentMethWording
            ]);
            $this->emit('refreshPayMeths');
            $this->cancel();
        }elseif ($type == "edit") {
            PaymentMethod::where('PaymentMethID',$this->PaymentMethID)->update([
                "PaymentMethWording" => $this->PaymentMethWording
            ]);
            $this->emit('refreshPayMeths');
        }
    }

    public function getId($id,$wording)
    {
        $this->PaymentMethWording = $wording;
        $this->PaymentMethID = $id;
        $this->type = "edit";
    }

    public function cancel()
    {
        $this->PaymentMethWording = null;
        $this->PaymentMethID = null;
        $this->type = "save";
    }

    public function render()
    {
        return view('livewire.providers.provider-payment-crud',[
            'payMeths' => PaymentMethod::get()
        ]);
    }
}
