<?php

namespace App\Http\Livewire\Providers;

use App\Models\Rawmatrequire;
use App\Providers\RawMatServiceProvider;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithFileUploads;

class ProviderPaymentGrid extends Component
{
    use WithFileUploads;

    public Rawmatrequire $item;
    // public $paymentRef;
    // public $paymentMode;
    public $paymentFile;
    public $weekDate;
    public $status;

    protected $rules=[
        'item.PaymentRef' => 'required|nullable',
        'item.PaymentMethPaymentMethID' => 'required|nullable',
        'paymentFile' => 'required',

    ];

    public function validatePayment()
    {
        $this->validate();
        // $this->validate([
        //     "paymentRef" => "required",
        //     "paymentMode" => "required",
        //     "paymentFile" => "required"
        // ]);

        $rawMatsRequired = RawMatServiceProvider::getRawMatByProviderDetail($this->weekDate,$this->item->rawmatprovider->provider->ProviderID);
        // dd($rawMatsRequired);

        // save file
        $this->paymentFile->storePubliclyAs('paymentAttach',$this->paymentFile->getClientOriginalName(),'public');
        // dd($this->paymentFile);
        foreach ($rawMatsRequired as $key => $rawMatRequired) {
            $rawMatRequired->PaymentRef = $this->item->PaymentRef;
            $rawMatRequired->PaidAt = Carbon::now();
            $rawMatRequired->PaymentMethPaymentMethID = $this->item->PaymentMethPaymentMethID;
            $rawMatRequired->PaymentAttachement = $this->paymentFile->getClientOriginalName();
            $rawMatRequired->save();
        }
        // dd(RawMatServiceProvider::getRawMatByProviderDetail($this->weekDate,$this->item->rawmatprovider->provider->ProviderID));
    }

    public function mount(){
        // dd($this->item);
        // ?use this to a conditionnal component depending on paid or not
        $this->status = $this->item->PaymentRef != null ? 'btn-disabled' : 'btn-success';
    }


    public function render()
    {
        return view('livewire.providers.provider-payment-grid');
    }
}
