<?php

namespace App\Http\Livewire\Providers;

use App\Models\Rawmatrequire;
use Carbon\Carbon;
use Livewire\Component;

class GrandBookList extends Component
{
    public $searchValue;
    // public $selectedWeek;
    public $startOfWeek;
    public $endOfWeek;
    public $selectedDateStart;
    public $selectedDateEnd;

    protected $queryString = [
        // 'selectedWeek',
        'startOfWeek','endOfWeek',
    ];


    public function getGrandBookList()
    {
        $validateData = [

            // 'selectedWeek' => 'required',
            'startOfWeek' => 'required',
            'endOfWeek' => 'required|after_or_equal:startOfWeek',
        ];

        $this->validate($validateData);

        // get list of stock service rawmat
        // $weekSplit = explode("-W",$this->selectedWeek);

        // $date = Carbon::now();
        // $date->setISODate($weekSplit[0],$weekSplit[1]);
        $this->selectedDateStart = Carbon::parse($this->startOfWeek)->format("Y-m-d");
        $this->selectedDateEnd = Carbon::parse($this->endOfWeek)->format("Y-m-d");
        // dd($this->startOfWeek,$this->endOfWeek);
    }

    public function render()
    {
        return view('livewire.providers.grand-book-list', [
            // 'rawmatreq' => Rawmatrequire::get()
            'rawmatreq' => Rawmatrequire::with('purchaseorderservice','rawmatprovider.rawmaterial.rawmatfamily','rawmatprovider.provider','purchaseorderservice.service','purchaseorderservice.purchaseorderprovider.grandbookprovider','paymentmethod')
            ->whereHas('rawmatprovider.provider', function ($query){
                $query->where('ProviderName','like', '%' . $this->searchValue . '%');
            })
            ->whereHas('purchaseorderservice.purchaseorderprovider', function ($query){
                $query->whereBetween('POProvDateReceive', [$this->selectedDateStart,$this->selectedDateEnd]);
            })
            ->get()
        ]);
    }
}
