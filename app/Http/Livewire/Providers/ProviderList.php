<?php

namespace App\Http\Livewire\Providers;

use App\Models\Provider;
use Livewire\Component;

class ProviderList extends Component
{
    public $selectID;
    public $listeners = [
        'refreshProviderList' => '$refresh'
    ];

    public function selectProvider($ProviderCode , $action)
    {
        $this->selectID = $ProviderCode;

        if ($action == "update") {
            $this->emit('getSelectedProvider' , $this->selectID);
            $this->dispatchBrowserEvent('openProviderModal');
        }else {
            $this->dispatchBrowserEvent('openProviderDeleteModal');
        }
    }

    public function deleteProvider()
    {
        Provider::destroy($this->selectID);
        $this->dispatchBrowserEvent('closeProviderDeleteModal');
    }


    public function render()
    {
        return view('livewire.providers.provider-list' , [
            'providers' => Provider::get()
        ]);
    }
}
