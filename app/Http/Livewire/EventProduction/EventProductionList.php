<?php

namespace App\Http\Livewire\EventProduction;

use App\Providers\ArticleServiceProvider;
use Livewire\Component;

class EventProductionList extends Component
{
    public $ServiceIDActive = null;
    public $ClassIDActive = "*";
    public $EventTypeIDActive = "*";
    public $searchValue;

    public function render()
    {
        return view('livewire.event-production.event-production-list',[
            'eventarticles' => ArticleServiceProvider::getArticles($this->searchValue,$this->ServiceIDActive,$this->EventTypeIDActive,$this->ClassIDActive)
        ]);
    }
}
