<?php

namespace App\Http\Livewire\EventProduction;

use App\Models\Eventarticle;
use App\Models\Rawmatservice;
use App\Providers\RawMatServiceProvider;
use Livewire\Component;

class EventProductionGridList extends Component
{
    public $item;
    public $Rawmats;
    public $neededRawmats;
    public $classGrid = "flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded";
    public $rules = [
        "item.EventArticleQty" => "required"
    ];

    public function getRawMats($code){
        if (getClassInstance($this->item) === Articleservice::class){
            return  RawMatServiceProvider::getRawMats($this->item['article']['ArticleID'],$code,null);
        }else if(getClassInstance($this->item) === Eventarticle::class){
            return  RawMatServiceProvider::getRawMats($this->item['article']['ArticleID'],null,$code);
        }
    }

    function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
    
        return false;
    }

    public function setQty($code)
    {
        // dd($code);s
        $this->Rawmats = $this->getRawMats($code);

        $rawmatevents = [];
        foreach ($this->Rawmats as $key => $value) {
            $RawmatQte = Rawmatservice::select('RawMatServiceQty')
                                    ->where('RawMaterialRawMaterialID',$value->rawmaterialprovider->rawmaterial->RawMaterialID)
                                    ->get()->toArray();
            $rawmatevents[$key]["Qte"] = (!empty($RawmatQte)) ? $RawmatQte[0]["RawMatServiceQty"] : 0;
            $rawmatevents[$key]["Rawmat"] = $value->rawmaterialprovider->rawmaterial->RawMaterialName;
        }

        $sumRawmats = array_reduce(
            $rawmatevents,
            function (array $rawmat, array $item) {
                $city = $item['Rawmat'];
                if (array_key_exists($city, $rawmat)) {
                    $rawmat[$city]['Qte'] += $item['Qte'];
                } else {
                    $rawmat[$city] = $item;
                }
                return $rawmat;
            },
            array()
        );
        $sumRawmats = array_values($sumRawmats);

        if ($this->in_array_r(0,$sumRawmats)) {
            $this->neededRawmats = $sumRawmats;
            session()->flash('msg', 'Quantité de matières première insuffisante');
        } else {
            Eventarticle::find($this->item["EventArticleID"])->update(["EventArticleQty" => $this->item["EventArticleQty"]]);
        }
        

    }

    public function render()
    {
        return view('livewire.event-production.event-production-grid-list');
    }
}
