<?php

namespace App\Http\Livewire;

use App\Models\Rawmatrequire;
use Livewire\Component;

class RawMatRequireForm extends Component
{

    public $rawMatRequire = 0;
    public $rawMat = null;
    public int $key = 0;
    public $serviceID = null;
    public $eventID = null;
    public $tempRawMatReqQte = 0;
    public $qtyData = [];
    public $classGrid = "flex items-center h-10 justify-center text-center bg-white border border-slate-800 rounded";

    public $rules = [
        "rawMatRequire.RawMatRequireQty" => "required",
        "tempRawMatReqQte" => "required"
    ];

    public $listeners = [
        "getQtyRwmatreq"
    ];


    public function getQtyRwmatreq($PODId,$ServcieID){
        if ($ServcieID == $this->rawMat->service->ServiceID) {
            Rawmatrequire::create([
                "RawMaterialRawMaterialID" => $this->rawMat->rawmaterialprovider->rawmaterial->RawMaterialID,
                "PurchaseOrderServicePOServID" => $PODId,
                "RawMatRequireQty" => $this->rawMatRequire
            ]);
        }
    }

    public function mount()
    {
        // dd($this->rawMat);
        $dataRawmatreq = Rawmatrequire::
        whereHas('purchaseorderservice', function ($query){
            $query->where('ServiceServiceID',$this->rawMat->service->ServiceID);
        })
        // !replace where by whereHas
        // !RawMaterialRawMaterialID in rawmatrequire is replaced by RawMatProviderRawMatProviderID
        ->whereHas('rawmatprovider', function ($query){
            $query->where('RawMaterialRawMaterialID',$this->rawMat->rawmaterialprovider->rawmaterial->RawMaterialID);
        })
        // ->where("RawMaterialRawMaterialID",$this->rawMat->rawmaterialprovider->rawmaterial->RawMaterialID)
        ->get();
        foreach ($dataRawmatreq as $value) {
            $this->rawMatRequire = $value->RawMatRequireQty;
        }
        // dd($this->rawMatRequire);
    }

    public function render()
    {
        return view('livewire.raw-mat-require-form');
    }
}
