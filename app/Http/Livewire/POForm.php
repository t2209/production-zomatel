<?php

namespace App\Http\Livewire;

use App\Models\Articleservice;
use App\Models\Purchaseorderservice;
use App\Models\Rawmatarticle;
use App\Models\Service;
use App\Providers\ArticleServiceProvider;
use App\Providers\RawMatServiceProvider;
use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;

class POForm extends Component
{
    public $serviceID=null;
    public $eventID=null;
    public $articles = [];
    public $rawMats = [];
    public $classGrid = "bg-marron_focus p-2 font-semibold text-white rounded-md flex items-center";
    public $POServDateNeed;
    public $test = 0;

    public function mount(){
        $this->articles = ArticleServiceProvider::getArticles(null,$this->serviceID,null,'*');
        $this->rawMats = RawMatServiceProvider::getRawMats('*',$this->serviceID,$this->eventID);
    }

    public function savePOService()
    {
        $serviceIDs = Service::select("ServiceID")->get()->toArray();
        $ServiceServiceID = ($this->serviceID == "*") ? $serviceIDs : $this->serviceID;

        // dd( is_array($ServiceServiceID));

        $this->validate([
            "POServDateNeed" => "required | after_or_equal:".date("Y-m-d")
        ]);

        if (is_array($ServiceServiceID)) {
            // dd($ServiceServiceID);
            for ($i=0; $i < count($ServiceServiceID); $i++) { 
                // dd($ServiceServiceID[0]["ServiceID"]);
                $POservice = Purchaseorderservice::create([
                    "PurchaseOrderProviderPurchaseOrderProviderID" => null,
                    "ServiceServiceID" => $ServiceServiceID[$i]["ServiceID"],
                    "POServDateNeed" => $this->POServDateNeed,
                    "POServDateSend" => date("Y-m-d")
                ]);
                $this->emit("getQtyRwmatreq",$POservice->POServID,$ServiceServiceID[$i]["ServiceID"]);
            }
        } else {
            $data = [
                "PurchaseOrderProviderPurchaseOrderProviderID" => null,
                "ServiceServiceID" => $ServiceServiceID,
                "POServDateNeed" => $this->POServDateNeed,
                "POServDateSend" => date("Y-m-d")
            ];
            $POservice = Purchaseorderservice::create($data);
            $this->emit("getQtyRwmatreq",$POservice->POServID,$ServiceServiceID);
        }


    }

    public function render()
    {
        return view('livewire.POService.p-o-form');
    }
}
