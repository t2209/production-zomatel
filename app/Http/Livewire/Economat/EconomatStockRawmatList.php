<?php

namespace App\Http\Livewire\Economat;

use App\Models\Rawmateconomat;
use Carbon\Carbon;
use Livewire\Component;

class EconomatStockRawmatList extends Component
{
    public $selectedWeek;
    public $weekShow;
    public $servicesID = null;
    public $startOfWeek , $endOfWeek;

    protected $queryString = [
        'selectedWeek' => ['except' => '*'],
        'weekShow' => ['except' => '*']
    ];


    public function getStockService()
    {
        $validateData = [
            'selectedWeek' => 'required',
        ];

        $this->validate($validateData);

        // set week view
        $weekView = explode("-W",$this->selectedWeek);
        $this->weekShow = "Semaine " . $weekView[1] . " , " . $weekView[0];

        // get list of stock service
        $date = Carbon::now();
        $date->setISODate($weekView[0],$weekView[1]);

        $this->startOfWeek = Carbon::parse($date->startOfWeek())->format("Y-m-d");
        $this->endOfWeek = Carbon::parse($date->endOfWeek())->format("Y-m-d");

        $this->servicesID = Rawmateconomat::with('rawmaterial')
                            // select('ServiceServiceID','ArticleServiceDate','ArticleArticleID',)->
                            ->whereBetween('RawMatEconomatDate', [$this->startOfWeek,$this->endOfWeek])
                            // ->orderBy('rawmaterial.RawMaterialAccountNum', 'desc')
                            ->groupBy('RawMaterialRawMaterialID')
                            ->selectRaw('*, SUM(RawMatEconomatQty) as RawMatEconomatQtySum')
                            ->orderBy('RawMaterialRawMaterialID','ASC')
                            ->get();

        // dd($this->servicesID);

    }

    public function unsetVars()
    {
        $this->servicesID = null;
    }
    public function render()
    {
        return view('livewire.economat.economat-stock-rawmat-list');
    }
}
