<?php

namespace App\Http\Livewire\POProvider;

use App\Http\Controllers\ArticlesRawmatController;
use App\Models\Grandbookprovider;
use App\Models\Purchaseorderprovider;
use App\Models\Purchaseorderservice;
use App\Models\Rawmatfamily;
use App\Models\Rawmatrequire;
use App\Providers\RawMatServiceProvider;
use Livewire\Component;

class POProviderListPurchase extends Component
{
    public $items;
    public $weekDate;
    public $searchValue;
    public $sumPurchasePrice;
    public $family = null;
    public $adress = null;
    // active class
    public $activeAdress = false;
    public $activeAll = true;
    public $activeFamily = null;
    // listners
    public $listeners = [
        'CUPOProd'
    ];

    public function CUPOProd($POServiID)
    {
        // add GrandBool
        $GrandBook = Grandbookprovider::create([
            "GrandBookProviderDebit" => 0,
            "GrandBookProviderCredit" => $this->sumPurchasePrice,
            "GrandBookProviderReport" => 0,
            "GrandBookProviderSold" => 0 - floatval($this->sumPurchasePrice)
        ]);
        // add POProd
        $POProd = Purchaseorderprovider::create([
            "GrandBookProviderGrandBookProviderID" => $GrandBook->GrandBookProviderID,
            "POProvDateSend" => date("Y-m-d"),
            "POProvDateReceive" => null
        ]);
        // up POServ
        for ($i=0; $i < count($POServiID); $i++) { 
            Purchaseorderservice::find($POServiID[$i])->update([
                "PurchaseOrderProviderPurchaseOrderProviderID" => $POProd->PurchaseOrderProviderID
            ]);
        }

    }

    public function mount()
    {
        $this->sumPurchasePrice = array_sum(array_column($this->items,'RawMatRequirePurchasePrice'));
    }

    public function setFilter()
    {
        $this->items = RawMatServiceProvider::getRawMatforProvider($this->weekDate,$this->searchValue,$this->family,$this->adress);
        $this->sumPurchasePrice = array_sum(array_column($this->items,'RawMatRequirePurchasePrice'));
    }

    public function search()
    {
        $this->setFilter();
    }

    public function familyFilter($familyFilter)
    {
        $this->family = $familyFilter;
        $this->activeAll = false;
        $this->activeFamily = $familyFilter;
        $this->setFilter();
    }

    public function adressFilter($adressFilter)
    {
        $this->activeAdress = true;
        $this->activeAll = false;
        $this->adress = $adressFilter;
        $this->setFilter();
    }

    public function unsetFilter()
    {
        $this->family = null;
        $this->adress = null;
        $this->activeAdress = false;
        $this->activeAll = true;
        $this->activeFamily = null;
        $this->setFilter();
    }

    public function render()
    {
        $articleRawMatController =  new ArticlesRawmatController();
        return view('livewire.p-o-provider.p-o-provider-list-purchase',[
            'avgFamily' => $articleRawMatController->avgRawmatProviderPurchase($this->items),
            'families' => Rawmatfamily::get()
        ]);
    }
}
