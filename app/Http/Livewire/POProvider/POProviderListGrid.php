<?php

namespace App\Http\Livewire\POProvider;

use App\Models\Rawmatrequire;
use Livewire\Component;

class POProviderListGrid extends Component
{
    public $POService = null;
    public $searchValue;
    public $POSId;
    public $key = 0;
    public $classGrid = "flex items-center mt-2 rounded-sm h-10 bg-gray-100";
    public $rawMatRequires;

    public function showDetails($POServiceID)
    {
        $this->POSId = $POServiceID;
        $this->rawMatRequires = Rawmatrequire::with('rawmaterial','rawmaterial.rawmatproviders')
                                            ->where("PurchaseOrderServicePOServID",$POServiceID)->get();
        // dd($this->rawMatRequires);
    }

    public function search()
    {
        $this->rawMatRequires = Rawmatrequire::with('rawmaterial','rawmaterial.rawmatproviders')
                                            ->whereHas('rawmaterial', function ($query) {
                                                $query->where('RawMaterialName','like', '%' . $this->searchValue . '%');
                                            })
                                            ->where("PurchaseOrderServicePOServID",$this->POSId)->get();
    }

    public function render()
    {
        return view('livewire.p-o-provider.p-o-provider-list-grid');
    }
}
