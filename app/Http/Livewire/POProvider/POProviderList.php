<?php

namespace App\Http\Livewire\POProvider;

use App\Models\Purchaseorderprovider;
use App\Models\Purchaseorderservice;
use App\Models\Rawmatrequire;
use App\Providers\RawMatServiceProvider;
use Carbon\Carbon;
use Livewire\Component;

class POProviderList extends Component
{
    public $POServDateSend = null;
    public $checkPOProd;
    public $weekSelection;
    public $searchValue;
    public $btnActive;
    public $RawmatProd;
    public $classGrid = "flex h-10 items-center font-mono bg-marron_focus text-white rounded-sm";
    public $startOfWeek,$endOfWeek,$weekDate;

    protected $queryString = [
        'startOfWeek' => ['except' => '*'],
        'endOfWeek' => ['except' => '*'],
        'weekDate' => ['except' => '*'],
        'weekSelection' => ['except' => '*']
    ];

    public function dateFilter()
    {
        $this->btnActive = "Active";

        $this->weekDate = Carbon::now();
        $weekView = explode("-W",$this->weekSelection);
        $this->weekDate->setISODate($weekView[0],$weekView[1]);

        $this->startOfWeek = Carbon::parse($this->weekDate->startOfWeek())->format("Y-m-d");
        $this->endOfWeek = Carbon::parse($this->weekDate->endOfWeek())->format("Y-m-d");

        $POProdId = Purchaseorderservice::select('PurchaseOrderProviderPurchaseOrderProviderID')
        ->whereBetween('POServDateSend',[$this->startOfWeek,$this->endOfWeek])->get();
        foreach ($POProdId as $value) {
            if ($value->PurchaseOrderProviderPurchaseOrderProviderID == null) {
                $this->checkPOProd = "NotSend";
            }
        }

        // dd($this->startOfWeek,$this->endOfWeek);
    }

    public function unsetBtnActive()
    {
        $this->btnActive = null;
    }

    public function getRawmatForProd()
    {
        $this->RawmatProd = RawMatServiceProvider::getRawMatforProvider($this->weekDate);
        // dd($this->RawmatProd);
    }

    public function addUpPO()
    {
        // get POServID
        $POServiID = Purchaseorderservice::select('POServID')->whereBetween('POServDateSend',[$this->startOfWeek,$this->endOfWeek])->get()->toArray();
        foreach ($POServiID as $key => $value) {
            $POServiID[$key] = $value['POServID'];
        }

        // create GrandBook / POProd / up POServe 
        $this->emit('CUPOProd',$POServiID);
    }

    public function render()
    {
        return view('livewire.p-o-provider.p-o-provider-list',[
            "POServices" => Purchaseorderservice::with('service')
                                                ->whereHas('service', function ($query) {
                                                    $query->where('ServiceName','like', '%' . $this->searchValue . '%');
                                                })
                                                ->whereBetween('POServDateSend',[$this->startOfWeek,$this->endOfWeek])
                                                ->get()
        ]);
    }
}
