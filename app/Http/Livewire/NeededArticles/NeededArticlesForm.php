<?php

namespace App\Http\Livewire\NeededArticles;

use App\Models\Articleservice;
use App\Models\Rawmatrequire;
use App\Providers\RawMatServiceProvider;
use Livewire\Component;

class NeededArticlesForm extends Component
{
    public $article;
    public int $key = 0;
    public $serviceID = null;
    public $eventID = null;
    // vars stock
    public $sfWeek = 0;
    public $stockPhysique = 0;
    public $dmd = 0;
    public $RawMatRequireQty = [];
    // vars rawmats
    public $articleRawmats;

    public $listeners = [
        "showRawmats"
    ];

    public function mount()
    {
        $this->sfWeek = $this->article['ArticleServiceQty'];
        $this->articleRawmats = $this->getRawMats();
        $this->dmd = ($this->article['ArticleServiceMax'] > $this->stockPhysique) ? $this->article['ArticleServiceMax'] - $this->stockPhysique : 0;

        foreach ($this->articleRawmats as $value) {
            array_push($this->RawMatRequireQty,[
                // "RawMaterialRawMaterialID" => $value->rawmaterialprovider->rawmaterial->RawMaterialID,

                // !use rawmatproviderID instead of direct rawmaterialID
                "RawMatProviderRawMatProviderID" => $value->rawmaterialprovider->RawMatProviderID,
                "RawMatArticleQty" => $value->RawMatArticleQty * $this->dmd
            ]);
        }
    }

    public function getRawMats(){
        if (getClassInstance($this->article) === Articleservice::class){
            return  RawMatServiceProvider::getRawMats($this->article['article']['ArticleID'],$this->serviceID,null);
        }else if(getClassInstance($this->article) === Eventarticle::class){
            return  RawMatServiceProvider::getRawMats($this->article['article']['ArticleID'],null,$this->eventID);
        }
    }

    public function showRawmats($POServID)
    {
        for ($i=0; $i < count($this->RawMatRequireQty); $i++) {
            $Rawmatrequire = new Rawmatrequire;
            // $Rawmatrequire->RawMaterialRawMaterialID = $this->RawMatRequireQty[$i]["RawMaterialRawMaterialID"];
            $Rawmatrequire->RawMatProviderRawMatProviderID = $this->RawMatRequireQty[$i]["RawMatProviderRawMatProviderID"];
            $Rawmatrequire->PurchaseOrderServicePOServID = $POServID;
            $Rawmatrequire->RawMatRequireQty = $this->RawMatRequireQty[$i]["RawMatArticleQty"];
            $Rawmatrequire->RawMatReceiveQty = 0;
            try {
                $Rawmatrequire->save();
            } catch (\Throwable $th) {
                throw $th;
            }
        }
    }

    public function render()
    {
        return view('livewire.needed-articles.needed-articles-form');
    }
}
