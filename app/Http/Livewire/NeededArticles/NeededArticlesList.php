<?php

namespace App\Http\Livewire\NeededArticles;

use App\Models\Purchaseorderservice;
use App\Providers\ArticleServiceProvider;
use App\Providers\RawMatServiceProvider;
use Livewire\Component;

class NeededArticlesList extends Component
{
    public $serviceID=null;
    public $eventID=null;
    public $articles = [];
    public $rawMats = [];
    public $classGrid = "flex h-10 items-center font-mono bg-marron_focus text-white rounded-sm";
    public $POServDateNeed;

    public function mount()
    {
        $this->POServDateNeed = date("Y-m-d");
        $this->articles = ArticleServiceProvider::getArticles(null,$this->serviceID,null,'*');
        $this->rawMats = RawMatServiceProvider::getRawMats('*',$this->serviceID,$this->eventID);    
    }

    public function saveRawmatrequire()
    {
        $this->validate([
            "POServDateNeed" => "required | after_or_equal:".date("Y-m-d")
        ]);
        
        try {
            $POService = Purchaseorderservice::create([
                "PurchaseOrderProviderPurchaseOrderProviderID" => null,
                "ServiceServiceID" => $this->serviceID,
                "POServDateNeed" => $this->POServDateNeed,
                "POServDateSend" => date("Y-m-d")
            ]);
            $this->emit('showRawmats',$POService->POServID);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function render()
    {
        return view('livewire.needed-articles.needed-articles-list');
    }
}
