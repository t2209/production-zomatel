<?php

namespace App\Http\Livewire\Articles;

use App\Models\ArticleClass;
use Livewire\Component;

class ArticlesClassList extends Component
{
    public $selectID;
    public $listeners = [
        'refreshArticleClassList' => '$refresh'
    ];

    public function selectArticleClass($ID , $action)
    {
        $this->selectID = $ID;
        if ($action == "update") {
            $this->emit('getSelectedArticleClass' , $this->selectID);
            $this->dispatchBrowserEvent('openArticlesClassModal');
        }else {
            $this->dispatchBrowserEvent('openArticlesClassDeleteModal');
        }
    }

    public function deleteArticleClass()
    {
        ArticleClass::destroy($this->selectID);
        $this->dispatchBrowserEvent('closeArticlesClassDeleteModal');
    }
    public function render()
    {
        return view('livewire.articles.articles-class-list', [
            'articlesclass' => ArticleClass::get()
        ]);
    }
}
