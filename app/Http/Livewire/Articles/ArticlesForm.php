<?php

namespace App\Http\Livewire\Articles;

use App\Models\Article;
use Livewire\Component;
use Livewire\WithFileUploads;
use Intervention\Image\ImageManager;


class ArticlesForm extends Component
{
    use WithFileUploads;
    public $modelID;
    public $ArticleName;
    public $ArticleUnity;
    public $ArticlePurchasePrice;
    public $ArticleBarCode;
    public $ArticlePhoto;
    protected $listeners = [
        'getSelectedArticle'
    ];

    public function getSelectedArticle($ID)
    {
        $this->modelID = $ID;

        $Box = Article::find($this->modelID);

        $this->ArticleName = $Box->ArticleName;
        $this->ArticleUnity = $Box->ArticleUnity;
        $this->ArticlePurchasePrice = $Box->ArticlePurchasePrice;
        $this->ArticleBarCode = $Box->ArticleBarCode;
    }

    public function clearVars()
    {
        $this->ArticleName = null;
        $this->ArticleUnity = null;
        $this->ArticlePurchasePrice = null;
        $this->ArticleBarCode = null;
        $this->ArticlePhoto = null;
    }

    public function save()
    {
        $validateData = [
            "ArticleName" => "required",
            "ArticleUnity" => "required",
            "ArticleBarCode" => "required"
        ];

        $data = [
            "ArticleName" => $this->ArticleName,
            "ArticleUnity" => $this->ArticleUnity,
            // "ArticlePurchasePrice" => $this->ArticlePurchasePrice,
            "ArticleBarCode" => $this->ArticleBarCode
        ];

        if (!empty($this->ArticlePhoto)) {
            $imagesHashName = $this->ArticlePhoto->hashName();

            $data = array_merge($data,[
                "ArticlePhoto" => $imagesHashName
            ]);

            // save image
            $this->ArticlePhoto->store('public/articlePhotos');

            // thumbnail
            $manager = new ImageManager();
            $image = $manager->make($this->ArticlePhoto->getRealPath())->resize(300,300);
            $image->save('articlePhotos_thumb/'.$imagesHashName);
        }

        // $this->validate($validateData);
        // dd($this->modelID);
        if ($this->modelID) {
            Article::find($this->modelID)->update($data);
        } else {
            Article::create($data);
        }
        $this->clearVars();
        // $this->emit("refreshArticleList");
        $this->emit('refreshArticleList');
        $this->emit("refreshArticleCardList");
        $this->dispatchBrowserEvent("closeArticleModal");

    }

    public function render()
    {
        return view('livewire.articles.articles-form');
    }
}
