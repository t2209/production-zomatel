<?php

namespace App\Http\Livewire\Articles;

use App\Models\ArticleClass;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class ArticlesClassForm extends Component
{
    public $modelID;
    public $ArticleClassWording;
    protected $listeners = [
        'getSelectedArticleClass'
    ];

    public function getSelectedArticleClass($ID)
    {
        $this->modelID = $ID;

        $ArticleClass = ArticleClass::find($this->modelID);

        $this->ArticleClassWording = $ArticleClass->ArticleClassWording;
    }

    public function clearVars()
    {
        $this->ArticleClassWording = null;
    }

    public function save()
    {
        $validateData = [
            'ArticleClassWording' => 'required',
        ];
        $this->validate($validateData);

        // dd($this->modelID);

        $ArticleClass = new ArticleClass;
        if ($this->modelID != null) {
            DB::transaction(function () use ($ArticleClass) {
                $ArticleClass->where("ArticleClassID",$this->modelID)
                ->update([
                    'ArticleClassWording' => $this->ArticleClassWording,
                ]);
            });
        }else {
            $ArticleClass->ArticleClassWording = $this->ArticleClassWording;
            DB::transaction(function () use ($ArticleClass) {
                $ArticleClass->save();
            });
        }
        $this->clearVars();
        $this->dispatchBrowserEvent('closeArticlesClassModal');
        $this->emit("refreshArticleClassList");
    }
    public function render()
    {
        return view('livewire.articles.articles-class-form');
    }
}
