<?php

namespace App\Http\Livewire\Articles;

use App\Models\Article;
use Livewire\Component;

class ArticlesListCard extends Component
{
    public Article $item;
    public $selectID;
    protected $listeners = [
        'refreshArticleCardList' => '$refresh'
    ];

    public function selectArticle($ID , $action)
    {
        $this->selectID = $ID;
        if ($action == "delete") {
            $this->dispatchBrowserEvent('openArticleDeleteModal');
        }else {
            $this->emit('getSelectedArticle' , $this->selectID);
            $this->dispatchBrowserEvent('openArticleModal');
        }
    }

    public function delete($ID)
    {
        Article::destroy($ID);
        // $this->emit("refreshArticle");
        $this->emit('refreshArticleList');
    }

    public function render()
    {
        return view('livewire.articles.articles-list-card');
    }
}
