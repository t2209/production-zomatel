<?php

namespace App\Http\Livewire\Articles;

use App\Models\Article;
use Livewire\Component;

class ArticlesList extends Component
{
    public  $articles = [];
    public function mount(){
        $this->articles = Article::get(); 
    }

    protected $rules=[
        'articles'=>'required'
    ];

    // protected $listeners = ['refreshArticleList' => 'getListArticle'];

    protected function getListeners()
    {
        return ['refreshArticleList' => 'getListArticle'];
    }

    public function getListArticle()
    {
        // dd('ok');
        $this->articles = Article::get(); 
 
    }
    
    public function render()
    {
        return view('livewire.articles.articles-list');
    }
}
