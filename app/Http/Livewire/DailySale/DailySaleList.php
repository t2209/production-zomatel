<?php

namespace App\Http\Livewire\DailySale;

use App\Providers\OrderProvider;
use Carbon\Carbon;
use Livewire\Component;

class DailySaleList extends Component
{
    public $selectedDate;
    public $orderID = null;
    public $orderNum = null;
    public $articleOrders;
    public $totalOrder;

    protected $queryString = ['selectedDate'];

    public function boot()
    {
        $this->selectedDate = date("Y-m-d");
    }

    public function calculOrderTotal($arrayOrders)
    {
        $array_article_service = $arrayOrders->toArray();
        for ($i=0; $i < count($array_article_service); $i++) { 
            $array_article_service[$i]["Price"] = $array_article_service[$i]["article_service"]["ArticleServiceArticlePrice"] * $array_article_service[$i]["ArticleOrderQty"];
        }

        $this->totalOrder = array_sum(array_column($array_article_service,"Price"));
    }

    public function orderDetails($orderID,$orderNum)
    {
        $this->orderID = $orderID;
        $this->orderNum = $orderNum;

        // ? get articleOrder
        $this->articleOrders = OrderProvider::getArticleOrders('history',$this->orderID);

        // ? get total of order
        // $this->totalOrder = OrderProvider::getTotalOrder($this->orderID);

        $this->calculOrderTotal($this->articleOrders);

    }

    public function resetVars()
    {
        $this->orderID = null;
    }

    public function render()
    {
        return view('livewire.daily-sale.daily-sale-list',[
            'orders' => OrderProvider::getCookerOrder("dailyHistory",$this->selectedDate,"All",null,null,null),
            'totalAllOrder' => array_sum(array_column(OrderProvider::getTotalAllOrder("simple","*",$this->selectedDate),"Price")) 
        ]);
    }
}
