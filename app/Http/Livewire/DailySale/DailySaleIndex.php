<?php

namespace App\Http\Livewire\DailySale;

use App\Models\DailySale;
use Livewire\Component;
use Livewire\WithFileUploads;
use Rap2hpoutre\FastExcel\FastExcel;

class DailySaleIndex extends Component
{
    use WithFileUploads;
    public $excelFile;
    public $excelDate;
    public $data;
    public $daySaleValue;
    public $qteSum;

    function removeElementWithValue($array, $key, $value){
        foreach($array as $subKey => $subArray){
             if($subArray[$key] == $value){
                  unset($array[$subKey]);
             }
        }
        return $array;
   }

    public function import()
    {
        $this->validate([
            "excelFile" => "required|mimes:xlsx",
            "excelDate" =>"required"
        ]);
        // dd($this->excelFile);
        $this->data = (new FastExcel)->import($this->excelFile->path())->toArray();
        
        // remove blank lines
        $this->data = array_values($this->removeElementWithValue($this->data,"Famille/Produit",""));
        
        // search key sum and day's sale value
        $key = array_search("Total général remise incluse",array_column($this->data,'Famille/Produit'));
        $this->daySaleValue = $this->data[$key]["Total TTC"];

        $key2[] = $this->data;
        // calcul sum qte
        $this->qteSum = $this->data[$key]["Total Qté"];
        
        // remove 0
        $this->data = array_values($this->removeElementWithValue($this->data,"Identifiant\n prod/fam",0));

        


    }

    public function unsetFile()
    {
        $this->data = null;
    }

    public function saveData()
    {
        // save file
        $this->excelFile->storePubliclyAs('saleExcel',$this->excelDate . '.xlsx');

        // save to database
        DailySale::create([
            "Date" => $this->excelDate,
            "Value" => $this->daySaleValue
        ]);
    }

    public function render()
    {
        return view('livewire.daily-sale.daily-sale-index');
    }
}
