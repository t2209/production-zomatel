<?php

namespace App\Http\Livewire\Dashboard;

use App\Models\DailySale;
use App\Http\Controllers\ArticlesRawmatController;
use App\Providers\ArticleServiceProvider;
use App\Providers\OrderProvider;
use App\Providers\RawMatServiceProvider;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Livewire\Component;

class DashboardDataList extends Component
{
    public $week;
    public $weekNumber = null;
    public $orderData,$orderValue;
    public $weekDate;
    public $activeWeek , $active = false;
    public $avgFamily;
    public $familyStat;
    public $weekSaleData;
    public $articleSI,$rawmatSI,$rawmatAvgSI;
    public $ca;
    public $files;

    // vars ca
    public $selectedService = "*";
    public $dateBegin , $dateEnd;

    // vars for articles sails
    public $weekForSails;
    public $selectedServiceForSails;

    protected $queryString = [
        'week',
        'weekNumber',
        'weekDate',
        'orderValue'
    ];



    public function getResultData()
    {
        // set vars
        $this->validate([
            "week" => 'required'
        ]);
        $weekSplit = explode("-W",$this->week);
        $this->weekNumber = $weekSplit[1];
        $this->activeWeek = true;

        $this->emit('sendData',$this->week,$this->selectedService);


        // get appro value
        $this->weekDate = Carbon::now();
        $this->weekDate->setISODate($weekSplit[0],$weekSplit[1]);
        // 19-26
        $this->orderData = RawMatServiceProvider::getRawMatforProvider($this->weekDate)->toArray();

        // ?sum of total price per provider
        $this->orderValue = array_sum(array_column($this->orderData,'TotalPrice'));
        // dd($this->orderValue);

        // set order avg families
        // dd($this->avgFamily);
        // ?average appro
        $this->avgFamily = RawMatServiceProvider::getRawMatAvg($this->weekDate);
        // !old code avgFamily
        // $artRawMatController = new ArticlesRawmatController;
        // !$artRawMatController->avgRawmatProviderPurchase($this->orderData);

        $articleRawMatController =  new ArticlesRawmatController();
        // dd($this->week);

        // ?average stock article and values of the stock
        $this->familyStat =  $articleRawMatController->avgFamily(
            ArticleServiceProvider::getArticles(
                null,
                "*",
                "*",
                "*",
                false
                // $this->week
           )
        );
        // dd($this->familyStat);
        // dd('stock initialee en valeur des matiere premiere divise en matiere premiere par famille',RawMatServiceProvider::getRawMatsInitial($this->week));

        $resultColumn = array_column($this->avgFamily,"RawMatWording");
        array_multisort($resultColumn, SORT_ASC , $this->avgFamily);

        $compareColumn = array_column($this->familyStat,"name");
        array_multisort($compareColumn, SORT_ASC , $this->familyStat);

        // week sale
        $this->weekSaleData = RawMatServiceProvider::getWeekSale($this->weekDate);
        $this->getDailySales();

        // ?stock initiale
        // per article
        // $this->articleSI = array_sum(array_column($this->familyStat,"value"));
        // !update to get all value
        $this->articleSI = array_sum(array_column(  $articleRawMatController->avgFamily(
            ArticleServiceProvider::getArticles(
                null,
                "*",
                "*",
                "*",
                false,
                $this->week
            ),true
        ),"value"));

        // per rawmat
        $this->rawmatAvgSI = RawMatServiceProvider::getRawMatsInitial($this->week);
        $this->rawmatSI = array_sum(array_column($this->rawmatAvgSI,"TotalPrice"));

        foreach ($this->rawmatAvgSI as $key => $value) {
            $this->rawmatAvgSI[$key]["Avg"] = round(($value["TotalPrice"] * 100) / $this->rawmatSI,2);
        }
        $olumn = array_column($this->rawmatAvgSI,"RawMatWording");
        array_multisort($olumn, SORT_ASC , $this->rawmatAvgSI);

        // ? CA of the week
        $weekSplit = explode("-W",$this->week);

        $date = Carbon::now();
        $date->setISODate($weekSplit[0],$weekSplit[1]);

        $this->dateBegin = Carbon::parse($date->startOfWeek())->format("Y-m-d");
        $this->dateEnd = Carbon::parse($date->endOfWeek())->format("Y-m-d");

        $sailValue = array_sum(array_column(OrderProvider::getTotalAllOrder("filter",$this->selectedService,null,$this->dateBegin,$this->dateEnd),"Price"));
        $this->ca = round(($sailValue - ($sailValue * 0.2)),2);
        // dd($this->ca);
        // dd($this->rawmatAvgSI,$this->rawmatSI);

    }

    public function unsetVars()
    {
        // $this->weekNumber = null;
        $this->activeWeek = false;
    }

    public function getDailySales()
    {
        $this->files = RawMatServiceProvider::getDetailsWeekSale($this->weekDate);
    }


    public function setComparaisonVars()
    {
        // dd("manao comparaison");
        $this->classSetComp = "hidden";
        $this->classUnsetComp = null;
    }

    public function unsetComparaisonVars()
    {
        $this->classSetComp = null;
        $this->classUnsetComp = "hidden";
    }

    public function render()
    {
        return view('livewire.dashboard.dashboard-data-list',[
            "services" => getAllServices()
        ]);
    }
}
