<?php

namespace App\Http\Livewire\Dashboard;

use Livewire\Component;

class DashboardDataArticleSails extends Component
{
    public $week;
    public $selectedService;
    
    protected $listeners = [
        "sendData"
    ];

    public function sendData($week,$selectedService)
    {
        $this->week = $week;
        $this->selectedService = $selectedService;
    }
    public function render()
    {
        return view('livewire.dashboard.dashboard-data-article-sails');
    }
}
