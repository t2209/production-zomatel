<?php

namespace App\Http\Livewire\ArticlesRawmat;

use App\Models\Event;
use App\Models\Rawmaterial;
use App\Models\Service;
use App\Models\Article;
use App\Models\ArticleClass;
use App\Models\Articleservice;
use App\Models\Eventarticle;
use App\Models\Provider;
use App\Models\Rawmatarticle;
use App\Models\Rawmatprovider;
use App\Providers\RawMatServiceProvider;
use Livewire\Component;

class ArticlesRawmatForm extends Component
{
    public $inputs = [];
    public $i = 0;
    public $toggleEvent = "all";
    public $eventState = "false";
    public $serviceState = "false";
    public $allState = "true";
    public $ottPlatform = '';
    public $ArticleName = '';
    public $rawmats;
    // varible for crud
    public $EventEventID;
    public $ServiceServiceID;
    public $RawMaterialRawMaterialID;
    public $RawMatArticleQty;
    public $ArticleArticleID;
    public $AllServiceArticlePrice, $EventArticleArticlePrice , $ArticleServiceArticlePrice;
    public $ArticleClassArticleClassID;
    public $AllServiceArticleQteMax;
    // article Services Vars
    public $ArticleServiceMax;
    public $EventArticleQty;
    public $article = null;

    // ?only for view begin
    public $charge = 0;
    public $actualPrice;

    // ?only for view end


    // event toggle
    public function eventToogle()
    {
        if ($this->toggleEvent == "event") {
            $this->eventState = "true";
            $this->serviceState = "false";
            $this->allState = "false";

        }elseif ($this->toggleEvent == "service") {
            $this->serviceState = "true";
            $this->eventState = "false";
            $this->allState = "false";
        }else {
            $this->serviceState = "false";
            $this->eventState = "false";
            $this->allState = "true";
        }
    // ?reset all other state value
        $this->getActualPrice();
    }

    // multiple input form
    public function add($i)
    {
        $i = $i + 1;
        $this->i = $i;
        array_push($this->inputs ,$i);
        $this->dispatchBrowserEvent('initSelect');
        // emit change to eventDetail
        $this->emit('setTempArticleRawMats',$this->createRawMatArticles());

    }

    public function remove($i)
    {
        unset($this->RawMaterialRawMaterialID[$this->inputs[$i]]);
        unset($this->RawMatArticleQty[$this->inputs[$i]]);
        unset($this->inputs[$i]);
        // emit change to eventDetail
        $this->emit('setTempArticleRawMats',$this->createRawMatArticles());

        // dd($this->RawMaterialRawMaterialID, $i);
    }


    public function resetVarsToggle($action)
    {
        if ($action == "all") {
            $this->EventEventID = null;
            $this->ServiceServiceID = null;
        }elseif ($action == "event") {
            $this->ServiceServiceID = null;
        }elseif ($action == "service") {
            $this->EventEventID = null;
        }
    }
    public function resetAllVars()
    {
        $this->RawMaterialRawMaterialID = null;
        $this->RawMatArticleQty = null;
        $this->EventEventID = null;
        $this->ServiceServiceID = null;
        $this->ArticleArticleID = null;
        $this->ArticleClass = null;
    }

    // save rawMatArticle
    public function save()
    {
        // first validation
        $this->validate([
            "ArticleArticleID" => "required",
            "ArticleClassArticleClassID" => "required"
        ]);

        // toggle validation
        if ($this->allState == "true") {
            $this->validate([
                "AllServiceArticleQteMax" => "required",
                "AllServiceArticlePrice" => "required"
            ]);
        }
        if ($this->eventState == "true") {
            $this->validate([
                "EventEventID" => "required",
                "EventArticleQty" => "required",
                "EventArticleArticlePrice" => "required"
            ]);
        }
        if ($this->serviceState == "true") {
            $this->validate([
                "ServiceServiceID" => "required",
                "ArticleServiceMax" => "required",
                "ArticleServiceArticlePrice" => "required"
            ]);
        }

        // // get data from multi input
        // $arrayRawMatOld = [];
        // $arrayRawMat = [];

        // if (!empty($this->RawMaterialRawMaterialID) || !empty($this->RawMatArticleQty)) {
        //     $arrayRawMatOld = array_map(function($a,$b){ return array($a,$b);} , $this->RawMaterialRawMaterialID , $this->RawMatArticleQty);
        // }
        // for ($i=0; $i < count($arrayRawMatOld); $i++) {
        //     $arrayRawMat[$i]["RawMaterialRawMaterialID"] = $arrayRawMatOld[$i][0];
        //     $arrayRawMat[$i]["RawMatArticleQty"] = $arrayRawMatOld[$i][1];
        // }


        // add rawmatarticle
        if ($this->EventEventID == null && $this->ServiceServiceID == null) {
            $services = Service::get();

            // add rawmatarticle
            for ($k=0; $k < count($this->createArrayRawMat()); $k++) {
                foreach ($services as $key => $value) {
                    $RawMatArticleAll = new Rawmatarticle;
                    $RawMatArticleAll->EventEventID = null;
                    $RawMatArticleAll->ServiceServiceID = $value->ServiceID;
                    $RawMatArticleAll->ArticleArticleID = $this->ArticleArticleID;
                    $RawMatArticleAll->RawMaterialRawMaterialID = $this->createArrayRawMat()[$k]["RawMaterialRawMaterialID"];
                    $RawMatArticleAll->RawMatArticleQty = $this->createArrayRawMat()[$k]["RawMatArticleQty"];
                    try {
                        $RawMatArticleAll->save();
                    } catch (\Throwable $th) {
                        throw $th;
                    }
                }
            }

            // add articleservices
            foreach ($services as $key => $valueAS) {
                $ArticleService = New Articleservice;
                $ArticleService->ServiceServiceID = $valueAS->ServiceID;
                $ArticleService->ArticleArticleID = $this->ArticleArticleID;
                $ArticleService->ArticleServiceQty = 0;
                $ArticleService->ArticleServiceMax = $this->AllServiceArticleQteMax;
                $ArticleService->ArticleServiceArticlePrice = $this->AllServiceArticlePrice;
                $ArticleService->ArticleServiceDate = date("Y-m-d");
                try {
                    $ArticleService->save();
                } catch (\Throwable $th) {
                    throw $th;
                }
            }

        }else {
            // add rawmatarticles
            // for ($j=0; $j < count($arrayRawMat); $j++) {
            //     $RawMatArticle = new Rawmatarticle;
            //     $RawMatArticle->EventEventID = $this->EventEventID;
            //     $RawMatArticle->ServiceServiceID = $this->ServiceServiceID;
            //     $RawMatArticle->ArticleArticleID = $this->ArticleArticleID;
            //     $RawMatArticle->RawMaterialRawMaterialID = $arrayRawMat[$j]["RawMaterialRawMaterialID"];
            //     $RawMatArticle->RawMatArticleQty = $arrayRawMat[$j]["RawMatArticleQty"];
            //     try {
            //         $RawMatArticle->save();
            //     } catch (\Throwable $th) {
            //         throw $th;
            //     }
            // }
            foreach ($this->createRawMatArticles() as $key => $RawMatArticle) {
                try {
                    // ?unset this property because this is not used is database but just for the view in stat
                    unset($RawMatArticle->rawmaterialprovider);

                    $RawMatArticle->save();
                } catch (\Throwable $th) {
                    throw $th;
                }
            }
        }

        // add articles services
        if ($this->ServiceServiceID != null) {
            $ArticleService = New Articleservice;
            $ArticleService->ServiceServiceID = $this->ServiceServiceID;
            $ArticleService->ArticleArticleID = $this->ArticleArticleID;
            $ArticleService->ArticleServiceQty = 0;
            // $ArticleService->ArticleServiceMax = $this->ArticleServiceMax;
            $ArticleService->ArticleServiceArticlePrice = $this->ArticleServiceArticlePrice;
            $ArticleService->ArticleServiceDate = date("Y-m-d");
            try {
                $ArticleService->save();
            } catch (\Throwable $th) {
                throw $th;
            }
        }

        // add event articles
        if ($this->EventEventID != null) {
            $EventArticle = new Eventarticle;
            $EventArticle->EventEventID = $this->EventEventID;
            $EventArticle->ArticleArticleID = $this->ArticleArticleID;
            $EventArticle->EventArticleQty = $this->EventArticleQty;
            $EventArticle->EventArticleArticlePrice = $this->EventArticleArticlePrice;
            $EventArticle->EventArticleDate = date("Y-m-d");
            try {
                $EventArticle->save();
            } catch (\Throwable $th) {
                throw $th;
            }
        }

        $this->resetAllVars();
        $this->emit('refreshArticleFTList');
        $this->emit('refreshArticleCard');
        $this->dispatchBrowserEvent('closeRwMatArtModal');
    }

    public function mount()
    {
       $this->rawmats = Rawmaterial::with('rawmatproviders','rawmatproviders.provider')
       ->orderBy('RawMaterialName','ASC')
       ->get();

       $this->setData();


    //    dd($this->rawmats);
    }

    // ?to fill data for duplication
    public function setData(){
        $this->resetAllVars();
         //!duplicate article
       if($this->article != null){
        // dd('charge',$this->charge);
        $this->ArticleArticleID = $this->article['ArticleArticleID'];
        $this->ArticleClassArticleClassID = $this->article['article']['articleclass']['ArticleClassID'];
        $rawMats = null;
        if (getClassInstance($this->article) === Articleservice::class) {
            $rawMats = RawMatServiceProvider::getRawMats($this->ArticleArticleID,$this->article['ServiceServiceID'],null );

        }else if(getClassInstance($this->article) === Eventarticle::class){
            $rawMats = RawMatServiceProvider::getRawMats($this->ArticleArticleID,null,$this->article['EventEventID']);
        }
        // dd($rawMats);
        $this->i = count($rawMats);
        foreach ($rawMats as $key => $rawMat) {
            // dd(getRawMatLastPrice($rawMat->rawmaterialprovider->RawMaterialRawMaterialID)[0]);
            $this->RawMaterialRawMaterialID[$key]= getRawMatLastPrice($rawMat->rawmaterialprovider->RawMaterialRawMaterialID)[0]->RawMatProviderID;
            $this->RawMatArticleQty[$key]=$rawMat->RawMatArticleQty;
            $key >= 1 ? $this->inputs[$key - 1]=$key : null;
            //  $this->inputs[$key]=$key;
        }
        // set value of article charge depending on FT
        $this->setCharge();
        $this->getActualPrice();

       }
    }

    public function setCharge(){

        if ($this->RawMaterialRawMaterialID != null  ) {
            foreach ($this->RawMaterialRawMaterialID as  $RawmMaterialID) {
                $this->charge += Rawmatprovider::find($RawmMaterialID)->RawMatProviderUnitPrice;
            };
        }
    }


    public function createRawMatArticles(){
        $arrayArticle = [];
        for ($j=0; $j < count($this->createArrayRawMat()); $j++) {
            $RawMatArticle = new Rawmatarticle;
            // dd($RawMatArticle);
            $RawMatArticle->EventEventID = $this->EventEventID;
            $RawMatArticle->ServiceServiceID = $this->ServiceServiceID;
            $RawMatArticle->ArticleArticleID = $this->ArticleArticleID;
            $RawMatArticle->RawMaterialRawMaterialID = $this->createArrayRawMat()[$j]["RawMaterialRawMaterialID"];
            $RawMatArticle->RawMatArticleQty = $this->createArrayRawMat()[$j]["RawMatArticleQty"];

            // ?use for stat begin
            $RawMatArticle->rawmaterialprovider = Rawmatprovider::with('rawmaterial.rawmatfamily')->find($this->createArrayRawMat()[$j]["RawMaterialRawMaterialID"]);
            // ?use for stat end

            array_push($arrayArticle,$RawMatArticle);
            // try {
            //     $RawMatArticle->save();
            // } catch (\Throwable $th) {
            //     throw $th;
            // }
        }
        return $arrayArticle;
    }

    public function createArrayRawMat(){
         // get data from multi input
         $arrayRawMatOld = [];
         $arrayRawMat = [];

         if (!empty($this->RawMaterialRawMaterialID) || !empty($this->RawMatArticleQty)) {
             $arrayRawMatOld = array_map(function($a,$b){ return array($a,$b);} , $this->RawMaterialRawMaterialID , $this->RawMatArticleQty);
         }
         for ($i=0; $i < count($arrayRawMatOld); $i++) {
             $arrayRawMat[$i]["RawMaterialRawMaterialID"] = $arrayRawMatOld[$i][0];
             $arrayRawMat[$i]["RawMatArticleQty"] = $arrayRawMatOld[$i][1];
         }
         return $arrayRawMat;
    }

    //? get the actual price filled (ex: eventPrice or ServicePrice or All service Price)
    public function getActualPrice(){
        foreach ([$this->allState, $this->eventState, $this->serviceState] as $key =>$articlePurpose) {
            if ($articlePurpose === "true") {
                $this->actualPrice = intval(
                    [
                        $this->AllServiceArticlePrice,$this->EventArticleArticlePrice,$this->ArticleServiceArticlePrice
                    ][$key]
                );
            }
        }
    }

    public function updatedAllServiceArticlePrice(){
        $this->getActualPrice();
    }

    public function updatedArticleServiceArticlePrice(){
        $this->getActualPrice();
    }


    public function updatedEventArticleArticlePrice(){
        $this->getActualPrice();
        $this->updatedEventEventID();
        $this->emit('setTempArticle',floatval($this->EventArticleQty),floatval($this->actualPrice), $this->createRawMatArticles(), $this->createRawMatArticles() );
        // $this->tempArticleRawMats = $this->createRawMatArticles();
    }

    public function updatedEventArticleQty(){
        // $this->getActualPrice();
        // dd($this->createRawMatArticles());
        $this->updatedEventEventID();
        $this->emit('setTempArticle',$this->EventArticleQty,$this->actualPrice, $this->createRawMatArticles());

    }

    public function updatedRawMaterialRawMaterialID(){
        $this->emit('setTempArticleRawMats',$this->createRawMatArticles());
    }

    public function updatedRawMatArticleQty(){
        $this->emit('setTempArticleRawMats',$this->createRawMatArticles());
    }

    public function updatedEventEventID(){
        $this->emit('getSelectedEvent' , $this->EventEventID);

    }
    public function render()
    {
        return view('livewire.articles-rawmat.articles-rawmat-form',[
            'events' => Event::get(),
            'services' => Service::get(),
            'providers' => Provider::get(),
            'articles' => Article::get(),
            'articlesClasses' => ArticleClass::get(),
        ]);
    }
}
