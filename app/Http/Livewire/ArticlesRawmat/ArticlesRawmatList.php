<?php

namespace App\Http\Livewire\ArticlesRawmat;

use App\Http\Controllers\ArticlesRawmatController;
use App\Models\ArticleClass;
use App\Models\Event;
use App\Models\EventType;
use App\Models\Rawmatarticle;
use App\Models\Service;
use App\Providers\ArticleServiceProvider;
use App\Providers\RawMatServiceProvider;
use Carbon\Carbon;
use Livewire\Component;

class ArticlesRawmatList extends Component
{
    // public array $avgFamilyGen;
    // public array $statArticlesGen;
    // public $articleFT;
    // public $articleAR;
    public $ServiceIDActive = "*";
    public $ClassIDActive = "*";
    // public $EventIDActive = "*";
    public $EventTypeIDActive = "*";
    public $services= [];
    public $eventTypes= [];
    public $searchValue;
    // public $filteredBy = "*";


    protected $queryString = [
        'ServiceIDActive' => ['except' => "*"],
        'ClassIDActive' => ['except' => "*"],
        'EventTypeIDActive' => ['except' => '*'],

    ];

    public $listeners  = [
        'refreshArticleFTList' => '$refresh'
    ];

    // public function refreshArticleFTList()
    // {
    //     $this->loadData();
    // }

    public function loadData(){
        $this->services= Service::get();
        $this->eventTypes= EventType::get();
        // $this->EventTypeIDActive = "*";
        // $this->ServiceIDActive="*";
    }

    public function mount()
    {
        $this->loadData();
        $this->ServiceIDActive === 'null' ? $this->services = [] : null;
        $this->EventTypeIDActive === 'null' ? $this->eventTypes = [] : null;

    }

    // ?set filter by service
    public function resetFilter(){
        $this->EventTypeIDActive= '*';
        $this->ServiceIDActive='*';
    }

    public function resetTypeFilter(){
        $this->resetFilter();
        $this->loadData();
    }

    public function setServiceFilter($ServiceID)
    {
       $this->EventTypeIDActive = json_encode(null);
       $this->ServiceIDActive= $ServiceID;
    }

    public function setClassFilter($ArticleClassID)
    {
       $this->ClassIDActive= $ArticleClassID;
    }

    public function setEventFilter($EventTypeID)
    {
       $this->ServiceIDActive= json_encode(null);
       $this->EventTypeIDActive = $EventTypeID;
    }

    public function onlyEvent(){
        $this->loadData();
        $this->ServiceIDActive=json_encode(null);
        $this->EventTypeIDActive='*';
        $this->services = [];
        // dd($this->eventTypes);

    }

    public function onlyService(){
        $this->loadData();
        $this->EventTypeIDActive= json_encode(null);
        $this->ServiceIDActive='*';
        $this->eventTypes = [];
        // dd($this->eventTypes);

    }

    public function render()
    {
        $articleRawMatController =  new ArticlesRawmatController();

        return view('livewire.articles-rawmat.articles-rawmat-list',[
            // 'test'=> ArticleServiceProvider::getArticles(null,'*','*','*',false,'2022-W3'),
            'articleFT' => ArticleServiceProvider::getArticles($this->searchValue,$this->ServiceIDActive,$this->EventTypeIDActive,$this->ClassIDActive),
            'articleAR' => ArticleServiceProvider::getArticles($this->ServiceIDActive,$this->EventTypeIDActive,$this->ClassIDActive,true),
            'services' => $this->services,
            'eventTypes' => $this->eventTypes,
            'articleClasses' =>ArticleClass::get(),
            'articleStat' => $articleRawMatController->statArticles(
        ArticleServiceProvider::getArticles(
                            $this->searchValue,
                            $this->ServiceIDActive,
                             $this->EventTypeIDActive,
                             $this->ClassIDActive
                )
            ),
            'familyStat' =>  $articleRawMatController->avgFamily(
                ArticleServiceProvider::getArticles(
                    $this->searchValue,
                    $this->ServiceIDActive,
                    $this->EventTypeIDActive,
                    $this->ClassIDActive
               )
            ),

        ]);
    }
}
