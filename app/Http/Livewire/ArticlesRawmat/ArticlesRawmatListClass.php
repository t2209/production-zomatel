<?php

namespace App\Http\Livewire\ArticlesRawmat;

use App\Http\Controllers\ArticlesRawmatController;
use App\Models\ArticleClass;
use App\Models\Rawmatarticle;
use App\Providers\ArticleServiceProvider;
use Livewire\Component;

class ArticlesRawmatListClass extends Component
{
    public array $avgFamilyGen;
    public array $statArticlesGen;
    public $articleFT;
    public $articleAR;
    public $ClassIDActive = "*";
    public $Class = "AllClass";
    public $RawMatAvg;
    public $SearchValue;
    public $listeners  = [
        'refreshArticleClass'
    ];

    protected $queryString = [
        'ClassIDActive' => ['except' => "*"],
    ];

    public function refreshArticleClass()
    {
        $this->setServiceFilter($this->ClassIDActive,$this->Class);
    }

    public function setServiceFilter($ClassID,$Class)
    {
        $this->Class = $Class;
        // dd($this->Class);
        if ($this->Class == "perClass") {
            $this->ClassIDActive = $ClassID;

            // $this->articleFT = Rawmatarticle::with('event','service','article','rawmaterialprovider',
            // 'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial','articleClass')
            // ->groupBy('RawMatArticleCode')
            // ->where('RawMatArticleArchived',false)
            // ->where('ArticleClassArticleClassID',$ClassID)
            // ->where('EventEventID',null)
            // ->get();

            // $this->articleAR = Rawmatarticle::with('event','service','article','rawmaterialprovider',
            // 'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial','articleClass')
            // ->groupBy('RawMatArticleCode')
            // ->where('RawMatArticleArchived',true)
            // ->where('ArticleClassArticleClassID',$ClassID)
            // ->where('EventEventID',null)
            // ->get();
            $this->articleFT = ArticleServiceProvider::getArticles('*','*',$ClassID);

            $this->articleAR= ArticleServiceProvider::getArticles('*','*',$ClassID,true);

            // $this->RawMatAvg = Rawmatarticle::with('event','service','article','rawmaterialprovider',
            // 'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial','rawmaterialprovider.rawmaterial.rawmatfamily','articleClass')
            // ->where('ArticleClassArticleClassID',$ClassID)
            // ->where('RawMatArticleArchived',false)
            // ->where('EventEventID',null)
            // ->get();

            // $this->avgFamilyGen = app('App\Http\Controllers\ArticlesRawmatController')->avgFamily($this->RawMatAvg);
            $articleRawMatController =  new ArticlesRawmatController;
            // $this->avgFamilyGen = $articleRawMatController->avgFamily($RawMat);
            $this->statArticlesGen = $articleRawMatController->statArticles($this->articleFT);
        } else {
            $this->ClassIDActive = "All";

            $this->articleFT = Rawmatarticle::with('event','service','article','rawmaterialprovider',
            'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial','articleClass')
            ->groupBy('RawMatArticleCode')
            ->where('RawMatArticleArchived',false)
            ->where('EventEventID',null)
            ->get();

            $this->articleAR = Rawmatarticle::with('event','service','article','rawmaterialprovider',
            'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial','articleClass')
            ->groupBy('RawMatArticleCode')
            ->where('RawMatArticleArchived',true)
            ->where('EventEventID',null)
            ->get();

            // dd($this->articleAR);

            $this->RawMatAvg = Rawmatarticle::with('event','service','article','rawmaterialprovider',
            'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial','rawmaterialprovider.rawmaterial.rawmatfamily','articleClass')
            ->where('EventEventID',null)
            ->where('RawMatArticleArchived',false)
            ->get();

            $this->avgFamilyGen = app('App\Http\Controllers\ArticlesRawmatController')->avgFamily($this->RawMatAvg);
            $this->statArticlesGen = app('App\Http\Controllers\ArticlesRawmatController')->statArticles($this->RawMatAvg,$this->articleFT);
        }


    }

    public function loadData()
    {
        // sleep(5);
        $this->articleFT = Rawmatarticle::with('event','service','article','rawmaterialprovider',
        'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial','articleClass')
        ->groupBy('RawMatArticleCode')
        ->where('RawMatArticleArchived',false)
        ->where('EventEventID',null)
        ->get();

        $this->articleAR = Rawmatarticle::with('event','service','article','rawmaterialprovider',
        'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial','articleClass')
        ->groupBy('RawMatArticleCode')
        ->where('RawMatArticleArchived',true)
        ->where('EventEventID',null)
        ->get();

        $this->RawMatAvg = Rawmatarticle::with('event','service','article','rawmaterialprovider',
        'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial','rawmaterialprovider.rawmaterial.rawmatfamily','articleClass')
        ->where('EventEventID',null)
        ->where('RawMatArticleArchived',false)
        ->get();

        $this->avgFamilyGen = app('App\Http\Controllers\ArticlesRawmatController')->avgFamily($this->RawMatAvg);
        $this->statArticlesGen = app('App\Http\Controllers\ArticlesRawmatController')->statArticles($this->RawMatAvg,$this->articleFT);
    }

    public function render()
    {
        return view('livewire.articles-rawmat.articles-rawmat-list-class',[
            'articleClasses' => ArticleClass::get()
        ]);
    }
}
