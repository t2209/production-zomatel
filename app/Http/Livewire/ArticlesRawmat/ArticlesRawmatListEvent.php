<?php

namespace App\Http\Livewire\ArticlesRawmat;

use Livewire\Component;
use App\Models\Rawmatarticle;

class ArticlesRawmatListEvent extends Component
{
    public array $avgFamilyGen;
    public array $statArticlesGen;

    public function mount()
    {
        $RawMat = Rawmatarticle::with('event','service','article','rawmaterialprovider',
        'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial','rawmaterialprovider.rawmaterial.rawmatfamily','articleClass')
        ->where('EventEventID','!=',null)
        ->where('RawMatArticleArchived',false)
        ->get();

        $this->avgFamilyGen = app('App\Http\Controllers\ArticlesRawmatController')->avgFamily($RawMat);
        $this->statArticlesGen = app('App\Http\Controllers\ArticlesRawmatController')->statArticles($RawMat,$RawMat);
    }

    public function render()
    {
        return view('livewire.articles-rawmat.articles-rawmat-list-event',[
            'articleFT' => Rawmatarticle::with('event','service','article','rawmaterialprovider',
            'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial','articleClass')
            ->groupBy('RawMatArticleCode')
            ->where('EventEventID','!=',null)
            ->where('RawMatArticleArchived',false)
            ->get()
        ]);
    }
}
