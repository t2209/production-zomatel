<?php

namespace App\Http\Livewire\ArticlesRawmat;

use App\Http\Controllers\ArticlesRawmatController;
use App\Models\Articleservice;
use Livewire\Component;
use App\Models\Rawmatarticle;
use App\Models\Service;
use App\Providers\ArticleServiceProvider;
use App\Providers\RawMatServiceProvider;

class ArticlesRawmatListService extends Component
{
    public array $avgFamilyGen;
    public array $statArticlesGen;
    public $RawmatarticlesAvg;
    public $Rawmatarticles;
    public $ServiceIDAvg = null;
    public $ServiceIDActive = "*";

    // vas for PO
    public $ServicesRwmat;

    protected $queryString = [
        'ServiceIDActive' => ['except' => "*"],
    ];


    public function getArticles($serviceID, $eventID){
        // return Articleservice::
        // with(['article.rawmatarticles.rawmaterial','article.articleclass','service'])
        // ->when($serviceID != '*', function($query) use ($serviceID) {
        //     return $query->where('ServiceServiceID', '=', $serviceID);
        //   })
        //   ->get();

        return  Rawmatarticle::with('event.articles.articleclass','service.articles.articleclass',
            'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial.rawmatfamily','article.articleclass')
            // ->groupBy('RawMatArticleCode')
            // ->where('article.ArticleIsArchived',false)
            ->with(["article" => function($q){
                $q->where('article.ArticleIsArchived', false);
            }])
            // ->where('ServiceServiceID',$serviceID)
            ->when($serviceID != '*', function($query) use ($serviceID) {
                return $query->where('ServiceServiceID', '=', $serviceID);
              })
            ->when($eventID != '*', function($query) use ($eventID) {
                return $query->where('EventEventID', '=', $eventID);
            })
            // ->where('EventEventID',$eventID)
            ->get();
            // ->unique('ArticleArticleID');

    }
    public function setServiceFilter($ServiceID,$filter)
    {
        $articleRawmatController = new ArticlesRawmatController;

        if ($filter == "perClass") {
            // $this->Rawmatarticles = Rawmatarticle::
            // ->with('event','service.articles','article.boxes','rawmaterialprovider',
            // 'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial','articleClass')
            // ->join('article')
            // with('service')
            // ->groupBy('RawMatArticleCode')
            // // ->where('RawMatArticleArchived',false)
            // ->where('ServiceServiceID',$ServiceID)
            // ->orWhere('ServiceServiceID',null)
            // ->where('EventEventID',null)
            // ->get()
            // ->unique('ArticleArticleID');
            // // dd($this->Rawmatarticles);
            $this->Rawmatarticles=ArticleServiceProvider::getArticles($ServiceID,null,'*');
            // $this->RawmatarticlesAvg=RawMatServiceProvider::getRawMats('*',$ServiceID,null);

            // $this->RawmatarticlesAvg = Rawmatarticle::with('event','service','article','rawmaterialprovider',
            // 'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial','rawmaterialprovider.rawmaterial.rawmatfamily','articleClass')
            // ->where('ServiceServiceID',$ServiceID)
            // ->where('RawMatArticleArchived',false)
            // ->orWhere('ServiceServiceID',null)
            // ->where('EventEventID',null)
            // ->get();

            // $articleRawmatController = new ArticlesRawmatController;

            // $this->avgFamilyGen = $articleRawmatController->avgFamily($this->RawmatarticlesAvg);

            $this->statArticlesGen =$articleRawmatController->statArticles($this->Rawmatarticles);
            // dd($this->avgFamilyGen);
            // dd($this->Rawmatarticles);


            $this->ServiceIDActive = $ServiceID;
        } else {
            // $this->RawmatarticlesAvg = Rawmatarticle::with('event','service.articles','article','rawmaterialprovider',
            // 'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial','rawmaterialprovider.rawmaterial.rawmatfamily')
            // ->where('RawMatArticleArchived',false)
            // ->where('ServiceServiceID',"!=",null)
            // ->orWhere('ServiceServiceID',null)
            // ->where('EventEventID',null)
            // ->get();

            // $this->Rawmatarticles = Rawmatarticle::with('event','service','article','rawmaterialprovider',
            // 'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial')
            // ->groupBy('RawMatArticleCode')
            // ->where('RawMatArticleArchived',false)
            // ->where('ServiceServiceID',"!=",null)
            // ->orWhere('ServiceServiceID',null)
            // ->where('EventEventID',null)
            // ->get();
            $this->Rawmatarticles=ArticleServiceProvider::getArticles('*',null,'*');
            // dd($this->Rawmatarticles);

            // $this->avgFamilyGen = $articleRawmatController->avgFamily($this->RawmatarticlesAvg);
            $this->statArticlesGen = $articleRawmatController->statArticles($this->Rawmatarticles);

            $this->ServiceIDActive = "*";
        }

    }


    public function mount()
    {
        $articleRawmatController = new ArticlesRawmatController;

        // dd($this->getArticles('*',null));
        // $this->Rawmatarticles=$this->getArticles('*',null);
        $this->Rawmatarticles=ArticleServiceProvider::getArticles('*',null,'*');
        // dd($this->Rawmatarticles);
        // $this->RawmatarticlesAvg = Rawmatarticle::with('event','service','article','rawmaterialprovider',
        // 'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial','rawmaterialprovider.rawmaterial.rawmatfamily')
        // // ->where('RawMatArticleArchived',false)
        // ->with(["article" => function($q){
        //     $q->where('article.ArticleIsArchived', false);
        // }])
        // // ->where('article.ArticleIsArchived',false)
        // ->where('ServiceServiceID',"!=",null)
        // ->orWhere('ServiceServiceID',null)
        // ->where('EventEventID',null)
        // ->get();
        // $this->Rawmatarticles= $this->getArticles()
        // $this->Rawmatarticles = Rawmatarticle::with('event','service.articles','article','rawmaterialprovider',
        // 'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial')
        // ->groupBy('RawMatArticleCode')
        // // ->where('article.ArticleIsArchived',false)
        // ->with(["article" => function($q){
        //     $q->where('article.ArticleIsArchived', false);
        // }])
        // ->where('ServiceServiceID',"!=",null)
        // ->orWhere('ServiceServiceID',null)
        // ->where('EventEventID',null)
        // ->get();

        // $this->avgFamilyGen = $articleRawmatController->avgFamily($this->RawmatarticlesAvg);
        // $this->statArticlesGen = $articleRawmatController->statOfArticles($this->Rawmatarticles);
    }

    // Purchase Order
    public function getServiceRwmat()
    {
        if ($this->ServiceIDActive != "All") {
            $this->ServicesRwmat = Rawmatarticle::with('event','service','article','rawmaterialprovider',
            'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial','articleClass')
            // ->where('RawMatArticleArchived',false)
            ->with(["article" => function($q){
                $q->where('article.ArticleIsArchived', false);
            }])
            ->where('ServiceServiceID',$this->ServiceIDActive)
            ->orWhere('ServiceServiceID',null)
            ->where('EventEventID',null)
            ->get();
            // dd('ok');

            $arrayRawMat = [];
            foreach ($this->ServicesRwmat as $key => $value) {
                $arrayRawMat[$key]['Id'] = $value->rawmaterialprovider->rawmaterial->RawMaterialID;
                $arrayRawMat[$key]['Wording'] = $value->rawmaterialprovider->rawmaterial->RawMaterialName;
            }

            $this->ServicesRwmat = array_map("unserialize", array_unique(array_map("serialize", $arrayRawMat)));


            // dd($arrayRawMat);
        } else {
            dd("Get all");
        }

    }

    public function render()
    {
        return view('livewire.articles-rawmat.articles-rawmat-list-service',[
            'articleFT' => ArticleServiceProvider::getArticles($this->ServiceIDActive,null,'*'),
            'services' => Service::get()
        ]);
    }
}
