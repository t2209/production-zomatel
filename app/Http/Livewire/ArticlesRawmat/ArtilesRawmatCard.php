<?php

namespace App\Http\Livewire\ArticlesRawmat;

use App\Models\Article;
use App\Models\Articleservice;
use App\Models\Eventarticle;
use App\Models\Rawmatarticle;
use App\Models\Rawmaterial;
use App\Providers\ArticleServiceProvider;
use App\Providers\RawMatServiceProvider;
use Livewire\Component;

class ArtilesRawmatCard extends Component
{
    public $item;
    public $counterStock;
    public $getCountStock;
    public $counterStockEvent;
    public $getCountStockEvent;
    public $RawMatCode;
    public $RawMat;
    public $arrayFt;
    public $arraySum;
    public $arrayFamilyAvg;
    public $arrayFamilyAvgFinal;
    public $arraySumAvg;
    public $arrayFamilyPerCent;
    public $ArticleName;
    // for update
    public $rawmatProdIdNotChange;
    public $rawMatUnity;
    public $arrayRawmats;
    public $CodeDlt;
    public $IdForUp;
    public $RawmatName;
    public $Rawmatproviders;
    public $RawmatproviderId;
    public $RawmatproviderQte;
    // for add
    public $CheckAdd;
    public $RawmatproviderIdAdd , $RawmatproviderQteAdd;
    // public float $priceTTC= 0;

    // !code = articleServiceID || eventArticleID
    public function getRawMats($code){
        if (getClassInstance($this->item) === Articleservice::class){
            return  RawMatServiceProvider::getRawMats($this->item['article']['ArticleID'],$code,null);
        }else if(getClassInstance($this->item) === Eventarticle::class){
            return  RawMatServiceProvider::getRawMats($this->item['article']['ArticleID'],null,$code);
        }
    }

    public function getRawMatArt($code)
    {
        // dd($code);
        $this->RawMat = $this->getRawMats($code);
        // dd($this->RawMat);
        // $this->RawMatCode = $code;
        // $this->RawMat = Rawmatarticle::with('event','service','article','rawmaterialprovider',
        //     'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial','rawmaterialprovider.rawmaterial.rawmatfamily')
        //     ->where('RawMatArticleCode',$this->RawMatCode)
        //     ->where('RawMatArticleArchived',false)
        //     ->get();

        // dd($this->RawMat);

        // set array for ft
        $this->arrayFt = [];
        foreach ($this->RawMat as $key => $value) {
            $this->arrayFt[$key]["ID"] = Rawmaterial::with('rawmatproviders')->where('RawMaterialID',$value->rawmaterialprovider->RawMaterialRawMaterialID)->get();
            $this->arrayFt[$key]["RawMatID"] = $value->RawMatArticleID;
            $this->arrayFt[$key]["Produit"] = $value->rawmaterialprovider->rawmaterial->RawMaterialName;
            $this->arrayFt[$key]["Quantité"] = $value->RawMatArticleQty;
            $this->arrayFt[$key]["Unité"] = $value->rawmaterialprovider->rawmaterial->RawMaterialUnity;

            // calcul prix de revient
            $prixAchat = $this->arrayFt[$key]["ID"][0]->rawmatproviders[0]->RawMatProviderUnitPrice;
            // $prixAchat = $value->rawmaterialprovider->RawMatProviderUnitPrice;

            $this->arrayFt[$key]["PrixAchat"] = number_format(floatval($prixAchat), 2, '.', ' ') . " Ar";

            $prixRevient = $prixAchat * $value->RawMatArticleQty;

            $this->arrayFt[$key]["PrixRevient"] = number_format(floatval($prixRevient), 2, '.', ' ') . " Ar";
            $this->arrayFt[$key]["PrixRevientCalcul"] = $prixRevient;
            $this->arrayFt[$key]["Family"] = $value->rawmaterialprovider->rawmaterial->rawmatfamily->RawMatWording;

        }
        // dd($this->arrayFt);


        $this->arraySum = [];
        $this->arraySum["SumPrixRevient"] = array_sum(array_column($this->arrayFt,'PrixRevientCalcul'));
        // dd( $this->arraySum["SumPrixRevient"]);
        foreach ($this->arrayFt as $key => $rawMat) {
            $this->arrayFt[$key]["SumPrixRevient"] = $this->arraySum["SumPrixRevient"];
            $this->arrayFt[$key]["Ratio"] = (floatval($this->arrayFt[$key]["PrixRevientCalcul"]) * 100) / floatval($this->arraySum["SumPrixRevient"]);
        }
        // dd($this->arrayFt);


        // get vars

        $PrixVenteTTC = floatval(getPrice($this->item));
        $PrixVenteHT = $PrixVenteTTC * (100/120);
        // dd($PrixVenteHT);
        // foreach ($this->RawMat as $key => $value) {
            // // $this->ArticleName = $value->article->ArticleName;
            // $PrixVenteTTC = floatval($value->ArticlePriceTTC);
            // $PrixVenteHT = $PrixVenteTTC * (100/120);

            $this->arraySum["PrixVenteTTC"] = number_format(floatval($PrixVenteTTC), 2, '.', ' ') . " Ar";
            $this->arraySum["PrixVenteHT"] = number_format(floatval($PrixVenteHT), 2, '.', ' ') . " Ar";

            $Coeff = floatval($PrixVenteHT) / floatval($this->arraySum["SumPrixRevient"]);
            $CoutMatc = floatval($this->arraySum["SumPrixRevient"]) / floatval($PrixVenteHT) * 100;

            $this->arraySum["Coeff"] = number_format(floatval($Coeff), 2, '.', '');
            $this->arraySum["CoutMat"] = number_format(floatval($CoutMatc), 2, '.', '');;
        // }

        // set family percent
        $this->arrayFamilyPerCent = [];
        foreach ($this->RawMat as $key => $value) {
            $this->arrayFt[$key]["ID"] = Rawmaterial::with('rawmatproviders')->where('RawMaterialID',$value->rawmaterialprovider->RawMaterialRawMaterialID)->get();
            $this->arrayFamilyPerCent[$key]["Family"] = $value->rawmaterialprovider->rawmaterial->rawmatfamily->RawMatWording;
            // calcul prix de revient
            $prixAchat = $this->arrayFt[$key]["ID"][0]->rawmatproviders[0]->RawMatProviderUnitPrice;
            $prixRevient = floatval($prixAchat) * floatval($value->RawMatArticleQty);

            $this->arrayFamilyPerCent[$key]["Count"] = $prixRevient;
            // $this->arrayFamilyPerCent[$key]["PA"] = $prixAchat;
            // $this->arrayFamilyPerCent[$key]["Qte"] = floatval($value->RawMatArticleQty);
        }

        // dd($this->arrayFamilyPerCent);

        $arrayTemp = [];


        foreach ($this->arrayFamilyPerCent as $key => $rawMat) {
            $arrayTemp[$key]["Family"] = $this->arrayFamilyPerCent[$key]["Family"];
            $arrayTemp[$key]["Count"] = $this->arrayFamilyPerCent[$key]["Count"];
        }

        // dd($arrayTemp);

        $total = array_sum(array_column($arrayTemp,'Count'));

        // dd($total);

        foreach ($arrayTemp as $key => $value) {
            $arrayTemp[$key]["Total"] = $total;
            $arrayTemp[$key]["Avg"] = round($arrayTemp[$key]["Count"] * 100 / $total, 2);
        }

        $this->arrayFamilyAvgFinal = $arrayTemp;

        // avg total
        $this->arraySumAvg = array_reduce(
            $this->arrayFamilyAvgFinal,
            function (array $carry, array $item) {
                $city = $item['Family'];
                if (array_key_exists($city, $carry)) {
                    $carry[$city]['Avg'] += $item['Avg'];
                } else {
                    $carry[$city] = $item;
                }
                return $carry;
            },
            array()
        );
        $this->arraySumAvg = array_values( $this->arraySumAvg);


        // dd( $this->arraySumAvg);
    }

    public function deleteArticle($code,$id,$type)
    {
        // dd($type);
        // dd($this->getRawMats($code));
        // delete rawmatarticle
        foreach ($this->getRawMats($code) as $key => $value) {
            Rawmatarticle::destroy($value->RawMatArticleID);
        }
        // delete articles service or event
        if ($type == "Service") {
            Articleservice::destroy($id);
        }else {
            Eventarticle::destroy($id);
        }
        $this->emit('refreshArticleFTList');


    }

    // archived
    public function setArchived($ArticleID,$type)
    {
        // dd(Article::find($code));
        try {
            Article::find($ArticleID)->update(['ArticleIsArchived' => $type]);
            $this->emit('refreshArticleFTList');
        } catch (\Throwable $th) {
            throw $th;
        }
    }


    // update
    public function setDataUp($code)
    {
        // dd($code);
        $this->CodeDlt = $code;
        // $Rawmats = Rawmatarticle::with('event','service','article','rawmaterialprovider',
        // 'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial','articleClass')
        //                         ->where("RawMatArticleCode",$code)
        //                         ->get();

        $this->arrayRawmats = [];
        $Rawmats = $this->getRawMats($code);
        // dd($Rawmats);

        foreach ($Rawmats as $key => $value) {
            // $LatestPrice = Rawmaterial::with('rawmatproviders')->where('RawMaterialID',$value->rawmaterialprovider->RawMaterialRawMaterialID)->get();
            $this->arrayRawmats[$key]["Id"] = $value->RawMatArticleID;
            $this->arrayRawmats[$key]["RawmatId"] = $value->RawMaterialRawMaterialID;
            $this->arrayRawmats[$key]["Rawmat"] = $value->rawmaterialprovider->rawmaterial->RawMaterialName;
            $this->arrayRawmats[$key]["Qte"] = $value->RawMatArticleQty;
            $this->arrayRawmats[$key]["Unity"] = $value->rawmaterialprovider->rawmaterial->RawMaterialUnity;
            $this->arrayRawmats[$key]["RawmatPrice"] =$value->rawmaterialprovider->RawMatProviderUnitPrice;
        }

        // dd($this->arrayRawmats);

        // set rawmatprovider data

        $this->Rawmatproviders = Rawmaterial::with('rawmatproviders','rawmatproviders.provider')
        ->orderBy('RawMaterialName','ASC')
        ->get();

    }

    public function refershDataUp()
    {
        // dd('ok');
        $Rawmats = $this->getRawMats($this->CodeDlt);
        //  Rawmatarticle::with('event','service','article','rawmaterialprovider',
        // 'rawmaterialprovider.provider','rawmaterialprovider.rawmaterial','articleClass')
        //                         ->where("RawMatArticleCode",$this->CodeDlt)
        //                         ->get();

        $this->arrayRawmats = [];

        foreach ($Rawmats as $key => $value) {
            $LatestPrice = Rawmaterial::with('rawmatproviders')->where('RawMaterialID',$value->rawmaterialprovider->RawMaterialRawMaterialID)->get();
            $this->arrayRawmats[$key]["Id"] = $value->RawMatArticleID;
            $this->arrayRawmats[$key]["RawmatId"] = $value->RawMaterialRawMaterialID;
            $this->arrayRawmats[$key]["Rawmat"] = $value->rawmaterialprovider->rawmaterial->RawMaterialName;
            $this->arrayRawmats[$key]["Qte"] = $value->RawMatArticleQty;
            $this->arrayRawmats[$key]["RawmatPrice"] = $LatestPrice[0]->rawmatproviders[0]->RawMatProviderUnitPrice;
        }
    }

    // delete ft rawmat
    public function delete($Id)
    {
        // dd($Id);
        Rawmatarticle::destroy($Id);
        $this->refershDataUp();
        $this->emit('refreshArticleFTList');
        $this->emit('refreshArticleCard');
    }

    // set up form
    public function setFormUp($Id,$Qte,$Name,$Unity)
    {
        // dd("RawMatProdID",$Id);
        $this->IdForUp = $Id;
        $this->RawmatproviderQte = $Qte;
        $this->RawmatName = $Name;
        $this->rawMatUnity = $Unity;
    }

    public function unsetFormUp()
    {
        unset($this->IdForUp);
    }

    public function updateData()
    {
        // dd($this->RawmatproviderId);
        Rawmatarticle::where("RawMatArticleID",$this->IdForUp)->update([
            "RawMaterialRawMaterialID" => $this->RawmatproviderId,
            "RawMatArticleQty" => $this->RawmatproviderQte
        ]);
        $this->refershDataUp();
        $this->emit('refreshArticleFTList');
        $this->emit('refreshArticleCard');
    }

    // add new rawmat
    public function setFormAdd()
    {
        $this->CheckAdd = "true";
        $this->unsetFormUp();
    }

    public function unsetFormAdd()
    {
        unset($this->CheckAdd);
        $this->unsetFormUp();
        $this->RawmatproviderQteAdd = null;
        $this->RawmatproviderIdAdd = null;
    }

    public function addData()
    {
        // dd($this->item);
        // dd($this->RawmatproviderIdAdd,$this->RawmatproviderQteAdd);
        try {
            $Newrawmatarticle = new Rawmatarticle;
            $Newrawmatarticle->EventEventID = (array_key_exists('event',$this->item)) ? $this->item["event"]["EventID"] : null ;
            $Newrawmatarticle->ServiceServiceID = (array_key_exists('service',$this->item)) ? $this->item["service"]["ServiceID"] : null ;
            $Newrawmatarticle->RawMaterialRawMaterialID = $this->RawmatproviderIdAdd;
            $Newrawmatarticle->RawMatArticleQty = $this->RawmatproviderQteAdd;
            $Newrawmatarticle->ArticleArticleID = $this->item["ArticleArticleID"];

            $Newrawmatarticle->save();

            $this->unsetFormAdd();
            $this->refershDataUp();
            $this->emit('refreshArticleFTList');
            $this->emit('refreshArticleCard');

        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function render()
    {

        return   view('livewire.articles-rawmat.artiles-rawmat-card');
    }
}
