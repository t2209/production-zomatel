<?php

namespace App\Http\Livewire\Caisse;

use App\Models\Fund;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class CaisseForm extends Component
{
    public $modelID;
    public $FundType;
    public $Fund;
    public $Observation;
    protected $listeners = [
        'getSelectedFund'
    ];

    public function clearVars(){
        $this->FundType = 'selected';
        $this->Fund = null;
        $this->Observation = null;
    }

    public function getSelectedFund($FundID)
    {
        $this->modelID = $FundID;
        $Fund = Fund::find($this->modelID);
        if ($Fund->Entry == 0) {
            $this->Fund = $Fund->Outflow;
            $this->FundType = 'Outflow';
        } else {
            $this->Fund = $Fund->Entry;
            $this->FundType = 'Entry';
        }
        $this->Observation = $Fund->Observation;
    }

    public function save(){
        $validateData = [
            'Fund' => 'required|numeric',
            'FundType' => 'required|different:selected',
            'Observation' => 'required',
        ];
        $this->validate($validateData);
        // dd($this->FundType);

        $Fund = new Fund;
        if ($this->modelID) {

            DB::transaction(function () use ($Fund) {

                switch ($this->FundType) {
                    case 'Entry':
                        $Fund->where("FundID",$this->modelID)
                            ->update([
                            'Entry' => $this->Fund,
                            'Outflow' => 0,
                            'Observation' => $this->Observation,
                            ]);
                        break;
                    case 'Outflow':
                        $Fund->where("FundID",$this->modelID)
                            ->update([
                            'Outflow' => $this->Fund,
                            'Entry' => 0,
                            'Observation' => $this->Observation,
                            ]);
                        break;    
                    case 'selected':
                        break;    
                    default:
                        break;
                }
            });

        } else {

            switch ($this->FundType) {
                case 'Entry':
                    $Fund->Entry = $this->Fund;
                    $Fund->Outflow = 0;
                    break;
                case 'Outflow':
                    $Fund->Entry = 0;
                    $Fund->Outflow = $this->Fund;
                    break;    
                case 'selected':
                    break;    
                default:
                    break;
            }
            $Fund->Observation = $this->Observation;
            DB::transaction(function () use ($Fund) {
                $Fund->save();
            });
        }


        $this->clearVars();
        $this->dispatchBrowserEvent('closeTransactionModal');
        $this->emit("refreshFundList");
    }

    public function render()
    {
        return view('livewire.caisse.caisse-form');
    }
}
