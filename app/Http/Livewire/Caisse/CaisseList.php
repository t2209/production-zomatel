<?php

namespace App\Http\Livewire\Caisse;

use App\Models\Fund;
use Livewire\Component;

class CaisseList extends Component
{
    public $selectID;
    public $listeners = [
        'refreshFundList' => '$refresh'
    ];

    public function selectFund($FundID , $action)
    {
        $this->selectID = $FundID;
        switch ($action) {
            case 'update':
                $this->emit('getSelectedFund' , $this->selectID);
                $this->dispatchBrowserEvent('openTransactionModal');
                break;
            case 'delete':
                $this->dispatchBrowserEvent('openFundDeleteModal');
                break;
            default:
                # code...
                break;
        }
    }

    public function deleteFund()
    {
        Fund::destroy($this->selectID);
        $this->dispatchBrowserEvent('closeFundDeleteModal');
    }

    public function render()
    {
        return view('livewire.caisse.caisse-list', [
            'fund' => Fund::with('Provider')->get()
        ]);
    }
}
