<?php

namespace App\Http\Livewire\Freezer;

use App\Models\Freezer;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class FreezerForm extends Component
{
    public $modelID;
    public $FreezerCapacity;
    public $FreezerNum;
    protected $listeners = [
        'getSelectedFreezer'
    ];

    public function getSelectedFreezer($ID)
    {
        $this->modelID = $ID;

        $Freezer = Freezer::find($this->modelID);

        $this->FreezerCapacity = $Freezer->FreezerCapacity;
        $this->FreezerNum = $Freezer->FreezerNum;
    }

    public function clearVars()
    {
        $this->FreezerNum = null;
        $this->FreezerCapacity = null;
    }

    public function save()
    {
        $validateData = [
            'FreezerNum' => 'required',
            'FreezerCapacity' => 'required'
        ];
        $this->validate($validateData);

        // dd($this->modelID);

        $Freezer = new Freezer;
        if ($this->modelID != null) {
            DB::transaction(function () use ($Freezer) {
                $Freezer->where("FreezerID",$this->modelID)
                ->update([
                    'FreezerNum' => $this->FreezerNum,
                    'FreezerCapacity' => $this->FreezerCapacity,
                ]);
            });
        }else {
            $Freezer->FreezerNum = $this->FreezerNum;
            $Freezer->FreezerCapacity = $this->FreezerCapacity;
            DB::transaction(function () use ($Freezer) {
                $Freezer->save();
            });
        }
        $this->clearVars();
        $this->dispatchBrowserEvent('closeModal');
        $this->emit("refreshFreezerList");
    }

    public function render()
    {
        return view('livewire.freezer.freezer-form');
    }
}
