<?php

namespace App\Http\Livewire\Freezer;

use App\Models\Freezer;
use Livewire\Component;

class FreezerList extends Component
{
    public $selectID;
    public $listeners = [
        'refreshFreezerList' => '$refresh'
    ];

    public function selectFreezer($ID , $action)
    {
        $this->selectID = $ID;
        if ($action == "update") {
            $this->emit('getSelectedFreezer' , $this->selectID);
            $this->dispatchBrowserEvent('openModal');
        }else {
            $this->dispatchBrowserEvent('openDeleteModal');
        }
    }

    public function deleteFreeze()
    {
        Freezer::destroy($this->selectID);
        $this->dispatchBrowserEvent('closeDeleteModal');
    }

    public function render()
    {
        return view('livewire.freezer.freezer-list', [
            'freezers' => Freezer::get()
        ]);
    }
}
