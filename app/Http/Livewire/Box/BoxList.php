<?php

namespace App\Http\Livewire\Box;

use App\Models\Box;
use Livewire\Component;

class BoxList extends Component
{
    public $selectID;
    public $listeners = [
        'refreshBoxList' => '$refresh'
    ];

    public function selectBox($ID , $action)
    {
        $this->selectID = $ID;

        if ($action == "update") {
            $this->emit('getSelectedBox' , $this->selectID);
            $this->dispatchBrowserEvent('openBoxModal');
        }else {
            $this->dispatchBrowserEvent('openBoxDeleteModal');
        }
    }

    public function deleteBox()
    {
        Box::destroy($this->selectID);
        $this->dispatchBrowserEvent('closeBoxDeleteModal');
    }

    public function render()
    {
        return view('livewire.box.box-list' , [
            'boxes' => Box::
            with('freezer')
            ->get()
        ]);
    }
}
