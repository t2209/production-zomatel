<?php

namespace App\Http\Livewire\Box;

use App\Models\Box;
use App\Models\Freezer;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class BoxForm extends Component
{
    public $modelID;
    public $BoxNum;
    public $FreezerFreezerID;
    protected $listeners = [
        'getSelectedBox'
    ];

    public function getSelectedBox($ID)
    {
        $this->modelID = $ID;

        $Box = Box::find($this->modelID);

        $this->BoxNum = $Box->BoxNum;
        $this->FreezerFreezerID = $Box->FreezerFreezerID;
    }

    public function clearVars()
    {
        $this->BoxNum = null;
        $this->FreezerFreezerID = null;
    }

    public function save()
    {
        $validateData = [
            'BoxNum' => 'required|numeric',
            'FreezerFreezerID' => 'required'
        ];
        $this->validate($validateData);

        $Box = new Box;
        if ($this->modelID) {
            DB::transaction(function () use ($Box) {
                $Box->where("BoxID",$this->modelID)
                ->update([
                    'BoxNum' => $this->BoxNum,
                    'FreezerFreezerID' => $this->FreezerFreezerID,
                ]);
            });
        } else {
            $Box->BoxNum = $this->BoxNum;
            $Box->FreezerFreezerID = $this->FreezerFreezerID;

            DB::transaction(function () use ($Box) {
                $Box->save();
            });
        }

        $this->clearVars();
        $this->dispatchBrowserEvent('closeBoxModal');
        $this->emit('refreshBoxList');
    }

    public function render()
    {
        return view('livewire.box.box-form',[
            'freezers' => Freezer::get()
        ]);
    }
}
