<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Grandbookprovider
 * 
 * @property int $GrandBookProviderID
 * @property float $GrandBookProviderDebit
 * @property float $GrandBookProviderCredit
 * @property float $GrandBookProviderReport
 * @property float $GrandBookProviderSold
 * 
 * @property Collection|Purchaseorderprovider[] $purchaseorderproviders
 *
 * @package App\Models
 */
class Grandbookprovider extends Model
{
	protected $table = 'grandbookprovider';
	protected $primaryKey = 'GrandBookProviderID';
	public $timestamps = false;

	protected $casts = [
		'GrandBookProviderDebit' => 'float',
		'GrandBookProviderCredit' => 'float',
		'GrandBookProviderReport' => 'float',
		'GrandBookProviderSold' => 'float'
	];

	protected $fillable = [
		'GrandBookProviderDebit',
		'GrandBookProviderCredit',
		'GrandBookProviderReport',
		'GrandBookProviderSold'
	];

	public function purchaseorderproviders()
	{
		return $this->hasMany(Purchaseorderprovider::class, 'GrandBookProviderGrandBookProviderID');
	}
}
