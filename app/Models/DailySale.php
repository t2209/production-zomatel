<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DailySale
 * 
 * @property int $DailySaleID
 * @property Carbon|null $Date
 * @property float $Value
 *
 * @package App\Models
 */

class DailySale extends Model
{
    protected $table = 'daily_sales';
	protected $primaryKey = 'DailySaleID';
	public $timestamps = true;

    protected $fillable = [
		'Date',
		'Value'
	];

    protected $dates = [
		'Date'
	];
}
