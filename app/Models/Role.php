<?php

/**
 * Created by Reliese Model.
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Role
 *
 * @property int $RoleID
 *
 * @property string $RoleName
 *
 * @package App\Models
 */
class Role extends Model
{
    use HasFactory;

	protected $table = 'role';
	protected $primaryKey = 'RoleID';
	public $timestamps = true;

	protected $fillable = [
		'RoleName'
	];

    public function user()
	{
		return $this->belongsTo(User::class, 'RoleRoleID');
	}
}
