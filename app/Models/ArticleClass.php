<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Service
 *
 * @property int $ArticleClassID
 * @property string|null $ArticleClassWording
 *
 * @property Collection|Article[] $articles
 *
 * @package App\Models
 */
class ArticleClass extends Model
{
	protected $table = 'articleclass';
	protected $primaryKey = 'ArticleClassID';
	public $timestamps = false;

	protected $fillable = [
		'ArticleClassWording'
	];


	public function articles()
	{
		return $this->hasMany(Article::class, 'ArticleClassID');
	}

}
