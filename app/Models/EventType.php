<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;


/**
 * Class EventType
 *
 * @property int $EventTypeEventTypeID
 * @property string|null $EventTypeName
 *
 * @property Collection|Event[] $events
 *
 * @package App\Models
 */
class EventType extends Model
{
    use HasFactory;
    protected $table = 'eventtype';
    protected $primaryKey = 'EventTypeID';

    protected $fillable = [
		'EventTypeName'
	];

    public function events()
	{
		return $this->hasMany(Event::class, 'EventTypeID');
	}

}
