<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Freezer
 *
 * @property int $PaymentMethID
 * @property string $PaymentMethWording
 *
 * @property Collection|Rawmatrequire[] $rawmatrequires
 *
 * @package App\Models
 */

class PaymentMethod extends Model
{
    protected $table = 'payment_methods';
	protected $primaryKey = 'PaymentMethID';
	public $timestamps = true;

	protected $fillable = [
		'PaymentMethWording'
	];

    public function rawmatrequire()
    {
        return $this->hasOne(Rawmatrequire::class, 'PaymentMethID');
    }
}
