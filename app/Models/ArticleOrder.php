<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ArticleOrder
 *
 * @property int $ArticleOrderID
 * @property int $ArticleServiceArticleServiceID
 * @property int $OrderOrderID
 * @property int $ArticleOrderQty
 *
 *
 * @property Order $order
 * @property ArticleService $articleService
 *
 *
 * @package App\Models
 */
class ArticleOrder extends Model
{
    use HasFactory;
    protected $table = 'articleorder';
	protected $primaryKey = 'ArticleOrderID';
	public $timestamps = true;

    protected $casts =[
        'ArticleServiceArticleServiceID' => 'int',
        'OrderOrderID' => 'int',
        'ArticleOrderQty' => 'int'
    ];

    protected $fillable =[
        'ArticleServiceArticleServiceID',
        'OrderOrderID',
        'ArticleOrderQty'

    ];

    public function order()
	{
		return $this->belongsTo(Order::class, 'OrderOrderID');
	}

    public function articleService()
	{
		return $this->belongsTo(Articleservice::class, 'ArticleServiceArticleServiceID');
	}

}
