<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Provider
 *
 * @property int $ProviderID
 * @property string|null $ProviderCode
 * @property string|null $ProviderName
 * @property string|null $ProviderAdress
 * @property string|null $ProviderPhone
 *
 * @property Collection|Rawmatprovider[] $rawmatproviders
 * @property Collection|Fund[] $rawmatproviders
 *
 *
 * @package App\Models
 */
class Provider extends Model
{
	protected $table = 'provider';
	protected $primaryKey = 'ProviderID';
	public $timestamps = false;

	protected $fillable = [
		'ProviderCode',
		'ProviderName',
		'ProviderAdress',
		'ProviderPhone'
	];

	public function rawmatproviders()
	{
		return $this->hasMany(Rawmatprovider::class, 'ProviderProviderID');
	}

    public function funds()
	{
		return $this->hasMany(Fund::class, 'ProviderProviderID');
	}
}
