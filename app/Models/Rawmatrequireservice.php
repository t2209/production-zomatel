<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Rawmatrequireservice
 *
 * @property int $RawMatRequireServiceID
 * @property int|null $RawMaterialRawMaterialID
 * @property int $ServiceServiceID
 * @property float $RawMatRequireServiceQtyNeeded
 * @property Carbon $RawMatRequireServiceDate
 * @property Carbon $RawMatRequireServiceDateConfirm
 *
 * @property Service $service
 * @property Rawmaterial|null $rawmaterial
 *
 * @package App\Models
 */
class Rawmatrequireservice extends Model
{
	protected $table = 'rawmatrequireservice';
	protected $primaryKey = 'RawMatRequireServiceID';
	public $timestamps = true;

	protected $casts = [
		'RawMaterialRawMaterialID' => 'int',
		'ServiceServiceID' => 'int',
		'RawMatRequireServiceQtyNeeded' => 'float',
	];

	protected $dates = [
		'RawMatRequireServiceDate',
		'RawMatRequireServiceDateConfirm'
	];

	protected $fillable = [
		'RawMaterialRawMaterialID',
		'ServiceServiceID',
		'RawMatRequireServiceQtyNeeded',
		'RawMatRequireServiceDate',
		'RawMatRequireServiceDateConfirm'
	];

    public function service()
	{
		return $this->belongsTo(Service::class, 'ServiceServiceID');
	}

	public function rawmaterial()
	{
		return $this->belongsTo(Rawmaterial::class, 'RawMaterialRawMaterialID');
	}
}
