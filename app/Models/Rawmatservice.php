<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Rawmatservice
 * 
 * @property int $RawMatServiceID
 * @property int|null $ServiceServiceID
 * @property int|null $RawMaterialRawMaterialID
 * @property float $RawMatServiceQty
 * @property Carbon|null $RawMatServiceDate
 * 
 * @property Rawmaterial|null $rawmaterial
 * @property Service|null $service
 *
 * @package App\Models
 */
class Rawmatservice extends Model
{
	protected $table = 'rawmatservice';
	protected $primaryKey = 'RawMatServiceID';
	public $timestamps = false;

	protected $casts = [
		'ServiceServiceID' => 'int',
		'RawMaterialRawMaterialID' => 'int',
		'RawMatServiceQty' => 'float'
	];

	protected $dates = [
		'RawMatServiceDate'
	];

	protected $fillable = [
		'ServiceServiceID',
		'RawMaterialRawMaterialID',
		'RawMatServiceQty',
		'RawMatServiceDate'
	];

	public function rawmaterial()
	{
		return $this->belongsTo(Rawmaterial::class, 'RawMaterialRawMaterialID');
	}

	public function service()
	{
		return $this->belongsTo(Service::class, 'ServiceServiceID');
	}
}
