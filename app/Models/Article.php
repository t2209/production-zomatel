<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Article
 *
 * @property int $ArticleID
 * @property string|null $ArticleName
 * @property string|null $ArticleUnity
 * @property string|null $ArticlePurchasePrice
 * @property string|null $ArticleBarCode
 * @property string|null $ArticlePhoto
 * @property float $ArticlePrice
 * @property int $ArticleClassArticleClassID
 * @property int $ArticleCode
 *
 *
 * @property Collection|Box[] $boxes
 * @property Collection|Service[] $services
 * @property Collection|Event[] $events
 * @property Collection|Rawmatarticle[] $rawmatarticles
 *
 * @package App\Models
 */
class Article extends Model
{
	protected $table = 'article';
	protected $primaryKey = 'ArticleID';
	public $timestamps = false;

	protected $fillable = [
		'ArticleName',
		'ArticleUnity',
		'ArticlePurchasePrice',
		'ArticleBarCode',
		'ArticlePhoto',
        'ArticleIsArchived',
		'ArticlePrice',
        'ArticleCode',
		'ArticleClassArticleClassID'
	];

	public function boxes()
	{
		return $this->belongsToMany(Box::class, 'articlebox', 'ArticleArticleID', 'BoxBoxID')
					->withPivot('ArticleBoxID', 'ArticleBoxQty');
	}

	public function services()
	{
		return $this->belongsToMany(Service::class, 'articleservice', 'ArticleArticleID', 'ServiceServiceID')
					->withPivot('ArticleServiceID', 'ArticleServiceQty', 'ArticleServiceMax');
	}

	public function events()
	{
		return $this->belongsToMany(Event::class, 'eventarticle', 'ArticleArticleID', 'EventEventID')
					->withPivot('EventArticleID', 'EventArticleQty');
	}

	public function rawmatarticles()
	{
		return $this->hasMany(Rawmatarticle::class, 'ArticleArticleID');
	}

    public function articleclass()
	{
		return $this->belongsTo(ArticleClass::class, 'ArticleClassArticleClassID');
	}
}
