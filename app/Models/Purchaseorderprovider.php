<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Purchaseorderprovider
 * 
 * @property int $PurchaseOrderProviderID
 * @property int|null $GrandBookProviderGrandBookProviderID
 * @property Carbon|null $POProvDateSend
 * @property Carbon|null $POProvDateReceive
 * 
 * @property Grandbookprovider|null $grandbookprovider
 * @property Collection|Purchaseorderservice[] $purchaseorderservices
 *
 * @package App\Models
 */
class Purchaseorderprovider extends Model
{
	protected $table = 'purchaseorderprovider';
	protected $primaryKey = 'PurchaseOrderProviderID';
	public $timestamps = false;

	protected $casts = [
		'GrandBookProviderGrandBookProviderID' => 'int'
	];

	protected $dates = [
		'POProvDateSend',
		'POProvDateReceive'
	];

	protected $fillable = [
		'GrandBookProviderGrandBookProviderID',
		'POProvDateSend',
		'POProvDateReceive'
	];

	public function grandbookprovider()
	{
		return $this->belongsTo(Grandbookprovider::class, 'GrandBookProviderGrandBookProviderID');
	}

	public function purchaseorderservices()
	{
		return $this->hasMany(Purchaseorderservice::class, 'PurchaseOrderProviderPurchaseOrderProviderID');
	}
}
