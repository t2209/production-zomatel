<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Order
 *
 * @property int $OrderID
 * @property int $OrderNum
 *
 * @property Collection|OrderTable[] $ordertables
 *
 * @package App\Models
 */
class Order extends Model
{
    use HasFactory;
    protected $table = 'order';
	protected $primaryKey = 'OrderID';
	public $timestamps = true;


    protected $fillable = [
        'OrderNum',
	];

    public function ordertables()
	{
		return $this->hasMany(OrderTable::class, 'OrderOrderID');
	}


}
