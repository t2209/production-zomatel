<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Ticketing
 *
 * @property int $TicketingID
 * @property int $TicketValue
 * @property int $TicketQty
 * @property int $FundFuncID
 *
 *
 *@property Fund $Fund
 *@package App\Models
 */
class Ticketing extends Model
{
    use HasFactory;
    protected $table = 'ticketing';
	protected $primaryKey = 'TicketingID';
    public $timestamps = true;

    protected $fillable = [
        'TicketQty' ,
        'TicketValue',
        'FundFundID'
    ];

    public function fund()
	{
		return $this->belongsTo(Fund::class, 'FundFundID');
	}
}
