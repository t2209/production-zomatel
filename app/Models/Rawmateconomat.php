<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Rawmateconomat
 *
 * @property int $RawMatEconomatID
 * @property int $EconomatEconomatID
 * @property int $RawMatProviderRawMatProviderID
 * @property float $RawMatEconomatQty
 * @property Carbon|null $RawMatEconomatDate
 *
 * @property Economat $economat
 * @property Rawmaterial $rawmaterial
 *
 * @package App\Models
 */
class Rawmateconomat extends Model
{
	protected $table = 'rawmateconomat';
	protected $primaryKey = 'RawMatEconomatID';
	public $timestamps = false;

	protected $casts = [
		'EconomatEconomatID' => 'int',
		'RawMatProviderRawMatProviderID' => 'int',
		'RawMatEconomatQty' => 'float'
	];

	protected $dates = [
		'RawMatEconomatDate'
	];

	protected $fillable = [
		'EconomatEconomatID',
		'RawMatProviderRawMatProviderID',
		'RawMatEconomatQty',
		'RawMatEconomatDate'
	];

	public function economat()
	{
		return $this->belongsTo(Economat::class, 'EconomatEconomatID');
	}

	public function rawmatprovider()
	{
		return $this->belongsTo(Rawmatprovider::class, 'RawMatProviderRawMatProviderID');
	}
}
