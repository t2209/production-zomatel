<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Economat
 * 
 * @property int $EconomatID
 * 
 * @property Collection|Rawmateconomat[] $rawmateconomats
 *
 * @package App\Models
 */
class Economat extends Model
{
	protected $table = 'economat';
	protected $primaryKey = 'EconomatID';
	public $timestamps = false;

	public function rawmateconomats()
	{
		return $this->hasMany(Rawmateconomat::class, 'EconomatEconomatID');
	}
}
