<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Rawmatarticle
 *
 * @property int $RawMatArticleID
//  * @property int $RawMatArticleCode
 * @property int|null $EventEventID
 * @property int $ServiceServiceID
 * @property int|null $RawMaterialRawMaterialID
 * @property float $RawMatArticleQty
 * @property int $ArticleArticleID

 *
 * @property Article $article
 * @property Service|null $service
 * @property ArticleClass|null $articleclass
 * @property Event|null $event
 * @property Rawmatprovider|null $rawmaterialprovider
 * @property Rawmaterial|null $rawmaterial
 *
 * @package App\Models
 */
class Rawmatarticle extends Model
{
	protected $table = 'rawmatarticle';
	protected $primaryKey = 'RawMatArticleID';
	public $timestamps = true;

	protected $casts = [
		// 'RawMatArticleCode' => 'int',
		'EventEventID' => 'int',
		'ServiceServiceID' => 'int',
		'RawMaterialRawMaterialID' => 'int',
		'RawMatArticleQty' => 'float',
		// 'ArticlePriceTTC' => 'float',
		'ArticleArticleID' => 'int',
		// 'ArticleClassArticleClassID' => 'int'
	];

	protected $fillable = [
		// 'RawMatArticleCode',
		'EventEventID',
		'ServiceServiceID',
		'RawMaterialRawMaterialID',
		'RawMatArticleQty',
		'ArticleArticleID',
		// 'ArticlePriceTTC',
		// 'ArticleClassArticleClassID'
	];

	public function article()
	{
		return $this->belongsTo(Article::class, 'ArticleArticleID');
	}

	public function service()
	{
		return $this->belongsTo(Service::class, 'ServiceServiceID');
	}

	// public function articleclass()
	// {
	// 	return $this->belongsTo(ArticleClass::class, 'ArticleClassArticleClassID');
	// }

	public function event()
	{
		return $this->belongsTo(Event::class, 'EventEventID');
	}

	public function rawmaterialprovider()
	{
		return $this->belongsTo(Rawmatprovider::class, 'RawMaterialRawMaterialID');
	}

    // public function rawmaterial()
	// {
	// 	return $this->belongsTo(Rawmaterial::class, 'RawMaterialRawMaterialID');
	// }
}
