<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Purchaseorderservice
 * 
 * @property int $POServID
 * @property int|null $PurchaseOrderProviderPurchaseOrderProviderID
 * @property int|null $ServiceServiceID
 * @property Carbon|null $POServDateNeed
 * @property Carbon|null $POServDateSend
 * 
 * @property Purchaseorderprovider|null $purchaseorderprovider
 * @property Service|null $service
 * @property Collection|Rawmatrequire[] $rawmatrequires
 *
 * @package App\Models
 */
class Purchaseorderservice extends Model
{
	protected $table = 'purchaseorderservice';
	protected $primaryKey = 'POServID';
	public $timestamps = false;

	protected $casts = [
		'PurchaseOrderProviderPurchaseOrderProviderID' => 'int',
		'ServiceServiceID' => 'int'
	];

	protected $dates = [
		'POServDateNeed',
		'POServDateSend'
	];

	protected $fillable = [
		'PurchaseOrderProviderPurchaseOrderProviderID',
		'ServiceServiceID',
		'POServDateNeed',
		'POServDateSend'
	];

	public function purchaseorderprovider()
	{
		return $this->belongsTo(Purchaseorderprovider::class, 'PurchaseOrderProviderPurchaseOrderProviderID');
	}

	public function service()
	{
		return $this->belongsTo(Service::class, 'ServiceServiceID');
	}

	public function rawmatrequires()
	{
		return $this->hasMany(Rawmatrequire::class, 'PurchaseOrderServicePOServID');
	}
}
