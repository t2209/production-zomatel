<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Articlebox
 * 
 * @property int $ArticleBoxID
 * @property int $BoxBoxID
 * @property float $ArticleBoxQty
 * @property int $ArticleArticleID
 * 
 * @property Box $box
 * @property Article $article
 *
 * @package App\Models
 */
class Articlebox extends Model
{
	protected $table = 'articlebox';
	protected $primaryKey = 'ArticleBoxID';
	public $timestamps = false;

	protected $casts = [
		'BoxBoxID' => 'int',
		'ArticleBoxQty' => 'float',
		'ArticleArticleID' => 'int'
	];

	protected $fillable = [
		'BoxBoxID',
		'ArticleBoxQty',
		'ArticleArticleID'
	];

	public function box()
	{
		return $this->belongsTo(Box::class, 'BoxBoxID');
	}

	public function article()
	{
		return $this->belongsTo(Article::class, 'ArticleArticleID');
	}
}
