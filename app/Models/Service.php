<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use App\Http\Livewire\RawMaterials\RawMaterial;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Service
 *
 * @property int $ServiceID
 * @property int $UserUserID
 *
 * @property string|null $ServiceName
 *
 * @property Collection|Article[] $articles
 * @property Collection|Purchaseorderservice[] $purchaseorderservices
 * @property Collection|Rawmatarticle[] $rawmatarticles
 * @property Collection|Rawmatservice[] $rawmatservices
 * @property Collection|Table[] $tables
 *
 *
 * @package App\Models
 */
class Service extends Model
{
	protected $table = 'service';
	protected $primaryKey = 'ServiceID';
	public $timestamps = false;

	protected $fillable = [
		'ServiceName',
        'UserUserID'
	];

	public function articles()
	{
		return $this->belongsToMany(Article::class, 'articleservice', 'ServiceServiceID', 'ArticleArticleID')
					->withPivot('ArticleServiceID', 'ArticleServiceQty', 'ArticleServiceMax','ArticleServiceArticlePrice');
	}

    public function rawmats()
	{
		return $this->belongsToMany(RawMaterial::class, 'rawmatservice', 'ServiceServiceID', 'RawMaterialRawMaterialID')
					->withPivot('RawMatServiceID', 'RawMatServiceQty', 'RawMatServiceDate');

	}

	public function purchaseorderservices()
	{
		return $this->hasMany(Purchaseorderservice::class, 'ServiceServiceID');
	}

	public function rawmatarticles()
	{
		return $this->hasMany(Rawmatarticle::class, 'ServiceServiceID');
	}

	public function rawmatservices()
	{
		return $this->hasMany(Rawmatservice::class, 'ServiceServiceID');
	}

    public function tables()
	{
		return $this->hasMany(Table::class, 'ServiceServiceID');
	}

    public function user()
    {
        return $this->hasOne(User::class,'ServiceServiceID');
    }
}
