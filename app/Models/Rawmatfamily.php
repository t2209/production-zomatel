<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Rawmatfamily
 * 
 * @property int $RawMatFamilyID
 * @property string|null $RawMatWording
 * @property string|null $RawMatFamilyColor
 * 
 * @property Collection|Rawmaterial[] $rawmaterials
 *
 * @package App\Models
 */
class Rawmatfamily extends Model
{
	protected $table = 'rawmatfamily';
	protected $primaryKey = 'RawMatFamilyID';
	public $timestamps = false;

	protected $fillable = [
		'RawMatWording',
		'RawMatFamilyColor'
	];

	public function rawmaterials()
	{
		return $this->hasMany(Rawmaterial::class, 'RawMatFamilyRawMatFamilyID');
	}
}
