<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Rawmatprovider
 *
 * @property int $RawMatProviderID
 * @property float $RawMatProviderUnitPrice
 * @property int $RawMaterialRawMaterialID
 * @property int $ProviderProviderID
 *
 * @property Provider $provider
 * @property Rawmaterial $rawmaterial
 * @property Collection|Rawmatrequire[] $rawmatrequires
 *
 *
 * @package App\Models
 */
class Rawmatprovider extends Model
{
	protected $table = 'rawmatprovider';
	protected $primaryKey = 'RawMatProviderID';
	public $timestamps = false;

	protected $casts = [
		'RawMatProviderUnitPrice' => 'float',
		'RawMaterialRawMaterialID' => 'int',
		'ProviderProviderID' => 'int'
	];

	protected $fillable = [
		'RawMatProviderUnitPrice',
		'RawMaterialRawMaterialID',
		'ProviderProviderID'
	];

	public function provider()
	{
		return $this->belongsTo(Provider::class, 'ProviderProviderID');
	}

	public function rawmaterial()
	{
		return $this->belongsTo(Rawmaterial::class, 'RawMaterialRawMaterialID');
	}

    public function rawmatrequires()
	{
		return $this->hasMany(Rawmatrequire::class, 'RawMatProviderRawMatProviderID');
	}
}
