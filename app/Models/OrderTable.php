<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OrderTable
 *
 * @property int $OrderTableID
 * @property int $OrderOrderID
 * @property int $TableTableID
 *
 * @property Order $order
 * @property Table $table
 *
 * @package App\Models
 */
class OrderTable extends Model
{
    use HasFactory;
    protected $table = 'ordertable';
	protected $primaryKey = 'OrderID';
	public $timestamps = false;

    protected $fillable = [
		'OrderOrderID' ,
		'TableTableID' ,
	];

	protected $casts = [
		'OrderOrderID' => 'int',
		'TableTableID' => 'int',
	];


    public function table()
	{
		return $this->belongsTo(Table::class, 'TableTableID');
	}

    public function order()
	{
		return $this->belongsTo(Order::class, 'OrderOrderID');
	}

}
