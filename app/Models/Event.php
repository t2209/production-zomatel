<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Event
 *
 * @property int $EventID
 * @property string|null $EventName
 * @property Carbon|null $EventBeginDate
 * @property Carbon|null $EventEndDate
 * @property string|null $EventDetail
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Collection|Article[] $articles
 * @property Collection|Rawmatarticle[] $rawmatarticles
 *
 * @package App\Models
 */
class Event extends Model
{
	protected $table = 'event';
	protected $primaryKey = 'EventID';
	protected $dates = [
		'EventBeginDate',
		'EventEndDate'
	];

	protected $fillable = [
		'EventName',
		'EventBeginDate',
		'EventEndDate',
		'EventDetail'
	];

	public function articles()
	{
		return $this->belongsToMany(Article::class, 'eventarticle', 'EventEventID', 'ArticleArticleID')
					->withPivot('EventArticleID', 'EventArticleQty');
	}

	public function rawmatarticles()
	{
		return $this->hasMany(Rawmatarticle::class, 'EventEventID');
	}

    public function eventType()
	{
		return $this->belongsTo(EventType::class, 'EventTypeEventTypeID');
	}
}
