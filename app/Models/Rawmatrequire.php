<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Rawmatrequire
 *
 * @property int $RawMatRequireID
 * @property int|null $RawMatProviderRawMatProviderID
 * @property int $PurchaseOrderServicePOServID
 * @property float $RawMatRequireQty
 * @property float $RawMatReceiveQty
 * @property string $PaymentRef
 * @property string $PaymentAttachement
 * @property Carbon $PaidAt
 *
 * @property Purchaseorderservice $purchaseorderservice
 * @property Rawmatprovider|null $rawmatprovider
 * @property PaymentMethod $paymentmethod
 *
 * @package App\Models
 */
class Rawmatrequire extends Model
{
	protected $table = 'rawmatrequire';
	protected $primaryKey = 'RawMatRequireID';
	public $timestamps = false;

	protected $casts = [
		'RawMatProviderRawMatProviderID' => 'int',
		'PurchaseOrderServicePOServID' => 'int',
		'RawMatRequireQty' => 'float',
		'RawMatReceiveQty' => 'float',
        'PaymentRef' => 'string',
        'PaymentAttachement' => 'string',

        'PaidAt' => 'date'

	];

	protected $fillable = [
		'RawMatProviderRawMatProviderID',
        'PurchaseOrderServicePOServID',
		'RawMatRequireQty',
		'RawMatReceiveQty',
        'PayementRef',
        'PaymentAttachement',
        'PaidAt'
	];

	public function purchaseorderservice()
	{
		return $this->belongsTo(Purchaseorderservice::class, 'PurchaseOrderServicePOServID');
	}

	public function rawmatprovider()
	{
		return $this->belongsTo(Rawmatprovider::class, 'RawMatProviderRawMatProviderID');
	}

    public function paymentmethod()
	{
		return $this->belongsTo(PaymentMethod::class, 'PaymentMethPaymentMethID');
	}
}
