<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Freezer
 * 
 * @property int $FreezerID
 * @property float $FreezerCapacity
 * @property string|null $FreezerNum
 * 
 * @property Collection|Box[] $boxes
 *
 * @package App\Models
 */
class Freezer extends Model
{
	protected $table = 'freezer';
	protected $primaryKey = 'FreezerID';
	public $timestamps = false;

	protected $casts = [
		'FreezerCapacity' => 'float'
	];

	protected $fillable = [
		'FreezerCapacity',
		'FreezerNum'
	];

	public function boxes()
	{
		return $this->hasMany(Box::class, 'FreezerFreezerID');
	}
}
