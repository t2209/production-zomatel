<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Box
 * 
 * @property int $BoxID
 * @property int $FreezerFreezerID
 * @property string|null $BoxNum
 * 
 * @property Freezer $freezer
 * @property Collection|Article[] $articles
 *
 * @package App\Models
 */
class Box extends Model
{
	protected $table = 'box';
	protected $primaryKey = 'BoxID';
	public $timestamps = false;

	protected $casts = [
		'FreezerFreezerID' => 'int'
	];

	protected $fillable = [
		'FreezerFreezerID',
		'BoxNum'
	];

	public function freezer()
	{
		return $this->belongsTo(Freezer::class, 'FreezerFreezerID');
	}

	public function articles()
	{
		return $this->belongsToMany(Article::class, 'articlebox', 'BoxBoxID', 'ArticleArticleID')
					->withPivot('ArticleBoxID', 'ArticleBoxQty');
	}
}
