<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Rawmaterial
 *
 * @property int $RawMaterialID
 * @property int|null $RawMatFamilyRawMatFamilyID
 * @property string|null $RawMaterialAccountNum
 * @property string|null $RawMaterialName
 * @property string|null $RawMaterialUnity
 * @property bool $RawMaterialForProd
 *
 * @property Rawmatfamily|null $rawmatfamily
 * @property Collection|Rawmatarticle[] $rawmatarticles
 * @property Collection|Rawmateconomat[] $rawmateconomats
 * @property Collection|Rawmatprovider[] $rawmatproviders
//  * @property Collection|Rawmatrequire[] $rawmatrequires
 * @property Collection|Rawmatservice[] $rawmatservices
 *
 * @package App\Models
 */
class Rawmaterial extends Model
{
	protected $table = 'rawmaterial';
	protected $primaryKey = 'RawMaterialID';
	public $timestamps = false;

	protected $casts = [
		'RawMatFamilyRawMatFamilyID' => 'int',
		'RawMaterialForProd' => 'bool'
	];

	protected $fillable = [
		'RawMatFamilyRawMatFamilyID',
		'RawMaterialAccountNum',
		'RawMaterialName',
		'RawMaterialUnity',
		'RawMaterialForProd'
	];

	public function rawmatfamily()
	{
		return $this->belongsTo(Rawmatfamily::class, 'RawMatFamilyRawMatFamilyID');
	}

	public function rawmatarticles()
	{
		return $this->hasMany(Rawmatarticle::class, 'RawMaterialRawMaterialID');
	}

	// public function rawmateconomats()
	// {
	// 	return $this->hasMany(Rawmateconomat::class, 'RawMaterialRawMaterialID')
    //     ->orderBy('RawMatEconomatDate','desc')
    //     ->limit(1);
	// }

	public function rawmatproviders()
	{
		return $this->hasMany(Rawmatprovider::class, 'RawMaterialRawMaterialID')->orderBy('RawMatProviderDate','DESC');

    }

	public function providers()
	{
		return $this->belongsToMany(Provider::class, 'rawmatprovider', 'RawMaterialRawMaterialID', 'ProviderProviderID')
					->withPivot('RawMatProviderID', 'ProviderProviderID', 'RawMatProviderUnitPrice');
	}

	// public function rawmatrequires()
	// {
	// 	return $this->hasMany(Rawmatrequire::class, 'RawMaterialRawMaterialID');
	// }

	public function rawmatservices()
	{
		return $this->hasMany(Rawmatservice::class, 'RawMaterialRawMaterialID');
	}

    public function services()
	{
		return $this->belongsToMany(Service::class, 'rawmatservice', 'RawMaterialRawMaterialID', 'ServiceServiceID')
					->withPivot('RawMatServiceID', 'RawMatServiceQty', 'RawMatServiceDate')
                    ->orderBy('RawMatServiceDate','desc')
                    ->limit(1);
	}
}
