<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Foundation\Auth\Access\Authorizable;

/**
 * Class User
 *
 * @property int $UserID
 * @property string $name
 * @property string $password
 * @property string $email
 * @property int $ServiceServiceID
 * @property int $WaiterID
 *
 * @property int $RoleRoleID
 *
 * @package App\Models
 */
class User extends Model implements
AuthenticatableContract,
AuthorizableContract,
CanResetPasswordContract
{
    use HasFactory;
    use Authenticatable, Authorizable, CanResetPassword, MustVerifyEmail;

    protected $table = 'users';
	protected $primaryKey = 'UserID';
	public $timestamps = true;

    protected $fillable = [
		'name',
        'ServiceServiceID',
        'RoleRoleID',
        'password',
        'email'
	];

    public function service()
    {
        return $this->belongsTo(Service::class,'UserUserID');
    }

    public function role()
    {
        return $this->belongsTo(Role::class,'RoleRoleID');
    }
}
