<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Fund
 *
 * @property int $FundID
 * @property int|null $ProviderProviderID
 *
 * @property float $Outflow
 * @property float $Entry
 * @property string $Observation
 *
 *@property Collection|Ticketing[] $Ticketings
 *@property Provider $Provider
 * @package App\Models
 */
class Fund extends Model
{
    use HasFactory;
    protected $table = 'fund';
	protected $primaryKey = 'FundID';
	public $timestamps = true;

    protected $casts=[
        'Outflow' => 'float',
        'Entry' => 'float',
        'Observation' => 'string',
        'ProviderProviderID' => 'int'
    ];

    protected $fillable = [
        'Outflow' ,
        'Entry' ,
        'Observation',
        'ProviderProviderID'
    ];

    public function provider()
	{
		return $this->belongsTo(Provider::class, 'ProviderProviderID');
	}

    public function tickectings()
	{
		return $this->hasMany(Ticketing::class, 'FundFundID');
	}

}
