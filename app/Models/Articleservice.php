<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Articleservice
 *
 * @property int $ArticleServiceID
 * @property int $ServiceServiceID
 * @property int $ArticleArticleID
 * @property float $ArticleServiceQty
 * @property float $ArticleServiceInvQty
 * @property float $ArticleServiceLogQty
 * @property float $ArticleServiceMax
 * @property float $ArticleServiceArticlePrice
 *
 * @property Carbon|null $ArticleServiceDate
 *
 * @property Article $article
 * @property Service $service
 *
 * @package App\Models
 */
class Articleservice extends Model
{
	protected $table = 'articleservice';
	protected $primaryKey = 'ArticleServiceID';
	public $timestamps = true;

	protected $casts = [
		'ServiceServiceID' => 'int',
		'ArticleArticleID' => 'int',
		'ArticleServiceQty' => 'float',
		'ArticleServiceInvQty' => 'float',
		'ArticleServiceLogQty' => 'float',
		'ArticleServiceMax' => 'float',
		'ArticleServiceArticlePrice' => 'float',

	];

	protected $dates = [
		'ArticleServiceDate'
	];

	protected $fillable = [
		'ServiceServiceID',
		'ArticleArticleID',
		'ArticleServiceQty',
		'ArticleServiceInvQty',
		'ArticleServiceLogQty',
		'ArticleServiceMax',
		'ArticleServiceDate',
		'ArticleServiceArticlePrice',

	];

	public function article()
	{
		return $this->belongsTo(Article::class, 'ArticleArticleID');
	}

	public function service()
	{
		return $this->belongsTo(Service::class, 'ServiceServiceID');
	}
}
