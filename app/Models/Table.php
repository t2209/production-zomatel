<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Table
 *
 * @property int $TableID
 * @property int $TableNum
 * @property int $ServiceServiceID
 *
 * @property Collection|OrderTable[] $ordertables
 *
 * @package App\Models
 */
class Table extends Model
{
    use HasFactory;
    protected $table = 'table';
	protected $primaryKey = 'TableID';
    public $timestamps = false;

    protected $fillable = [
        'ServiceServiceID',
        'TableNum',
	];

    public function service()
    {
        return $this->belongsTo(Service::class,'ServiceServiceID');
    }

    public function ordertables()
	{
		return $this->hasMany(OrderTable::class, 'TableTableID');
	}
}
