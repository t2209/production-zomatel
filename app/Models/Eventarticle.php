<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Eventarticle
 *
 * @property int $EventArticleID
 * @property int $EventEventID
 * @property int|null $ArticleArticleID
 * @property float $EventArticleQty
 * @property float $EventArticleArticlePrice
 * @property float $EventArticleLogQty
 * @property float $EventArticleInvQty
//  * @property float $EventArticleMax
 * @property Carbon|null $EventArticleDate
 *
 * @property Article|null $article
 * @property Event $event
 *
 * @package App\Models
 */
class Eventarticle extends Model
{
	protected $table = 'eventarticle';
	protected $primaryKey = 'EventArticleID';
	public $timestamps = true;

	protected $casts = [
		'EventEventID' => 'int',
		'ArticleArticleID' => 'int',
		'EventArticleQty' => 'float',
		'EventArticleLogQty' => 'float',
		'EventArticleInvQty' => 'float',
        'EventArticleArticlePrice' => 'float'
		// 'EventArticleMax' => 'float'
	];

	protected $dates = [
		'EventArticleDate'
	];

	protected $fillable = [
		'EventEventID',
		'ArticleArticleID',
		'EventArticleQty',
		'EventArticleLogQty',
		'EventArticleInvQty',
		// 'EventArticleMax',
		'EventArticleDate',
        'EventArticleArticlePrice'
	];

	public function article()
	{
		return $this->belongsTo(Article::class, 'ArticleArticleID');
	}

	public function event()
	{
		return $this->belongsTo(Event::class, 'EventEventID');
	}
}
