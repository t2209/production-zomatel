<?php
// namespace App\Helpers;

// class AppHelper
// {

//     function getCode($item){
//         return array_key_exists('ArticleServiceID',$item) ? $item['ArticleServiceID'] :(
//             array_key_exists('EventArticleID',$item) ? $item['EventArticleID'] : null
//         );
//     }
// }

use App\Models\Articleservice;
use App\Models\Eventarticle;
use App\Models\Rawmaterial;
use App\Models\Rawmatprovider;
use App\Models\Role;
use App\Models\Service;
use App\Providers\ArticleServiceProvider;
use App\Providers\RawMatServiceProvider;

function getCode($item){
    return array_key_exists('ArticleServiceID',$item) ? $item['ServiceServiceID'] :(
        array_key_exists('EventArticleID',$item) ? $item['EventEventID'] : null
    );
}

function getOwnerID($item){
    return array_key_exists('ArticleServiceID',$item) ? "service".$item['ArticleServiceID'] :(
        array_key_exists('EventArticleID',$item) ? "event".$item['EventArticleID'] : null
    );
}

function getClassInstance($item){
    return array_key_exists('ArticleServiceID',$item) ? Articleservice::class :(
        array_key_exists('EventArticleID',$item) ? Eventarticle::class : null
    );
}

function getPrice($item){

   return  array_key_exists('ServiceServiceID',$item) ? ArticleServiceProvider::getServiceArticlePrice($item['ArticleArticleID'],$item['ServiceServiceID'])  :
    (
        array_key_exists('EventEventID',$item) ?
        ArticleServiceProvider::getEventArticlePrice($item['ArticleArticleID'],$item['EventEventID'])
        :0
    );
}

function getRawMatLastPrice(int $rawmatID ){
    return Rawmatprovider::where('RawMaterialRawMaterialID',$rawmatID)->orderBy('RawMatProviderDate','DESC')->limit(1)->get();
}

function getAllServices(){
    return Service::get();
}

function getRoles(){
    return Role::get();
}

function getArticle($articleID, $serviceID, $eventID){
    return ArticleServiceProvider::getArticle($articleID,$serviceID,$eventID);
}

function getRawMat($rawMatID,$articleID, $serviceID, $eventID, $isArchived = false){
    return RawMatServiceProvider::getRawMat($rawMatID,$articleID, $serviceID, $eventID, $isArchived);
}

function setMonneyMask($price)
{
    return number_format((float)($price), 0, '.', ' ') . " Ar";
}

function classGridBody()
{
    return "flex h-8 bg-gray-100 items-center";
}

function getRawMatByProvider($week,$ProviderID,$searchValue = null,$filterFamily = null,$adressfilter = null){
    return RawMatServiceProvider::getRawMatByProvider($week,$ProviderID,$searchValue,$filterFamily,$adressfilter);
}
 function getRawMatEconomatQty($rawMatID)
{
    return RawMatServiceProvider::getRawMatEconomatQte($rawMatID);
}

function getPriceHT($priceTTC){
    $TVA = 20 / 100;
    return $priceTTC-($priceTTC * $TVA );
}
