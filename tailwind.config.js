module.exports = {
  purge: [
    './resources//*.blade.php',
    './resources//.js',
    './resources/**/.vue',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: "#17B9A6", // primary
        secondary: "#B06140", // secondary
        success: "#1EAE22", // success
        warning: "#E09D00", // warning
        danger: "#A10222", // danger
        dark: "rgba(83, 83, 83, 0.473)", // dark
        marron : "#f8aa89", // lite
        marron_focus : "#c2633a", // focus
        primary_focus : "#139b8b", // pr-focus
        success_focus : "#058d09", // sc-focus
        info : "#84dfd4", // info
        info_lite : "#bff3ed", // info-lite
      },
      gridTemplateColumns: {
        // Simple 16 column grid
      //  '16': 'repeat(2, minmax(0, 1fr))',
      //  'body': '20% 77%',
      //  'content':'30px 600px',
        // Complex site-specific column configuration
       'footer': '200px minmax(900px, 1fr) 100px',
      //  'table-col-14' : 'repeat(14,minmax(0,1fr)',
      },
      gap: {
         '11': '2.75rem',
         '13': '3.25rem',
        },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('daisyui'),
  ],

  daisyui: {
    styled: true,
    themes: false,
    base: true,
    utils: true,
    logs: true,
    rtl: true,
  },
}