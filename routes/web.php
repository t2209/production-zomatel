<?php

// use App\Http\Controllers\ArticleClassController;

use App\Http\Controllers\ArticleClassController;
use App\Http\Controllers\ArticlesController;
use App\Http\Controllers\ArticlesRawmatController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BoxController;
use App\Http\Controllers\CaisseController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\FreezerController;
use App\Http\Controllers\ProviderController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\RawMatController;
use App\Http\Controllers\ProdController;
use App\Http\Controllers\POServiceConroller;
use App\Http\Controllers\EconomatController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SaleController;
use App\Http\Controllers\TableController;
use App\Models\Provider;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('pages.dashboard');

})->middleware('auth');

// Routes Raw materials
Route::get('/rw-mat', [RawMatController::class , 'index'])->name('RawMaterial');
// Routes Articels
Route::get('/articles', [ArticlesController::class , 'index'])->name('Articles');
// Routes Box
Route::get('/box', [BoxController::class , 'index'])->name('Boites');
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// Route::get('/login', [LoginController::class])->name('login');

Route::get('/events', [EventController::class , 'index'])->name('Events');
// Routes Box
Route::get('/providers', [ProviderController::class , 'index'])->name('Fournisseurs');
// Routes Service
Route::get('/services', [ServiceController::class , 'index'])->name('Service');
// Routes Service
Route::get('/production', [ProdController::class , 'serviceProd'])->name('Production');
Route::get('/eventProduction', [ProdController::class , 'eventProd'])->name('eventProduction');
Route::get('/freezer', [FreezerController::class , 'index'])->name('Congelateurs');
// Routes Articles Raw Mat
Route::get('/articlesRawmat', [ArticlesRawmatController::class , 'index'])->name('ArticlesRawmat');
// Route::get('/articlesRawmat/service/{id?}', [ArticlesRawmatController::class , 'loadService'])->name('ArticlesRawmatService');
// Route::get('/articlesRawmat/event', [ArticlesRawmatController::class , 'loadEvent'])->name('ArticlesRawmatEvent');
// Route::get('/articlesRawmat/class', [ArticlesRawmatController::class , 'loadClass'])->name('ArticlesRawmatClass');
// Route purchase order
Route::get('/purchaseOrder', [POServiceConroller::class , 'index'])->name('purchaseOrder');
// Route article class
Route::get('/article-class', [ArticleClassController::class , 'index'])->name('Classe d\'articles');
// Route event-type
Route::get('/type-events', [EventController::class , 'typeevent'])->name('Type d\'évènt');
// Route list POprovider
Route::get('/listPOProvider', [POServiceConroller::class , 'POProdRedirection'])->name('Liste achat fournisseur');
// Route list rawmat received
Route::get('/listRwMatReceive', [POServiceConroller::class , 'RwMatReceive'])->name('Liste arrivages achats');
// Route request service
Route::get('/service-request', [POServiceConroller::class , 'ServiceRequest'])->name('Demande par service');
// Route service stock
Route::get('/service-stock', [ServiceController::class , 'ServiceStock'])->name('Stock par service');
// Route service stock rawmat
Route::get('/service-stock-rawmat', [ServiceController::class , 'ServiceStockRawMat'])->name('Stock par service: MP1');
// Route economat stock rawmat
Route::get('/economat-stock-rawmat', [EconomatController::class , 'EconomatStockRawMat'])->name('Stock economat MP1');
// Route control panel
Route::get('/cpl', [HomeController::class , 'ControlPanel'])->name('Control Panel');
// Route daily sail
Route::get('/dailysaleExcel', [SaleController::class , 'index'])->name('Daily sale Execl');
Route::get('/dailysale', [SaleController::class , 'dailySaleIndex'])->name('Daily sale');
// Route payment for prod
Route::get('/paymentProvider', [ProviderController::class , 'payment'])->name('Payment Provider');
// Route balance
Route::get('/balance', [ProviderController::class , 'balance'])->name('Balance de compte');
// Route payment mode
Route::get('/crudPayment', [ProviderController::class , 'crudPayment'])->name('CRUD Payement');
// Routes caisse
Route::get('/caisses', [CaisseController::class , 'index'])->name('Caisse');
// Route grandbook
Route::get('/grand-book', [ProviderController::class , 'grandBook'])->name('Grand Livre');
// Route table
Route::get('/table', [TableController::class , 'index'])->name('Table');
// Route Waiter
Route::get('/uiWaiter', [TableController::class , 'waiterIndex'])->name('App Serveur');
// Route cooker
Route::get('/uiCooker', [TableController::class , 'cookerIndex'])->name('App Cuisinier');
// Route ca
Route::get('/ca', [SaleController::class , 'caIndex'])->name('CA');
Route::get('/test',[TableController::class, 'test']);



