{{-- The whole world belongs to you. --}}
<div class="space-y-5 font-mono" x-data>
   {{-- Title --}}
   <div class="flex justify-center items-center">
        <div class="text-3xl font-bold">
            <span class="bg-clip-text text-transparent font-mono tracking-tight bg-gray-600">
                Stock des matières premières
            </span>
        </div>
    </div>

    {{-- Filter week --}}
    <div class="flex justify-between">
        <div class="flex items-center space-x-2">
            @if ($selectedWeek == null | $selectedWeek == "")
            <h1 class="text-danger">Choississez une semaine ...</h1>
            @else
                <h1>Résultat de la {{ $weekShow }}</h1>
            @endif
        </div>

        <div class="flex items-center space-x-2">
            <h1>Selectionner une semaine</h1>
            <input wire:change='unsetVars' wire:model="selectedWeek" type="week" class="input input-bordered">
            <button wire:click='getStockService' class="btn btn-outline tooltip" data-tip="Voir le résultat">Voir</button>
        </div>
    </div>
    <div class="flex justify-end">
        @error('selectedWeek') <span class="error text-danger">{{ $message }}</span> @enderror
    </div>

    {{-- lists of rawmateconomat --}}
    <div class="overflow-x-auto justify-center space-y-2">
        <div class="grid grid-cols-5 gap-1 w-full h-full">
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Identification</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">MP1</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Famille</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Quantité</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Date</div>
        </div>
        <div class="grid space-y-1">
            @if($selectedWeek == null || $selectedWeek == "")
                <div class="flex justify-center">
                    <span class="text-danger">Choississez une semaine ...</span>
                </div>
            @endif
            @if ($servicesID != null)
                @foreach ($servicesID as $item)
                <div class="grid grid-cols-5 gap-1">
                    <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded">{{$item->rawmaterial->RawMaterialAccountNum}}</div>
                    <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded">{{$item->rawmaterial->rawmatfamily->RawMatWording}}</div>
                    <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded">{{$item->rawmaterial->RawMaterialName}}</div>
                    <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded">{{$item->RawMatEconomatQtySum}}</div>
                    <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded">{{date("d/m/Y", strtotime($item->RawMatEconomatDate))}}</div>
                </div>
                @endforeach
            @else
                <div class="flex justify-center">
                    <span class="text-danger">Pas de données ...</span>
                </div>
            @endif
        </div>
    </div>
</div>
