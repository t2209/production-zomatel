<div class="px-52" x-data="
{
    notify:@entangle('notifyNewOrder')
}">
    {{-- filter service --}}
    <div>
        <button wire:click='filterService("")' class="btn {{ ($dataFilterService == '') ? 'btn-active' : 'btn-outline' }}">
            Tout
        </button>
        @foreach ($services as $serv)
            <button wire:click='filterService("{{ $serv->ServiceName }}")' class="btn {{ ($serv->ServiceName == $dataFilterService) ? 'btn-active' : 'btn-outline' }}">
                {{ $serv->ServiceName }}
            </button>
        @endforeach
    </div>

    <div x-show="notify"  class="shadow-lg alert alert-success">
        <div>
          <svg xmlns="http://www.w3.org/2000/svg" class="flex-shrink-0 w-6 h-6 stroke-current" fill="none" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" /></svg>
          <span>Your purchase has been confirmed!</span>
        </div>
      </div>
    {{-- filter status --}}
    <div class="my-2">
        @foreach ($status as $statu)
            <button wire:click='filterStatu("{{ $statu["data"] }}")'
                class="btn {{ ($statu["data"] == $dataFilterStatus) ? 'btn-active' : 'btn-outline' }} {{ $statu["color"] }}">
                {{ $statu["libelle"] }}
            </button>
        @endforeach
    </div>
    {{-- serach field --}}
    <div class="flex gap-2 my-2">
        <input wire:model='dataFilterOrder' type="text" placeholder="Recherche Commande" class="w-full max-w-xs input input-bordered">
        <input wire:model='dataFilterTable' type="text" placeholder="Recherche Table" class="w-full max-w-xs input input-bordered">
    </div>

    {{-- List of order by orderNum --}}
    <div class="flex flex-wrap gap-2">
        {{-- @dd($orders) --}}
        @foreach ($orders as $key => $order)
            <div class="shadow stats border-b-8
            {{
            ($order['Status'] == 'pendingAction') ? 'border-gray-700' : (($order['Status'] == 'canceled') ? 'border-yellow-500' : 'border-green-700')
            }}
            ">
                <div class="flex flex-col stat">
                    <div class="stat-value">{{ $order[0]->OrderNum }}</div>
                    {{-- {{ count($order) }} --}}
                    <div>
                        {{-- @foreach ($order as $i => $item)
                            <div class="font-semibold stat-title">Table n° {{ var_dump($item[$i]) }}</div>
                        @endforeach --}}
                        @for ($i = 0; $i < count($order) - 1; $i++)
                         <div class="font-semibold stat-title">Table n° {{ $order[$i]["TableNum"] }}</div>
                        @endfor
                    </div>
                    <div class="flex items-end justify-center h-full mt-2 stat-desc">
                        <label for="orderDtl_modal" wire:click='orderDetails({{ $order[0]->OrderID }},"{{ $order[0]->OrderNum }}")' class="btn btn-sm modal-button">Voir</label>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    {{-- Modal details --}}
    <input type="checkbox" id="orderDtl_modal" class="modal-toggle">
    <div class="modal">
        <div class="space-y-3 modal-box" style="width: 75%; max-width: 75%; height: 75%; max-height: 75%;">
            <div class="flex justify-center font-mono text-xl font-semibold">
                <h1>Liste des articles dans le commande : {{ $orderNum }}</h1>
            </div>
            @if ($orderID == null)
                <p>Chargement</p>
            @else
                <div class="flex justify-end gap-1">
                    <div class="gap-2 bg-gray-400 border-0 badge">
                        Attente d'action
                        {{ $articleOrderStatus[""] }}
                    </div>
                    <div class="gap-2 bg-blue-500 border-0 badge">
                        En préparation
                        {{ $articleOrderStatus["pending"] }}
                    </div>
                    <div class="gap-2 bg-green-600 border-0 badge">
                        Fait
                        {{ $articleOrderStatus["done"] }}
                    </div>
                    <div class="gap-2 bg-yellow-500 border-0 badge">
                        Annuler
                        {{ $articleOrderStatus["canceled"] }}
                    </div>
                    <div class="gap-2 bg-gray-700 border-0 badge">
                        Retour
                        {{ $articleOrderStatus["return"] }}
                    </div>
                </div>
                {{-- @dd($articleOrders) --}}
                <div class="grid grid-cols-5 gap-1 text-white">
                    <div class="p-2 bg-gray-600 rounded ">Articles</div>
                    <div class="p-2 text-right bg-gray-600 rounded">Prix unitaire</div>
                    <div class="p-2 text-center bg-gray-600 rounded">Quantité commandée</div>
                    <div class="p-2 text-right bg-gray-600 rounded">Prix total</div>
                    <div class="p-2 text-center bg-gray-600 rounded">Action</div>
                </div>

                @foreach ($articleOrders as $artOrder)
                    <div class="grid grid-cols-5 gap-1">
                        <div class="flex items-center h-10 pl-2 bg-gray-200 border-l-8 rounded"
                        style="border-color: {{
                            ($artOrder->ArticleOrderStatus == 'done') ? '#009485' :
                            (($artOrder->ArticleOrderStatus == 'pending') ? '#2094f3' :
                            (($artOrder->ArticleOrderStatus == 'canceled') ? '#ff9900' :
                            (($artOrder->ArticleOrderStatus == 'return') ? '#3d4451':
                            'gray')))
                        }}"
                        >

                            {{ $artOrder->articleService->article->ArticleName }}
                        </div>

                        <div class="flex items-center justify-end h-10 pr-2 bg-gray-200 rounded">
                            {{ setMonneyMask($artOrder->articleService->ArticleServiceArticlePrice) }}
                        </div>

                        <div class="flex items-center justify-center h-10 bg-gray-200 rounded">
                            {{ $artOrder->ArticleOrderQty }}
                        </div>

                        <div class="flex items-center justify-end h-10 pr-2 bg-gray-200 rounded">
                            {{ setMonneyMask($artOrder->articleService->ArticleServiceArticlePrice * $artOrder->ArticleOrderQty) }}
                        </div>

                        <div class="flex items-center justify-center h-10 gap-1 rounded">
                            {{-- Pending --}}
                            <button x-on:click="confirm('Mettre {{ str_replace("'"," ",$artOrder->articleService->article->ArticleName) }} en cours de preparation ?') ? @this.operatorArticleOrder('pending',{{ $artOrder->ArticleOrderID }}) : false" class="btn btn-sm btn-circle btn-info">
                                <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
                                </svg>
                            </button>
                            {{-- done --}}
                            <button x-on:click="confirm('Mettre {{ str_replace("'"," ",$artOrder->articleService->article->ArticleName) }} en cours de preparation ?') ? @this.operatorArticleOrder('done',{{ $artOrder->ArticleOrderID }}) : false" class="btn btn-sm btn-circle btn-success">
                                <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M5 13l4 4L19 7" />
                                </svg>
                            </button>
                            {{-- cancel --}}
                            <button x-on:click="confirm('Annuler {{ str_replace("'"," ",$artOrder->articleService->article->ArticleName) }} du commande ?') ? @this.operatorArticleOrder('cancel',{{ $artOrder->ArticleOrderID }}) : false" class="btn btn-sm btn-circle btn-warning">
                                <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
                                </svg>
                            </button>
                            {{-- return --}}
                            <button x-on:click="confirm('Mettre {{ str_replace("'"," ",$artOrder->articleService->article->ArticleName) }} en cours de preparation ?') ? @this.operatorArticleOrder('return',{{ $artOrder->ArticleOrderID }}) : false" class="btn btn-sm btn-circle">
                                <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M8 7h12m0 0l-4-4m4 4l-4 4m0 6H4m0 0l4 4m-4-4l4-4" />
                                </svg>
                            </button>
                        </div>
                    </div>
                @endforeach

                {{-- <div class="grid grid-cols-5 gap-1">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div class="flex items-center justify-end h-10 pr-2 font-semibold bg-gray-200 rounded">{{ setMonneyMask($totalOrder) }}</div>
                    <div></div>
                </div> --}}
            @endif
            <div class="modal-action gap-x-4">
                <label wire:click='resetVars' for="orderDtl_modal" class="btn">Fermer</label>
            </div>
        </div>
    </div>
</div>
