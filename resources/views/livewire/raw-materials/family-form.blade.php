<div x-data="{ normal : true , conf : false , update: false , cancel : true}">
    <div x-show="normal" style="background-color: {{ $item->RawMatFamilyColor }}" class="h-10 flex items-center m-2 px-5 text-white font-semibold transition-colors duration-150 rounded-lg focus:shadow-outline">
        
        <span x-show="cancel" class="mr-2">{{$item->RawMatWording}}</span>
        <input x-cloak style="display: none !important" wire:model="item.RawMatWording" x-show="update" class="input input-bordered" type="text">
        <h1 x-show="conf" class="font-bold">Confirmer</h1>
        {{-- btn update --}}
        {{-- yes --}}
        <span wire:click="update({{$item->RawMatFamilyID}})" x-show="update" x-on:click="{update = false , cancel = true}" class="inline-flex items-center justify-center mx-1 px-2 py-1 text-xs font-bold leading-none text-red-100 transform hover:scale-125">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
            </svg>
        </span>
        {{-- cancel --}}
        <span x-cloak style="display: none !important" x-show="update" x-on:click="{update = false , cancel = true}" class="inline-flex items-center justify-center mx-1 px-2 py-1 text-xs font-bold leading-none text-red-100 transform hover:scale-125">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
            </svg>
        </span>

        {{-- btn edit delete --}}
        <span  x-show="cancel" x-on:click="{update = true , cancel = false}" class="inline-flex items-center justify-center mx-1 px-2 py-1 text-xs font-bold leading-none text-red-100 transform hover:scale-125">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
            </svg>
        </span>
        <span x-show="cancel" x-on:click="{conf = true , normal = false}" class="inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-red-100 transform hover:scale-125">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
            </svg>
        </span>

    </div>

    <div x-cloak style="display: none !important" x-show="conf" class="h-10 flex items-center m-2 px-5 text-indigo-100 transition-colors duration-150 bg-danger rounded-lg focus:shadow-outline hover:bg-danger">
        <span class="mr-2">Supprimer {{$item->RawMatWording}}</span>        
        {{-- btn confirme delete --}}
        {{-- Yes --}}
        <span wire:click="delete({{$item->RawMatFamilyID}})" class="inline-flex items-center justify-center mx-1 px-2 py-1 text-xs font-bold leading-none text-red-100 transform hover:scale-125">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
            </svg>
        </span>
        {{-- cancel --}}
        <span x-on:click="{conf = false , normal = true}" class="inline-flex items-center justify-center mx-1 px-2 py-1 text-xs font-bold leading-none text-red-100 transform hover:scale-125">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
            </svg>
        </span>
    </div>
</div>