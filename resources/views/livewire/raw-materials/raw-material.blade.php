

<div class="flex h-screen w-full mt-4 bg-white border border-gray-100 rounded-2xl">
   @livewire('raw-materials.raw-mat-form')
    <div class="w-3/4 overflow-y-auto">
        {{-- Raw Family list --}}
        <div class="flex justify-center my-3">
            <h1 class="text-2xl font-semibold">Liste des matières premières : </h1>
        </div>
        @livewire('raw-materials.raw-mat-list')
    </div>
</div>

