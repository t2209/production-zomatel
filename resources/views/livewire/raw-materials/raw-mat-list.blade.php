<div>
    <div class="flex justify-center gap-2 my-5">
        @if ($RwmatFamID == "All")
            <button wire:click='setFamilyFilter("1","allFamily")' class="btn btn-active">Tout</button>
        @else
            <button wire:click='setFamilyFilter("1","allFamily")' class="btn btn-outline">Tout</button>
        @endif
        @foreach ($RawMatFamilies as $item)
        @if ($item->RawMatFamilyID == $RwmatFamID)
            <button wire:click='setFamilyFilter({{$item->RawMatFamilyID}},"perFamily")' class="btn btn-active">{{$item->RawMatWording}}</button>
        @else
            <button wire:click='setFamilyFilter({{$item->RawMatFamilyID}},"perFamily")' class="btn btn-outline">{{$item->RawMatWording}}</button>
        @endif
        @endforeach
    </div>
    <div class="flex flex-wrap gap-4 justify-center">

        @foreach ($RawMaterials as $item)
            @livewire('raw-materials.raw-form-ud',[
                'item' => $item
            ] , key($item->RawMaterialID))
        @endforeach
    </div>
</div>