<div x-data="{upState : false , firstState : true , delState : false , upProdState : false , firstProdState : true}" class="bg-white p-6 shadow-md border rounded-lg w-1/4 h-1/2" style="background-color: {{$item->RawMatFamilyColor}}">
    {{-- div first --}}
    <div x-show="firstState" id="firstState_{{$item->RawMaterialID}}">
        <h5 class="text-gray-900 font-bold text-base tracking-tight mb-2">{{$item->RawMaterialName}}</h5>
        <p class="font-normal text-gray-700 mb-3">Numéro compte : {{$item->RawMaterialAccountNum}}</p>
        <p class="font-normal text-gray-700 mb-3">Unité : {{$item->RawMaterialUnity}}</p>
    </div>

    {{-- div up --}}
    <div x-show="upState" x-cloak style="display: none !important">
        <input wire:model='item.RawMaterialName' type="text" class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-xs rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2 m-2">
        <input wire:model='item.RawMaterialAccountNum' type="text" class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-xs rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2 m-2">
        <input wire:model='item.RawMaterialUnity' type="text" class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-xs rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2 m-2">
    </div>

    {{-- div confirmation delete --}}
    <div x-show="delState" x-cloak style="display: none !important" class="text-center">
        <h1>Vous voulez supprimer {{$item->RawMaterialName}} ?</h1>
    </div>

    <div class="flex justify-end">
        {{-- first state  --}}
        <div x-show="firstState" class="flex items-center">

            <span x-on:click="{upState = true , firstState = false}" class="inline-flex items-center justify-center mx-1 px-2 py-1 text-xs font-bold leading-none text-black transform hover:scale-125">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                </svg>
            </span>
            <span x-on:click="{upState = false , firstState = false , delState = true}" class="inline-flex items-center justify-center mx-1 px-2 py-1 text-xs font-bold leading-none text-black transform hover:scale-125">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                </svg>  
            </span>
            <label for="provider_modal_{{$item->RawMaterialID}}" class="inline-flex items-center justify-center mx-1 px-2 py-1 text-xs font-bold leading-none text-black transform hover:scale-125">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
            </label>

        </div>

        {{-- up state --}}
        <div x-show="upState" x-cloak style="display: none !important">
            <span wire:click="updateRawMat({{$item->RawMaterialID}})" x-on:click="{upState = false , firstState = true}" class="inline-flex items-center justify-center mx-1 px-2 py-1 text-xs font-bold leading-none text-black transform hover:scale-125">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                </svg>
            </span>
            <span x-on:click="{upState = false , firstState = true}" class="inline-flex items-center justify-center mx-1 px-2 py-1 text-xs font-bold leading-none text-black transform hover:scale-125">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                    </svg>      
            </span>
        </div>

        {{-- delete state --}}
        <div x-show="delState" x-cloak style="display: none !important">
            <span wire:click="deleteRawMat({{$item->RawMaterialID}})" x-on:click="{upState = false , firstState = true , delState = false}" class="inline-flex items-center justify-center mx-1 px-2 py-1 text-xs font-bold leading-none text-black transform hover:scale-125">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                </svg>
            </span> 
            <span x-on:click="{upState = false , firstState = true , delState = false}" class="inline-flex items-center justify-center mx-1 px-2 py-1 text-xs font-bold leading-none text-black transform hover:scale-125">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                </svg>      
            </span>
        </div>
    </div>

    {{-- modal --}}
    <input type="checkbox" id="provider_modal_{{$item->RawMaterialID}}" class="modal-toggle inputModalId"> 
    <div class="modal">
        <div class="modal-box">
            <h1 class="text-center text-xl font-bold mb-5">Fournisseur de {{$item->RawMaterialName}} avec prix</h1>

            {{-- up provider --}}
            <div x-show="upProdState" class="p-5 my-3 card bg-base-200 text-center">
                <h1>Modification du fournisseur {{$RwMatProdName}} : {{number_format((float)$RwMatProdPrice, 0, '.', ' ')}} Ar</h1>
                <input type="text" wire:model="RawMatID" class="hidden">
                <div class="grid grid-cols-2 gap-2">
                    <div>
                        <label class="label">
                            <span class="label-text">Fournisseur</span>
                        </label>    
                        <select wire:model="ProdID" class="select select-bordered w-full max-w-xs">
                            <option selected="selected">Fournisseurs</option> 
                            @foreach ($ProviderUp as $prodUp)
                                <option value="{{$prodUp->ProviderID}}">{{$prodUp->ProviderName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-control">
                        <label class="label">
                        <span class="label-text">Prix actuelle</span>
                        </label> 
                        <input wire:model="RwMatProdPrice" type="text" class="input input-bordered">
                    </div>
                </div>
            </div>

            <table class="table w-full">
                <thead>
                    <tr>
                        <th>Fournisseur</th>
                        <th>Date</th>
                        <th class="text-right">Prix</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($Provider as $prod)
                        <tr class="hover">
                            <td>{{$prod->Provider->ProviderName}}</td>
                            <td>{{date("d/m/Y",strtotime($prod->RawMatProviderDate))}}</td>
                            <td class="text-right">{{number_format((float)$prod->RawMatProviderUnitPrice, 0, '.', ' ')}} Ar</td>
                            <td class="text-center">
                                {{-- first && up state --}}
                                <div>
                                    <button x-show="firstProdState" x-on:click="{upProdState = true , firstProdState = false}" wire:click="setUpProd('{{$prod->RawMaterialRawMaterialID}}','{{$prod->Provider->ProviderID}}','{{$prod->Provider->ProviderName}}','{{$prod->RawMatProviderUnitPrice}}')" class="btn btn-outline btn-circle btn-sm">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                                        </svg>
                                    </button> 
                                    <button x-show="upProdState" x-on:click="{upProdState = false , firstProdState = true }" class="btn btn-outline btn-circle btn-sm">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                                        </svg>
                                    </button> 
                                    <button class="btn btn-outline btn-circle btn-sm" x-on:click="confirm('Are you sure?') ? @this.deleteRwMatProd({{$prod->RawMatProviderID}}) : false">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                        </svg>
                                    </button> 
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="modal-action">
                <label x-show="upProdState" wire:click="updateRawMatProd" x-on:click="{upProdState = false , firstProdState = true }" class="btn btn-primary">Modifier</label> 
                <label for="provider_modal_{{$item->RawMaterialID}}" onclick="closeModal()" class="btn">Fermer</label>
            </div>
        </div>
    </div>
</div>
