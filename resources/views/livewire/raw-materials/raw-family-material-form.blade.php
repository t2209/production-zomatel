<div class="p-2 flex flex-col space-y-3">
    <h1 class="text-center text-2xl font-semibold">Ajout Nouveau Famille</h1>
    <input wire:model="RawMatWording" type="text" class="m-2 input input-bordered">
    @if ($errors->has('RawMatWording'))
        <p class="text-danger">{{$errors->first('RawMatWording')}}</p>
    @endif

    <div class="flex justify-center gap-2">
        <label for="colorPicker" class="text-lg font-semibold">Couleur</label>
        <input type="color" wire:model='RawMatFamilyColor' id="colorPicker">
    </div>
    @if ($errors->has('RawMatFamilyColor'))
        <p class="text-danger">{{$errors->first('RawMatFamilyColor')}}</p>
    @endif
    <div class="flex justify-center">
        <button wire:click="save" type="button" class="btn btn-primary w-1/3">Ajouter</button>
    </div>
  
</div>