<div class="w-1/4 bg-white rounded-xl h-screen overflow-y-auto">
    <div class="flex justify-center mt-3">
        <h1 class="text-2xl font-semibold">Formulaire</h1>
    </div>
    {{-- input raw mat --}}
    <div class="p-5">
        <div class="mb-6">
            <label for="RawMaterialName" class="text-sm font-medium text-gray-900 block mb-2">Nom article</label>
            <input wire:model="RawMaterialName" type="text" id="RawMaterialName" class="input input-bordered w-full">
            @if ($errors->has('RawMaterialName'))
                <p class="text-danger">{{$errors->first('RawMaterialName')}}</p>
            @endif
        </div>
        <div class="mb-6">
            <label for="RawMatFamilyRawMatFamilyID" class="text-sm font-medium text-gray-900 block mb-2">ID Famille</label>
            <select wire:model="RawMatFamilyRawMatFamilyID" id="RawMatFamilyRawMatFamilyID" class="input input-bordered w-full">
                <option>Selectionner une famille</option>
                @foreach ($rawFamilies as $item)
                    <option value="{{$item->RawMatFamilyID}}">{{$item->RawMatWording}}</option>
                @endforeach
            </select>
            @if ($errors->has('RawMatFamilyRawMatFamilyID'))
                <p class="text-danger">{{$errors->first('RawMatFamilyRawMatFamilyID')}}</p>
            @endif
        </div>
        <div class="grid grid-cols-1 gap-4">
            <div class="mb-6">
                <label for="RawMaterialAccountNum" class="text-sm font-medium text-gray-900 block mb-2">Numéro de compte</label>
                <input wire:model="RawMaterialAccountNum" type="text" id="RawMaterialAccountNum" class="input input-bordered w-full">
                @if ($errors->has('RawMaterialAccountNum'))
                    <p class="text-danger">{{$errors->first('RawMaterialAccountNum')}}</p>
                @endif
            </div>
            <div class="mb-6">
                <label for="RawMaterialUnity" class="text-sm font-medium text-gray-900 block mb-2">Unité</label>
                <input wire:model="RawMaterialUnity" type="text" id="RawMaterialUnity" class="input input-bordered w-full">
                @if ($errors->has('RawMaterialUnity'))
                    <p class="text-danger">{{$errors->first('RawMaterialUnity')}}</p>
                @endif
            </div>
        </div>
        <div class="p-6 card bordered">
            <div class="form-control">
              <label class="cursor-pointer label">
                <span class="label-text">Pour production</span> 
                    <input wire:model="RawMaterialForProd" type="checkbox" class="toggle">
              </label>
            </div>
        </div>
    </div>
    {{-- multiple input --}}
    <div class="px-5">
        <div class="my-2 add-input">
            <div class="flex justify-center {{-- gap-2 --}}">
                    {{-- <input type="text" class="form-control" placeholder="Enter Name" wire:model="name.0"> --}}
                <div class="">
                    <div class="my-2">
                        <select class="select select-bordered w-full" wire:model="ProviderID.0">
                            <option selected="selected">Fournisseurs</option> 
                            @foreach ($providers as $item)
                                <option value="{{$item->ProviderID}}">{{$item->ProviderName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="my-2">
                        <input wire:model="RawMatProviderUnitPrice.0" type="text" placeholder="Prix en Ariary" class="input input-bordered w-full price">
                    </div>
                </div>    
    
                {{-- <div class="flex justify-center items-center w-1/4">
                    <button class="btn btn-circle text-white btn-info btn-sm" wire:click.prevent="add({{$i}})">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                        </svg>
                    </button>
                </div> --}}
            </div>
        </div>
        {{-- @foreach($inputs as $key => $value)
            <div class="my-2 add-input">
                <div class="flex gap-2">
                    <div class="w-3/4">
                        <div class="my-2">
                            <select class="select select-bordered w-full" wire:model="ProviderID.{{$value}}">
                                <option selected="selected">Fournisseurs</option> 
                                @foreach ($providers as $item)
                                    <option value="{{$item->ProviderID}}">{{$item->ProviderName}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="my-2">
                            <input wire:model="RawMatProviderUnitPrice.{{$value}}" type="text" placeholder="Prix en Ariary" class="input input-bordered w-full price">
                        </div>
                    </div> 
    
                    <div class="flex justify-center items-center w-1/4">
                        <button class="btn btn-circle btn-danger btn-sm" wire:click.prevent="remove({{$key}})">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M18 12H6" />
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        @endforeach --}}
    </div>
    
    <div class="px-5 flex justify-center">
        <button wire:click="saveRawMat" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center">
            Ajouter le matière première
        </button>
    </div>

    {{-- crud family --}}
    <div x-data="{openModal:false}" class="p-5 flex justify-center">
        <!-- Modal toggle -->
        <button x-on:click="{openModal = true}" class="block text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center" type="button" data-modal-toggle="familyModal">
            Voir les familles
        </button>
        
        {{-- family modals --}}
        <div x-cloak   x-show="openModal" style="display: none !important"  class="min-w-screen h-screen animated fadeIn faster  fixed  left-0 top-0 flex justify-center items-center inset-0 z-50 outline-none focus:outline-none bg-no-repeat bg-center bg-cover">
            <div class="absolute bg-black opacity-80 inset-0 z-0"></div>
         <div class="w-full  max-w-lg p-5 relative mx-auto my-auto rounded-xl shadow-lg  bg-white ">
           <!--content-->
           <div class="">
             <!--body-->
             <div class="text-center p-5 flex-auto justify-center">
                <div class="flex flex-wrap px-5">
                    @if (count($rawFamilies) == 0)
                        <h1 class="mx-2">Vide</h1>
                    @else
                        @foreach ($rawFamilies as $item)
                            @livewire('raw-materials.family-form',[
                                'item' => $item
                            ] , key($item->RawMatFamilyID))
                        @endforeach
                    @endif
                </div>
                {{-- add raw family --}}
                @livewire('raw-materials.raw-family-material-form')
            </div>
             <!--footer-->
             <div class="p-3  mt-2 text-center space-x-4 md:block">
                 <button wire:click="refeshPage" x-on:click="{openModal = false}" class="mb-2 md:mb-0 bg-white px-5 py-2 text-sm shadow-sm font-medium tracking-wider border text-gray-600 rounded-full hover:shadow-lg hover:bg-gray-100">
                     Fermer
                 </button>
             </div>
           </div>
         </div>
        </div>

    </div>

    <script
    src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/autonumeric/4.1.0/autoNumeric.js" integrity="sha512-w5udtBztYTK9p9QHQR8R1aq8ke+YVrYoGltOdw9aDt6HvtwqHOdUHluU67lZWv0SddTHReTydoq9Mn+X/bRBcQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        // $(document).ready(function () {
            new AutoNumeric('.price', {
                // currencySymbol : ' €',
                decimalCharacter : ',',
                digitGroupSeparator : ' ',
            });   
        // });
    </script>
  
</div>
