<div class="grid grid-cols-6 gap-1">
    {{-- <div class="{{$classGrid}}">
        {{$item->ArticleServiceID}}
    </div> --}}
    {{-- @dd($item) --}}

    <div class="{{$classGrid}}">
        {{$item["service"]["ServiceName"]}}
    </div>

    <div class="{{$classGrid}}">
        {{$item["article"]["ArticleName"]}}
    </div>

    <div class="flex justify-center">
        <input wire:model="item.ArticleServiceQty" class="input input-bordered w-1/2 text-center h-full" type="text" readonly>
    </div>

    <div class="flex justify-center">
        <input wire:model="newQte" class="input input-bordered w-1/2 text-center h-full" type="number">
    </div>

    <div class="flex flex-col justify-center">
        <select wire:model='BoxBoxID' class="select select-bordered w-full">
            <option selected="selected" value="">Choisir son boite</option>
            @foreach ($boxes as $box)
                <option value="{{$box->BoxID}}">{{$box->BoxNum}} | Congelateur N°: {{$box->freezer->FreezerNum}}</option>
            @endforeach
        </select>
    </div>

    <div class="flex flex-col items-center h-10 justify-center text-center w-3/4">
        <span wire:click='product({{ getCode($item)}})' class="inline-flex items-center justify-center mx-1 px-2 py-1 text-xs font-bold leading-none transform hover:scale-125  tooltip bg-white border border-gray-100 hover:border-double hover:bg-transparent rounded" data-tip="Produire">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
            </svg>
        </span>
        <label wire:loading wire:target='product'>Reussie</label>
    </div>



    @if ($errors->has('BoxBoxID'))
        <div class="flex justify-center col-span-5">
            <div class="alert alert-error">Veuillez séléctionner une boite</div>
        </div>
    @endif

    <input type="checkbox" id="{{$item["ArticleServiceID"]}}" class="modal-toggle" {{ $modalCheck }}>
    <div class="modal">
    <div class="modal-box">
        @if (session()->has('msg'))
            <div class="text-center">
                <h1 class="badge badge-warning text-lg my-2">Quantité insuffisant</h1>
                <table class="table w-1/2">
                    <tbody>
                        <tr>
                            <th class="text-left">Matières premières</th>
                            <th class="text-center">Qte dispo</th>
                            <th class="text-center">Qte voulue</th>
                        </tr>
                        @foreach (session('msgData') as $item)
                            <tr class="hover {{ ($item["QteNeeded"] > $item["QteDispo"] || $item["QteDispo"] == 0) ? 'text-warning font-semibold' : '' }} ">
                                <td class="text-left">{{ $item["RawMaterialName"] }}</td>
                                <td class="text-center">{{ $item["QteDispo"] }}</td>
                                <td class="text-center">{{ $item["QteNeeded"] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{-- <button wire:click="exitWarning" class="btn btn-sm my-2">Fermer</button> --}}
            </div>
        @endif
        <div class="modal-action">
            <label wire:click="exitWarning" class="btn">Fermer</label>
        </div>
    </div>
    </div>
</div>
