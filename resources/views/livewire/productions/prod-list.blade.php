<div class="w-full p-10 mt-4 bg-white border border-gray-200 rounded-xl">
    {{-- @dd($articleservice) --}}
    <div class="flex justify-center items-center my-5">
        <div class="flex items-center gap-5">
            <div class="text-4xl font-bold">
                <span class="bg-clip-text text-transparent font-mono tracking-tight bg-gray-600">
                  Liste des productions
                </span>
            </div>
            {{-- <div class="flex items-center tooltip" data-tip="Nouveau production?">
                <label for="prod__modal" class="btn btn-primary modal-button">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 4v16m8-8H4" />
                    </svg>
                </label>
            </div> --}}
        </div>
    </div>

   {{-- modal add Prod --}}
   {{-- <input type="checkbox" id="prod__modal" class="modal-toggle">
   <div class="modal">
       <div class="modal-box max-w-3xl w-3xl">
          @livewire('productions.prod-form')
       </div>
   </div> --}}

   <div class="py-3 flex gap-2">
        <div class="form-control">
            <input wire:model.debounce.500ms='searchValue' type="text" placeholder="Recherche nom article ..." class="input input-bordered">
        </div>
        <div>
            <button wire:click='unsetServiceID' class="btn {{ 
                    ($ServiceIDActive == '*') ? 'btn-active' : 'btn-outline'
                }}">Tout</button>
        </div>
        @foreach ($services as $item)
            <div>
                <button wire:click='setServiceID({{ $item->ServiceID }})' class="btn {{ 
                        ($ServiceIDActive == $item->ServiceID) ? "btn-active" : "btn-outline"
                    }}">{{ $item->ServiceName }}</button>
            </div>
        @endforeach
   </div>

    {{-- lists of Prod --}}
    <div class="overflow-x-auto flex flex-col justify-center space-y-2">
        <div class="grid grid-cols-6 gap-1 w-full h-full">
            {{-- <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Num</div> --}}
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Service</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Article</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Qte dispo</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Qte produit</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Boite</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white w-3/4">Actions</div>
        </div>
        <div class="space-y-2">
            @foreach ($articleservice as $item)
                @livewire('productions.prod-grid-list', ['item' => $item], key($item["ArticleServiceID"]))
            @endforeach
        </div>
    </div>
</div>
