<div>
    {{-- @dd($articles) --}}
    <div>
        <p class="font-medium">Quels articles produit voulez-vous produire ?</p>
    </div>
    <div>
        {{-- input for select --}}
        <div class="grid grid-cols-2 gap-2">
            <div class="form-control">
                <label class="label">
                    <span class="label-text">Liste des articles</span>
                </label>
                <select wire:model="ArticleArticleID" id="ArticleArticleID"  class="select select-bordered w-full">
                    <option selected="selected">Article produit</option>
                    @foreach ($articles as $item)
                        <option value="{{$item["ArticleArticleID"]}}||{{$item["service"]["ServiceID"]}}">{{$item["article"]["ArticleName"]}} - {{$item["service"]["ServiceName"]}}</option>
                    @endforeach
                </select>
                @if ($errors->has('ArticleArticleID'))
                    <p class="text-danger">{{$errors->first('ArticleArticleID')}}</p>
                @endif
            </div>
    
            <div class="form-control">
                <label class="label">
                    <span class="label-text">Liste des boîtes</span>
                </label>
                    <select wire:model="BoxBoxID" id="BoxBoxID"  class="select select-bordered w-full">
                        <option  selected="selected">Boîtes</option>
                        @foreach ($boxes as $item)
                            @if ($boxes == NULL)
                                <option>Aucune boîte disponible</option>
                            @else
                                <option value="{{$item->BoxID}}">Boîte N°:{{$item->BoxNum}} / Cong N°:{{$item->FreezerFreezerID}} </option>
                            @endif
                        @endforeach
                    </select>
                    @if ($errors->has('BoxBoxID'))
                        <p class="text-danger">{{$errors->first('BoxBoxID')}}</p>
                    @endif
            </div>
        </div>

        {{-- input qty and date --}}
        <div class="grid grid-cols-2 gap-2">
            <div class="flex justify-center">
                <div class="form-control w-1/2">
                    <label class="label">
                        <span class="label-text">Quantité produite</span>
                    </label>
                    <input wire:model="ArticleBoxQty" placeholder="Qte" type="number" min="0" step=".01" class="input input-bordered">
                    @if ($errors->has('ArticleBoxQty'))
                        <a href="#" class="label-text-alt text-danger">{{$errors->first('ArticleBoxQty')}}</a>
                    @endif
                </div>
            </div>
    
            <div class="flex justify-center">
                <div class="form-control w-1/2">
                    <label class="label">
                    <span class="label-text">Date de production</span>
                    </label>
                    <input wire:model="RawMatServiceDate" type="date" class="input input-bordered">
                    @if ($errors->has('RawMatServiceDate'))
                        <a href="#" class="label-text-alt text-danger">{{$errors->first('RawMatServiceDate')}}</a>
                    @endif
                </div>
            </div>
        </div>

        <div class="modal-action flex justify-center gap-x-8">
            <button wire:click="save" class="btn btn-primary transition duration-700 ease-in-out">Ajouter</button>
            <button wire:click="clearVars" onclick="closeModal()" class="btn transition duration-700 ease-in-out">Fermer</button>
        </div>
    </div>
</div>
<script>
    function closeModal() {
        document.getElementById("prod__modal").checked = false;
    }
        window.addEventListener('closeProdModal', event => {
        document.getElementById("prod__modal").checked = false;
    })
        window.addEventListener('openProdModal', event => {
        document.getElementById("prod__modal").checked = true;
    })
        window.addEventListener('openProdDeleteModal', event => {
        document.getElementById("deleteProdModal").checked = true;
    })
        window.addEventListener('closeProdDeleteModal', event => {
        document.getElementById("deleteProdModal").checked = false;
    })
</script>
