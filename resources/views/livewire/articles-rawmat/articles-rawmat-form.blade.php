<div class="flex flex-col h-full min-h-full place-content-between max-h-96" x-data="{
    eventState : {{$eventState}} ,
    serviceState : {{$serviceState}} ,
    allState : {{$allState}},
    openEventTab:{{$EventEventID == null ? 'false' : 'true'}}
}">
    <div class="flex flex-row h-full overflow-y-hidden bg-red-900">

        <div class="w-full h-full max-h-full overflow-y-scroll bg-green-700">
            {{-- select article --}}
            <div class="flex gap-4">
                <div class="w-full form-control">
                    <label class="label">
                        <span class="text-xl font-bold label-text">Article</span>
                    </label>
                    <select wire:model="ArticleArticleID" class="w-full max-w-xs select select-bordered">
                        <option selected="selected" value="">Séléction un article</option>
                        @foreach ($articles as $item)
                            <option value="{{$item->ArticleID}}">{{$item->ArticleName}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('ArticleArticleID'))
                        <a href="#" class="label-text-alt text-danger">{{$errors->first('ArticleArticleID')}}</a>
                    @endif
                </div>
                <div class="w-full form-control">
                    <label class="label">
                        <span class="text-xl font-bold label-text">Classification</span>
                    </label>
                    <select wire:model='ArticleClassArticleClassID' class="w-full max-w-xs select select-bordered">
                        <option selected="selected" value="">Séléction classe</option>
                        @foreach ($articlesClasses as $item)
                            <option value="{{$item->ArticleClassID}}">{{$item->ArticleClassWording}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('ArticleClassArticleClassID'))
                        <a href="#" class="label-text-alt text-danger">{{$errors->first('ArticleClassArticleClassID')}}</a>
                    @endif
                </div>

            </div>

            {{-- add rawmat --}}
            <div>
                <div class="mt-5">
                    <h1 class="text-xl font-bold label-text">Matières première | Fournisseur | Prix | Unité</h1>
                </div>
                <div class="my-2 add-input">
                    <div class="flex items-center justify-around gap-2">
                            {{-- <input type="text" class="form-control" placeholder="Enter Name" wire:model="name.0"> --}}
                        <div class="w-full">
                            <select wire:model="RawMaterialRawMaterialID.0" class="w-full select select-bordered">
                                <option selected="selected" class="text-justify">Sélectionner la matière première</option>
                                @foreach ($rawmats as $rm)
                                {{-- @dd($rm->rawmatproviders) --}}
                                <option
                                value="{{$rm->rawmatproviders[0]->RawMatProviderID}}"
                                >

                                {{$rm->RawMaterialName}} | {{$rm->rawmatproviders[0]->provider->ProviderName}} |
                                        {{number_format((float)$rm->rawmatproviders[0]->RawMatProviderUnitPrice, 0, '.', ' ')}} Ar |
                                        {{$rm->RawMaterialUnity}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="flex justify-center">
                            <input wire:model="RawMatArticleQty.0" type="number" step=".1" min="0" placeholder="Qte" class="w-3/4 text-center input input-bordered">
                        </div>

                        <div class="flex justify-center w-1/5">
                            <button class="text-white btn btn-circle btn-info btn-sm" wire:click.prevent="add({{$i}})">
                                <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
                @foreach($inputs as $key => $value)
                    <div class="my-2 add-input">
                        <div class="flex items-center justify-around gap-2">
                            <div class="w-full">
                                <select wire:model="RawMaterialRawMaterialID.{{$value}}" class="w-full select select-bordered">
                                    <option selected="selected" class="text-justify">Sélectionner la matière première</option>
                                    @foreach ($rawmats as $rm)
                                        <option value="{{$rm->rawmatproviders[0]->RawMatProviderID}}">
                                            {{$rm->RawMaterialName}} | {{$rm->rawmatproviders[0]->provider->ProviderName}} |
                                            {{number_format((float)$rm->rawmatproviders[0]->RawMatProviderUnitPrice, 0, '.', ' ')}} Ar |
                                            {{$rm->RawMaterialUnity}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="flex justify-center">
                                <input wire:model="RawMatArticleQty.{{$value}}" type="number" step=".1" min="0" placeholder="Qte" class="w-3/4 text-center input input-bordered">
                            </div>

                            <div class="flex justify-center w-1/5">
                                <button class="btn btn-circle btn-danger btn-sm" wire:click.prevent="remove({{$key}})">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M18 12H6" />
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            {{-- title check --}}
            <div class="flex justify-center mt-5">
                <h1 class="text-2xl">Spécification</h1>
            </div>



            {{-- checkbox event && service --}}
            <div class="flex justify-around p-6">
                <div class="form-control">
                <label class="gap-4 cursor-pointer label">
                    <span class="label-text">Pour tout les services</span>
                    <input wire:click="eventToogle" x-on:click="@this.resetVarsToggle('all')" wire:model="toggleEvent" type="radio" name="opt" checked="checked" class="radio" value="all">
                </label>
                </div>
                <div class="form-control">
                <label class="gap-4 cursor-pointer label">
                    <span class="label-text">Pour une seul service</span>
                    <input wire:click="eventToogle" x-on:click="@this.resetVarsToggle('service')" wire:model="toggleEvent"  type="radio" name="opt" class="radio" value="service">
                </label>
                </div>
                <div class="form-control">
                <label class="gap-4 cursor-pointer label">
                    <span class="label-text">Pour évènement</span>
                    <input wire:click="eventToogle" x-on:click="@this.resetVarsToggle('event')" wire:model="toggleEvent" type="radio" name="opt" class="radio" value="event">
                </label>
                </div>
            </div>

            {{-- Select service && Event --}}
            <div class="flex justify-center">
                {{--ALL service --}}
                <div x-show="allState">
                    <div class="w-full form-control">
                        <label class="label">
                            <span class="label-text">Quantité max</span>
                        </label>
                        <input wire:model="AllServiceArticleQteMax" placeholder="Qte max" min="0" value="0"  type="number" class="input input-bordered">
                        @if ($errors->has('AllServiceArticleQteMax'))
                            <a href="#" class="label-text-alt text-danger">{{$errors->first('AllServiceArticleQteMax')}}</a>
                        @endif
                    </div>
                    <div class="w-full form-control">
                        <label class="label">
                            <span class="label-text">Prix</span>
                        </label>
                        <input wire:model="AllServiceArticlePrice" placeholder="Prix en TTC" type="number" min="0" value="0" class="input input-bordered">
                        @if ($errors->has('AllServiceArticlePrice'))
                            <a href="#" class="label-text-alt text-danger">{{$errors->first('AllServiceArticlePrice')}}</a>
                        @endif
                    </div>
                </div>
                {{-- select event --}}
                <div x-show="eventState" class="form-control">
                    <label class="label">
                    <span class="label-text">Pour évènement</span>
                    </label>
                    <select wire:model="EventEventID" class="w-full max-w-xs select select-bordered">
                        <option selected="selected" value="">Choisir un évènement</option>
                        @foreach ($events as $item)
                            <option value="{{$item->EventID}}">{{$item->EventName}} du {{date("d/m/y" , strtotime($item->EventBeginDate))}} au {{date("d/m/y" , strtotime($item->EventEndDate))}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('EventEventID'))
                        <a href="#" class="label-text-alt text-danger">Séléctionner un évènement</a>
                    @endif
                    <div class="form-control">
                        <label class="label">
                        <span class="label-text">Quantité</span>
                        </label>
                        <input wire:model='EventArticleQty' type="number" placeholder="Qte " value="0"  min="0" class="input input-bordered">
                        @if ($errors->has('EventArticleQty'))
                            <a href="#" class="label-text-alt text-danger">{{$errors->first('EventArticleQty')}}</a>
                        @endif
                    </div>
                    <div id="servicePrice" class="w-full form-control">
                        <label class="label">
                            <span class="label-text">Prix</span>
                        </label>
                        <input wire:model="EventArticleArticlePrice" placeholder="Prix en TTC" min="0" value="0"  type="number" class="input input-bordered">
                        @if ($errors->has('EventArticleArticlePrice'))
                            <a href="#" class="label-text-alt text-danger">{{$errors->first('EventArticleArticlePrice')}}</a>
                        @endif
                    </div>
                </div>

                {{-- select service --}}
                <div x-show="serviceState" class="form-control">
                    <label class="label">
                    <span class="label-text">Services</span>
                    </label>
                    <select wire:model="ServiceServiceID" class="w-full max-w-xs select select-bordered">
                        <option selected="selected" value="">Selection service</option>
                        @foreach ($services as $item)
                            <option value="{{$item->ServiceID}}">{{$item->ServiceName}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('ServiceServiceID'))
                        <a href="#" class="label-text-alt text-danger">Séléctionner une service</a>
                    @endif
                    <div class="form-control">
                        <label class="label">
                        <span class="label-text">Quantité Max</span>
                        </label>
                        <input wire:model='ArticleServiceMax' type="number" placeholder="Qte max" value="0"  min="0" class="input input-bordered">
                        @if ($errors->has('ArticleServiceMax'))
                            <a href="#" class="label-text-alt text-danger">{{$errors->first('ArticleServiceMax')}}</a>
                        @endif
                    </div>
                    <div class="w-full form-control">
                        <label class="label">
                            <span class="label-text">Prix</span>
                        </label>
                        <input wire:model="ArticleServiceArticlePrice" placeholder="Prix en TTC" value="0"  type="number" min="0" class="input input-bordered">
                        @if ($errors->has('ArticleServiceArticlePrice'))
                            <a href="#" class="label-text-alt text-danger">{{$errors->first('ArticleServiceArticlePrice')}}</a>
                        @endif
                    </div>
                </div>
            </div>

            {{-- table calcul begin --}}
            <div  class="flex justify-center w-full">
                <table class="table table-auto table-compact">
                    <thead>
                        {{-- <tr>
                            <th class="text-left">Prix TTC</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class="text-right"></th>
                        </tr> --}}
                        <tr>
                            <th class="text-left">Charge</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class="text-right">{{setMonneyMask($charge)}}</th>
                        </tr>
                        <tr>
                            <th class="text-left">Coefficient</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class="text-right">
                                {{$charge != 0 ? getPriceHT( $actualPrice ) / $charge : setMonneyMask(0)}}
                            {{-- {{$actualPrice}} --}}
                            </th>
                        </tr>
                        <tr>
                            <th class="text-left">Coût matiére</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class="text-right">{{$charge != 0  ? (getPriceHT( $actualPrice ) / $charge) * 100 : setMonneyMask(0)}}</th>
                        </tr>
                    </thead>
                </table>
            </div>
            {{-- table calcul end --}}
        </div>

        <div x-show="openEventTab" class="w-5/6 h-full overflow-y-scroll ">
            @livewire('events.event-detail',[
                'forEventView'=>false,
            ])
        </div>

    </div>

    {{-- btn --}}
    <div class="bottom-0 flex justify-center gap-4 mt-5">
        <button wire:click="save" class="btn btn-primary">Ajouter</button>
        <label wire:click="setData" for="articlesRawmat_modal{{$article != null ? $article['ArticleArticleID']:''}}" class="btn">Fermer</label>
    </div>
    <script>
        window.addEventListener('closeRwMatArtModal', event => {
            document.getElementById("articlesRawmat_modal").checked = false;
        })
    </script>
</div>
