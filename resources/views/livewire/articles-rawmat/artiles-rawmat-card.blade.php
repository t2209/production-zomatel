<div x-data>
    {{-- @dd($item) --}}
    <div class="w-full shadow-md stats">
        <div class="relative stat hover:bg-gray-200 {{$item['article']['ArticleIsArchived'] ? 'bg-gray-400': ''}}">
            <div class="stat-figure">
                <div>
                <div class="w-16 h-16 p-1">
                    <img src="{{url('storage/articlePhotos_thumb/' . $item['article']['ArticlePhoto'])}}" alt="Image article" class="mask mask-squircle">
                </div>
                </div>
            </div>
          <div class="font-bold">{{$item['article']['ArticleName']}}</div>
          {{-- event service --}}
          @if (array_key_exists('event',$item))
              <div class="badge badge-accent">{{$item['event']['EventName']}}</div>
          @endif
          @if (array_key_exists('service',$item))
              <div class="badge">{{$item['service']['ServiceName']}}</div>
          @endif
          {{-- @if ( array_key_exists('ServiceServiceID',$item) == null && $item['EventEventID'] == null)
              <div class="badge badge-primary">Génerale</div>
          @endif --}}

          {{-- classification --}}
        <div class="my-1 badge badge-outline badge-md">{{$item['article']['articleclass']['ArticleClassWording']}}</div>

          {{-- <div>{{getOwnerID($item)}}</div> --}}

          <div class="flex items-center my-2 font-semibold">{{
            number_format((float)getPrice($item), 0, '.', ' ')
          }} Ar</div>


          {{-- btn action --}}
          {{-- @dd( getClassInstance($item)) --}}
          <div class="grid grid-cols-2">
            <label wire:click="getRawMatArt({{ getCode($item) }})" for="ft_modal_{{getOwnerID($item)}}" class="inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-black transform hover:scale-125 modal-button">
                <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01" />
                </svg>
            </label>

            <label wire:click='setDataUp({{getCode($item)}})' for="up_modal_{{getOwnerID($item)}}" class="inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-black transform hover:scale-125 modal-button">
                <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                </svg>
            </label>

            <label for="dl_modal_{{getOwnerID($item)}}" class="inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-black transform hover:scale-125">
                <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                </svg>
            </label>

            <label for="articlesRawmat_modal{{$item['ArticleArticleID']}}" class="btn btn-primary modal-button">Duplicate</label>



            @if ($item['article']['ArticleIsArchived'] == false)
                @if ($item['ArticleQtyGen'] == 0)
                    <label for="ar_modal_{{getOwnerID($item)}}" class="inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-black transform hover:scale-125 modal-button">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13.875 18.825A10.05 10.05 0 0112 19c-4.478 0-8.268-2.943-9.543-7a9.97 9.97 0 011.563-3.029m5.858.908a3 3 0 114.243 4.243M9.878 9.878l4.242 4.242M9.88 9.88l-3.29-3.29m7.532 7.532l3.29 3.29M3 3l3.59 3.59m0 0A9.953 9.953 0 0112 5c4.478 0 8.268 2.943 9.543 7a10.025 10.025 0 01-4.132 5.411m0 0L21 21" />
                        </svg>
                    </label>
                @endif
            @else
                @if ($item['ArticleQtyGen'] != 0)
                    <label for="unar_modal_{{getOwnerID($item)}}" class="inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-black transform hover:scale-125 modal-button">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z" />
                        </svg>
                    </label>
                @endif
            @endif

          </div>

          {{-- Counter stock --}}
              <div class="absolute flex items-center justify-center w-8 h-8 font-bold shadow-inner top-2 right-2 badge badge-ghost badge-sm">
                  {{
                    array_key_exists('ArticleServiceQty',$item) ? $item['ArticleServiceQty'] :
                    (
                    array_key_exists('EventArticleQty',$item) ? $item['EventArticleQty']
                    :
                    0
                    )
                  }}
            </div>

        </div>
    </div>

    {{-- modal FT --}}
    <input type="checkbox" id="ft_modal_{{getOwnerID($item)}}" class="modal-toggle">
    <div class="modal">
        <div class="modal-box h-5/6 max-h-5/6" style="width: 75%; max-width: 75%;">
            <input class="hidden" wire:model="RawMatCode" type="text">
            <div class="flex flex-col h-full min-h-full place-content-between max-h-96">
                <div class="h-full max-h-full overflow-y-auto">
                    <div class="p-2 font-sans text-2xl font-semibold text-center">
                        <h1>{{$item['article']['ArticleName']}}</h1>
                    </div>
                    @if (isset($arrayFt) && isset($arraySum))
                        <div class="flex gap-2">
                            {{-- table rawmat --}}
                            <div class="w-3/4 overflow-x-auto">
                                <table class="table w-full table-compact">
                                    <thead>
                                        <tr>
                                            {{-- <th>ID</th> --}}
                                            <th>Matière première</th>
                                            <th class="text-center">Quantité</th>
                                            <th class="text-center">Unité</th>
                                            <th class="text-right">Prix d'achat</th>
                                            <th class="text-right">Prix de revient</th>
                                            <th class="text-left">Caté Famille</th>
                                            <th class="text-right">Ration</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- @for ($i = 0; $i < count($arrayFt); $i++) --}}
                                        @foreach ($arrayFt as $info)
                                        <tr>
                                            <td> {{ $info["Produit"] }} </td>
                                            <td class="text-center"> {{ $info["Quantité"] }} </td>
                                            <td class="text-center"> {{ $info["Unité"] }} </td>
                                            <td class="text-right"> {{ $info["PrixAchat"] }} </td>
                                            <td class="text-right"> {{ $info["PrixRevient"] }} </td>
                                            <td class="text-left"> {{ $info["Family"] }}    </td>
                                            <td class="text-right"> {{ round($info["Ratio"],2) }} %</td>
                                        </tr>
                                        @endforeach
                                        {{-- @endfor --}}
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Total</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th class="text-right">{{ number_format(floatval($arraySum["SumPrixRevient"]), 2, '.', ' ') . " Ar"}}</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                            {{-- table calcul --}}
                            <div  class="flex justify-center w-1/4">
                                <table class="table table-auto table-compact">
                                    <thead>
                                        <tr>
                                            <th class="text-left">Prix TTC</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th class="text-right">{{ $arraySum["PrixVenteTTC"] }}</th>
                                        </tr>
                                        <tr>
                                            <th class="text-left">Prix HT</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th class="text-right">{{ $arraySum["PrixVenteHT"] }}</th>
                                        </tr>
                                        <tr>
                                            <th class="text-left">Coefficient</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th class="text-right">{{ $arraySum["Coeff"] }}</th>
                                        </tr>
                                        <tr>
                                            <th class="text-left">Coût matiére</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th class="text-right">{{ $arraySum["CoutMat"] }}</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>

                        </div>

                    <div class="p-2 font-sans text-2xl font-semibold text-center">
                        <h1>Pourcentage de famille</h1>
                    </div>

                    <div class="w-full shadow stats">
                        @if ($arraySumAvg != null)
                            @for ($i = 0; $i < count($arraySumAvg); $i++)
                                <div class="stat place-items-center place-content-center">
                                    <div class="text-lg font-semibold">{{ $arraySumAvg[$i]["Family"] }}</div>
                                    <div class="text-2xl stat-value">{{ $arraySumAvg[$i]["Avg"] }} %</div>
                                </div>
                            @endfor
                        @endif
                    </div>

                    @else
                        <div class="flex items-center justify-center overflow-y-hidden h-5/6">
                            <img class="w-16 h-16" src="{{URL('/Images/loader.svg')}}" alt="">
                        </div>
                    @endif
                </div>


                <div class="bottom-0 flex justify-end mt-5">
                    <label for="ft_modal_{{getOwnerID($item)}}" class="btn">Fermer</label>
                </div>
            </div>

        </div>
    </div>

    {{-- modal delete --}}
    <input type="checkbox" id="dl_modal_{{getOwnerID($item)}}" class="modal-toggle">
    <div class="modal">
      <div class="modal-box">
        <p>Supprimer {{$item['article']['ArticleName']}}?</p>
        <div class="gap-2 modal-action">

          <label wire:click='deleteArticle({{getCode($item)}},{{
          array_key_exists('ArticleServiceID',$item) ? $item['ArticleServiceID'] :
            (
            array_key_exists('EventArticleID',$item) ? $item['EventArticleID']
            :
            0
            )
          }},"{{
            array_key_exists('ArticleServiceID',$item) ? "Service" :
            (
            array_key_exists('EventArticleID',$item) ? "Event"
            :
            0
            )
          }}")' class="btn btn-primary">Oui</label>

          <label for="dl_modal_{{getOwnerID($item)}}" class="btn">Non</label>

        </div>
      </div>
    </div>

    {{-- modal set archived --}}
    <input type="checkbox" id="ar_modal_{{getOwnerID($item)}}" class="modal-toggle">
    <div class="modal">
      <div class="modal-box">
        <p>Vous voulez archivé l'article : {{$item['article']['ArticleName']}} ?</p>
        <div class="gap-2 modal-action">
          <label wire:click='setArchived({{$item['article']['ArticleID']}},true)' for="ar_modal_{{getOwnerID($item)}}" class="btn btn-primary">Oui</label>
          <label for="ar_modal_{{getOwnerID($item)}}" class="btn">Non</label>
        </div>
      </div>
    </div>

    {{-- modal set unarchived --}}
    <input type="checkbox" id="unar_modal_{{getOwnerID($item)}}" class="modal-toggle">
    <div class="modal">
      <div class="modal-box">
        <p>Vous voulez réactiver l'article : {{$item['article']['ArticleName']}} ?</p>
        <div class="gap-2 modal-action">
          <label wire:click='setArchived({{$item['article']['ArticleID']}},false)' for="unar_modal_{{getOwnerID($item)}}" class="btn btn-primary">Oui</label>
          <label for="unar_modal_{{getOwnerID($item)}}" class="btn">Non</label>
        </div>
      </div>
    </div>


    {{-- modal update rawmatarticle --}}
    <input type="checkbox" id="up_modal_{{getOwnerID($item)}}" class="modal-toggle">
    <div class="modal">
        <div class="modal-box h-5/6 max-h-5/6" style="width: 75%; max-width: 75%;">

            <div class="flex flex-col h-full min-h-full place-content-between max-h-96">
                <div class="h-full max-h-full overflow-y-auto">
                    <div class="flex justify-center">
                        <h1 class="text-2xl font-semibold">Modification du fiche téchnique de l'article {{$item['article']['ArticleName']}}</h1>
                    </div>
                    {{-- table for rawmats --}}
                    @if (isset($arrayRawmats))
                    {{-- @dd($arrayRawmats) --}}

                        <div class="grid grid-cols-2 gap-10">
                            <div class="my-5">
                                <div class="flex space-x-2">
                                    <h1 class="text-lg font-semibold">Liste des matières premières</h1>
                                    <button wire:click='setFormAdd' class="btn btn-circle btn-outline btn-sm btn-primary">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 4v16m8-8H4" />
                                        </svg>
                                    </button>
                                </div>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            {{-- <th>Rawmatprovider ID (UP)</th> --}}
                                            <th class="text-left">Matières premières</th>
                                            <th class="text-center">Quantité(s)</th>
                                            <th class="text-center">Unité</th>
                                            <th>Prix d'achat</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($arrayRawmats as $rawMat)
                                        {{-- @dd($arrayRawmats) --}}
                                            <tr class="hover">
                                                {{-- <td> {{$arrayRawmats[$i]["RawmatId"]}} </td> --}}
                                                <td class="text-left"> {{$rawMat["Rawmat"]}} </td>
                                                <td class="text-center"> {{$rawMat["Qte"]}} </td>
                                                <td class="text-center"> {{$rawMat["Unity"]}} </td>
                                                <td> {{ setMonneyMask($rawMat["RawmatPrice"]) }} </td>
                                                <td class="text-center">
                                                    <button wire:click='setFormUp({{$rawMat["Id"]}},{{$rawMat["Qte"]}},"{{$rawMat["Rawmat"]}}","{{ $rawMat["Unity"] }}")' class="btn btn-outline btn-circle btn-sm">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z" />
                                                        </svg>
                                                    </button>
                                                    <button x-on:click="confirm('Supprimer {{$rawMat["Rawmat"]}}?') ? @this.delete({{$rawMat["Id"]}}) : false" class="btn btn-error btn-circle btn-outline btn-sm">
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                                                        </svg>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="my-5">
                                @if (isset($IdForUp))
                                    <div class="flex items-center justify-center h-full">
                                        <div class="flex flex-col space-y-5">
                                            <h1 class="text-xl font-semibold text-center">Modification de {{$RawmatName}}</h1>

                                            {{-- <div>
                                                <input wire:model='rawmatProdIdNotChange' type="text">
                                                <label>{{ $RawmatName }} | {{ $rawMatUnity }}</label>
                                            </div> --}}

                                            <select wire:model='RawmatproviderId' class="w-full max-w-xs select select-bordered">
                                                <option>Choisir un nouveau matière première</option>
                                                @foreach ($Rawmatproviders as $rm)
                                                {{-- @dd($rm) --}}
                                                    <option value="{{$rm->rawmatproviders[0]->RawMatProviderID}}">
                                                        {{$rm->RawMaterialName}} | {{$rm->rawmatproviders[0]->provider->ProviderName}} |
                                                        {{number_format((float)$rm->rawmatproviders[0]->RawMatProviderUnitPrice, 0, '.', ' ')}} Ar |
                                                        {{$rm->RawMaterialUnity}}
                                                    </option>
                                                @endforeach
                                            </select>

                                            <div class="flex justify-center">
                                                <div class="w-1/2 form-control">
                                                    <label class="label">
                                                        <span class="label-text">Quantité</span>
                                                    </label>
                                                    <input wire:model='RawmatproviderQte' type="text" placeholder="Quantité" class="input input-bordered">
                                                </div>
                                            </div>

                                            <div class="flex justify-center space-x-2">
                                                <button wire:click='updateData' class="btn btn-outline btn-sm btn-circle btn-success">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                                                    </svg>
                                                </button>

                                                <button wire:click='unsetFormUp' class="btn btn-outline btn-sm btn-circle">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                                                    </svg>
                                                </button>
                                            </div>
                                            <div wire:loading wire:target="updateData" class="flex justify-center">
                                                Mise à jour ...
                                            </div>
                                        </div>
                                    </div>
                                @else
                                @if (isset($CheckAdd))
                                    <div class="flex items-center justify-center h-full">
                                        <div class="flex flex-col space-y-5">
                                            <h1 class="text-xl font-semibold text-center">Ajout nouveau matière première</h1>

                                            <select wire:model='RawmatproviderIdAdd' class="w-full max-w-xs select select-bordered">
                                                <option selected>Choisir un nouveau matière première</option>
                                                @foreach ($Rawmatproviders as $rmAdd)
                                                    <option value="{{$rmAdd->rawmatproviders[0]->RawMatProviderID}}">
                                                        {{$rmAdd->RawMaterialName}} | {{$rmAdd->rawmatproviders[0]->provider->ProviderName}} |
                                                        {{number_format((float)$rmAdd->rawmatproviders[0]->RawMatProviderUnitPrice, 0, '.', ' ')}} Ar |
                                                        {{$rmAdd->RawMaterialUnity}}
                                                    </option>
                                                @endforeach
                                            </select>

                                            <div class="flex justify-center">
                                                <div class="w-1/2 form-control">
                                                    <label class="label">
                                                        <span class="label-text">Quantité</span>
                                                    </label>
                                                    <input wire:model='RawmatproviderQteAdd' type="text" placeholder="Quantité" class="input input-bordered">
                                                </div>
                                            </div>

                                            <div class="flex justify-center space-x-2">
                                                <button wire:click='addData' class="btn btn-outline btn-sm btn-circle btn-success">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                                                    </svg>
                                                </button>

                                                <button wire:click='unsetFormAdd' class="btn btn-outline btn-sm btn-circle">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                                                    </svg>
                                                </button>
                                            </div>
                                            <div wire:loading wire:target="addData" class="flex justify-center">
                                                Ajout ...
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="flex items-center justify-center h-full">
                                        <img class="w-16 h-16" src="{{URL('/Images/loaderForm.svg')}}" alt="">
                                    </div>
                                @endif
                                @endif
                            </div>
                        </div>
                    @else
                        <div class="flex items-center justify-center overflow-y-hidden h-5/6">
                            <img class="w-16 h-16" src="{{URL('/Images/loader.svg')}}" alt="">
                        </div>
                    @endif
                </div>

                <div class="bottom-0 flex justify-end mt-5">
                    <label for="up_modal_{{getOwnerID($item)}}" class="btn">Fermer</label>
                </div>
            </div>

        </div>
    </div>

    {{-- modal duplicate article begin --}}
    <input type="checkbox" id="articlesRawmat_modal{{$item['ArticleArticleID']}}" class="modal-toggle">
    <div class="modal">
        <div class="w-3/4 max-w-full max-h-full h-3/4 modal-box">
            @livewire('articles-rawmat.articles-rawmat-form',[
                'article'=>$item
            ])
    </div>
    </div>
    {{-- modal duplicate article end --}}

</div>
