<div>
    <div>

      @if (isset($articleFT) && isset($articleAR))
      <div class="flex flex-col h-screen max-h-screen min-h-screen place-content-between">

        {{-- list article --}}
        <div class="h-full max-h-full overflow-y-auto">
            <div class="flex items-center justify-center gap-4 my-5 text-2xl">
                <div class="text-3xl font-extrabold">
                  <span class="text-transparent bg-clip-text bg-gradient-to-r from-blue-400 to-blue-500">
                    Listes des articles avec F.T.
                  </span>
                </div>
                <label for="articlesRawmat_modal" class="btn btn-primary modal-button">Nouveau</label>
            </div>
            {{-- modal add boxes --}}
            <input type="checkbox" id="articlesRawmat_modal" class="modal-toggle">
            <div class="modal">
                <div class="max-w-3xl modal-box w-3xl">
                    @livewire('articles-rawmat.articles-rawmat-form')
            </div>
            </div>
            {{-- menu --}}
            <div class="flex justify-center py-4">
                <ul class="items-stretch px-3 shadow-lg menu bg-base-100 horizontal rounded-box">
                    <li class="{{ $EventTypeIDActive === '*' && $ServiceIDActive === '*'  ? 'bordered' : ''}}" wire:click.prevent="resetTypeFilter">
                      <a href="">Tout</a>
                    </li>
                    {{-- <li class="">
                      <a href="/articlesRawmat/class">Par Type</a>
                    </li> --}}
                    {{-- @dd($ServiceIDActive) --}}
                    <li class="{{$EventTypeIDActive === 'null' ? 'bordered' : ''}}" wire:click.prevent="onlyService">
                      <a href="">Par Service</a>
                    </li>
                    <li class="{{ $ServiceIDActive === 'null' ? 'bordered' : ''}}" wire:click.prevent="onlyEvent">
                      <a href="">Evenement</a>
                    </li>
                  </ul>
            </div>


          {{-- list of service and event type--}}
          <div>
            <button wire:click="resetFilter" class="btn {{$ServiceIDActive == "*" || $EventTypeIDActive =='*' ? 'btn-active' : 'btn-outline'}}">Tout </button>
            @foreach ($services as $index => $service)
                <button wire:click="setServiceFilter({{$service->ServiceID}})" class="btn {{$ServiceIDActive != $service->ServiceID ? 'btn-outline' :'btn-active'}} " :key="{{time().$index}}" >{{$service->ServiceName}}</button>
            @endforeach
            @foreach ($eventTypes as $index => $eventType)
                <button wire:click="setEventFilter({{$eventType->EventTypeID}})" wire:key="{{time().$index}}"  class="btn {{$EventTypeIDActive != $eventType->EventTypeID ? 'btn-outline' : 'btn-active'}} ">{{$eventType->EventTypeName}}</button>
            @endforeach
          </div>




            {{-- btn for filter per class --}}
            <div class="my-3">
                <button wire:click='setClassFilter("*")'
                    class="btn {{
                    $ClassIDActive == "*" ? 'btn-active' : 'btn-outline'}}"
                >
                    Tout les types
                </button>
                @foreach ($articleClasses as $articleClass)
                    <button wire:click='setClassFilter({{$articleClass->ArticleClassID}})' wire:key='"{{$articleClass->ArticleClassID}}' class="btn
                        {{$ClassIDActive == $articleClass->ArticleClassID ? 'btn-active' : 'btn-outline'}}
                        ">
                        {{$articleClass->ArticleClassWording}}
                    </button>
                @endforeach
            </div>

            <div class="w-1/3 my-5 form-control">
              <input wire:model='searchValue' placeholder="Recherche . . ." class="input input-bordered" type="text">
            </div>

            {{-- list of article with FT --}}
            {{-- @dd($articleFT) --}}
            <div class="grid grid-cols-4 gap-4">
                @foreach ($articleFT as $index => $item)
                    @livewire('articles-rawmat.artiles-rawmat-card',[
                        'item' => $item
                    ],key($index.getOwnerID($item).'article'))
                @endforeach
            </div>

          {{-- Heading  archived --}}
          <div class="my-3">
            <h1 class="font-sans text-2xl font-extrabold">Article archivé</h1>
          </div>
          {{-- list of archived --}}
          <div class="grid grid-cols-4 gap-4">
            @if (count($articleAR) == 0)
                <div>
                    <h1 class="font-semibold">Pas d'articles archivées</h1>
                </div>
            @else
              @foreach ($articleAR as $index => $item)
                  @livewire('articles-rawmat.artiles-rawmat-card',[
                      'item' => $item,
                  ],key($index.getOwnerID($item).'articleArchived'))
              @endforeach
            @endif
          </div>

        </div>

        {{-- avg --}}
        <div class="bottom-0 flex justify-center w-full gap-2 mt-5">
          {{-- avg family --}}
          <div class="mb-3 shadow stats">
            @if ($familyStat != null)
                @for ($i = 0; $i < count($familyStat); $i++)
                  <div class="stat place-items-center place-content-center">
                      <div class="text-lg font-semibold">{{ $familyStat[$i]["name"] }}</div>
                      <div class="flex flex-col text-2xl font-semibold stat-value">
                        {{ $familyStat[$i]["percentage"] }} %
                        <progress class="progress progress-primary" value="{{ $familyStat[$i]["percentage"] }}" max="100"></progress>
                      </div>
                  </div>
                @endfor
            @endif
          </div>
          {{-- stat articles --}}
          <div class="mb-3 shadow stats">
            @if ($articleStat != null)
              <div class="stat place-items-center place-content-center">
                  <div class="text-lg font-semibold">Coût Matière</div>
                  <div class="flex flex-col text-2xl font-semibold stat-value">
                    {{ $articleStat["CoutMat"] }}
                  </div>
              </div>
              <div class="stat place-items-center place-content-center">
                  <div class="text-lg font-semibold">Coefficient</div>
                  <div class="flex flex-col text-2xl font-semibold stat-value">
                    {{ $articleStat["Coeff"] }}
                  </div>
              </div>
            @endif
          </div>

          {{-- bon de commande --}}
          <div class="flex items-center gap-x-2">
              {{-- demande par service --}}
              {{-- <div class="flex items-center tooltip" data-tip="Commander?">
                  <label for="POModal" class="btn btn-success modal-button">Bon commmande</label>
              </div> --}}
              {{-- articels needed --}}
            <div class="flex items-center">
              <label for="NeededModal" class="btn btn-success modal-button">Demande Fournisseur</label>
            </div>

              <div class="flex items-center tooltip" data-tip="Appro?">
                  <label for="RQSModal" class="btn btn-info modal-button">Demande Service</label>
              </div>
          </div>

          <input type="checkbox" id="POModal" class="modal-toggle">
          <div class="modal">
            <div class="w-full h-full max-w-full max-h-full modal-box">
                @livewire('p-o-form', ['serviceID' => $ServiceIDActive], key('service'.$ServiceIDActive))
            </div>
          </div>

          <input type="checkbox" id="RQSModal" class="modal-toggle">
          <div class="modal">
            <div class="w-full h-full max-w-full max-h-full modal-box">
                @livewire('rqs-form', ['serviceID' => $ServiceIDActive], key('service1'.$ServiceIDActive))
            </div>
          </div>

          <input type="checkbox" id="NeededModal" class="modal-toggle">
          <div class="modal">
            <div class="modal-box" style="width: 95% ; max-width: 95%; height: 95%; max-height: 95%">
                {{-- @livewire('p-o-form', ['serviceID' => $ServiceIDActive], key('service'.$ServiceIDActive)) --}}
                @livewire('needed-articles.needed-articles-list', ['serviceID' => $ServiceIDActive], key('service2'.$ServiceIDActive))
            </div>
          </div>


        </div>

      </div>
      @else
        {{-- loading --}}
        Loading ...
      @endif

    </div>
</div>
