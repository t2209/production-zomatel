<div class="flex flex-col place-content-between h-full min-h-full max-h-full">
    <div class="text-4xl font-bold text-center mb-5">
        <span class="bg-clip-text text-transparent font-mono tracking-tight bg-gray-600">
          Liste des articles
        </span>
    </div>

    {{-- @dd($articles) --}}
    {{-- form componennt --}}
    <div class="overflow-y-auto h-full max-h-full space-y-2">
        <div class="grid grid-cols-8 space-x-1">
            <div class="{{$classGrid}} pl-2">Articles</div>
            <div class="{{$classGrid}} justify-center">Stock Logiciel</div>
            <div class="{{$classGrid}} justify-center">Stock Physique</div>
            <div class="{{$classGrid}} justify-center">Stock Final Week</div>
            <div class="{{$classGrid}} justify-center">Ecart</div>
            <div class="{{$classGrid}} justify-center">Max</div>
            <div class="{{$classGrid}} justify-center">Demande</div>
            <div class="{{$classGrid}} justify-center">Détails</div>
        </div>
        <div class="grid space-y-2">
            @foreach ($articles as $key => $article)
                @livewire('needed-articles.needed-articles-form',['article' => $article,'key'=>$key,'serviceID'=>$serviceID,'eventID'=>$eventID], key($article["ArticleServiceID"]))
            @endforeach
        </div>
    </div>
    
    <div class="bottom-0 flex justify-between gap-2">
        <div class="flex items-center space-x-2">
            <div>
                <h1 class="font-normal font-mono">Date du besoin</h1>
            </div>
            <div class="form-control">
                <input wire:model='POServDateNeed' class="input input-bordered" type="date">
            </div>
            @if ($errors->has('POServDateNeed'))
                <p class="text-danger">{{$errors->first('POServDateNeed')}}</p>
            @endif
        </div>
        <div>
            <button wire:click='saveRawmatrequire' class="btn btn-md btn-primary">Confirmer</button>
            <label for="NeededModal" class="btn btn-md">Fermer</label>
        </div>
    </div>
</div>
