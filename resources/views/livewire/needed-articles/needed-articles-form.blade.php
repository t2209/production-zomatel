<div class="grid grid-cols-8 space-x-1">
    {{-- @dd($article) --}}
    {{-- name --}}
    <div class="flex h-10 font-semibold items-center rounded-sm">{{$article['article']['ArticleName']}}</div>
    {{-- Stock log --}}
    <div class="flex justify-center"><input type="number" class="input input-bordered w-1/2 h-full text-center"></div>
    {{-- Stock physique --}}
    <div class="flex justify-center"><input wire:model='stockPhysique' type="number" class="input input-bordered w-1/2 h-full text-center"></div>
    {{-- Stock final week --}}
    <div class="flex justify-center"><input wire:model='sfWeek' type="number" class="input input-bordered w-1/2 h-full text-center"></div>
    {{-- Ecart --}}
    <div class="flex h-10 font-semibold justify-center items-center rounded-sm">
        {{
            $ecart = $stockPhysique - $sfWeek
        }}
    </div>
    {{-- Max --}}
    <div class="flex h-10 font-semibold justify-center items-center rounded-sm">{{$article['ArticleServiceMax']}}</div>
    {{-- Demande --}}
    <div class="flex h-10 font-semibold justify-center items-center rounded-sm">
        {{
            $dmd = ($article['ArticleServiceMax'] > $stockPhysique) ? $article['ArticleServiceMax'] - $stockPhysique : 0
        }}
    </div>
    {{-- modal --}}
    <div class="flex h-10 justify-center items-center rounded-sm">
        <label for="dl_{{getOwnerID($article)}}" class="btn btn-circle btn-outline btn-sm">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01" />
            </svg>
        </label>
    </div>

    {{-- modal table --}}
    <input type="checkbox" id="dl_{{getOwnerID($article)}}" class="modal-toggle"> 
    <div class="modal">
        <div class="modal-box w-4xl max-w-4xl">
            <div>
                @if (isset($articleRawmats))
                    <div class="grid grid-cols-5 space-x-1">
                        <div class="h-7 text-white font-semibold flex items-center pl-2 rounded-sm bg-gray-600">Libelle</div>
                        <div class="h-7 text-white font-semibold flex items-center justify-center pl-2 rounded-sm bg-gray-600">Qte</div>
                        <div class="h-7 text-white font-semibold flex items-center justify-end pr-2 rounded-sm bg-gray-600">Prix</div>
                        <div class="h-7 text-white font-semibold flex items-center justify-center pl-2 rounded-sm bg-gray-600">Qte necessaire</div>
                        <div class="h-7 text-white font-semibold flex items-center justify-end pr-2 rounded-sm bg-gray-600">Prix acheté</div>
                    </div>
                    @foreach ($articleRawmats as $item)
                    <div class="grid grid-cols-5 space-x-1">
                        <div class="h-7 font-semibold flex items-center pl-2 rounded-sm">{{$item->rawmaterialprovider->rawmaterial->RawMaterialName}}</div>
                        <div class="h-7 font-semibold flex items-center justify-center pl-2 rounded-sm">{{$item->RawMatArticleQty}}</div>
                        <div class="h-7 font-semibold flex items-center justify-end pr-2 rounded-sm">
                            {{number_format((float)$item->rawmaterialprovider->RawMatProviderUnitPrice, 0, '.', ' ')}} Ar
                        </div>
                        <div class="h-7 font-semibold flex items-center justify-center pl-2 rounded-sm">{{$item->RawMatArticleQty * $dmd}}</div>
                        <div class="h-7 font-semibold flex items-center justify-end pr-2 rounded-sm">
                            {{number_format((float)$item->rawmaterialprovider->RawMatProviderUnitPrice * ($item->RawMatArticleQty * $dmd), 0, '.', ' ')}} Ar
                        </div>
                    </div>
                    @endforeach
                @else
                    Chargement . . .
                @endif
            </div>
            <div class="modal-action">
                <label for="dl_{{getOwnerID($article)}}" class="btn btn-sm">Fermer</label>
            </div>
        </div>
    </div>
</div>
