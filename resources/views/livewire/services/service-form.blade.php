<div>
    <div class="grid gap-y-4">
        <div class="form-control">
            <label class="input-group input-group-vertical input-group-lg">
            <span>Nom du service : </span>
            <input placeholder="Service..." class="input input-bordered input-lg" wire:model='ServiceName' type="text">
                @if ($errors->has('ServiceName'))
                    <a href="#" class="label-text-alt text-danger">{{$errors->first('ServiceName')}}</a>
                @endif
            </label>
        </div>
    </div>
    <div class="modal-action flex justify-center gap-x-8">
        <button wire:click="save" class="btn btn-primary">Ajouter</button>
        <button wire:click="clearVars" onclick="closeModal()" class="btn">Fermer</button>
    </div>
</div>
<script>
    function closeModal() {
        document.getElementById("service__modal").checked = false;
    }
        window.addEventListener('closeServiceModal', event => {
        document.getElementById("service__modal").checked = false;
    })
        window.addEventListener('openServiceModal', event => {
        document.getElementById("service__modal").checked = true;
    })
        window.addEventListener('openServiceDeleteModal', event => {
        document.getElementById("deleteServiceModal").checked = true;
    })
        window.addEventListener('closeServiceDeleteModal', event => {
        document.getElementById("deleteServiceModal").checked = false;
    })
</script>
