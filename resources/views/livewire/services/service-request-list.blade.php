<div class="space-y-5 font-mono w-full mt-4 bg-white border border-gray-100 rounded-2xl p-10" x-data>
    {{-- Title --}}
    <div class="flex justify-center items-center">
        <div class="text-3xl font-bold">
            <span class="bg-clip-text text-transparent font-mono tracking-tight bg-gray-600">
              Listes des demandes des services
            </span>
        </div>
    </div>

    {{-- Filter week --}}
    <div class="flex justify-between">
        <div class="flex items-center space-x-2">
            <h1>{{ $weekShow }}</h1>
        </div>

        <div class="flex items-center space-x-2">
            <h1>Selectionner une semaine</h1>
            <input wire:change='unsetVars' wire:model="selectedWeek" type="week" class="input input-bordered">
            <button wire:click='getServiceOrder' class="btn btn-outline">Voir</button>
        </div>
    </div>

    {{-- Headind --}}
    <div>
        <h1>Services | Date</h1>
    </div>

    {{-- select services --}}
    <div>
        @if ($servicesID != null)
        <div class="grid grid-cols-4 space-x-2">
            {{-- @dd($servicesID) --}}
            @foreach ($servicesID as $item)
                <div class="bg-gray-50 p-5 rounded-md">
                    <div class="flex justify-between">
                        <div>
                            @if ($item->service != null)
                                <div class="stat-value text-gray-700">{{ $item->service->ServiceName }}</div> 
                            @endif
                            <div class="stat-desc font-semibold">{{ date("d/m/Y",strtotime($item->RawMatRequireServiceDate)) }}</div>
                            @if ($item->RawMatRequireServiceDateConfirm != null)
                                <div class="stat-desc font-semibold">Confirmer le {{ date("d/m/Y",strtotime($item->RawMatRequireServiceDateConfirm)) }}</div>
                            @endif
                        </div>

                        <div class="flex items-center gap-2">
                            <div class="stat-figure text-gray-600 hover:text-gray-700 transform hover:scale-125">
                                <label for="my-modal-2" wire:click='getServiceOrderRwmat({{ $item->ServiceServiceID }},"{{ $item->RawMatRequireServiceDate }}")'>
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01" />
                                    </svg>
                                </label>
                            </div> 
                            <div class="stat-figure text-gray-600 hover:text-gray-700 transform hover:scale-125
                            {{ 
                                ($item->RawMatRequireServiceDateConfirm != null) ? "hidden" : ""
                            }}">
                                <label x-on:click="confirm('Valider cette commande ?') ? @this.confirmOrder({{ $item->ServiceServiceID }},'{{ $item->RawMatRequireServiceDate }}') : false">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                                    </svg>
                                </label>
                            </div> 
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        @else
            Cliquez sur voir
        @endif
    </div>

    {{-- modal details --}}
    <input type="checkbox" id="my-modal-2" class="modal-toggle"> 
    <div class="modal">
        <div class="modal-box" style="width: 80%; max-width: 80%; height: 80%; max-height: 80%">
            <div class="h-full flex flex-col place-content-between">
                {{-- table grid --}}
                <div class="h-full max-h-full overflow-y-auto">
                    <div class="grid grid-cols-4 space-x-2">
                        <div class="h-10 rounded bg-marron_focus text-white flex items-center pl-2">Matières</div>
                        <div class="h-10 rounded bg-marron_focus text-white flex items-center justify-center">Quantité demandées</div>
                        <div class="h-10 rounded bg-marron_focus text-white flex items-center justify-center">Quantité disponible</div>
                        <div class="h-10 rounded bg-marron_focus text-white flex items-center justify-center">Action</div>
                    </div>
    
                    <div class="grid">
                        @if ($orderData != null)
                            @foreach ($orderData as $item)
                                @livewire('services.services-list-grid',[
                                    'rawmat' => $item
                                ],key($item->RawMatRequireServiceID))
                            @endforeach
                        @endif
                    </div>
                </div>

                <div class="bottom-0 flex justify-end mt-5">
                    <label wire:click='getServiceOrder' for="my-modal-2" class="btn">Fermer</label>
                </div>
            </div>
        </div>
    </div>
</div>
