<div class="grid grid-cols-4 space-x-2 my-2">
    <div class="{{ classGridBody() }} pl-2">{{ $rawmat->rawmaterial->RawMaterialName }}</div>
    <div class="{{ classGridBody() }} justify-center">{{ $rawmat->RawMatRequireServiceQtyNeeded }}</div>
    <div class="{{ classGridBody() }} justify-center">{{ $economatQte }}</div>
    <div class="flex justify-center">
        <button wire:click='validateServiceOrder' class="btn btn-success btn-sm">Valider</button>    
    </div>
</div>
