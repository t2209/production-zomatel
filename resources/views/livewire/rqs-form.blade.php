<div class="overflow-x-auto space-y-2">
    <div class="grid grid-cols-5 gap-1">
      <div class="{{$classGrid}}">Libélle</div>
      <div class="{{$classGrid}}">Famille</div>
      <div class="{{$classGrid}}">Unite</div>
      <div class="{{$classGrid}}">Demande</div>
      <div class="{{$classGrid}}">P.U</div>
    </div>

    <div class="grid space-y-2">
      @foreach ($rawMats as $key =>$rawMat)
        @livewire('raw-mat-require-service-form', ['rawMat' => $rawMat,'key'=>$key,'serviceID'=>$serviceID,'eventID'=>$eventID], key($rawMat->RawMatArticleID))
      @endforeach
    </div>

    <div class="form-control w-1/6 mx-3">
        <label class="label">
            <span class="label-text text-lg font-semibold">Date du demande</span>
        </label>
        <input type="date" wire:model='RawMatRequireServiceDate' class="input input-bordered">
        @if ($errors->has('RawMatRequireServiceDate'))
            <p class="text-danger">{{$errors->first('RawMatRequireServiceDate')}}</p>
        @endif
    </div>

    <div class="modal-action gap-2">
      <label wire:click='saveRQService' class="btn btn-primary">Confirmer</label>
      <label for="RQSModal" class="btn">Fermer</label>
    </div>
  </div>
