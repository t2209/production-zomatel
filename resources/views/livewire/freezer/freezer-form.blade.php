<div>
    <div class="grid grid-cols-2 gap-4">
        <div class="form-control">
            <label class="label">
              <span class="label-text">Numéro du congelateur</span>
            </label>
            <input wire:model="FreezerNum" type="text" placeholder="Numéro du congelateur" class="input input-bordered">
            <label class="label">
                @if ($errors->has('FreezerNum'))
                    <a href="#" class="label-text-alt text-danger">{{$errors->first('FreezerNum')}}</a>
                @else
                    <a href="" class="label-text-alt" style="color: transparent">Txt</a>
                @endif
            </label>
        </div>
        <div class="form-control">
            <label class="label">
                <span class="label-text">Capacité du congelateur</span>
            </label>
            <input wire:model="FreezerCapacity" type="number" placeholder="Capacité du congelateur" class="input input-bordered">
            <label class="label">
                @if ($errors->has('FreezerCapacity'))
                    <a href="#" class="label-text-alt text-danger">{{$errors->first('FreezerCapacity')}}</a>
                @endif
            </label>
        </div>
    </div>
    <div class="modal-action flex justify-center gap-x-8">
        <button wire:click="save" class="btn btn-primary">Ajouter</button>
        <button onclick="closeModal()" class="btn">Fermer</button>
    </div>
</div>
<script>
    function closeModal() {
        document.getElementById("my-modal-2").checked = false;
    }
    window.addEventListener('closeModal', event => {
        document.getElementById("my-modal-2").checked = false;
    })
    window.addEventListener('openModal', event => {
        document.getElementById("my-modal-2").checked = true;
    })
    window.addEventListener('openDeleteModal', event => {
        document.getElementById("deleteModal").checked = true;
    })
    window.addEventListener('closeDeleteModal', event => {
        document.getElementById("deleteModal").checked = false;
    })
</script>
