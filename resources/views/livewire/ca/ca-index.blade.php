<div class="space-y-10">

  {{-- filter service --}}
  <div>
    <div class="flex gap-1">
        <div class="form-control w-full max-w-xs">
          <label class="label">
            <span class="label-text">Séléctionner une service</span>
          </label>
          <select wire:model='selectedService' class="select select-bordered">
            <option value="*" selected>Tout</option>
            @foreach ($services as $service)
              <option value="{{ $service->ServiceName }}">{{ $service->ServiceName }}</option>
            @endforeach
          </select>
        </div>
    </div>
  </div>

  {{-- fliter date--}}
  <div class="grid grid-cols-3 gap-3">

    {{-- filter week --}}
    <div class="flex gap-1">
        <div class="form-control w-full max-w-xs">
            <label class="label">
              <span class="label-text">Séléctionnez une semaine</span>
            </label>
            <input wire:model='selectedWeek' type="week" placeholder="Type here" class="input input-bordered w-full max-w-xs">
        </div>
        <div class="flex items-end">
            <button wire:click='weekFilter("week")' class="btn {{ ($activeBtn == 'week') ? 'btn-active' : 'btn-outline' }}">Voir</button>
        </div>
    </div>
    
    {{-- filter month --}}
    <div class="flex gap-1">
        <div class="form-control w-full max-w-xs">
            <label class="label">
              <span class="label-text">Séléctionnez une mois</span>
            </label>
            <input wire:model='selectedMonth' type="month" placeholder="Type here" class="input input-bordered w-full max-w-xs">
        </div>
        <div class="flex items-end">
            <button wire:click='monthFilter("month")' class="btn {{ ($activeBtn == 'month') ? 'btn-active' : 'btn-outline' }}">Voir</button>
        </div>
    </div>
    
    {{-- filter year --}}
    <div class="flex gap-1">
        <div class="form-control w-full max-w-xs">
          <label class="label">
            <span class="label-text">Séléctionner une année</span>
          </label>
          <select wire:model='selectedYear' class="select select-bordered">
            <option selected>Année</option>
            @for ($i = 0; $i < count($years); $i++)
              <option value="{{ $years[$i] }}">{{ $years[$i] }}</option>
            @endfor
          </select>
        </div>
        <div class="flex items-end">
          <button wire:click='yearFilter("year")' class="btn {{ ($activeBtn == 'year') ? 'btn-active' : 'btn-outline' }}">Voir</button>
        </div>
    </div>
      
  </div>
    
  {{-- Error --}}
  <div class="flex justify-center">
      @error('selectedMonth') <span class="error text-danger">{{ $message }}</span> @enderror
      @error('selectedYear') <span class="error text-danger">{{ $message }}</span> @enderror
  </div>

  {{-- data --}}
  <div class="flex justify-center items-center">
    <div class="alert shadow-lg">
        <div>
          <div>
            <span class="text-xl">Valeur du CA du {{ $filterView }}</span>
            @if($selectedService != "*")
              <div class="flex justify-center">
                <span class="badge">{{ $selectedService }}</span>
              </div>
            @endif
            <h3 class="text-2xl font-bold text-center">{{ setMonneyMask($ca - ($ca * (0.2)))  }}</h3>
          </div>
        </div>
    </div>
  </div>

</div>
