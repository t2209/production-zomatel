    {{-- To attain knowledge, add things every day; To attain wisdom, subtract things every day. --}}
<div class="grid grid-cols-5 gap-1">
    {{-- @dd($rawMat) --}}
    <div class="{{$classGrid}}">
        {{$rawMat->rawmaterialprovider->rawmaterial->RawMaterialName}}
    </div>
    <div class="{{$classGrid}}">
        {{$rawMat->rawmaterialprovider->rawmaterial->rawmatfamily->RawMatWording}}
    </div>
    <div class="{{$classGrid}}">
        {{$rawMat->rawmaterialprovider->rawmaterial->RawMaterialUnity}}
    </div>
    <div>
        <input wire:model="qtyRawMatRequireServ" type="number" placeholder="Qté demandé" class="input input-bordered w-full h-full text-center">
    </div>
    <div class="{{$classGrid}}">
        {{ $unitPrice = $rawMat->rawmaterialprovider->RawMatProviderUnitPrice}}
    </div>
{{-- {{var_dump($qtyData)}} --}}
</div>

