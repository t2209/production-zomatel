<div class="grid grid-cols-4 space-x-2">
    {{-- @dd($rwmat) --}}
    <div class="bg-gray-100 h-10 flex items-center my-2 bordered pl-2">
        {{ $rwmat->rawmaterial->RawMaterialName }}
    </div>

    <div class="bg-gray-100 h-10 flex items-center my-2 bordered justify-center">
        {{ $rwmat->RawMatRequireQty }}
    </div>

    <div class="h-10 flex justify-center my-2">
        <input wire:model='qteReceived' type="number" class="input input-bordered w-1/2 h-full text-center" placeholder="0" min="0">
    </div>

    <div class="bg-gray-100 h-10 flex items-center my-2 bordered justify-center">
        {{ $rwmat->RawMatRequireQty - $qteReceived }}
    </div>
</div>
