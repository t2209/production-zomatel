<div class="space-y-5 font-mono w-full p-10 mt-4 bg-white border border-gray-200 rounded-xl">
    {{-- title --}}
    <div class="flex justify-center items-center">
        <div class="text-3xl font-bold">
            <span class="bg-clip-text text-transparent font-mono tracking-tight bg-gray-600">
              Arrivages achats
            </span>
        </div>
    </div>

    {{-- filter semaine --}}
    <div class="flex justify-between">
        <div class="flex items-center space-x-2">
            <h1>{{$weekView}}</h1>
            @if (!empty($POProdDateRecev))
                <div class="badge badge-warning">Déja confirmé</div>
            @endif
        </div>
        <div class="flex items-center space-x-2">
            <h1 class="font-normal font-mono">Selectionner une semaine</h1>
            <input wire:model='week' class="input input-bordered" type="week">
            <button wire:click='getRawMatList' class="btn">Voir</button>
        </div>
    </div>
    <div class="flex justify-end">
        @error('week') <span class="error text-danger">{{ $message }}</span> @enderror
    </div>
    {{-- notif date --}}
    <div>
        @if ($startWeek != null)
            <h1>Achats envoyées du {{date("d/m/Y",strtotime($startWeek))}} au {{date("d/m/Y",strtotime($endWeek))}}</h1>
        @endif
    </div>

    {{-- list of raw mats --}}
    <div class="flex justify-center">
        <div class="w-3/4">
            {{-- table --}}
            <div class="grid grid-cols-4 space-x-2">
                <div class="{{$classGrid}} justify-center pl-2 rounded-lg ">Produits</div>
                <div class="{{$classGrid}} justify-center rounded-lg">Quantités envoyées</div>
                <div class="{{$classGrid}} justify-center rounded-lg">Quantités reçues</div>
                <div class="{{$classGrid}} justify-center rounded-lg">Ecart</div>
            </div>

            <div>
                @if (!isset($POProd))
                    <div class="my-4 flex justify-center">
                        Selectionner une semaine
                    </div>
                @else
                    @if (count($POProd) == 0)
                        <div class="my-4 flex justify-center">
                            <h1>Pas de données pour cette semaine</h1>
                        </div>
                    @else
                        @foreach ($POProd as $item)
                            @foreach ($item as $key => $rwmat)
                                <div class="grid">
                                    @livewire('raw-mat-receive.raw-mat-receive-list-grid',[
                                        'rwmat' => $rwmat,
                                        'key' => $rwmat->RawMatRequireID
                                    ] , key($rwmat['RawMatRequireID']))
                                </div>
                            @endforeach
                        @endforeach
                    @endif
                @endif
            </div>
            {{-- footer --}}
            @if($POProd == null)
                <center class="text-danger">Aucune donnée ...</center>
            @else
                <div class="flex justify-center my-5">
                    <button wire:click='upQteRecev' class="btn btn-success">
                        {{ (!empty($POProdDateRecev)) ? "Modifier" : "Confirmer" }}
                    </button>
                </div>
            @endif
        </div>
    </div>
</div>
