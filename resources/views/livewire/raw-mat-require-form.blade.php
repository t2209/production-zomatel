<div class="grid grid-cols-12 gap-1">
    {{-- @dd($rawMatRequire)  --}}
    <div class="{{$classGrid}}">
        {{$rawMat->rawmaterialprovider->rawmaterial->RawMaterialName}}
    </div>
    <div class="{{$classGrid}}">
        {{$rawMat->rawmaterialprovider->provider->ProviderAdress}}
    </div>
    <div class="{{$classGrid}}">
        {{$rawMat->rawmaterialprovider->rawmaterial->rawmatfamily->RawMatWording}}
    </div>
    <div class="{{$classGrid}}">
        {{$rawMat->rawmaterialprovider->rawmaterial->RawMaterialUnity}}
    </div>
    <div class="{{$classGrid}}">
        {{ $serviceQty = count($rawMat->rawmaterialprovider->rawmaterial->services) > 0 ? $rawMat->rawmaterialprovider->rawmaterial->services[0]->pivot->RawMatServiceQty
            : 0 }}
    </div>
    <div class="{{$classGrid}}">
        {{-- !old code to get rawmat qty in economat --}}
        {{-- {{ $economatQty = count($rawMat->rawmaterialprovider->rawmaterial->rawmateconomats) > 0 ? $rawMat->rawmaterialprovider->rawmaterial->rawmateconomats->first()->RawMatEconomatQty : 0}} --}}
        {{-- @dd(getRawMatEconomatQty($rawMat->RawMaterialRawMaterialID)) --}}
        {{$economatQty = getRawMatEconomatQty($rawMat->RawMaterialRawMaterialID)}}
    </div>
    <div class="{{$classGrid}}">
        {{
            $maxQty =
            getArticle($rawMat->ArticleArticleID,$serviceID,$eventID)[0]['ArticleServiceMax']
            *
            getRawMat($rawMat->RawMaterialRawMaterialID,$rawMat->ArticleArticleID,$serviceID,$eventID)[0]['RawMatArticleQty']

        }}
    </div>
    <div>
        <input wire:model="rawMatRequire" type="number" placeholder="Qte demande" class="w-full h-full text-center input input-bordered">
    </div>
    <div class="{{$classGrid}}">
        {{
            $approQty = $maxQty - ($serviceQty + $economatQty) > 0 ? ($maxQty - ($serviceQty + $economatQty) + $rawMatRequire) : (0 + $rawMatRequire)
        }}
    </div>
    <div class="{{$classGrid}}">
        {{ $unitPrice = $rawMat->rawmaterialprovider->RawMatProviderUnitPrice}}
    </div>
    <div class="{{$classGrid}}">
        {{$unitPrice * $economatQty}}
    </div>
    <div class="{{$classGrid}}">
        {{$unitPrice * $approQty}}
    </div>
{{-- {{var_dump($qtyData)}} --}}
</div>
