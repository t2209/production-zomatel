{{-- If your happiness depends on money, you will never be happy with yourself. --}}
<div>
    <div class="flex justify-center items-center my-5">
        <div class="flex items-center gap-5">
            <div class="text-4xl font-bold">
                <span class="bg-clip-text text-transparent font-mono tracking-tight bg-gray-600">
                    Balance de compte des fournisseurs
                </span>
            </div>
        </div>
    </div>
    {{-- select date --}}
    <div class="flex justify-between">
        <div class="flex items-center space-x-2">
            <h1>Filtre date du </h1>
            <input wire:model="selectedDateStart" type="date" class="input input-bordered">
        </div>
        <div class="flex items-center space-x-2">
            <input wire:model="selectedDateEnd" type="date" class="input input-bordered">
            <button wire:click='getGrandBookList' class="btn btn-outline tooltip" data-tip="Voir le résultat">Voir</button>
        </div>
    </div>
    <div class="flex justify-between">
        @error('selectedDateStart') <span class="error text-danger">{{ $message }}</span> @enderror
        @error('selectedDateEnd') <span class="error text-danger">{{ $message }}</span> @enderror
    </div>

    <div class="flex justify-center space-x-2">
        @if ( $selectedDateStart == null | $selectedDateStart == "")
            @if ( $selectedDateEnd == null | $selectedDateEnd == "" )
            <h1 class="text-danger">Choississez l'intervalle de date ...</h1>
            @else
                <h1>Balance du - au {{ date("d/m/Y",strtotime($selectedDateEnd)) }}</h1>
            @endif
        @elseif( $selectedDateStart != null | $selectedDateStart != "" )
            @if(  $selectedDateEnd == null | $selectedDateEnd == "" )
                <h1>Balance du {{ date("d/m/Y",strtotime($selectedDateStart)) }} au - </h1>
            @else
                <h1>Balance du {{ date("d/m/Y",strtotime($selectedDateStart)) }} au {{ date("d/m/Y",strtotime($selectedDateEnd)) }}</h1>
            @endif
        @endif
    </div>

    {{-- list balance --}}
    <div class="overflow-x-auto flex justify-center">
        <div class="grid grid-cols-6 gap-1 w-full h-full">
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Code</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Fournisseurs</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Débit</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Crédit</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Solde Débit</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Solde Crédit</div>
                @foreach ($providers as $item)
                    <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded">{{$item->ProviderCode}}</div>
                    <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded">{{$item->ProviderName}}</div>
                    <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded"></div>
                    <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded"></div>
                    <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded"></div>
                    <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded"></div>
                @endforeach
        </div>
    </div>
</div>
