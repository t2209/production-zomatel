<div class="grid grid-cols-6 gap-2 my-2">
    {{-- collapse section --}}
    <div class="flex items-center h-full pl-2 ">
        <div class="border collapse w-96 rounded-box border-base-300 collapse-arrow">
            <input type="checkbox">
            <div class="text-xl font-medium collapse-title">
                {{ $item->rawmatprovider->provider->ProviderName}}
            </div>
            <div class="collapse-content">
              <ul>
                  @foreach ( getRawMatByProvider($weekDate,$item->rawmatprovider->provider->ProviderID)  as $item)
                    <li>{{$item->rawmatprovider->rawmaterial->RawMaterialName}} <span>{{$item->TotalQty.$item->rawmatprovider->rawmaterial->RawMaterialUnity." | ".setMonneyMask($item->TotalPrice) }}</span> </li>
                  @endforeach
              </ul>
            </div>
          </div>
    </div>
    {{-- <div class="flex items-center h-full pl-2 bg-gray-200 rounded">{{ $item["rawmaterial"]["rawmatproviders"][0]["provider"]["ProviderName"] }}</div> --}}
    <div class="flex items-center justify-end h-full pr-2 bg-gray-200 rounded">{{ setMonneyMask($item->TotalPrice) }}</div>
    <div class="flex flex-col items-center h-full">
        <select wire:model='item.PaymentMethPaymentMethID' class="w-full max-w-xs select select-bordered" {{$item->PaymentRef != null ? 'disabled readonly' : ''}}>
            <option selected="selected">Mode payement</option>
            @foreach (\App\Models\PaymentMethod::get() as $paymentMethod)
            <option value="{{$paymentMethod->PaymentMethID}}">{{$paymentMethod->PaymentMethWording}}</option>
            @endforeach
            {{-- <option value="1">Chèque</option> --}}
            {{-- <option value="2">Espèce</option> --}}
        </select>
        @error('paymentMode') <span class="text-red-600 error">{{ $message }}</span> @enderror
    </div>
    <div class="flex flex-col">
        <input wire:model='paymentFile' type="file" class="{{$item->PaymentRef == null ? '' : 'hidden'}}">
        @error('paymentFile') <span class="text-red-600 error">{{ $message }}</span> @enderror

        <div class="grid w-32 h-32 ">
            <a href="{{asset('storage/paymentAttach/'.$item->PaymentAttachement)}}" download class="text-lg underline">Download</a>
            <embed class="w-full h-full" name="plugin" src="{{asset('storage/paymentAttach/'.$item->PaymentAttachement)}}" type="application/pdf">

        </div>

    </div>
    <div>
        <input wire:model='item.PaymentRef' type="text" class="input input-bordered" placeholder="Référence du payement" {{$item->PaymentRef != null ? 'disabled readonly' : ''}} >
        @error('paymentRef') <span class="text-red-600 error">{{ $message }}</span> @enderror
    </div>
    <div class="flex justify-between h-full">
        <div class="flex justify-center w-1/2">
            <button wire:click='validatePayment' class="btn btn-sm {{$status}}">Valider</button>
        </div>
        <div class="flex justify-center w-1/2">
            <div class="btn btn-sm  {{$item->PaymentRef == null ? 'btn-disabled' : 'btn-disabled btn-success'}} btn-circle btn-outline">
                <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                </svg>
            </div>
        </div>
    </div>
</div>
