{{-- In work, do what you enjoy. --}}
<div>
    {{-- title --}}
    <div class="flex justify-center items-center">
        <div class="flex items-center gap-5">
            <div class="text-4xl font-bold">
                <span class="bg-clip-text text-transparent font-mono tracking-tight bg-gray-600">
                    Grand livre
                </span>
            </div>
        </div>
    </div>
    {{-- select week --}}
    <div class="flex justify-between">
            <div class="flex items-center space-x-2">
                {{-- <h1>Semaine</h1>
                <input wire:model="selectedWeek" type="week" class="input input-bordered">
                <button wire:click='getGrandBookList' class="btn btn-outline tooltip" data-tip="Voir le résultat de la semaine donnée">Voir</button> --}}
            </div>
            <div class="flex items-center space-x-2">
                {{-- Filter service--}}
                {{-- <div class="py-3 flex gap-2">
                    <div class="form-control">
                        <input wire:model.debounce.500ms='searchValue' type="text" placeholder="Nom de fournisseur ..." class="input input-bordered">
                    </div>
                    <div class="flex items-center space-x-2">
                        <button class="btn bg-transparent tooltip" data-tip="Voir">
                            <img src="https://img.icons8.com/cute-clipart/32/000000/search.png"/>
                        </button>
                    </div>
                </div> --}}
            </div>
    </div>
    <div class="flex justify-center">
        {{-- @error('selectedWeek') <span class="error text-danger">{{ $message }}</span> @enderror --}}
    </div>

    {{-- select date --}}
    <div class="flex justify-between px-5">
        <div class="flex items-center space-x-2">
            <h1>Filtre date du </h1>
            <input wire:model="startOfWeek" type="date" class="input input-bordered">
        </div>
        <div class="flex items-center space-x-2">
            <h1>au</h1>
            <input wire:model="endOfWeek" type="date" class="input input-bordered">
            <button wire:click='getGrandBookList' class="btn btn-outline tooltip" data-tip="Filtrer">Voir</button>
        </div>
    </div>
    <div class="flex justify-center">
        @error('startOfWeek') <span class="error text-danger">{{ $message }}</span> @enderror
    </div>
    <div class="flex justify-center">
        @error('endOfWeek') <span class="error text-danger">{{ $message }}</span> @enderror
    </div>

    <div class="flex justify-center space-x-2 my-5">
        @if ( $selectedDateStart == null | $selectedDateStart == "")
            @if ( $selectedDateEnd == null | $selectedDateEnd == "" )
                <h1 class="text-danger">Choississez l'intervalle de date</h1>
            @else
                <h1>Grand Livre du - au {{ date("d/m/Y",strtotime($endOfWeek)) }}</h1>
            @endif
        @elseif( $selectedDateStart != null | $selectedDateStart != "" )
            @if( $selectedDateEnd == null | $selectedDateEnd == "" )
                <h1>Grand Livre du {{ date("d/m/Y",strtotime($startOfWeek)) }} au - </h1>
            @elseif( $selectedDateEnd != null | $selectedDateEnd != "" && $selectedDateStart == $selectedDateEnd )
                <h1>Grand Livre du {{ date("d/m/Y",strtotime($selectedDateEnd)) }}</h1>
            @else
                <h1>Grand Livre du {{ date("d/m/Y",strtotime($selectedDateStart)) }} au {{ date("d/m/Y",strtotime($selectedDateEnd)) }}</h1>
            @endif
        @endif
    </div>

    {{-- <div class="flex justify-center space-x-2">
        @if ($selectedWeek == null | $selectedWeek == "")
        <h1 class="text-danger"> Choississez une semaine </h1>
        @else
            <h1>Grand Livre de la Semaine {{ $selectedWeek}}</h1>
        @endif
    </div> --}}

    {{-- grand-book --}}
    <div class="overflow-x-auto flex justify-center p-5">
        <div class="grid grid-cols-10 gap-1 w-full h-full">
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Service</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Produit</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Fournisseur</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Famille</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Quantité</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">P.U</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Mode de paiement</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Référence</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Débit</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Date d'achat</div>
                @if ( $startOfWeek == null | $startOfWeek == "")
                @if ( $endOfWeek == null | $endOfWeek == "" )
                    <h1 class="text-danger">...</h1>
                @else
                    <h1>...</h1>
                @endif
                @elseif( $startOfWeek != null | $startOfWeek != "" )
                    @if(  $endOfWeek == null | $endOfWeek == "" )
                        <h1>...</h1>
                    @else
                        @foreach ($rawmatreq as $itemreq)
                            <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded">{{$itemreq->purchaseorderservice->service->ServiceName}}</div>
                            <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded">{{$itemreq->rawmatprovider->rawmaterial->RawMaterialName}}</div>
                            <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded">{{$itemreq->rawmatprovider->provider->ProviderName}}</div>
                            <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded">{{$itemreq->rawmatprovider->rawmaterial->rawmatfamily->RawMatWording}}</div>
                            <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded"></div>
                            <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded">{{$itemreq->rawmatprovider->RawMatProviderUnitPrice}}</div>
                            <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded">{{$itemreq->paymentmethod->PaymentMethWording}}</div>
                            <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded">{{$itemreq->PaymentRef}}</div>
                            <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded"></div>
                            <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded">{{date("d/m/Y" , strtotime($itemreq->purchaseorderservice->purchaseorderprovider->POProvDateReceive))}}</div>
                        @endforeach
                    @endif
                @endif
        </div>
    </div>
</div>
