<div>
    <h1 class="text-3xl font-bold tracking-wide text-center text-gray-600">Payement des fournisseurs</h1>
    {{-- filter --}}
    <div class="flex justify-between">
        <div>

        </div>
        <div class="flex items-center gap-2">
            @error('weekBlade') <span class="text-red-700 error">{{ $message }}</span> @enderror
            <input wire:model='weekBlade' type="week" class="input input-bordered">
            <button wire:click='getDataOfWeek' class="btn btn-outline">Voir</button>
            {{-- not used anymore --}}
            {{-- <button wire:click='getDataOfWeekPerProd' class="btn btn-outline">Voir (Par fournisseur)</button> --}}
        </div>
    </div>
    {{-- data --}}
    <div class="grid grid-cols-6 gap-2 my-4">
        {{-- <div class="flex items-center h-10 pl-2 font-semibold text-white rounded bg-marron_focus">Produits</div> --}}
        <div class="flex items-center h-10 pl-2 font-semibold text-white rounded bg-marron_focus">Fournisseur</div>
        <div class="flex items-center justify-end h-10 pr-2 font-semibold text-white rounded bg-marron_focus">Montant</div>
        <div class="flex items-center justify-center h-10 font-semibold text-white rounded bg-marron_focus">Mode de payement</div>
        <div class="flex items-center justify-center h-10 font-semibold text-white rounded bg-marron_focus">Piece joint</div>
        <div class="flex items-center justify-center h-10 font-semibold text-white rounded bg-marron_focus">Réference</div>
        <div class="flex items-center justify-between h-10 font-semibold text-white rounded bg-marron_focus">
            <div class="flex justify-center w-1/2">Actions</div>
            <div class="flex justify-center w-1/2">Status</div>
        </div>
    </div>

    @if ($RawmatProd != null &&  count($RawmatProd) != 0 )
        @foreach ($RawmatProd as $item)
            @livewire('providers.provider-payment-grid',[
                "item" => $item,
                "weekDate"=>$weekDate
            ],key($item["RawMatRequireID"]))
        @endforeach
    @else
        <div class="font-mono text-lg tracking-wide text-center">Pas de paiement a effectuer</div>
    @endif
</div>
