<div>
    <div>
        <p class="font-medium">Veuillez remplir les formulaires pour un nouveau formulaire</p>
    </div>
    <div class="grid gap-y-8 gap-x-4">
        <div class="form-control">
            <label class="label">
            <span class="label-text">Code Fornisseur</span>
            </label>
            <input wire:model="ProviderCode" type="text" placeholder="Code d'identification" class="input input-bordered">
                @if ($errors->has('ProviderCode'))
                    <a href="#" class="label-text-alt text-danger">{{$errors->first('ProviderCode')}}</a>
                @endif

        </div>
        <div class="form-control">
            <label class="label">
            <span class="label-text">Nom du fournisseur</span>
            </label>
            <input wire:model="ProviderName" type="text" placeholder="Fournisseur" class="input input-bordered">
                @if ($errors->has('ProviderName'))
                    <a href="#" class="label-text-alt text-danger">{{$errors->first('ProviderName')}}</a>
                @endif
        </div>
        <div class="form-control">
            <label class="label">
            <span class="label-text">Adresse du fournisseur</span>
            </label>
            <input wire:model='ProviderAdress' class="input input-bordered">
                @if ($errors->has('ProviderAdress'))
                    <a href="#" class="label-text-alt text-danger">{{$errors->first('ProviderAdress')}}</a>
                @endif
        </div>
        <div class="form-control">
            <label class="input-group input-group-vertical input-group-lg">
            <span>Contact du fournisseur</span>
            <input placeholder="Téléphone" class="input input-bordered input-lg" wire:model='ProviderPhone' type="text">
                @if ($errors->has('ProviderPhone'))
                    <a href="#" class="label-text-alt text-danger">{{$errors->first('ProviderPhone')}}</a>
                @endif
            </label>
        </div>
    </div>
    <div class="modal-action flex justify-center gap-x-8">
        <button wire:click="save" class="btn btn-primary">Ajouter</button>
        <button wire:click="clearVars" onclick="closeModal()" class="btn">Fermer</button>
    </div>
</div>
<script>
    function closeModal() {
        document.getElementById("provider__modal").checked = false;
    }
        window.addEventListener('closeProviderModal', event => {
        document.getElementById("provider__modal").checked = false;
    })
        window.addEventListener('openProviderModal', event => {
        document.getElementById("provider__modal").checked = true;
    })
        window.addEventListener('openProviderDeleteModal', event => {
        document.getElementById("deleteProviderModal").checked = true;
    })
        window.addEventListener('closeProviderDeleteModal', event => {
        document.getElementById("deleteProviderModal").checked = false;
    })
</script>
