<div>
    {{-- @dd($providers) --}}
    {{-- title --}}
    <div class="flex justify-center items-center my-5">
        <div class="flex items-center gap-5">
            <div class="text-4xl font-bold">
                <span class="bg-clip-text text-transparent font-mono tracking-tight bg-gray-600">
                    Liste des fournisseurs
                </span>
            </div>
            <div class="flex items-center tooltip" data-tip="Nouveau fournisseur?">
                <label for="provider__modal" class="btn btn-primary modal-button">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                    </svg>
                </label>
            </div>
        </div>
    </div>

   {{-- modal add provider --}}
   <input type="checkbox" id="provider__modal" class="modal-toggle">
   <div class="modal">
       <div class="modal-box card">
          @livewire('providers.provider-form')
       </div>
   </div>

    {{-- delete modal --}}
    <input type="checkbox" id="deleteProviderModal" class="modal-toggle">
    <div class="modal">
        <div class="modal-box">
            <p>Vous voulez supprimer le fournisseur indiqué ?</p>
            <div class="modal-action">
            <button wire:click="deleteProvider" class="btn btn-primary mr-2">Oui</button>
            <label for="deleteProviderModal" class="btn">Non</label>
            </div>
        </div>
    </div>

    {{-- lists of providers --}}
    <div class="overflow-x-auto flex justify-center">
        <div class="grid grid-cols-5 gap-1 w-full h-full">
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Code</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Fournisseurs</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Adresse</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Téléphone</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white w-3/4">Actions</div>
            {{-- list --}}
                @foreach ($providers as $item)
                    <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded">{{$item->ProviderCode}}</div>
                    <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded">{{$item->ProviderName}}</div>
                    <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded">{{$item->ProviderAdress}}</div>
                    <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded">{{$item->ProviderPhone}}</div>
                    <div class="flex items-center h-10 justify-center text-center w-3/4">
                        <span
                            wire:click="selectProvider({{$item->ProviderID}},'update')"
                            class="cursor-pointer inline-flex items-center justify-center mx-1 px-2 py-1 text-xs font-bold leading-none transform hover:scale-125  tooltip bg-white border border-gray-100 hover:border-double hover:bg-transparent rounded" data-tip="update">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                            </svg>
                        </span>
                        <span
                            wire:click="selectProvider({{$item->ProviderID}},'delete')"
                            class="cursor-pointer inline-flex items-center justify-center mx-1 px-2 py-1 text-xs font-bold leading-none transform hover:scale-125  tooltip bg-white border border-gray-100 hover:border-double hover:bg-transparent rounded" data-tip="delete">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                            </svg>
                        </span>
                    </div>
                @endforeach
        </div>
    </div>
</div>

