<div>
    <div class="text-3xl font-bold">
        <span class="bg-clip-text text-transparent font-mono tracking-tight bg-gray-600">
            Gestion mode de payement
        </span>
    </div>

    {{-- layout --}}
    <div class="grid grid-cols-5">
        <div class="col-span-2">
            <h1>Formulaire</h1>
            <div class="flex flex-col gap-y-2">
                <div class="form-control">
                    <label class="label">
                        <span class="label-text">Méthode de payement</span>
                    </label> 
                    <input wire:model='PaymentMethWording' type="text" placeholder="Ex: BNI" class="input input-bordered">
                </div>
                <div>
                    @error('PaymentMethWording') <span class="error text-red-600">{{ $message }}</span> @enderror
                </div>
                <div class="flex justify-center gap-2">
                    <button wire:click='save("{{ $type }}")' class="btn btn-primary">Ajouter</button>
                    <button wire:click='cancel' class="btn">Annuler</button>
                </div>
            </div>
        </div>
        <div class="col-span-3">
            <h1>Liste</h1>
            <div class="overflow-x-auto flex justify-center">
                <table class="table w-2/3">
                    <tbody>
                        <tr>
                            <th></th> 
                            <th>Mode de paiement</th>
                            <th class="text-center">Action</th>
                        </tr>
                        @foreach ($payMeths as $key => $item)
                            <tr>
                                <th>{{ $key + 1 }}</th> 
                                <td>{{ $item->PaymentMethWording }}</td>
                                <td class="text-center">
                                    <button wire:click='getId({{ $item->PaymentMethID }},"{{ $item->PaymentMethWording }}")' class="btn btn-sm btn-warning">Modifier</button>
                                    <button class="btn btn-sm btn-error">Supprimer</button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
