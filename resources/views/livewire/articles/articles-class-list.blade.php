<div>
    <div class="flex justify-center items-center my-5">
        <div class="flex items-center gap-5">
            <div class="text-4xl font-bold">
                <span class="bg-clip-text text-transparent font-mono tracking-tight bg-gray-600">
                    Liste des classes des articles
                </span>
            </div>
            <div class="flex items-center tooltip" data-tip="Nouvelle boîte?">
                <label for="ArticlesClass__modal" class="btn btn-primary modal-button">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 4v16m8-8H4" />
                    </svg>
                </label>
            </div>
        </div>
    </div>

   {{-- modal add ArticlesClass --}}
   <input type="checkbox" id="ArticlesClass__modal" class="modal-toggle">
   <div class="modal">
       <div class="modal-box glass card">
          @livewire('articles.articles-class-form')
       </div>
   </div>

    {{-- delete modal --}}
    <input type="checkbox" id="deleteArticlesClassModal" class="modal-toggle">
    <div class="modal ">
        <div class="modal-box">
            <p>Vous voulez supprimer la classe d'article indiquée ?</p>
            <div class="modal-action gap-x-4">
            <button wire:click="deleteArticleClass" class="btn btn-primary">Oui</button>
            <label for="deleteArticlesClassModal" class="btn">Non</label>
            </div>
        </div>
    </div>
    {{-- lists of articlecalss --}}
    <div class="overflow-x-auto flex justify-center">
        <div class="grid grid-cols-3 gap-1 w-3/4 h-full">
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Numéro</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Libellé</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white w-3/4">Actions</div>
                @foreach ($articlesclass as $item)
                    <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded">{{$item->ArticleClassID}}</div>
                    <div class="flex items-center h-10 justify-center text-center bg-white border border-gray-100 hover:border-double rounded">{{$item->ArticleClassWording}}</div>
                    <div class="flex items-center h-10 justify-center text-center w-3/4">
                        <span
                            wire:click="selectArticleClass({{$item->ArticleClassID}},'update')"
                            class="cursor-pointer inline-flex items-center justify-center mx-1 px-2 py-1 text-xs font-bold leading-none transform hover:scale-125  tooltip bg-white border border-gray-100 hover:border-double hover:bg-transparent rounded" data-tip="Modifier?">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                            </svg>
                        </span>
                        <span
                            wire:click="selectArticleClass({{$item->ArticleClassID}},'delete')"
                            class="cursor-pointer inline-flex items-center justify-center mx-1 px-2 py-1 text-xs font-bold leading-none transform hover:scale-125  tooltip bg-white border border-gray-100 hover:border-double hover:bg-transparent rounded" data-tip="Supprimer?">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                            </svg>
                        </span>
                    </div>
                @endforeach
        </div>
    </div>
</div>
