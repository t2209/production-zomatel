<div class="w-full p-10 mt-4 bg-white border border-gray-100 rounded-2xl">

<div class="flex justify-center items-center lg:gap-5 my-3">
    <h1 class="lg:text-3xl lg:text-center">Liste des articles {{count($articles)}}</h1>
    <label for="articles_modal" class="btn btn-primary modal-button">Nouveau</label>
</div>

{{-- modal add boxes --}}
<input type="checkbox" id="articles_modal" class="modal-toggle">
<div class="modal">
    <div class="modal-box">
        <div class="avatar w-full flex justify-center">
            <div class="mb-8 rounded-full w-48 h-48 ring ring-yellow-700 ring-offset-base-100 ring-offset-2">
              <img id="imgPreview" src="{{URL('/Images/imgPreview.svg')}}">
            </div>
        </div>
        @livewire('articles.articles-form')
    </div>
</div>

<div class="flex justify-center flex-wrap gap-2">
    @foreach ($articles as $index => $item)
    {{-- @dd($item->ArticleID) --}}
        @livewire('articles.articles-list-card',[
            'item' => $item
        ], key($index.$item->ArticleID))
    @endforeach
</div>
</div>
