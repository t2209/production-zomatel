<div>

<div class="p-3">
    <div class="flex justify-center">
        @if ($ArticleName != null && $ArticlePurchasePrice != null )
            <div class="badge badge-lg">{{$ArticleName}} {{number_format((float)$ArticlePurchasePrice, 0, '.', ' ')}} Ar</div>
        @else
            
        @endif
    </div>

    <div class="grid grid-cols-2 gap-2">
        <div class="form-control">
            <label class="label">
                <span class="label-text">Nom de l'article</span>
            </label> 
            <input wire:model="ArticleName" type="text" placeholder="Nom de l'article" class="input input-bordered">
            @if ($errors->has('ArticleName'))
                <a href="#" class="label-text-alt text-danger">{{$errors->first('ArticleName')}}</a>
            @endif
        </div>
        <div class="form-control">
            <label class="label">
                <span class="label-text">Unité</span>
            </label> 
            <input wire:model="ArticleUnity" type="text" placeholder="Unité" class="input input-bordered">
            @if ($errors->has('ArticleUnity'))
                <a href="#" class="label-text-alt text-danger">{{$errors->first('ArticleUnity')}}</a>
            @endif
        </div>
    </div>

    <div class="grid grid-cols-2 gap-2">
        <div class="form-control">
            <label class="label">
                <span class="label-text">Code bar</span>
            </label> 
            <input wire:model="ArticleBarCode" type="text" placeholder="Code bar" class="input input-bordered">
            @if ($errors->has('ArticleBarCode'))
            <a href="#" class="label-text-alt text-danger">{{$errors->first('ArticleBarCode')}}</a>
            @endif
        </div>
    </div>
    <div class="flex justify-center mt-5">
        <div wire:loading wire:target='ArticlePhoto'>
            <img class="w-10 h-10" src="{{URL('/Images/loader.svg')}}" alt="">
        </div>
        <label class="label">
            <span class="label-text">Photo</span>
        </label> 
        <input wire:model="ArticlePhoto" type="file" onchange="readURL(this);">
        @if ($errors->has('ArticlePhoto'))
            <a href="#" class="label-text-alt text-danger">{{$errors->first('ArticlePhoto')}}</a>
        @endif
    </div>

    <div class="flex justify-center gap-4 my-3">
        <button wire:click="save" class="w-1/3 btn btn-primary">Ajouter</button>
        <button onclick="closeArticleModal()" class="w-1/3 btn btn-dark">Fermer</button>
    </div>
</div>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imgPreview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<script>
    function closeArticleModal() {
        document.getElementById("articles_modal").checked = false;
    }
    window.addEventListener('closeArticleModal', event => {
        document.getElementById("articles_modal").checked = false;
    })
    window.addEventListener('openArticleModal', event => {
        document.getElementById("articles_modal").checked = true;
    })
    window.addEventListener('openArticleDeleteModal', event => {
        document.getElementById("deleteArticleModal").checked = true;
    })
    window.addEventListener('closeArticleDeleteModal', event => {
        document.getElementById("deleteArticleModal").checked = false;
    })
</script>
</div>
