<div x-data="{firstState : true , delState : false}">
    {{-- @dd($item) --}}
    <div class="card w-72 h-48 box-content shadow-sm lg:card-side bg-gray-200 text-primary-content">
        <div class="card-body">
            <div class="grid grid-cols-2 text-gray-800">
                <div class="flex avatar justify-center">
                    <div class="rounded-full w-24 h-24 ring ring-blue-400 ring-offset-base-100 ring-offset-2">
                        <img src="{{url('articlePhotos_thumb/' . $item->ArticlePhoto)}}">
                    </div>
                </div>
                <div class="flex items-center mt-3">
                    <h1 class="text-center text-lg font-bold">{{$item->ArticleName}}</h1>
                    {{-- <div class="badge badge-lg">{{number_format((float)$item->ArticlePurchasePrice, 0, '.', ' ')}} Ar</div> --}}
                </div>
            </div>
            <div class="justify-center mb-5 card-actions">
                {{-- first state --}}
                <div x-show="firstState">
                    <span class="inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-gray-800 transform hover:scale-125">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                        </svg>
                    </span>
                    <span wire:click="selectArticle({{$item->ArticleID}},'update')" class="inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-gray-800 transform hover:scale-125">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                        </svg>
                    </span>
                    <span x-on:click="{delState = true , firstState = false}" class="inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-gray-800 transform hover:scale-125">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                        </svg>
                    </span>
                </div>
                {{-- delete state --}}
                <div x-show="delState" x-cloak style="display: none !important">
                    <span wire:click="delete({{$item->ArticleID}})" class="inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-gray-800 transform hover:scale-125">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                        </svg>
                    </span>
                    <span x-on:click="{delState = false , firstState = true}" class="inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-gray-800 transform hover:scale-125">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                        </svg> 
                    </span>                    
                </div>
            </div>
        </div>
    </div>  
    {{-- delete modal --}}
    <input type="checkbox" id="deleteArticleModal" class="modal-toggle"> 
    <div class="modal">
        <div class="modal-box">
            <p>Vous voulez supprimer cette article : {{$item->ArticleName}} ?</p> 
            <div class="modal-action">
            <button class="btn btn-primary">Oui</button> 
            <label for="deleteArticleModal" class="btn">Non</label>
            </div>
        </div>
    </div>
</div>
