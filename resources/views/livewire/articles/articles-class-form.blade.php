<div>
    <div class="grid grid-cols-1 gap-4">
        <div class="form-control">
            <label class="label">
                <span class="label-text">Ajoutez ici le type d'article</span>
            </label>
            <input wire:model="ArticleClassWording" type="text" placeholder="Libellé" class="input input-bordered">
            <label class="label">
                @if ($errors->has('ArticleClassWording'))
                    <a href="#" class="label-text-alt text-danger">{{$errors->first('ArticleClassWording')}}</a>
                @endif
            </label>
        </div>
    </div>
    <div class="modal-action flex justify-center gap-x-8">
        <button wire:click="save" class="btn btn-primary transition duration-700 ease-in-out">Ajouter</button>
        <button wire:click="clearVars" onclick="closeArticlesClassModal()" class="btn transition duration-700 ease-in-out">Fermer</button>
    </div>
</div>
<script>
    function closeArticlesClassModal() {
        document.getElementById("ArticlesClass__modal").checked = false;
    }
        window.addEventListener('closeArticlesClassModal', event => {
        document.getElementById("ArticlesClass__modal").checked = false;
    })
        window.addEventListener('openArticlesClassModal', event => {
        document.getElementById("ArticlesClass__modal").checked = true;
    })
        window.addEventListener('openArticlesClassDeleteModal', event => {
        document.getElementById("deleteArticlesClassModal").checked = true;
    })
        window.addEventListener('closeArticlesClassDeleteModal', event => {
        document.getElementById("deleteArticlesClassModal").checked = false;
    })
</script>


