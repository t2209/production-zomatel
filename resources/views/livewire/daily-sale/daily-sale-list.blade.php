<div class="space-y-4">

    {{-- date selection --}}
    <div class="flex justify-between gap-2">
        <div class="form-control w-full max-w-xs">
            <label class="label">
              <span class="label-text">Séléctionnez une date</span>
            </label>
            <input wire:model='selectedDate' type="date" placeholder="Type here" class="input input-bordered w-full max-w-xs">
        </div>
        {{-- Total --}}
        <div class="alert shadow-lg">
            <div>
              <div>
                <span class="text-xs">Valeur total du vente</span>
                <h3 class="font-bold text-center">{{ setMonneyMask($totalAllOrder) }}</h3>
              </div>
            </div>
        </div>
    </div>

    {{-- list of data --}}

    <div class="flex flex-wrap gap-2">
        @foreach ($orders as $key => $order)
            <div class="shadow stats">
                <div class="stat flex flex-col">
                    <div class="stat-value">{{ $order[0]->OrderNum }}</div>
                    {{-- {{ count($order) }} --}}
                    <div>
                        @for ($i = 0; $i < count($order) - 1; $i++)
                         <div class="stat-title font-semibold">Table n° {{ $order[$i]["TableNum"] }}</div>
                        @endfor
                    </div>
                    <div class="stat-desc py-3">
                        <label for="orderDtl_modal" wire:click='orderDetails({{ $order[0]->OrderID }},"{{ $order[0]->OrderNum }}")' class="btn btn-sm modal-button">Voir</label>
                        {{-- <button class="btn btn-success btn-sm">Fait</button> --}}
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    {{-- Modal details --}}
    <input type="checkbox" id="orderDtl_modal" class="modal-toggle">
    <div class="modal">
        <div class="modal-box space-y-3" style="width: 75%; max-width: 75%; height: 75%; max-height: 75%;">
            <div class="flex justify-center text-xl font-mono font-semibold">
                <h1>Liste des articles dans le commande : {{ $orderNum }}</h1>
            </div>
            @if ($orderID == null)
                <p>Chargement</p>
            @else
                {{-- @dd($articleOrders) --}}
                <div class="grid grid-cols-5 text-white gap-1">
                    <div class="bg-gray-600 p-2 rounded">Articles</div>
                    <div class="bg-gray-600 p-2 rounded text-right">Prix unitaire</div>
                    <div class="bg-gray-600 p-2 rounded text-center">Quantité commandée</div>
                    <div class="bg-gray-600 p-2 rounded text-right">Prix total</div>
                    <div class="bg-gray-600 p-2 rounded text-center">Statu</div>
                </div>

                @foreach ($articleOrders as $artOrder)
                    <div class="grid grid-cols-5 gap-1">
                        <div class="bg-gray-200 h-10 pl-2 flex items-center rounded">{{ $artOrder->articleService->article->ArticleName }}</div>
                        <div class="bg-gray-200 h-10 pr-2 flex items-center rounded justify-end">{{ setMonneyMask($artOrder->articleService->ArticleServiceArticlePrice) }}</div>
                        <div class="bg-gray-200 h-10 flex items-center rounded justify-center">{{ $artOrder->ArticleOrderQty }}</div>
                        <div class="bg-gray-200 h-10 pr-2 flex items-center rounded justify-end">{{ setMonneyMask($artOrder->articleService->ArticleServiceArticlePrice * $artOrder->ArticleOrderQty) }}</div>
                        <div class="h-10 text-white font-semibold flex items-center rounded justify-center" style="background-color: {{
                            ($artOrder->ArticleOrderStatus == 'done') ? '#009485' : 
                            (($artOrder->ArticleOrderStatus == 'pending') ? '#2094f3' :
                            (($artOrder->ArticleOrderStatus == 'canceled') ? '#ff9900' :
                            (($artOrder->ArticleOrderStatus == 'return') ? '#3d4451':
                            'gray')))
                        }}">
                            {{ $artOrder->ArticleOrderStatus }}
                        </div>
                    </div>
                @endforeach

                <div class="grid grid-cols-5 gap-1">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div class="bg-gray-200 h-10 pr-2 flex items-center rounded justify-end font-semibold">{{ setMonneyMask($totalOrder) }}</div>
                    <div></div>
                </div>
            @endif
            <div class="modal-action gap-x-4">
                <label wire:click='resetVars' for="orderDtl_modal" class="btn">Fermer</label>
            </div>
        </div>
    </div>
</div>
