<div>
    <h1 class="text-xl font-bold tracking-wide text-gray-600">Vente journalier</h1>
    <div class="flex gap-4">
        <div class="flex items-center gap-2">
            <input wire:model='excelDate' wire:change='unsetFile' type="date" class="input input-bordered">
            <input wire:model='excelFile' type="file">
            <button wire:click='import' class="btn btn-success">Importer</button>
        </div>
        <div>
            <p>@error('excelFile') <span class="error text-red-600">{{ $message }}</span> @enderror</p>
            <p>@error('excelDate') <span class="error text-red-600">{{ $message }}</span> @enderror</p>
        </div>
    </div>

    {{-- day's sale value --}}
    @if ($data != null)
    <div class="flex justify-between mt-3">
        <div class="font-semibold">
            Valeur du vente du {{ date("d/m/Y",strtotime($excelDate)) }} : <span class="text-lg font-bold">{{ setMonneyMask($daySaleValue) }}</span>
        </div>
        <div class="font-semibold">
            Quantité produit vendu du {{ date("d/m/Y",strtotime($excelDate)) }} : <span class="text-lg font-bold">{{ $qteSum }}</span>
        </div>
    </div>
    @endif

    {{-- data --}}
    <div class="grid grid-cols-8 gap-1 my-5">
        <div class="flex bg-marron_focus h-10 items-center text-white font-semibold pl-2">Id</div>
        <div class="flex bg-marron_focus h-10 items-center text-white font-semibold pl-2">Famille / Produit</div>
        <div class="flex bg-marron_focus h-10 items-center text-white font-semibold justify-center">Qte</div>
        <div class="flex bg-marron_focus h-10 items-center text-white font-semibold justify-end pr-2">Total TTC</div>
        <div class="flex bg-marron_focus h-10 items-center text-white font-semibold justify-end pr-2">Coût</div>
        <div class="flex bg-marron_focus h-10 items-center text-white font-semibold justify-end pr-2">Total remise</div>
        <div class="flex bg-marron_focus h-10 items-center text-white font-semibold justify-end pr-2">TTC remisé</div>
        <div class="flex bg-marron_focus h-10 items-center text-white font-semibold justify-end pr-2">Total HT</div>
    </div>
    @if ($data != null)
        @foreach ($data as $item)
            <div class="grid grid-cols-8 gap-1 my-1 {{ ($item['Total Qté'] == '') ? 'font-bold my-3' : '' }}">
                <div class="{{ classGridBody() }} h-full pl-2">{{ $item["Identifiant\n prod/fam"] }}</div>
                <div class="{{ classGridBody() }} h-full pl-2">{{ $item["Famille/Produit"] }}</div>
                <div class="{{ classGridBody() }} h-full justify-center">{{ $item["Total Qté"] }}</div>
                <div class="{{ classGridBody() }} h-full justify-end pr-2">{{ setMonneyMask($item["Total TTC"]) }}</div>
                <div class="{{ classGridBody() }} h-full justify-end pr-2">{{ setMonneyMask($item["Coût"]) }}</div>
                <div class="{{ classGridBody() }} h-full justify-end pr-2">{{ setMonneyMask($item["Total remise"]) }}</div>
                <div class="{{ classGridBody() }} h-full justify-end pr-2">{{ setMonneyMask($item["TTC remisé"]) }}</div>
                <div class="{{ classGridBody() }} h-full justify-end pr-2">{{ setMonneyMask($item["Total HT"]) }}</div>
            </div>
        @endforeach
        <div class="flex justify-center gap-2 my-3">
            <button wire:click='saveData' class="btn btn-success">Confirmer</button>
            <button wire:click="unsetFile" class="btn">Annuler</button>
        </div>
    @else
        <div class="flex items-center justify-center gap-1">
            <h1 class="text-lg font-sans">Selectionner une fichier à voir</h1>
            <img src="https://img.icons8.com/color/32/000000/ms-excel.png"/>
        </div>
    @endif
</div>
