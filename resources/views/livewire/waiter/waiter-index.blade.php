<div class="space-y-4">
    {{-- @dd($articles) --}}

    <div class="flex justify-center items-center">
        <span wire:click='setExistingOrderTable' class="label-text text-blue-700 underline {{ ($existingOrderTable) ? 'hidden' : '' }}">Ajout article pour une commande existant ?</span>
        <span wire:click='setNewOrderTable' class="label-text text-blue-700 underline {{ ($newOrderTable) ? 'hidden' : '' }}">Nouvelle commande ?</span>
    </div>

    {{-- New order --}}
    <div class="{{ ($newOrderTable) ? '' : 'hidden' }}">
        {{-- order --}}
        <div class="flex justify-center">
            <div class="form-control w-full max-w-xs">
                <label class="label">
                  <span class="label-text">N° du commande</span>
                </label>
                {{-- new order --}}
                <input wire:model='OrderNum' type="text" placeholder="N° Commande" class="input input-bordered w-full max-w-xs {{ ($newOrder) ? '' : 'hidden' }}">
                @error('OrderNum') <span class="error text-red-600 {{ ($newOrder) ? '' : 'hidden' }}">{{ $message }}</span> @enderror
                
                {{-- order existing --}}
                <div class="w-full {{ ($existingOrder) ? '' : 'hidden' }}">
                    <select wire:model='OrderID' class="w-full select select-bordered">
                        <option selected="selected" class="text-justify">Sélectionner une commande</option>
                        @foreach ($orders as $order)
                        <option value="{{ $order->OrderID }}">{{ $order->OrderNum }}</option>
                        @endforeach
                    </select>
                    @error('OrderID') <span class="error text-red-600 {{ ($existingOrder) ? '' : 'hidden' }}">{{ $message }}</span> @enderror
                </div>
                <label class="label">
                  <span wire:click='setNewOrder' class="label-text text-blue-700 underline {{ ($newOrder) ? 'hidden' : '' }}">Nouveau commande ?</span>
                  <span wire:click='setExistingOrder' class="label-text text-blue-700 underline {{ ($existingOrder) ? 'hidden' : '' }}">Commande existant mais pour autre table ?</span>
                </label>
            </div>
        </div>
    
        {{-- tables --}}
        <div class="flex justify-center">
            <div class="form-control w-full max-w-xs">
                <label class="label">
                    <span class="label-text">Liste des tables</span>
                </label>
                <select wire:model='TableID' class="select select-bordered">
                    <option selected>Séléctioner une table</option>
                    @foreach ($tables as $table)
                        <option value="{{ $table->TableID }}">Table n° {{ $table->TableNum }} | {{ $table->service->ServiceName }}</option>
                    @endforeach
                </select>
                @error('TableID') <span class="error text-red-600">{{ $message }}</span> @enderror
            </div>
        </div>
    </div>

    {{-- add new article to existing table and order --}}
    <div class="{{ ($existingOrderTable) ? '' : 'hidden' }}">
        <div class="flex justify-center">
            <div class="form-control w-full max-w-xs">

                <label class="label">
                  <span class="label-text">Liste des commandes avec leur table</span>
                </label>

                <select wire:model='existingOrderID' class="select select-bordered">
                  <option selected>Commande | Table</option>
                  @foreach ($orderTables as $item)
                      <option value="{{ $item->OrderOrderID }}">{{ $item->order->OrderNum }} | Table n° {{ $item->table->TableNum }}</option>
                  @endforeach
                </select>
                @error('existingOrderID') <span class="error text-red-600">{{ $message }}</span> @enderror

            </div>
        </div>
    </div>
    
    {{-- articles --}}
    <hr>
    <h1 class="text-center text-lg font-semibold">Articles</h1>
    <div class="flex justify-center my-2 add-input">
        <div class="flex items-center justify-around gap-2">
            <div class="w-full">
                <select wire:model='ArticleServiceID.0' class="w-full select select-bordered">
                    <option selected="selected" class="text-justify">Sélectionner l'article</option>
                    @foreach ($articles as $article)
                        <option value='{{ $article["ArticleServiceID"] }}'>{{ $article["article"]["ArticleName"] }} | Qte : {{ $article["ArticleServiceQty"] }}</option>
                    @endforeach
                </select>
            </div>

            <div class="flex justify-center">
                <input wire:model='ArticleOrderQty.0' type="number" step="1" min="0" placeholder="Qte" class="w-3/4 text-center input input-bordered">
            </div>

            <div class="flex justify-center w-1/5">
                <button class="text-white btn btn-circle btn-info btn-sm" wire:click.prevent="add({{$i}})">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                    </svg>
                </button>
            </div>
        </div>
    </div>
    @foreach($inputs as $key => $value)
        <div class="flex justify-center my-2 add-input">
            <div class="flex items-center justify-around gap-2">
                <div class="w-full">
                    <select wire:model='ArticleServiceID.{{ $value }}' class="w-full select select-bordered">
                        <option selected="selected" class="text-justify">Sélectionner l'article</option>
                        @foreach ($articles as $article)
                            <option value='{{ $article["ArticleServiceID"] }}'>{{ $article["article"]["ArticleName"] }} | Qte : {{ $article["ArticleServiceQty"] }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="flex justify-center">
                    <input wire:model='ArticleOrderQty.{{ $value }}' type="number" step="1" min="0" placeholder="Qte" class="w-3/4 text-center input input-bordered">
                </div>

                <div class="flex justify-center w-1/5">
                    <button class="btn btn-circle btn-danger btn-sm" wire:click.prevent="remove({{$key}})">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M18 12H6" />
                        </svg>
                    </button>
                </div>
            </div>
        </div>
    @endforeach

    {{-- Button submit --}}
    <div class="flex gap-2 justify-center">
        <button wire:click='save' class="btn btn-success">Ajouter le commande</button>
        <button wire:click='resetUI' class="btn">Effacer</button>
    </div>
</div>


