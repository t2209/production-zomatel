<div class="grid grid-cols-4" x-data>

    {{-- form --}}
    <div class="col-span-1">

        <div class="flex justify-center">
            <h1 class="text-2xl font-semibold">Formulaire</h1>
        </div>

        <div>
            <div class="flex gap-2">
                <div class="form-control w-full max-w-xs">
                    <label class="label">
                      <span class="label-text">N° du table</span>
                    </label>
                    <input wire:model='TableNum' type="number" min="1" max="10" placeholder="Numero" class="input input-bordered w-full max-w-xs">
                    @error('TableNum') <span class="error text-red-600">{{ $message }}</span> @enderror
                </div>
                <div class="form-control w-full max-w-xs">
                    <label class="label">
                      <span class="label-text">Service</span>
                    </label>
                    <select wire:model="ServiceServiceID" class="select select-bordered">
                      <option selected>Séléctioner un service</option>
                      @foreach ($services as $service)
                          <option value="{{ $service->ServiceID }}">{{ $service->ServiceName }}</option>
                      @endforeach
                    </select>
                    @error('ServiceServiceID') <span class="error text-red-600">{{ $message }}</span> @enderror
                </div>
            </div>
            <div class="p-2 flex gap-2 justify-center">
                <button wire:click='saveTable' class="btn btn-primary">
                    {{ 
                        ($action == "add") ? "Ajouter" : "Modifier"    
                    }}
                </button>
                <button wire:click='clrVars' class="{{ ($action == 'add') ? 'hidden' : '' }} btn">Annuler</button>
            </div>
        </div>

    </div>

    {{-- list --}}
    <div class="col-span-3 space-y-4">
        <div class="flex justify-center">
            <h1 class="text-2xl font-semibold">Liste des tables</h1>
        </div>

        <div class="overflow-x-auto flex justify-center">
            <table class="table w-2/3">
              <tbody>
                <tr>
                  <th></th>
                  <th>Numero du table</th>
                  <th>Service</th>
                  <th class="text-center">Action</th>
                </tr>
                @foreach ($tables as $key => $table)
                    <tr class="hover">
                        <td>{{ $key + 1 }}</td>
                        <td>Table n° {{ $table->TableNum }}</td>
                        <td>{{ $table->service->ServiceName }}</td>
                        <td class="text-center">
                            <button wire:click='setUpdate("update",{{ $table->TableID }},{{ $table->TableNum }},{{ $table->ServiceServiceID }})' class="btn btn-sm btn-warning">Modifier</button>
                            <button x-on:click="confirm('Suppimer la table {{ $table->TableNum }}') ?  @this.deteleTable({{ $table->TableID }}) : false" class="btn btn-sm btn-error">Supprimer</button>
                        </td>
                    </tr>
                @endforeach
              </tbody>
            </table>
        </div>
    </div>
</div>
