    {{-- If your happiness depends on money, you will never be happy with yourself. --}}
<div>
    {{-- caisse --}}
    <div class="card lg:card-side text-neutral-content justify-center">
        <div class="max-w-md card-body bg-gray-500">
            <div class="flex justify-center card-title">Fond actuel : {{date("d/m/Y")}}</div>
            <div class="justify-center card-actions">
                <div class="flex items-center tooltip" data-tip="Effectuer une entrée ou sortie de fond?">
                    <label for="transaction__modal" class="btn btn-primary modal-button btn-sm">
                        Ajouter une entrée ou sortie de fond
                    </label>
                </div>
            </div>
        </div>
    </div>

    {{-- modal add entry --}}
    <input type="checkbox" id="transaction__modal" class="modal-toggle">
    <div class="modal">
        <div class="modal-box card">
            @livewire('caisse.caisse-form')
        </div>
    </div>
    {{-- fund delete modal --}}
    <input type="checkbox" id="deleteFundModal" class="modal-toggle">
    <div class="modal ">
        <div class="modal-box">
            <p>Vous voulez supprimer l'information indiqué ?</p>
            <div class="modal-action gap-x-4">
            <button wire:click="deleteFund" class="btn btn-primary">Oui</button>
            <label for="deleteFundModal" class="btn">Non</label>
            </div>
        </div>
    </div>

    <div class="w-full p-10 mt-4 bg-white border border-gray-200 rounded-xl">
        {{-- liste --}}
        <div class="flex justify-center overflow-x-auto">
            <div class="grid w-full h-full grid-cols-6 gap-1">
                <div class="flex items-center justify-center h-10 text-center text-white rounded-lg bg-marron_focus">Sorties</div>
                <div class="flex items-center justify-center h-10 text-center text-white rounded-lg bg-marron_focus">Entrées</div>
                <div class="flex items-center justify-center h-10 text-center text-white rounded-lg bg-marron_focus">Observation</div>
                <div class="flex items-center justify-center h-10 text-center text-white rounded-lg bg-marron_focus">Fournisseur</div>
                <div class="flex items-center justify-center h-10 text-center text-white rounded-lg bg-marron_focus">Date</div>
                <div class="flex items-center justify-center w-3/4 h-10 text-center text-white rounded-lg bg-marron_focus">Actions</div>
                @foreach ($fund as $item)
                    <div class="flex items-center justify-center h-full text-center bg-white border border-gray-300 rounded hover:border-double">{{number_format((float)$item->Outflow, 0, '.', ' ')}} Ar</div>
                    <div class="flex items-center justify-center h-full text-center bg-white border border-gray-300 rounded hover:border-double">{{number_format((float)$item->Entry, 0, '.', ' ')}} Ar</div>
                    <div class="flex items-center justify-center h-full text-center bg-white border border-gray-300 rounded hover:border-double">{{$item->Observation}}</div>
                    <div class="flex items-center justify-center h-full text-center bg-white border border-gray-300 rounded hover:border-double">
                        @if ($item->ProviderProviderID == NULL)
                        -
                        @else
                        {{$item->Provider->ProviderName}}
                        @endif
                    </div>
                    <div class="flex items-center justify-center h-full text-center bg-white border border-gray-300 rounded hover:border-double">{{ date("d/m/Y", strtotime($item->created_at)) }}</div>
                    <div class="flex items-center justify-center w-3/4 h-full text-center">
                        <span
                            wire:click="selectFund({{$item->FundID}},'update')"
                            class="inline-flex items-center justify-center px-2 py-1 mx-1 text-xs font-bold leading-none transform bg-white border border-gray-300 rounded cursor-pointer hover:scale-125 tooltip hover:border-double hover:bg-transparent" data-tip="Modifier?">
                            <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                            </svg>
                        </span>
                        <span
                            wire:click="selectFund({{$item->FundID}},'delete')"
                            class="inline-flex items-center justify-center px-2 py-1 mx-1 text-xs font-bold leading-none transform bg-white border border-gray-300 rounded cursor-pointer hover:scale-125 tooltip hover:border-double hover:bg-transparent" data-tip="Supprimer?">
                            <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                            </svg>
                        </span>

                    </div>
                    @endforeach
            </div>
        </div>
    </div>

</div>
