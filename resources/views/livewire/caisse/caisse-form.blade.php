<div>
    <div>
        <p class="font-medium text-center underline">Remplissez le formulaire du type de fond</p>
    </div>

    <div class="grid grid-cols-1 gap-8">
        <div class="flex item-center justify-center h-min">
            <div class="form-control w-full max-w-xs">
                <label class="label">
                    <span class="label-text">Veuillez bien vérifier le type d'entrée</span>
                </label>
                <select wire:model="FundType" id="FundType"  class="select select-bordered w-full">
                    <option selected="selected">Type d'action</option>
                    <option value="Entry">Entrée de caisse</option>
                    <option value="Outflow">Sortie de caisse</option>
                </select>
                @if ($errors->has('FundType'))
                    <p class="text-danger">{{$errors->first('FundType')}}</p>
                @endif
            </div>
        </div>
        <div class="flex item-center justify-center h-min">
            <div class="form-control w-full max-w-xs">
                <label class="label">
                <span class="label-text">Montant de l'entrée</span>
                </label>
                <input wire:model="Fund" type="number" placeholder="Entrez le montant" class="input input-bordered">
                    @if ($errors->has('Fund'))
                        <a href="#" class="label-text-alt text-danger">{{$errors->first('Fund')}}</a>
                    @endif
            </div>
        </div>

        <div class="form-control">
            <label class="input-group input-group-vertical input-group-lg">
            <span>Obsérvation</span>
            <input placeholder="..." class="input input-bordered input-lg" wire:model='Observation' type="text">
                @if ($errors->has('Observation'))
                    <a href="#" class="label-text-alt text-danger">{{$errors->first('Observation')}}</a>
                @endif
            </label>
        </div>
    </div>

    <div class="modal-action flex justify-center gap-x-8">
        <button wire:click="save" class="btn btn-primary">Ajouter</button>
        <button wire:click="clearVars" onclick="closeModal()" class="btn">Fermer</button>
    </div>
</div>
<script>
    function closeModal() {
        document.getElementById("transaction__modal").checked = false;
    }
        window.addEventListener('closeTransactionModal', event => {
        document.getElementById("transaction__modal").checked = false;
    })
        window.addEventListener('openTransactionModal', event => {
        document.getElementById("transaction__modal").checked = true;
    })
        window.addEventListener('openFundDeleteModal', event => {
        document.getElementById("deleteFundModal").checked = true;
    })
        window.addEventListener('closeFundDeleteModal', event => {
        document.getElementById("deleteFundModal").checked = false;
    })
</script>



