<div class="grid grid-cols-4 gap-1">
    <div class="{{$classGrid}}">
        {{$item["event"]["EventName"]}}
    </div>

    <div class="{{$classGrid}}">
        {{$item["article"]["ArticleName"]}}
    </div>

    <div class="flex justify-center">
        <input wire:model="item.EventArticleQty" type="number" class="input input-bordered text-center h-full">
    </div>

    <div class="flex justify-center items-center w-3/4 gap-2">
        <button wire:click='setQty({{getCode($item)}})' class="btn btn-outline btn-circle btn-sm">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
            </svg>
        </button>
        <label for="ed_{{getOwnerID($item)}}" class="btn btn-outline btn-circle btn-sm">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01" />
            </svg>
        </label>
    </div>

    @if (session()->has('msg'))
    <div class="flex justify-center col-span-4">
        <div class="alert alert-warning">
            {{ session('msg') }}
        </div>
    </div>
    @endif

    {{-- modal event detail --}}
    <input type="checkbox" id="ed_{{getOwnerID($item)}}" class="modal-toggle"> 
    <div class="modal">
        <div class="modal-box space-y-2">
            <div>
                <h1 class="text-lg font-semibold underline">Détails du {{$item["event"]["EventName"]}}</h1>
                <p>{{$item["event"]["EventDetail"]}}</p>
            </div>

            <div>
                <h1 class="text-lg font-semibold underline">Date de l'évènement : </h1>
                <p>{{date("d/m/Y",strtotime($item["event"]["EventBeginDate"]))}} au
                    {{date("d/m/Y",strtotime($item["event"]["EventEndDate"]))}}</p>
            </div>
            <div class="modal-action">
                <label for="ed_{{getOwnerID($item)}}" class="btn">Fermer</label>
            </div>
        </div>
    </div>

</div>
