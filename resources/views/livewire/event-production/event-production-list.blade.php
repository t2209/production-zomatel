<div>
    {{-- @dd($eventarticles) --}}
    {{-- title --}}
    <div class="flex justify-center items-center my-5">
        <div class="flex items-center gap-5">
            <div class="text-4xl font-bold">
                <span class="bg-clip-text text-transparent font-mono tracking-tight bg-gray-600">
                  Liste des productions évènements
                </span>
            </div>
        </div>
    </div>

    <div class="w-1/3 py-3">
        <div class="form-control">
            <input wire:model.debounce.500ms='searchValue' type="text" placeholder="Recherche nom article ..." class="input input-bordered">
        </div>
   </div>

    {{-- lists of Prod --}}
    <div class="overflow-x-auto flex flex-col justify-center space-y-2">
        <div class="grid grid-cols-4 gap-1 w-full h-full">
            {{-- <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Num</div> --}}
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Evènement</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Article</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white">Qté</div>
            <div class="flex items-center justify-center text-center rounded-lg bg-marron_focus h-10 text-white w-3/4">Actions</div>
        </div>
        <div class="space-y-2">
            @foreach ($eventarticles as $item)
                @livewire('event-production.event-production-grid-list', ['item' => $item], key($item["EventArticleID"]))
            @endforeach
        </div>
    </div>
</div>
