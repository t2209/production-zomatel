{{-- Nothing in the world is as soft and yielding as water. --}}
<div
    x-data="{
        forEventView:@entangle('forEventView')
    }">
    <div>
        <p class="font-medium text-center">Details de l'evenement</p>
        {{-- lists of events --}}
    <div class="flex justify-center overflow-x-auto">
        <div class="grid w-full h-full grid-cols-4 gap-1">
            <div class="flex items-center justify-center h-10 text-center text-white rounded-lg bg-marron_focus">Article</div>
            <div class="flex items-center justify-center h-10 text-center text-white rounded-lg bg-marron_focus">Prix Unitaire</div>
            <div class="flex items-center justify-center h-10 text-center text-white rounded-lg bg-marron_focus">Qte</div>
            <div class="flex items-center justify-center h-10 text-center text-white rounded-lg bg-marron_focus">Total</div>
                @foreach ($articles as $article)
                    <div class="flex items-center justify-center h-full text-center bg-white border border-gray-100 rounded hover:border-double">{{$article['article']['ArticleName']}}</div>
                    <div class="flex items-center justify-center h-full text-center bg-white border border-gray-100 rounded hover:border-double">{{setMonneyMask($article['EventArticleArticlePrice'])}}</div>
                    <div class="flex items-center justify-center h-full text-center bg-white border border-gray-100 rounded hover:border-double">{{$article['EventArticleQty']}}</div>
                    <div class="flex items-center justify-center h-full text-center bg-white border border-gray-100 rounded hover:border-double">{{setMonneyMask($article['TotalPrice'])}}</div>

                @endforeach

                {{-- for local operation not saved in database in article for event creation begin--}}
                @if (!empty($tempArticle))
                    <div class="flex items-center justify-center h-full text-center bg-white border border-gray-100 rounded hover:border-double">{{$tempArticle['article']['ArticleName']}}</div>
                    <div class="flex items-center justify-center h-full text-center bg-white border border-gray-100 rounded hover:border-double">{{setMonneyMask($tempArticle['EventArticleArticlePrice'])}}</div>
                    <div class="flex items-center justify-center h-full text-center bg-white border border-gray-100 rounded hover:border-double">{{$tempArticle['EventArticleQty']}}</div>
                    <div class="flex items-center justify-center h-full text-center bg-white border border-gray-100 rounded hover:border-double">{{setMonneyMask($tempArticle['TotalPrice'])}}</div>
                @endif



                {{-- for local operation not saved in database in article for event creation end--}}
        </div>
    </div>

    {{-- {{$event['EventEventID']}} --}}
    <div class="flex flex-col justify-items-end ">
        <h3>Dépense supplémentaire : <strong>
            {{$event != null ?setMonneyMask($event['ExpenseSupp']):0}}
        </strong></h3>
        <h3>Total : <strong>{{setMonneyMask($total)}}</strong> </h3>
        <h3>PAF conseillé: <strong> {{$event != null ? setMonneyMask($total / $event['EventGuest']) :' 0'}}</strong></h3>
    </div>

    <div class="justify-center mb-3 shadow stats">
        @if ($familyStat != null)
            @for ($i = 0; $i < count($familyStat); $i++)
              <div class="stat place-items-center place-content-center">
                  <div class="text-lg font-semibold">{{ $familyStat[$i]["name"] }}</div>
                  <div class="flex flex-col text-2xl font-semibold stat-value">
                    {{ $familyStat[$i]["percentage"] }} %
                    <progress class="progress progress-primary" value="{{ $familyStat[$i]["percentage"] }}" max="100"></progress>
                  </div>
              </div>
            @endfor
        @endif
      </div>
    </div>
    <div x-show="forEventView" class="flex justify-end modal-action gap-x-8">
        {{-- <button wire:click="save" class="btn btn-primary">Ajouter</button> --}}
        <button  onclick="closeModalDetail()" class="btn">Fermer</button>
    </div>
</div>
<script>
    function closeModalDetail() {
        document.getElementById("eventDetail__modal").checked = false;
    }
        window.addEventListener('closeEventDetailModal', event => {
        document.getElementById("eventDetail__modal").checked = false;
    })
        window.addEventListener('openEventDetailModal', event => {
        document.getElementById("eventDetail__modal").checked = true;
    })
</script>



