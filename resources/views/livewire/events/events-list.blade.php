<div class="w-full p-10 mt-4 bg-white border border-gray-200 rounded-xl">
    {{-- @dd($event) --}}
    <div class="flex items-center justify-center my-5">
        <div class="flex items-center gap-5 tooltip" data-tip="Aller à type d'évènement">
            {{-- <a href="/type-events">Go</a> --}}
        </div>
        <div class="flex items-center gap-5">
            <div class="text-4xl font-bold">
                <span class="font-mono tracking-tight text-transparent bg-gray-600 bg-clip-text">
                    Liste des évènements
                </span>
            </div>
            <div class="flex items-center tooltip" data-tip="Nouvelle boîte?">
                <label for="event__modal" class="btn btn-primary modal-button">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z" clip-rule="evenodd" />
                    </svg>
                </label>
            </div>
        </div>
    </div>

    {{-- modal add boxes --}}
    <input type="checkbox" id="event__modal" class="modal-toggle">
    <div class="modal">
        <div class="modal-box card">
           @livewire('events.events-form')
        </div>
    </div>

    {{-- modal details begin --}}
    <input type="checkbox" id="eventDetail__modal" class="modal-toggle">
    <div class="max-w-full modal">
        <div class="max-w-5xl modal-box card">
           @livewire('events.event-detail')
        </div>
    </div>
    {{-- modal details end --}}


    {{-- delete modal --}}
    <input type="checkbox" id="deleteEventModal" class="modal-toggle">
    <div class="modal ">
        <div class="modal-box">
            <p>Vous voulez supprimer l'évènement indiqué ?</p>
            <div class="modal-action gap-x-4">
            <button wire:click="deleteEvent" class="btn btn-primary">Oui</button>
            <label for="deleteEventModal" class="btn">Non</label>
            </div>
        </div>
    </div>

    {{-- lists of events --}}
    <div class="flex justify-center overflow-x-auto">
        <div class="grid w-full h-full grid-cols-6 gap-1">
            <div class="flex items-center justify-center h-10 text-center text-white rounded-lg bg-marron_focus">Evènement</div>
            <div class="flex items-center justify-center h-10 text-center text-white rounded-lg bg-marron_focus">Type</div>
            <div class="flex items-center justify-center h-10 text-center text-white rounded-lg bg-marron_focus">Début</div>
            <div class="flex items-center justify-center h-10 text-center text-white rounded-lg bg-marron_focus">Fin</div>
            <div class="flex items-center justify-center h-10 text-center text-white rounded-lg bg-marron_focus">Invites</div>
            <div class="flex items-center justify-center w-3/4 h-10 text-center text-white rounded-lg bg-marron_focus">Actions</div>
                @foreach ($events as $item)
                <div class="flex items-center justify-center h-full text-center bg-white border border-gray-300 rounded hover:border-double">{{$item->EventName}}</div>
                <div class="flex items-center justify-center h-full text-center bg-white border border-gray-300 rounded hover:border-double">{{$item->eventtype->EventTypeName}}</div>
                <div class="flex items-center justify-center h-full text-center bg-white border border-gray-300 rounded hover:border-double">{{date("d/m/y" , strtotime($item->EventBeginDate))}}</div>
                <div class="flex items-center justify-center h-full text-center bg-white border border-gray-300 rounded hover:border-double">{{date("d/m/y" , strtotime($item->EventEndDate))}}</div>
                <div class="flex items-center justify-center h-full text-center bg-white border border-gray-300 rounded hover:border-double">{{$item->EventGuest}}</div>
                <div class="flex items-center justify-center w-3/4 h-full text-center">
                    {{-- view detail button begin --}}
                    <span
                    wire:click="selectEvent({{$item->EventID}},'details')"
                        class="inline-flex items-center justify-center px-2 py-1 mx-1 text-xs font-bold leading-none transform bg-white border border-gray-300 rounded cursor-pointer hover:scale-125 tooltip hover:border-double hover:bg-transparent" data-tip="Voir Articles">
                        Voir
                        {{-- <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                    </svg> --}}
                    </span>
                    {{-- view detail button end --}}

                    <span
                        wire:click="selectEvent({{$item->EventID}},'update')"
                        class="inline-flex items-center justify-center px-2 py-1 mx-1 text-xs font-bold leading-none transform bg-white border border-gray-300 rounded cursor-pointer hover:scale-125 tooltip hover:border-double hover:bg-transparent" data-tip="Modifier?">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z" />
                        </svg>
                    </span>
                    <span
                        wire:click="selectEvent({{$item->EventID}},'delete')"
                        class="inline-flex items-center justify-center px-2 py-1 mx-1 text-xs font-bold leading-none transform bg-white border border-gray-300 rounded cursor-pointer hover:scale-125 tooltip hover:border-double hover:bg-transparent" data-tip="Supprimer?">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                        </svg>
                    </span>

                </div>
                @endforeach
        </div>
    </div>
</div>
