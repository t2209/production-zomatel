<div>

    <div>
        <p class="font-medium text-center text-danger">Remplissez le formulaire pour ajoutez un nouveau type d'evènement</p>
    </div>
    <div class="grid grid-cols-1 gap-8">
        <div class="form-control">
            <label class="label">
            <span class="label-text">Type d'évènement</span>
            </label>
            <input wire:model="EventTypeName" type="text" placeholder="Type d'évènement" class="input input-bordered">
                @if ($errors->has('EventTypeName'))
                    <a href="#" class="label-text-alt text-danger">{{$errors->first('EventTypeName')}}</a>
                @endif
        </div>
    </div>
    <div class="modal-action flex justify-center gap-x-8">
        <button wire:click="save" class="btn btn-primary">Ajouter</button>
        <button wire:click="clearVars" onclick="closeEventTypeModal()" class="btn">Fermer</button>
    </div>
</div>
<script>
    function closeEventTypeModal() {
        document.getElementById("event_type__modal").checked = false;
    }
        window.addEventListener('closeEventTypeModal', event => {
        document.getElementById("event_type__modal").checked = false;
    })
        window.addEventListener('openEventTypeModal', event => {
        document.getElementById("event_type__modal").checked = true;
    })
        window.addEventListener('openEventTypeDeleteModal', event => {
        document.getElementById("deleteEventTypeModal").checked = true;
    })
        window.addEventListener('closeEventTypeDeleteModal', event => {
        document.getElementById("deleteEventTypeModal").checked = false;
    })
</script>


