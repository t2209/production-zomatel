<div>
       {{-- @dd($eventArticle) --}}
       <div class="grid gap-2 grid-cols-2">
        <div class="flex justify-end items-center">
            <h1>Liste des articles de l'évènement</h1>
        </div>
        <div class="flex items-stretch">
            <label for="artevent__modal" class="btn btn-primary modal-button self-center tooltip tooltip-right" data-tip="Ajouter un article dans l'évènement?">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z" clip-rule="evenodd" />
                </svg>
            </label>
        </div>
    </div>

   {{-- modal add eventArticle --}}
   <input type="checkbox" id="artevent__modal" class="modal-toggle">
   <div class="modal">
       <div class="modal-box">
          @livewire('events.event-articles-form')
       </div>
   </div>

    {{-- delete modal --}}
    <input type="checkbox" id="deleteArtEventModal" class="modal-toggle">
    <div class="modal ">
        <div class="modal-box">
            <p>Vous voulez supprimer l'article ajouté ?</p>
            <div class="modal-action">
            <button wire:click="deleteArtEvent" class="btn btn-primary mr-2">Oui</button>
            <label for="deleteArtEventModal" class="btn">Non</label>
            </div>
        </div>
    </div>
</div>
