<div>

    <div>
        <p class="font-medium text-center">Remplissez le formulaire pour ajoutez un evènement</p>
    </div>
    <div class="grid grid-cols-1 gap-8">
        <div class="form-control">
            <label class="label">
            <span class="label-text">Evènement</span>
            </label>
            <input wire:model="EventName" type="text" placeholder="Nom de l'évènement" class="input input-bordered">
                @if ($errors->has('EventName'))
                    <a href="#" class="label-text-alt text-danger">{{$errors->first('EventName')}}</a>
                @endif
        </div>
    </div>
    <div class="grid grid-cols-1 gap-4">
        <div class="form-control">
            <label class="label">
                <span class="label-text">Type d'évènement</span>
            </label>
            <select wire:model="EventTypeEventTypeID" id="EventTypeEventTypeID"  class="select select-bordered w-full">
                <option selected="selected">Type d'évènement</option>
                @foreach ($eventtypes as $item)
                    <option value="{{$item->EventTypeID}}">{{$item->EventTypeName}} </option>
                @endforeach
            </select>
            <label class="label">
                <label class="label-text-alt">Choississez le type d'évènement à créer</label>
            </label>
            @if ($errors->has('EventTypeEventTypeID'))
                <p class="text-danger">{{$errors->first('EventTypeEventTypeID')}}</p>
            @endif
        </div>
    </div>
    <div class="grid grid-cols-2 gap-8">
        <div class="form-control">
            <label class="label">
            <span class="label-text">Début de l'évènement</span>
            </label>
            <input wire:model="EventBeginDate" class="input input-bordered" type="date">
                @if ($errors->has('EventBeginDate'))
                    <a href="#" class="label-text-alt text-danger">{{$errors->first('EventBeginDate')}}</a>
                @endif
        </div>
        <div class="form-control">
            <label class="label">
            <span class="label-text">Fin de l'évènement</span>
            </label>
            <input wire:model='EventEndDate' class="input input-bordered" type="date">
                @if ($errors->has('EventEndDate'))
                    <a href="#" class="label-text-alt text-danger">{{$errors->first('EventEndDate')}}</a>
                @endif
        </div>

    </div>
    <div class="grid grid-cols-1 gap-8">
        <div class="flex item-center justify-center h-min">
            <div class="form-control w-full max-w-xs">
                <label class="label text-center">
                    <span class="label-text flex justify-center">Nombre d'invités</span>
                </label>
                <input wire:model="EventGuest" type="number" min="0" class="input input-bordered input-group-xs">
                    @if ($errors->has('EventGuest'))
                        <a href="#" class="label-text-alt text-danger">{{$errors->first('EventGuest')}}</a>
                    @endif
            </div>
        </div>

        <div class="form-control">
            <label class="input-group input-group-vertical input-group-lg">
            <span>Détails de l'évènement</span>
            <input placeholder="..." class="input input-bordered input-lg" wire:model='EventDetail' type="text">
                @if ($errors->has('EventDetail'))
                    <a href="#" class="label-text-alt text-danger">{{$errors->first('EventDetail')}}</a>
                @endif
            </label>
        </div>

        <div class="flex item-center justify-center h-min">
            <div class="form-control w-full max-w-xs">
                <label class="label text-center">
                    <span class="label-text flex justify-center">Dépenses supplémentaires?</span>
                </label>
                <input wire:model="ExpenseSupp" type="number" min="0" step="any" class="input input-bordered input-group-xs">
                    @if ($errors->has('ExpenseSupp'))
                        <a href="#" class="label-text-alt text-danger">{{$errors->first('ExpenseSupp')}}</a>
                    @endif
            </div>
        </div>
    </div>
    <div class="modal-action flex justify-center gap-x-8">
        <button wire:click="save" class="btn btn-primary">Ajouter</button>
        <button wire:click="clearVars" onclick="closeModal()" class="btn">Fermer</button>
    </div>
</div>
<script>
    function closeModal() {
        document.getElementById("event__modal").checked = false;
    }
        window.addEventListener('closeEventModal', event => {
        document.getElementById("event__modal").checked = false;
    })
        window.addEventListener('openEventModal', event => {
        document.getElementById("event__modal").checked = true;
    })
        window.addEventListener('openEventDeleteModal', event => {
        document.getElementById("deleteEventModal").checked = true;
    })
        window.addEventListener('closeEventDeleteModal', event => {
        document.getElementById("deleteEventModal").checked = false;
    })
</script>


