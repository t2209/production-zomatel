<div>
    <div>
        <p class="normal-case ...">Quels articles voulez-vous ajoutez dans cet evènement ?</p>
    </div>
    <div class="grid max-w-full gap-3">
        <div class="form-control">
            <label class="label">
                <span class="label-text">Evènement</span>
            </label>
            <select wire:model="EventEventID" id="EventEventID"  class="select select-bordered w-full">
                <option selected="selected">Séléctionnez un évènement</option>
                @foreach ($events as $item)
                    <option value="{{$item->EventEventID}}||{{$item->ArticleArticleID}}||{{$item->ArticleCode}}">{{$item->event->EventName}} - ( du {{date("d/m/Y", strtotime($item->event->EventBeginDate))}} au {{date("d/m/Y", strtotime($item->event->EventEndDate))}} :
                        {{$item->article->ArticleName}} {{number_format((float)$item->ArticlePriceTTC, 0, '.', ' ')}} Ar )</option>
                @endforeach
            </select>
            <label class="label">
                <label class="label-text-alt">Choississez l'évènement</label>
            </label>
            @if ($errors->has('EventEventID'))
                <p class="text-danger">{{$errors->first('EventEventID')}}</p>
            @endif
        </div>

        <div class="form-control w-full max-w-xs">
            <label class="label">
            <span class="label-text">Quantité produite</span>
            </label>
            <input wire:model="EventArticleQty" type="number" min="0" step=".01" class="input input-bordered">
                @if ($errors->has('EventArticleQty'))
                    <a href="#" class="label-text-alt text-danger">{{$errors->first('EventArticleQty')}}</a>
                @endif
        </div>
        <div class="form-control w-full max-w-xs">
            <label class="label">
            <span class="label-text">Max</span>
            </label>
            <input wire:model="EventArticleMax" type="number" min="0" step=".01" class="input input-bordered">
                @if ($errors->has('EventArticleMax'))
                    <a href="#" class="label-text-alt text-danger">{{$errors->first('EventArticleMax')}}</a>
                @endif
        </div>
    </div>
    <div class="modal-action flex justify-center">
        <button wire:click="save" class="btn btn-primary mr-2 transition duration-700 ease-in-out">Ajouter</button>
        <button wire:click="clearVars" onclick="closeModal()" class="btn transition duration-700 ease-in-out">Fermer</button>
    </div>
</div>

<script>
    function closeModal() {
        document.getElementById("artevent__modal").checked = false;
    }
        window.addEventListener('closeArtEventModal', event => {
        document.getElementById("artevent__modal").checked = false;
    })
        window.addEventListener('openArtEventModal', event => {
        document.getElementById("artevent__modal").checked = true;
    })
        window.addEventListener('openArtEventDeleteModal', event => {
        document.getElementById("deleteArtEventModal").checked = true;
    })
        window.addEventListener('closeArtEventDeleteModal', event => {
        document.getElementById("deleteArtEventModal").checked = false;
    })
</script>
