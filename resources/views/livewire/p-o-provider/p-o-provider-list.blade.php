<div class="space-y-5 font-monow-full p-10 mt-4 bg-white border border-gray-200 rounded-xl">

    {{-- title --}}
    <div class="flex justify-center items-center">
        <div class="text-3xl font-bold">
            <span class="bg-clip-text text-transparent font-mono tracking-tight bg-gray-600">
              Liste des achats à faire
            </span>
        </div>
    </div>

    {{-- Date filter --}}
    <div class="flex justify-between items-center">
        @if ($POServDateSend != null)
            <div class="font-normal font-mono flex items-center space-x-2">
                <h1>Liste des achats à faire du {{date("d/m/Y",strtotime($POServDateSend))}}</h1>
                @if ($checkPOProd == "NotSend")
                    <button class="btn btn-sm btn-disabled btn-outline btn-error btn-circle">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                        </svg>
                    </button>
                @else
                    <button class="btn btn-sm btn-disabled btn-outline btn-success btn-circle">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                        </svg>
                    </button>
                @endif
            </div>
        @else
            <div></div>
        @endif
        <div class="flex items-center space-x-2">
            <h1 class="font-normal font-mono">Selectionner une date</h1>
            <input wire:change='unsetBtnActive' wire:model='weekSelection' class="input input-bordered" type="week">
            <button wire:click='dateFilter' class="btn {{($btnActive == 'Active') ? '' : 'btn-outline'}}">Voir</button>
        </div>
    </div>

    {{-- search input --}}
    <div>
        <input wire:model='searchValue' type="text" class="input input-bordered" placeholder="Recherche service ...">
    </div>

    {{-- lits POProd --}}
    <div>
        <div class="grid grid-cols-4 space-x-2">
            <div class="{{$classGrid}} pl-2 justify-center rounded-lg">Service</div>
            <div class="{{$classGrid}} justify-center rounded-lg">Date arrivage voulue</div>
            <div class="{{$classGrid}} justify-center rounded-lg">Date d'envoie</div>
            <div class="{{$classGrid}} justify-center rounded-lg">Détails</div>
        </div>
        <div class="grid">
            @if ($POServices->isEmpty())
                <div class="h-full flex justify-center items-center">
                    <h1 class="text-lg font-normal">En attende d'une semaine contenant des données</h1>
                    <img class="h-10 w-10" src="{{URL('/Images/search.svg')}}" alt="">
                </div>
            @else
                @foreach ($POServices as $key => $POService)
                    @livewire('p-o-provider.p-o-provider-list-grid',[
                        'POService' => $POService,
                        'key' => $key
                    ],key($POService->POServID))
                @endforeach
            @endif
        </div>
    </div>

    {{-- button show all --}}
    <div class="flex justify-center">
        @if (!$POServices->isEmpty())
            <label for="md_rwprov" wire:click='getRawmatForProd' class="btn btn-success">Voir le commande à envoyer</label>
        @endif
    </div>
    <input type="checkbox" id="md_rwprov" class="modal-toggle"> 
    <div class="modal">
        <div class="modal-box" style="height: 90%; width: 90%; max-width: 90%;">

            <div class="text-xl font-semibold text-center">
                <h1>Listes des achats à faire</h1>
            </div>

            <div class="flex flex-col place-content-between h-full">

                <div class="h-full overflow-y-auto">
                    @if ($RawmatProd == null)
                        Chargement ...
                    @else
                        @livewire('p-o-provider.p-o-provider-list-purchase',[
                            'items' => $RawmatProd,
                            'weekDate' => $weekDate
                        ])
                    @endif
                </div>

                <div class="bottom-0 flex justify-end my-3 gap-2">
                    <button wire:click='addUpPO' class="btn btn-success {{($checkPOProd != "NotSend") ? "hidden": ""}}">Confirmer</button>
                    <label for="md_rwprov" class="btn">Fermer</label>
                </div>

            </div>

        </div>
    </div>

</div>
