<div class="space-y-3">
    <div class="flex items-center justify-between px-1">
        <div class="flex items-center space-x-2">
            <input wire:model='searchValue' wire:keyup='search' type="text" placeholder="Recherche produits" class="input input-bordered">
            {{-- unsetFilter --}}
            <button wire:click='unsetFilter' class="btn btn-info btn-sm {{($activeAll) ? 'btn-active' : 'btn-outline'}}">Tout</button>
            {{-- familyFilter --}}
            @foreach ($families as $family)
                <button wire:click='familyFilter("{{$family->RawMatWording}}")' class="btn btn-info btn-sm {{($activeFamily == $family->RawMatWording) ? 'btn-active' : 'btn-outline'}}">{{$family->RawMatWording}}</button>
            @endforeach
            {{-- adressFilter --}}
            <button wire:click='adressFilter("Tana")' class="btn btn-error btn-sm {{($activeAdress) ? 'btn-active' : 'btn-outline'}}">Tana</button>
        </div>
        <div class="flex space-x-2">
            @foreach ($avgFamily as $item)
                <div class="p-2 rounded-sm shadow">
                    <div class="text-center font-semibold">{{$item['RawmatFamily']}}</div> 
                    <div class="text-center">{{$item['Avg']}} %</div> 
                </div>
            @endforeach
        </div>
    </div>

    {{-- @dd($items) --}}
    <div class="grid grid-cols-7 space-x-2">
        <div class="bg-gray-600 h-full flex items-center text-white rounded-sm pl-2">Produits</div>
        <div class="bg-gray-600 h-full flex items-center text-white rounded-sm pl-2">Adresse</div>
        <div class="bg-gray-600 h-full flex items-center text-white rounded-sm pl-2">Famille</div>
        <div class="bg-gray-600 justify-end h-full flex items-center text-white rounded-sm pr-2">Prix Unitaire</div>
        <div class="bg-gray-600 justify-center h-full flex items-center text-white rounded-sm">Quantité</div>
        <div class="bg-gray-600 justify-end h-full flex items-center text-white rounded-sm pr-2">Prix d'achat</div>
        <div class="bg-gray-600 justify-center h-full flex items-center text-white rounded-sm">Ratio</div>
    </div>

    @foreach ($items as $item)
        <div class="grid grid-cols-7 space-x-2">
            <div class="flex items-center h-full bg-gray-200 rounded-sm pl-2">{{$item['rawmaterial']['RawMaterialName']}}</div>
            <div class="flex items-center h-full bg-gray-200 rounded-sm pl-2">{{$item['rawmaterial']['rawmatproviders'][0]['provider']['ProviderAdress']}}</div>
            <div class="flex items-center h-full bg-gray-200 rounded-sm pl-2">{{$item['rawmaterial']['rawmatfamily']['RawMatWording']}}</div>
            <div class="flex justify-end items-center h-full bg-gray-200 rounded-sm pr-2">{{setMonneyMask($item['RawMatRequirePrice'])}}</div>
            <div class="flex justify-center items-center h-full bg-gray-200 rounded-sm">{{$item['RawMatRequireQtySum']}}</div>
            <div class="flex justify-end items-center h-full bg-gray-200 rounded-sm pr-2">{{setMonneyMask($item['RawMatRequirePurchasePrice'])}}</div>
            <div class="flex justify-center items-center h-full bg-gray-200 rounded-sm">
                {{ round((($item['RawMatRequirePurchasePrice'] / $sumPurchasePrice) * 100) , 2) }} %
            </div>
        </div>
    @endforeach

    <div class="grid grid-cols-7 space-x-2">
        <div class="bg-gray-600 h-full flex items-center text-white rounded-sm pl-2">Total</div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div class="flex justify-end items-center h-full bg-gray-600 rounded-sm pr-2 text-white">{{setMonneyMask($sumPurchasePrice)}}</div>
    </div>
    {{-- @dd($avgFamily) --}}
</div>
