<div class="grid grid-cols-4 space-x-2 font-mono">
   <div class="{{$classGrid}} pl-2">{{ $POService->service->ServiceName }} </div>
   <div class="{{$classGrid}} justify-center">{{ date("d/m/Y", strtotime($POService->POServDateSend)) }}</div>
   <div class="{{$classGrid}} justify-center">{{ date("d/m/Y", strtotime($POService->POServDateNeed)) }}</div>
   <div class="flex items-center mt-2 justify-center">
      <label wire:click='showDetails({{$POService->POServID}})' for="dl_{{$key}}" class="btn btn-outline btn-sm btn-circle">
         <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01" />
         </svg>
      </label>
   </div>

   {{-- modal details --}}
   <input type="checkbox" id="dl_{{$key}}" class="modal-toggle"> 
   <div class="modal">
      <div class="modal-box" style="height: 75%; width: 75%; max-width: 75%;">
         <div class="text-center my-5">
            <h1 class="text-xl font-semibold">Liste des matières premières à acheter</h1>
            <span class="badge">{{ $POService->service->ServiceName }}</span>
            <span class="badge">{{ date("d/m/Y", strtotime($POService->POServDateSend)) }}</span>
         </div>

         <div class="my-3">
            <input wire:model='searchValue' wire:keyup='search' type="text" class="input input-bordered" placeholder="Recherche produit">
         </div>

         <div class="flex flex-col place-content-between">

            <div class="overflow-y-auto h-full max-h-full space-y-2">
               @if ($rawMatRequires == null)
                  Chargement ...
               @else

                  <div class="grid grid-cols-4 space-x-2">
                     <div class="flex items-center h-7 bg-gray-600 rounded-sm pl-2 text-white">Produits</div>
                     <div class="flex items-center justify-end h-7 bg-gray-600 rounded-sm pr-2 text-white">Prix Unitaire</div>
                     <div class="flex items-center justify-center h-7 bg-gray-600 rounded-sm text-white">Quantité</div>
                     <div class="flex items-center justify-end h-7 bg-gray-600 rounded-sm pr-2 text-white">Prix d'achat</div>
                  </div>

                  @foreach ($rawMatRequires as $rawMatRequire)
                     <div class="grid grid-cols-4 space-x-2">
                        <div class="flex items-center h-7 bg-gray-100 rounded-sm pl-2">{{$rawMatRequire->rawmaterial->RawMaterialName}}</div>
                        <div class="flex justify-end items-center h-7 bg-gray-100 rounded-sm pr-2">
                           {{number_format((float)($rawMatRequire->rawmaterial->rawmatproviders[0]->RawMatProviderUnitPrice), 0, '.', ' ')}} Ar
                        </div>
                        <div class="flex justify-center items-center h-7 bg-gray-100 rounded-sm">{{$rawMatRequire->RawMatRequireQty}}</div>
                        <div class="flex justify-end items-center h-7 bg-gray-100 rounded-sm pr-2">
                           {{number_format((float)($rawMatRequire->RawMatRequireQty * $rawMatRequire->rawmaterial->rawmatproviders[0]->RawMatProviderUnitPrice), 0, '.', ' ')}} Ar
                        </div>
                     </div>
                  @endforeach

               @endif
            </div>

            <div class="bottom-0 flex justify-end my-3">
               <label for="dl_{{$key}}" class="btn">Fermer</label>
            </div>

         </div>

      </div>
   </div>
</div>
