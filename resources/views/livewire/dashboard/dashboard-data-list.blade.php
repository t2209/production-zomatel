<div class="space-y-5">
    {{-- week filter --}}
    <div class="flex gap-2">

        <div class="form-control w-full max-w-xs">
            <label class="label">
                <span class="label-text">Séléction semaine</span>
            </label>
            <div class="space-x-2">
                <input wire:model='week' wire:change='unsetVars' type="week" class="w-full input input-bordered">
            </div>
            @error('week') <span class="error text-danger">{{ $message }}</span> @enderror
        </div>

        <div class="form-control w-full max-w-xs">
            <label class="label">
              <span class="label-text">Séléction service</span>
            </label>
            <select wire:model='selectedService' wire:change='unsetVars' class="select select-bordered">
              <option value="*">Tout</option>
              @foreach($services as $value)
                  <option value="{{ $value->ServiceName }}">{{ $value->ServiceName }}</option>
              @endforeach
            </select>
        </div>

        <div class="flex items-end">
            <button wire:click='getResultData' class="btn btn-primary btn-outline{{
                ($activeWeek) ? "btn-active" : ""
            }}">Résultats
            </button>
        </div>

    </div>

    {{-- Heading Stock --}}
    <div>
        <div class="text-xl font-extrabold">
            <span class="text-transparent bg-clip-text bg-gradient-to-r from-gray-600 to-gray-500">
              Stocks
            </span>
        </div>
    </div>

    {{-- results data --}}
    @if ($weekNumber != null && $rawmatAvgSI != null)

        <div class="flex gap-3 my-2">
            {{-- ca --}}
            <div class="flex flex-col w-1/3 transition duration-200 transform bg-gray-200 shadow stat hover:scale-95 hover:shadow-inner rounded-box">
                <div class="text-lg font-semibold">CA , Semaine {{ $weekNumber }}</div>
                <div class="stat-value">{{ setMonneyMask($ca) }}</div>
            </div>
        </div>

        <div class="grid grid-cols-4 gap-2">
            {{-- initiale --}}
            <div class="flex flex-col transition duration-200 transform bg-gray-200 shadow stat hover:scale-95 hover:shadow-inner rounded-box">
                <div class="text-lg font-semibold">Stock Initiale , Semaine {{ $weekNumber }}</div>
                <div class="stat-value">{{ setMonneyMask($rawmatSI) }}</div>
                <div class="flex flex-wrap justify-center gap-y-2 gap-x-2">
                    @for ($i = 0; $i < count($rawmatAvgSI); $i++)
                            <div class="p-2 bg-gray-100 rounded-md shadow-lg">
                                <div class="font-semibold text-center">{{ $rawmatAvgSI[$i]["RawMatWording"] }}</div>
                                <div class="text-center">{{ $rawmatAvgSI[$i]["Avg"] }} %</div>
                            </div>
                    @endfor
                </div>

            </div>
            {{-- appro --}}
            <div class="col-span-2 transition duration-200 transform bg-gray-200 shadow stat hover:scale-95 hover:shadow-inner rounded-box">
                
                <div>
                    <div class="text-lg font-semibold">Appro , Semaine {{ $weekNumber }}</div>
                    <div class="stat-value">{{ setMonneyMask($orderValue) }}</div>
                </div>

                <div>
                    {{-- order data --}}
                    <div class="stat-actions">
                        <div class="flex flex-wrap justify-center gap-y-2 gap-x-2">
                            @foreach ($avgFamily as $item)
                                <div class="p-2 bg-gray-100 rounded-md shadow-lg">
                                    <div class="font-semibold text-center">{{$item['rawmatprovider']['rawmaterial']['rawmatfamily']['RawMatWording']}}</div>
                                    <div class="text-center">{{$item['Avg']}} %</div>
                                </div>
                            @endforeach
                        </div>
                    </div>
    
                    {{-- data to compare with --}}
                    <div class="stat-actions">
                        <h1 class="font-mono">Ration du fiche technique</h1>
                        <div class="flex flex-wrap justify-center gap-y-2 gap-x-2">
                        @for ($i = 0; $i < count($familyStat); $i++)
                            <div class="p-2 bg-gray-100 rounded-md shadow-lg">
                                <div class="font-semibold text-center">{{ $familyStat[$i]["name"] }}</div>
                                <div class="text-center">{{ $familyStat[$i]["percentage"] }} %</div>
                            </div>
                        @endfor
                        </div>
                    </div>
                </div>

            </div>
            {{-- stock final --}}
            <div class="flex flex-col transition duration-200 transform bg-gray-200 shadow stat hover:scale-95 hover:shadow-inner rounded-box">
                <div class="text-lg font-semibold">Stock Final , Semaine {{ $weekNumber }}</div>
    
                <div class="stat-value">{{ setMonneyMask($rawmatSI) }}</div>
                <div class="flex flex-wrap justify-center gap-y-2 gap-x-2">
                    @for ($i = 0; $i < count($rawmatAvgSI); $i++)
                            <div class="p-2 bg-gray-100 rounded-md shadow-lg">
                                <div class="font-semibold text-center">{{ $rawmatAvgSI[$i]["RawMatWording"] }}</div>
                                <div class="text-center">{{ $rawmatAvgSI[$i]["Avg"] }} %</div>
                            </div>
                    @endfor
                </div>
    
            </div>
        </div>



    @else
        <div class="my-5">
            En attente de données
        </div>
    @endif


    {{-- Heading Stock --}}
    <div>
        <div class="text-xl font-extrabold">
            <span class="text-transparent bg-clip-text bg-gradient-to-r from-gray-600 to-gray-500">
                Meilleure vente articles
            </span>
        </div>
    </div>
    {{-- Best sail --}}
    {{ $weekForSails }}
    @livewire('dashboard.dashboard-data-article-sails',[
        'weekF' => $weekForSails
    ])
</div>
