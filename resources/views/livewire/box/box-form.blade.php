<div>
    <div class="grid grid-cols-2 gap-8">
        <div class="form-control">
            <label class="label">
              <span class="label-text">Numéro du boîte</span>
            </label>
            <input wire:model="BoxNum" type="text" placeholder="Numéro du boîte" class="input input-bordered">
            @if ($errors->has('BoxNum'))
                <a href="#" class="label-text-alt text-danger">{{$errors->first('BoxNum')}}</a>
            @endif
        </div>
        <div class="form-control">
            <label class="label">
                <span class="label-text">Choisir son congélateur</span>
            </label>
            <select wire:model="FreezerFreezerID" class="select select-bordered w-full">
                @foreach ($freezers as $item)
                    @if (count($freezers) == 0)
                        <option value="">Congélateur vide</option>
                    @else
                        <option value="{{$item->FreezerID}}">Congelateur numéro {{$item->FreezerNum}}</option>
                    @endif
                @endforeach
            </select>
            @if ($errors->has('FreezerFreezerID'))
                <a href="#" class="label-text-alt text-danger">{{$errors->first('FreezerFreezerID')}}</a>
            @endif
        </div>
    </div>
    <div class="modal-action flex justify-center gap-x-8">
        <button wire:click="save" class="btn btn-primary">Ajouter</button>
        <button onclick="closeModal()" class="btn">Fermer</button>
    </div>
</div>
<script>
    function closeModal() {
        document.getElementById("boxes__modal").checked = false;
    }
    window.addEventListener('closeBoxModal', event => {
        document.getElementById("boxes__modal").checked = false;
    })
    window.addEventListener('openBoxModal', event => {
        document.getElementById("boxes__modal").checked = true;
    })
    window.addEventListener('openBoxDeleteModal', event => {
        document.getElementById("deleteBoxModal").checked = true;
    })
    window.addEventListener('closeBoxDeleteModal', event => {
        document.getElementById("deleteBoxModal").checked = false;
    })
</script>
