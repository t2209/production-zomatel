<div class="overflow-x-auto space-y-2">
  <div class="grid grid-cols-12 gap-1">
    <div class="{{$classGrid}}">Libélle</div> <div class="{{$classGrid}}">Addresse</div> <div class="{{$classGrid}}">Famille</div>
    <div class="{{$classGrid}}">Unite</div> <div class="{{$classGrid}}">Stock service</div> <div class="{{$classGrid}}">Stock Eco</div> <div class="{{$classGrid}}">Max</div> <div class="{{$classGrid}}">Demande</div>
    <div class="{{$classGrid}}">Appro</div> <div class="{{$classGrid}}">P.U</div> <div class="{{$classGrid}}">Valeur St.E</div> <div class="{{$classGrid}}">Valeur Appro</div>
  </div>

  <div class="grid space-y-2">
    @foreach ($rawMats as $key =>$rawMat)
      @livewire('raw-mat-require-form', ['rawMat' => $rawMat,'key'=>$key,'serviceID'=>$serviceID,'eventID'=>$eventID], key($rawMat->RawMatArticleID))
    @endforeach
  </div>

  <div class="form-control w-1/6 mx-3">
    <label class="label">
      <span class="label-text text-lg font-semibold">Date need</span>
    </label> 
    <input type="date" wire:model='POServDateNeed' class="input input-bordered">
    @if ($errors->has('POServDateNeed'))
      <p class="text-danger">{{$errors->first('POServDateNeed')}}</p>
    @endif
  </div>
    
  <div class="modal-action gap-2">
    <label wire:click='savePOService' class="btn btn-primary">Confirmer</label>
    <label for="POModal" class="btn">Fermer</label>
  </div>
</div>
