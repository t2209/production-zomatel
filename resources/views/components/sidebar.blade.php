<style>

  .transform-nav-item{
    transition: transform .3s;
    transform: translateX(var(--tw-translate-x)) translateY(var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
    --tw-translate-x: 11rem;
    }

  .translate{
    transition: transform .3s;
    transform: translateX(var(--tw-translate-x)) translateY(var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y)) !important;
    --tw-translate-x: 0rem !important;

  }

  .hide-text{
    transition: display .2s !important;
    display: none !important;
  }

  .show{
    transition: display .2s !important;
    display: flex !important;
    padding-left: 1rem !important;
  }

  .active-class{
    border-right: .3rem !important;
    border-color: #c2633a !important;
    border-style: none solid none none !important;
    transition: border-color 1s linear;
    /* height: 3.5rem; */

  }

  li:hover{
    background-color: #f8aa89 !important;
    border-color: white;
    border-width: 0px 3px 0px 3px;
    border-radius: .5rem;
    height: 3.5rem;
    -webkit-text-fill-color: white !important


  }
  .onhover:hover{
    font-weight: bold !important;
    transition: font-size .5s !important;
    transition: color .5s !important;
    font-size: 1.1rem
  }



</style>

  <div x-data="{showText : false}">
    <div class="drawer-side">
        <input id="my-drawer" type="checkbox" class="drawer-toggle">

        <div class="w-screen h-auto px-12 2xl:px-16 drawer-side">
          <label for="my-drawer" class="flex items-start justify-start drawer-overlay" style="background-color: transparent !important;"></label>

          <ul class="flex h-full py-2 overflow-y-auto text-gray-600 bg-white border border-gray-200 2xl:py-4 w-60 menu rounded-3xl">

          {{-- HamburgerButton --}}

          <button class="sticky transition">
            <div class="grid grid-cols-1">
              <div class="flex flex-col items-end justify-end transform 2xl:px-2 drawer-content">

                  <label for="my-drawer" onclick="openNav(); transformation(); hidding() " class="w-12 h-8 bg-gray-600 2xl:w-12 2xl:h-12 btn rounded-xl drawer-button">

                    <svg class="svg" xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 6h16M4 12h16M4 18h16" />
                    </svg>
                  </label>


              </div>
            </div>
          </button>


      {{-- /hamburgerButton --}}

          {{-- <div class="">
            <div class="flex items-center px-20 pt-4">
              <img class="pt-4" src={{url('images/zomatel.png')}} alt="logo-zomatel"/>
              </div>
            </div> --}}

{{--
                <div class="relative">
                <form action="" class="text-gray-500 transition duration-300 rounded focus-within:text-cyan-400 focus-within:bg-white focus-within:shadow search">
                  <div class="relative w-full">
                    <div class="absolute top-0 bottom-0 flex items-center h-full mb-auto left-4">
                      <svg xmlns="http://ww50w3.org/2000/svg" class="w-4 fill-current" viewBox="0 0 35.997 36.004">
                        <path id="Icon_awesome-search" data-name="search" d="M35.508,31.127l-7.01-7.01a1.686,1.686,0,0,0-1.2-.492H26.156a14.618,14.618,0,1,0-2.531,2.531V27.3a1.686,1.686,0,0,0,.492,1.2l7.01,7.01a1.681,1.681,0,0,0,2.384,0l1.99-1.99a1.7,1.7,0,0,0,.007-2.391Zm-20.883-7.5a9,9,0,1,1,9-9A8.995,8.995,0,0,1,14.625,23.625Z"></path>
                      </svg>
                    </div>
                    <input type="search" placeholder="Rechercher" name="search" id="search" class="w-full py-3 pl-12 pr-4 text-sm text-gray-500 placeholder-gray-500 transition-all bg-gray-200 bg-opacity-75 rounded outline-none focus:bg-transparent focus:rounded-3xl" />
                  </div>
                </form>
              </div> --}}


          <div class="gap-2 py-2 pl-2 2xl:gap-4 2xl:pl-0 2xl:py-4">

            <li>
                <a href="/" class="flex px-0 font-thin leading-3 text-gray-600 transition 2xl:font-semibold nav-side hover:text-gray-800">
                  <img class="w-4 2xl:w-6 2xl:h-8 transform-nav-item" src="{{url('svg/dashboard.svg')}}" alt="tableau de bord">
                  <div class="pl-8 hide-text">
                   Tableau de bord
                  </div>
                </a>
              </li>

              <li>
                <a href="/cpl" class="flex font-thin leading-3 text-gray-600 transition 2xl:font-semibold nav-side hover:text-gray-800">
                  <img class="w-4 2xl:w-6 2xl:h-8 transform-nav-item" src="https://img.icons8.com/fluency/48/000000/control-panel.png" alt="">
                    <div class="pl-8 hide-text">
                        Menu
                    </div>
                </a>
              </li>

              {{-- <li>
                <a href="/services" class="flex font-semibold text-gray-600 transition nav-side hover:text-gray-800">
                  <img class="w-6 transform-nav-item" src="{{url('svg/services.svg')}}" alt="">
                  <div class="pl-8 hide-text">
                   Service
                </div>
                  </a>
              </li> --}}
              <li>
                <a  href="/rw-mat" class="flex font-thin leading-3 text-gray-600 transition 2xl:font-semibold nav-side hover:text-gray-800">
                  <img class="w-4 2xl:w-6 2xl:h-8 transform-nav-item" src="{{url('svg/matBrutes.svg')}}" alt="">
                  <div class="pl-8 hide-text">
                  Matière première
                </div>
                </a>
              </li>
              {{-- <li >
                <a href="/providers" class="flex font-semibold text-gray-600 transition nav-side hover:text-gray-800">
                  <img class="w-6 transform-nav-item" src="{{url('svg/provider.svg')}}" alt="">
                  <div class="pl-8 hide-text">
                  Fournisseurs
                </div>
                </a>
              </li > --}}
              <li>
                <a href="/articles" class="flex font-thin leading-3 text-gray-600 transition 2xl:font-semibold nav-side hover:text-gray-800">
                  <img src="{{url('svg/articles.svg')}}" class="w-4 2xl:w-6 2xl:h-8 transform-nav-item" alt="" />
                  <div class="pl-8 hide-text">
                  Article
                </div>
                </a>
              </li>
              <li>
                <a href="/service-request" class="flex font-thin leading-3 text-gray-600 transition 2xl:font-semibold hover:text-gray-800">
                  <img src="{{url('svg/articlesFichetech.svg')}}" class="w-4 2xl:w-6 2xl:h-8 transform-nav-item" alt="" />
                  <div class="pl-8 hide-text">
                    Demande par service
                </div>
                  </a>
              </li>
              <li>
                <a href="/events" class="flex font-thin leading-3 text-gray-600 transition 2xl:font-semibold hover:text-gray-800">
                  <img src="{{url('svg/event.svg')}}" class="w-4 2xl:w-6 2xl:h-8 transform-nav-item" alt="" />
                  <div class="pl-8 hide-text">
                  Événements
                </div>
                </a>
              </li>
              {{-- <li>
                <a href="/box" class="flex font-semibold text-gray-600 transition hover:text-gray-800">
                  <img src="{{url('svg/fridge.svg')}}" class="w-6 transform-nav-item" alt="" />
                  <div class="pl-8 hide-text">
                  Congélateur
                </div>
                </a>
              </li> --}}
              <li>
                <a href="/articlesRawmat" class="flex font-thin leading-3 text-gray-600 transition 2xl:font-semibold hover:text-gray-800">
                  <img src="{{('svg/box.svg')}}" class="w-4 2xl:w-6 2xl:h-8 transform-nav-item" alt="" />
                  <div class="pl-8 hide-text">
                  Fiche Technique
                </div>
                </a>
              </li>
              <li>
                <a href="/production" class="flex font-thin leading-3 text-gray-600 transition 2xl:font-semibold hover:text-gray-800">
                  <img src="{{url('svg/productions.svg')}}" class="w-4 2xl:w-6 2xl:h-8 transform-nav-item" alt="" />
                  <div class="pl-8 hide-text">
                  Productions
                </div>
                </a>
              </li>
              <li>
                <a href="/listRwMatReceive" class="flex font-thin leading-3 text-gray-600 transition 2xl:font-semibold hover:text-gray-800">
                    <img class="w-4 2xl:w-6 2xl:h-8 transform-nav-item" src="{{url('svg/provider.svg')}}" alt="">
                    <div class="pl-8 hide-text">
                  Arrivage
                </div>
                </a>
              </li>
              <li>
                <a href="/listPOProvider" class="flex font-thin leading-3 text-gray-600 transition 2xl:font-semibold hover:text-gray-800 tooltip" data-tip="Achats à faire">
                    <img src="{{url('svg/articlesFichetech.svg')}}" class="w-4 2xl:w-6 2xl:h-8 transform-nav-item" alt="" />
                    <div class="h-4 pl-8 hide-text">
                    Achats probable
                </div>
                  </a>
              </li>
            </div>
          </ul>
        </div>
      </div>
  </div>
  <script>
    function transformation () {
      let navItems = document.querySelectorAll('.transform-nav-item');
      // console.log('item list',navItems);
      navItems.forEach(navItem => {
      navItem.classList.contains('translate') ? navItem.classList.remove('translate') : navItem.classList.add('translate')

      });
    }
  </script>
  <script>
    function hidding () {
      let itemTexts = document.querySelectorAll('.hide-text');
      // console.log('fucking hiding' ,itemTexts);
      itemTexts.forEach(itemText => {
        itemText.classList.contains('show') ? itemText.classList.remove('show') : itemText.classList.add('show')
      })
    }
  </script>
  {{-- active --}}
  <script>
    var anchors = document.querySelectorAll('a');
      for (let index = 0; index < anchors.length; index++) {
        pathname = window.location.pathname;
        // console.log("href",anchors[index].getAttribute("href"));
        if (anchors[index].getAttribute("href") == pathname) {
          anchors[index].classList.add("active-class")
          anchors[index].firstElementChild.classList.add("w-5")
          // console.log(anchors[index].firstElementChild);
        }
      }
  </script>


