<style>

.topBar{
  background: white;
  backdrop-filter: blur( 2.5px );
  -webkit-backdrop-filter: blur( 2.5px );
  border-radius: 10px;
  border: 1px solid rgba( 255, 255, 255, 0.18 );
}

</style>

@if (Auth::user() == null)
<script>window.location = "/login";</script>
@else
  <div class="z-50">
    <div class="lg:flex items-center py-4">
      <div class="flex items-center justify-end lg:pr-20 w-full h-16 gap-4 bg-white rounded-xl topBar"> 
          <div class="font-mono font-extrabold text-gray-600">
          {{-- {{Auth::user()->ServiceServiceID}} / --}}
          {{Auth::user()->role->RoleName}}
          </div>
          <div>
            <a href="/cpl" class="btn btn-sm">Menu</a>
          </div>
          <div>
            <form method="POST" action="{{ route('logout') }}">
                @csrf
                <button class="btn btn-warning btn-sm" type="submit">Deconnexion</button>
            </form>
          </div>
      </div>
    </div>
  </div>
@endif

