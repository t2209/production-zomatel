<!DOCTYPE html>
<html lang="en" class="bg-gray-300" >

 <head>
     <meta charset="utf-8" />
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

     <title>ZomaProd | @yield('title') </title>
     @section('styles')
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
      @livewireStyles()
     @show
 </head>
 <style>
 #main {
  transition: margin-left .3s;
  margin-left : 90px;
  }
  .open-nav{
    margin-left: 330px !important;
  }

  @media screen (max-height: 860px){
    #main{
        margin-left: 80px !important;
    }
  }

 </style>

 <body class="h-screen">
    <div class="grid grid-cols-body">
        {{-- navbar begin    --}}
        {{-- @section('navbar') --}}

        <div class="z-20 flex pt-4 flex-col-1">
            <div class="fixed w-auto xl:rounded-3xl">
        <x-sidebar/>
            </div>
        </div>

        {{-- <x-topbar/> --}}
        {{-- @show --}}
        {{-- navbar end    --}}

        {{-- topbar begin --}}

        {{-- topbar end --}}
        {{-- content begin --}}
       <div id="main" class="grid h-full grid-cols-1 pr-4 grid-rows-content gap-x-12">
           <div class="sticky sm:z-40">
            <x-topbar/>
            </div>
            <div class="z-50 w-full h-full sm:z-40">
                @yield('content')
            </div>
        </div>
        {{-- content end --}}
    </div>
 </body>

  {{-- footer begin --}}
  @section('footer')
    <footer></footer>
  @show
  {{-- footer end --}}

 @section('scripts')
 <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.8.2/dist/alpine.min.js" defer></script>
 <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
 <script src="{{url('/js/app.js')}}"></script>
 <script>
     function openNav() {
         let element = document.getElementById("main");
         // console.log('test class',element.classList.contains('open-nav'));
         element.classList.contains('open-nav') ? element.classList.remove('open-nav') : element.classList.add('open-nav')
        }
        </script>
    <script>
        // const checkbox = document.getElementById('checkbox');`
        </script>
     @livewireScripts()
 @show
</html>
