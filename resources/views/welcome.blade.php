<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Login</title>

    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="flex flex-col sm:justify-center  min-h-screen w-full bg-marron">
        <div class="bg-white relative sm:h-auto h-screen sm:w-max sm:mx-auto sm:rounded-3xl">

            {{-- card --}}
          <div class=" relative min-h-full min-w-full flex justify-center items-center sm:block w-3/4 shadow-lg ">
            
            {{-- content --}}
            <div class="w-full  bg-white max-h-screen max-w-screen rounded-lg shadow-3xl sm:flex py-2">

                <div class="sm:w-3/5 sm:h-auto  flex items-center justify-center">
                  <img class=" object-contain  h-1/2 w-1/2 sm:w-full sm:h-full flex justify-center items-center" src={{ url('svg/mobile-login-animate.svg')}} alt="login"/>
                </div>


                <div class="flex w-full flex-col box-border sm:w-2/5 h-1/2 sm:h-auto   justify-center items-center py-4 px-4">
                  <h4 class="mb-3 text-2xl font-semibold tracking-tight text-gray-800 ">Veuiller vous connecter</h4>

                  {{-- input --}}
                 
                  <div class="flex w-full items-center border-b border-teal-500 py-2">
                   <input type="email" class="appearance-none bg-transparent border-none w-full text-gray-700 sm:mr-3 sm:py-1 px-2 leading-tight focus:outline-none" placeholder="Adresse mail">
                 </div>
                 <div class="flex w-full items-center border-b border-teal-500 py-2">
                  <input type="password" class="appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none" placeholder="Mot de passe">
                </div>
         


                {{-- buttons --}}
                <div class="py-4 w-full">
                  <button class="bg-marron hover:bg-marron_focus text-white font-bold py-2 px-4 w-full rounded-full">
                    connexion
                  </button>
                </div>


                </div>
              </div>
            </div>
          {{-- </div> --}}
        </div>
      </div>
</body>
</html>
