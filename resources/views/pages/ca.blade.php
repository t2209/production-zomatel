@extends('base')
@section('title','CA')

@section('styles')
    @parent
   
@stop

@section('content')
<div class="grid">
    <div class="flex justify-center">
        <h1 class="text-2xl font-extrabold font-mono">CA</h1>
    </div>
    <div style="padding-left: 10%; padding-right: 10%">
        @livewire('ca.ca-index')
    </div>
</div>
@stop

@section('scripts')
    @parent
@stop