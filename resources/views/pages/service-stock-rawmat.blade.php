@extends('base')
@section('title','Stock par service : mp1')

@section('styles')
    @parent

@stop

@section('content')
@livewire('services.service-stock-rawmat-list')
@stop

@section('scripts')
    @parent
@stop
