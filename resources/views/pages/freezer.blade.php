@extends('base')
@section('title','Congelateur')

@section('styles')
    @parent
   
@stop

@section('content')
<div class="grid">
    @livewire('freezer.freezer-list')
</div>
@stop

@section('scripts')
    @parent
@stop