@extends('base')
@section('title','Demande par service')

@section('styles')
    @parent

@stop

@section('content')
@livewire('services.service-request-list')
@stop

@section('scripts')
    @parent
@stop
