@extends('base')
@section('title','Tableau de bord')

@section('styles')
    @parent

@stop

@section('content')
    {{-- @dd(
        \App\Providers\ArticleServiceProvider::getArticles(null,null,null,null)
        ) --}}
    <div class="w-full p-10 mt-4 bg-white border border-gray-100 rounded-2xl " >
        <div class="text-3xl font-extrabold text-center">
            <span class="text-transparent bg-clip-text bg-gradient-to-r from-gray-600 to-gray-800">
              Tableau de bord administrateur
            </span>
        </div>
        {{-- Results data --}}
        @livewire('dashboard.dashboard-data-list')
    </div>

@stop

@section('scripts')
    @parent
@stop
