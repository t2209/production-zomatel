@extends('base')
@section('title','Stock par service')

@section('styles')
    @parent

@stop

@section('content')
@livewire('services.service-stock-list')
@stop

@section('scripts')
    @parent
@stop
