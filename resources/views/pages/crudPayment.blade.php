@extends('base')
@section('title','Gestion mode de payement')

@section('styles')
    @parent
   
@stop

@section('content')
<div class="grid">
    @livewire('providers.provider-payment-crud')
</div>
@stop

@section('scripts')
    @parent
@stop