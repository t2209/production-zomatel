@extends('base')
@section('title','Production')

@section('styles')
    @parent

@stop

@section('content')
<div class="grid">
    {{-- <div class="bg-gray-400">
        <h1 class="text-center">Formulaire events</h1>
        @livewire('events.events-form')
    </div> --}}
    <div>
        {{-- <h1 class="text-center">Liste des évents</h1> --}}
        @livewire('productions.prod-list')
    </div>
</div>
@stop

@section('scripts')
    @parent
@stop
