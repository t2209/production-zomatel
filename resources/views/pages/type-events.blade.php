@extends('base')
@section('title','Type d\'évènement')

@section('styles')
    @parent

@stop

@section('content')
<div class="grid">
    @livewire('events.event-type-list')
</div>
@stop

@section('scripts')
    @parent
@stop
