<!DOCTYPE html>
<html lang="en" class="bg-gray-300" >

 <head>
     <meta charset="utf-8" />
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     @section('title','Voir Commande')

     <title>ZomaProd | Voir Commande </title>
     @section('styles')
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
      @livewireStyles()
     @show
 </head>

 <body class="p-2">
    {{-- Heading --}}
    <div class="flex justify-center p-2">
        <h1 class="text-lg font-semibold">Liste des commandes en cours</h1>
    </div>

    {{-- Component --}}
    @livewire('cooker.cooder-order-list')
 </body>

 @section('scripts')
     <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.8.2/dist/alpine.min.js" defer></script>
     <script src="https://unpkg.com/@themesberg/flowbite@1.1.1/dist/flowbite.bundle.js"></script>
     <script src="{{url('/js/app.js')}}"></script>
     @livewireScripts()
 @show
</html>
