@extends('base')
@section('title','Menu')

@section('styles')
    @parent

@stop

@section('content')
<div class="w-full 2xl:p-10 p-2 2xl:mt-4 mt-2 bg-white border border-gray-100 rounded-2xl">
<div class="grid sm:grid-cols-1">
    <div class="2xl:text-4xl text-xl font-bold text-center">
        <span class="bg-clip-text text-transparent bg-gradient-to-r from-gray-600 to-gray-800 tracking-wide">
            Menu
        </span>
    </div>

    <div class="flex justify-center">
        <div class="grid 2xl:grid-cols-3 grid-cols-4 w-3/4 2xl:p-5 2xl:gap-2">

            {{-- article --}}
            <div class="flex transform transition duration-100 hover:scale-95 hover:bg-gray-200 p-5 rounded-btn gap-2">
                <div class="flex justify-center 2xl:w-1/3 w-1/5">
                    <div class="rounded-full">
                        <img src="https://img.icons8.com/external-flatart-icons-flat-flatarticons/80/000000/external-grocery-grocery-flatart-icons-flat-flatarticons.png"/>
                    </div>
                </div>
                <div class="2xl:w-2/3 w-3/4">
                    <div class="2xl:text-xl text-base font-semibold">
                        <a href="/articles" class="tracking-wide hover:underline capitalize">
                            articles
                        </a>
                    </div>
                    <div>
                        <a href="/article-class" class="2xl:text-base text-blue-600 hover:underline font-mono text-sm cursor-pointer">Classe d'articles</a>
                    </div>
                    <div>
                        <a href="/articlesRawmat" class="2xl:text-base text-sm text-blue-600 hover:underline font-mono cursor-pointer">Fiche technique</a>
                    </div>
                </div>
            </div>
            {{-- raw mat --}}
            <div class="flex transform transition duration-100 hover:scale-95 hover:bg-gray-200 p-5 rounded-btn gap-2">
                <div class="flex justify-center 2xl:w-1/3 w-1/5">
                    <div class="rounded-full">
                        <img src="https://img.icons8.com/external-filled-outline-wichaiwi/80/000000/external-food-carrier-services-business-filled-outline-wichaiwi.png"/>
                    </div>
                </div>
                <div class="2xl:w-2/3 w-3/4">
                    <div class="2xl:text-xl text-base font-semibold">
                        <a href="/rw-mat" class="tracking-wide hover:underline capitalize">
                            matières premières
                        </a>
                    </div>
                    <div>
                        <a href="/article-class" class="2xl:text-base text-sm text-blue-600 hover:underline font-mono cursor-pointer">Stocks articles</a>
                    </div>
                    <div>
                        <a href="/service-stock-rawmat" class="2xl:text-base text-sm text-blue-600 hover:underline font-mono cursor-pointer">Stocks MP1</a>
                    </div>
                </div>
            </div>
            {{-- productions --}}
            <div class="flex transform transition duration-100 hover:scale-95 hover:bg-gray-200 p-5 rounded-btn gap-2">
                <div class="flex justify-center 2xl:w-1/3 w-1/5">
                    <div class="rounded-full">
                        <img src="https://img.icons8.com/external-justicon-flat-justicon/80/000000/external-kitchen-home-and-living-justicon-flat-justicon.png"/>
                    </div>
                </div>
                <div class="2xl:w-2/3 w-3/4">
                    <div class="2xl:text-xl text-base font-semibold">
                        <a href="/production" class="tracking-wide hover:underline capitalize">
                            productions
                        </a>
                    </div>
                    <div>
                        <a href="/article-class" class="2xl:text-base text-sm text-blue-600 hover:underline font-mono cursor-pointer">Stocks articles</a>
                    </div>
                    <div>
                        <a href="/service-stock-rawmat" class="2xl:text-base text-sm text-blue-600 hover:underline font-mono cursor-pointer">Stocks MP1</a>
                    </div>
                </div>
            </div>

            {{-- purchase --}}
            <div class="flex transform transition duration-100 hover:scale-95 hover:bg-gray-200 p-5 rounded-btn gap-2">
                <div class="flex justify-center 2xl:w-1/3 w-1/5">
                    <div class="rounded-full">
                        <img src="https://img.icons8.com/external-kmg-design-detailed-outline-kmg-design/80/000000/external-document-shopping-online-kmg-design-detailed-outline-kmg-design.png"/>
                    </div>
                </div>
                <div class="2xl:w-2/3 w-3/4">
                    <div class="2xl:text-xl text-base font-semibold">
                        <a href="/purchaseOrder" class="tracking-wide hover:underline capitalize">
                            commande
                        </a>
                    </div>
                    <div>
                        <a href="/service-request" class="2xl:text-base text-sm text-blue-600 hover:underline font-mono cursor-pointer">Demande par service</a>
                    </div>
                    <div>
                        <a href="/service-stock-rawmat" class="2xl:text-base text-sm text-blue-600 hover:underline font-mono cursor-pointer">Stocks MP1</a>
                    </div>
                </div>
            </div>

            {{-- purchase --}}
            <div class="flex transform transition duration-100 hover:scale-95 hover:bg-gray-200 p-5 rounded-btn gap-2">
                <div class="flex justify-center 2xl:w-1/3 w-1/5">
                    <div class="rounded-full">
                        <img src="https://img.icons8.com/color/80/000000/sell-stock.png"/>
                    </div>
                </div>
                <div class="w-2/3">
                    <div class="text-xl font-semibold">
                        <a href="/ca" class="tracking-wide hover:underline capitalize">
                            CA
                        </a>
                    </div>
                    <div>
                        <a href="/dailysale" class="text-base text-blue-600 hover:underline font-mono cursor-pointer">Vente journalier</a>
                    </div>
                </div>
            </div>

            {{-- services --}}
            <div class="flex transform transition duration-100 hover:scale-95 hover:bg-gray-200 p-5 rounded-btn gap-2">
                <div class="flex justify-center 2xl:w-1/3 w-1/5">
                    <div class="rounded-full">
                        <img class="2xl:w-24 2xl:h-24 w-16 h-16" src="{{url('svg/services.svg')}}"/>
                    </div>
                </div>
                <div class="2xl:w-2/3 w-3/4">
                    <div class="2xl:text-xl text-base font-semibold">
                        <a href="/services" class="tracking-wide hover:underline">
                            Services
                        </a>
                    </div>
                    <div>
                        <a href="/service-stock" class="2xl:text-base text-sm text-blue-600 hover:underline font-mono cursor-pointer">Stocks articles</a>
                    </div>
                    <div>
                        <a href="/service-stock-rawmat" class="2xl:text-base text-sm text-blue-600 hover:underline font-mono cursor-pointer">Stocks MP1</a>
                    </div>
                </div>
            </div>

            {{-- freeze --}}
            <div class="flex transform transition duration-100 hover:scale-95 hover:bg-gray-200 2xl:p-5 p-2 rounded-btn gap-2">
                <div class="2xl:w-1/3 w-1/5">
                    <div class="rounded-full">
                        <img class="w-24 h-24" src="{{url('svg/fridge.svg')}}"/>
                    </div>
                </div>
                <div class="2xl:w-2/3 w-3/4">
                    <div class="2xl:text-xl text-base font-semibold">
                        <a href="/freezer" class="tracking-wide hover:underline capitalize">
                            congélateur
                        </a>
                    </div>
                    <div>
                        <a href="/box" class="2xl:text-base text-sm text-blue-600 hover:underline font-mono cursor-pointer">Boîte</a>
                    </div>
                </div>
            </div>

            {{-- Economat --}}
            <div class="flex transform transition duration-100 hover:scale-95 hover:bg-gray-200 p-5 rounded-btn gap-2">
                <div class="flex justify-center 2xl:w-1/3 w-1/5">
                    <div class="rounded-full">
                        <img src="https://img.icons8.com/external-soft-fill-juicy-fish/80/000000/external-stock-financial-services-soft-fill-soft-fill-juicy-fish.png"/>
                    </div>
                </div>
                <div class="2xl:w-2/3 w-3/4">
                    <div class="2xl:text-xl text-base font-semibold">
                        <a href="/economat-stock-rawmat" class="tracking-wide hover:underline capitalize">
                            économat
                        </a>
                    </div>
                    <div>
                        <a href="/listPOProvider" class="2xl:text-base text-sm text-blue-600 hover:underline font-mono cursor-pointer">Appro fournisseur</a>
                    </div>
                    <div>
                        <a href="/listRwMatReceive" class="2xl:text-base text-sm text-blue-600 hover:underline font-mono cursor-pointer">Arrivage MP1</a>
                    </div>
                </div>
            </div>

            {{-- Providers --}}
            <div class="flex transform transition duration-100 hover:scale-95 hover:bg-gray-200 p-5 rounded-btn gap-2">
                <div class="flex justify-center 2xl:w-1/3 w-1/5">
                    <div class="rounded-full">
                        <img src="https://img.icons8.com/fluency/80/000000/packing.png"/>
                    </div>
                </div>
                <div class="2xl:w-2/3 w-3/4">
                    <div class="2xl:text-xl text-base font-semibold">
                        <a href="/providers" class="tracking-wide hover:underline capitalize">
                            fournisseur
                        </a>
                    </div>
                    <div>
                        <a href="/grand-book" class="2xl:text-base text-sm text-blue-600 hover:underline font-mono cursor-pointer">Grand Livre</a>
                    </div>
                    <div>
                        <a href="/balance" class="2xl:text-base text-sm text-blue-600 hover:underline font-mono cursor-pointer">Balance de compte fournisseur</a>
                    </div>
                </div>
            </div>

            {{-- Events --}}
            <div class="flex transform transition duration-100 hover:scale-95 hover:bg-gray-200 p-5 rounded-btn gap-2">
                <div class="flex justify-center 2xl:w-1/3 w-1/5">
                    <div class="rounded-full">
                        <img src="https://img.icons8.com/fluency/80/000000/event-accepted-tentatively.png"/>
                    </div>
                </div>
                <div class="2xl:w-2/3 w-3/4">
                    <div class="2xl:text-xl text-base font-semibold">
                        <a href="/events" class="tracking-wide hover:underline capitalize">
                            évènements
                        </a>
                    </div>
                    <div>
                        <a href="/type-events" class="2xl:text-base text-sm text-blue-600 hover:underline font-mono cursor-pointer">Type d'évènement</a>
                    </div>
                    <div>
                        <a href="/eventProduction" class="2xl:text-base text-sm text-blue-600 hover:underline font-mono cursor-pointer">Simulation et productions</a>
                    </div>
                </div>
            </div>

            {{-- Caisse --}}
            <div class="flex transform transition duration-100 hover:scale-95 hover:bg-gray-200 p-5 rounded-btn gap-2">
                <div class="flex justify-center 2xl:w-1/3 w-1/5">
                    <div class="rounded-full">
                        <img src="https://img.icons8.com/external-justicon-flat-justicon/80/000000/external-safe-hotel-essentials-justicon-flat-justicon.png"/>
                    </div>
                </div>
                <div class="2xl:w-2/3 w-3/4">
                    <div class="2xl:text-xl text-base font-semibold">
                        <a href="/caisses" class="tracking-wide hover:underline capitalize">
                            caisse
                        </a>
                    </div>
                    <div>
                        <a href="/paymentProvider" class="2xl:text-base text-sm text-blue-600 hover:underline font-mono cursor-pointer">Historique de paiement</a>
                    </div>
                    <div>
                        <a href="/crudPayment" class="2xl:text-base text-sm text-blue-600 hover:underline font-mono cursor-pointer">Mode de paiement</a>
                    </div>
                </div>
            </div>
            
            {{-- Menu pour les commandes dans le resto --}}
            <div class="flex transform transition duration-100 hover:scale-95 hover:bg-gray-200 p-5 rounded-btn gap-2">
                <div class="flex justify-center 2xl:w-1/3 w-1/5">
                    <div class="rounded-full">
                        <img src="https://img.icons8.com/external-vitaliy-gorbachev-blue-vitaly-gorbachev/80/000000/external-table-furniture-vitaliy-gorbachev-blue-vitaly-gorbachev-7.png"/>
                    </div>
                </div>
                <div class="2xl:w-2/3 w-3/4">
                    <div class="2xl:text-xl text-base font-semibold">
                        <a href="/table" class="tracking-wide hover:underline capitalize">
                            Table
                        </a>
                    </div>
                    <div>
                        <a href="/uiWaiter" class="2xl:text-base text-sm text-blue-600 hover:underline font-mono cursor-pointer">
                            UI Serveuse
                        </a>
                    </div>
                    <div>
                        <a href="/uiCooker" class="2xl:text-base text-sm text-blue-600 hover:underline font-mono cursor-pointer">
                            UI Cuisiner
                        </a>
                    </div>
                    {{-- <div>
                        <a href="/crudPayment" class="text-base text-blue-600 hover:underline font-mono cursor-pointer">Mode de paiement</a>
                    </div> --}}
                </div>
            </div>


        </div>
    </div>

</div>
</div>
@stop

@section('scripts')
    @parent
@stop
