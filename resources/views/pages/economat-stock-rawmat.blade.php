@extends('base')
@section('title','Stock economat : mp1')

@section('styles')
    @parent

@stop

@section('content')
@livewire('economat.economat-stock-rawmat-list')
@stop

@section('scripts')
    @parent
@stop
