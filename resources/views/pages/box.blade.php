@extends('base')
@section('title','Boites')

@section('styles')
    @parent
   
@stop

@section('content')
<div class="grid">
    @livewire('box.box-list')
</div>
@stop

@section('scripts')
    @parent
@stop