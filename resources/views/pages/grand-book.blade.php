@extends('base')
@section('title','Grand Livre')

@section('styles')
    @parent

@stop

@section('content')
<div class="grid">
    @livewire('providers.grand-book-list')
</div>
@stop

@section('scripts')
    @parent
@stop
