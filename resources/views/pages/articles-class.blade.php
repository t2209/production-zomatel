@extends('base')
@section('title','Classe d\'articles')

@section('styles')
    @parent

@stop

@section('content')
<div class="grid">
    @livewire('articles.articles-class-list')
</div>
@stop

@section('scripts')
    @parent
@stop
