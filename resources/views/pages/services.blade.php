@extends('base')
@section('title','Service')

@section('styles')
    @parent

@stop

@section('content')
<div class="grid">
    {{-- <div class="bg-gray-400">
        <h1 class="text-center">Formulaire events</h1>
        @livewire('services.service-form')
    </div> --}}
    <div class="bg-gray-200">
        {{-- <h1 class="text-center">Liste des fournisseurs</h1> --}}
        @livewire('services.service-list')
    </div>
</div>
@stop

@section('scripts')
    @parent
@stop
