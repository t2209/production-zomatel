@extends('base')
@section('title','Articles')

@section('styles')
    @parent
   
@stop

@section('content')
<div class="grid">
    @livewire('articles.articles-list')
</div>
@stop

@section('scripts')
    @parent
@stop