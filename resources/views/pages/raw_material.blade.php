@extends('base')
@section('title','Matière Premières')

@section('styles')
    @parent
   
@stop

@section('content')
@livewire('raw-materials.raw-material')
@stop

@section('scripts')
    @parent
@stop