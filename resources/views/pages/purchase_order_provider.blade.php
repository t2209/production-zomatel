@extends('base')
@section('title','Listes des achats')

@section('styles')
    @parent
@stop

@section('content')
<div class="grid">
    @livewire('p-o-provider.p-o-provider-list')
</div>
@stop

@section('scripts')
    @parent
@stop