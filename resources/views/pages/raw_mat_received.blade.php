@extends('base')
@section('title','Matière Premières')

@section('styles')
    @parent
   
@stop

@section('content')
@livewire('raw-mat-receive.raw-mat-receive-list')
@stop

@section('scripts')
    @parent
@stop