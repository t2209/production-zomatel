@extends('base')
@section('title','Articles , FT')

@section('styles')
    @parent
   
@stop

@section('content')
<div class="grid">
    @livewire('articles-rawmat.articles-rawmat-list-service')
</div>
@stop

@section('scripts')
    @parent
@stop