@extends('base')
@section('title','Vente journalier')

@section('styles')
    @parent
   
@stop

@section('content')
<div class="grid">
    @livewire('daily-sale.daily-sale-index')
</div>
@stop

@section('scripts')
    @parent
@stop