@extends('base')
@section('title','Event Production')

@section('styles')
    @parent

@stop

@section('content')
<div class="grid">
    <div>
        @livewire('event-production.event-production-list')
    </div>
</div>
@stop

@section('scripts')
    @parent
@stop
