@extends('base')
@section('title','Caisse')

@section('styles')
    @parent

@stop

@section('content')
<div class="grid">
    <div class="flex justify-center">
        <h1 class="text-2xl font-extrabold font-mono">Liste des ventes journalières par commande</h1>
    </div>
    <div style="padding-left: 10% ; padding-right: 10%">
        @livewire('daily-sale.daily-sale-list')
    </div>
</div>
@stop

@section('scripts')
    @parent
@stop
