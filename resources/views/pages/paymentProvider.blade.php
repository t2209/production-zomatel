@extends('base')
@section('title','Payement Fournisseur')

@section('styles')
    @parent
@stop

@section('content')
<div class="grid">
    @livewire('providers.provider-payment-index')
</div>
@stop

@section('scripts')
    @parent
@stop
