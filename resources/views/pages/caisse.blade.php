@extends('base')
@section('title','Caisse')

@section('styles')
    @parent

@stop

@section('content')
<div class="grid">
    @livewire('caisse.caisse-list')
</div>
@stop

@section('scripts')
    @parent
@stop
