<!DOCTYPE html>
<html lang="en" class="sm:bg-gray-300 bg-white" >

 <head>
     <meta charset="utf-8" />
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

     <title>ZomaProd | Serveur(se) </title>
     @section('styles')
      <link href="{{ mix('css/app.css') }}" rel="stylesheet">
      @livewireStyles()
     @show
 </head>

 <body class="p-2">
    {{-- Heading --}}
    <div class="flex justify-center p-2">
        <h1 class="text-lg font-semibold">Commande des clients</h1>
    </div>

    {{-- Component --}}
    <div class="w-full p-10 mt-4 bg-white border border-gray-100 rounded-2xl ">
    @livewire('waiter.waiter-index')
</div>
 </body>

 @section('scripts')
     <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.8.2/dist/alpine.min.js" defer></script>
     <script src="https://unpkg.com/@themesberg/flowbite@1.1.1/dist/flowbite.bundle.js"></script>
     <script src="{{url('/js/app.js')}}"></script>
     @livewireScripts()
 @show
</html>
