@extends('base')
@section('title','Balance de compte')

@section('styles')
    @parent

@stop

@section('content')
<div class="grid">
    @livewire('providers.balance-list')
</div>
@stop

@section('scripts')
    @parent
@stop
