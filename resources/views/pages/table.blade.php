@extends('base')
@section('title','Table')

@section('styles')
    @parent
@stop

@section('content')
<div class="grid">
    @livewire('table.table-list')
</div>
@stop

@section('scripts')
    @parent
@stop
