<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>

    <div class="flex flex-col w-full min-h-screen xl:justify-center bg-marron">
        <div class="relative h-screen bg-white xl:h-auto xl:w-1/3 xl:mx-auto xl:rounded-3xl">

            {{-- card --}}
            <div class="relative flex items-center justify-center w-1/3 min-w-full min-h-full shadow-lg sm:block">

            {{-- content --}}
                <div class="w-full max-h-screen py-2 bg-white rounded-lg max-w-screen shadow-3xl sm:flex">

                    <div class="flex items-center justify-center sm:w-3/5 sm:h-auto">
                        <img class="flex items-center justify-center object-contain w-1/2 h-1/2 sm:w-full sm:h-full" src={{ url('svg/mobile-login-animate.svg')}} alt="login"/>
                    </div>

                    <div class="box-border flex flex-col items-center justify-center w-full px-4 py-4 sm:w-2/5 h-1/2 sm:h-auto">
                        <h4 class="mb-3 text-2xl font-semibold tracking-tight text-gray-800 ">{{ __('Login') }}</h4>

                        {{-- input --}}
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="flex flex-col space-y-4">
                                <div class="w-full py-2">
                                    <input type="email" id="email" name="email" class="text-md block px-3 py-2 rounded-lg w-full 
                                    bg-white border-2 border-gray-300 placeholder-gray-500 shadow-md focus:placeholder-gray-500 focus:bg-white focus:border-marron focus:outline-none" placeholder="Mot de passe" required autocomplete="new-password">
                                    <div class="absolute right-0 z-10 flex items-center justify-center pr-3 text-sm leading-5 " placeholder="Adresse mail" value="{{ old('email') }}" required autocomplete="email">
                                </div>
    
                                @error('email')
                                <div>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                </div>
                                @enderror
    
                                <div class="relative flex flex-col w-full py-2"  x-data="{ show: true }">
                                    <input  :type="show ? 'password' : 'text'" id="password" name="password" class="text-md block px-3 py-2 rounded-lg w-full 
                                    bg-white border-2 border-gray-300 placeholder-gray-500 shadow-md focus:placeholder-gray-500 focus:bg-white focus:border-marron focus:outline-none" placeholder="Mot de passe" required autocomplete="new-password">
                                    <div class="absolute right-0 z-10 flex items-center justify-center pr-3 text-sm">
                                        <svg class="2xl:mt-2 mt-4 2xl:h-6 h-4 text-gray-700" fill="none" @click="show = !show" :class="{'hidden': !show, 'block':show }" xmlns="http://www.w3.org/2000/svg" viewbox="0 0 576 512">
                                            <path fill="currentColor" d="M572.52 241.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288 448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400a144 144 0 1 1 144-144 143.93 143.93 0 0 1-144 144zm0-240a95.31 95.31 0 0 0-25.31 3.79 47.85 47.85 0 0 1-66.9 66.9A95.78 95.78 0 1 0 288 160z"></path>
                                        </svg>
                                        <svg class="2xl:mt-2 2xl:h-6 mt-4 h-4 text-gray-700" fill="none" @click="show = !show" :class="{'block': !show, 'hidden':show }" xmlns="http://www.w3.org/2000/svg" viewbox="0 0 640 512">
                                            <path fill="currentColor" d="M320 400c-75.85 0-137.25-58.71-142.9-133.11L72.2 185.82c-13.79 17.3-26.48 35.59-36.72 55.59a32.35 32.35 0 0 0 0 29.19C89.71 376.41 197.07 448 320 448c26.91 0 52.87-4 77.89-10.46L346 397.39a144.13 144.13 0 0 1-26 2.61zm313.82 58.1l-110.55-85.44a331.25 331.25 0 0 0 81.25-102.07 32.35 32.35 0 0 0 0-29.19C550.29 135.59 442.93 64 320 64a308.15 308.15 0 0 0-147.32 37.7L45.46 3.37A16 16 0 0 0 23 6.18L3.37 31.45A16 16 0 0 0 6.18 53.9l588.36 454.73a16 16 0 0 0 22.46-2.81l19.64-25.27a16 16 0 0 0-2.82-22.45zm-183.72-142l-39.3-30.38A94.75 94.75 0 0 0 416 256a94.76 94.76 0 0 0-121.31-92.21A47.65 47.65 0 0 1 304 192a46.64 46.64 0 0 1-1.54 10l-73.61-56.89A142.31 142.31 0 0 1 320 112a143.92 143.92 0 0 1 144 144c0 21.63-5.29 41.79-13.9 60.11z"></path>
                                        </svg>

                                        {{-- <p class="cursor-pointer" @click="show= !show" x-text=" show ? '<a href='#'>show</a>' : 'Hide' "></p> --}}
                                        
                                    </div>
                                </div>

                                @error('password')
                                <div>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                </div>
                                @enderror
                            </div>

                            <div class="w-full py-4">
                                <button type="submit" class="w-full px-4 py-2 font-bold text-white rounded-full bg-marron hover:bg-marron_focus">
                                    {{ __('Se connecter') }}
                                </button>
                                <a href="/register" class="flex justify-center pt-4 font-thin underline text-2xs decoration-sky-500">Creer un compte ?</a>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>

