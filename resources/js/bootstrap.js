window._ = require('lodash');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

//  import Echo from 'laravel-echo';

//  window.Pusher = require('pusher-js');

//  let laravelEcho = new Echo({
//      broadcaster: 'pusher',
//     //  key: process.env.MIX_PUSHER_APP_KEY,
//      wsHost: process.env.MIX_PUSHER_HOST,
//     //  wsHost: window.location.hostname,
//      wsPort: process.env.MIX_PUSHER_PORT,
//     //  wssPort: process.env.MIX_PUSHER_PORT,
//     //  host:window.location.hostname+':6001',
//      forceTLS: false,
//      encrypted: true,
//     //  client: window.Pusher,
//      disableStats: true,
//      enabledTransports: ['ws'],
//  });

//  console.log('tset');

//  laravelEcho.channel('chan-test')
//      .listen('OrderCreatedEvent', (e) => {
//          console.log(e.message);
//      });

// window.Echo.channel('channel-order')
// .listen('OrderCreatedEvent', (e) =>{
//     console.log(e);
// })

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: process.env.MIX_PUSHER_APP_KEY,
    wsHost: process.env.MIX_PUSHER_HOST,
    wsPort: process.env.MIX_PUSHER_PORT,
    wssPort: process.env.MIX_PUSHER_PORT,
    forceTLS: false,
    encrypted: true,
    disableStats: true,
    enabledTransports: ['ws'],
    // enabledTransports: ['ws', 'wss'],
});



