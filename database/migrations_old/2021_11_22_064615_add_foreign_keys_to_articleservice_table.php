<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToArticleserviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articleservice', function (Blueprint $table) {
            $table->foreign(['ServiceServiceID'], 'possesses')->references(['ServiceID'])->on('service');
            $table->foreign(['ArticleArticleID'], 'contains4')->references(['ArticleID'])->on('article');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articleservice', function (Blueprint $table) {
            $table->dropForeign('possesses');
            $table->dropForeign('contains4');
        });
    }
}
