<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToRawmateconomatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rawmateconomat', function (Blueprint $table) {
            $table->foreign(['RawMaterialRawMaterialID'], 'have6')->references(['RawMaterialID'])->on('rawmaterial')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['EconomatEconomatID'], 'belongs2')->references(['EconomatID'])->on('economat')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rawmateconomat', function (Blueprint $table) {
            $table->dropForeign('have6');
            $table->dropForeign('belongs2');
        });
    }
}
