<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRawmatserviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rawmatservice', function (Blueprint $table) {
            $table->integer('RawMatServiceID', true);
            $table->integer('ServiceServiceID')->nullable()->index('dispose');
            $table->integer('RawMaterialRawMaterialID')->nullable()->index('compose2');
            $table->float('RawMatServiceQty', 10, 0);
            $table->date('RawMatServiceDate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rawmatservice');
    }
}
