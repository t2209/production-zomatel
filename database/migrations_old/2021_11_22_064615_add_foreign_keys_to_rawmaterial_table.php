<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToRawmaterialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rawmaterial', function (Blueprint $table) {
            $table->foreign(['RawMatFamilyRawMatFamilyID'], 'have')->references(['RawMatFamilyID'])->on('rawmatfamily')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rawmaterial', function (Blueprint $table) {
            $table->dropForeign('have');
        });
    }
}
