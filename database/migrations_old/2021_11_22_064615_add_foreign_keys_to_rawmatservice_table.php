<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToRawmatserviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rawmatservice', function (Blueprint $table) {
            $table->foreign(['ServiceServiceID'], 'dispose')->references(['ServiceID'])->on('service')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['RawMaterialRawMaterialID'], 'compose2')->references(['RawMaterialID'])->on('rawmaterial')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rawmatservice', function (Blueprint $table) {
            $table->dropForeign('dispose');
            $table->dropForeign('compose2');
        });
    }
}
