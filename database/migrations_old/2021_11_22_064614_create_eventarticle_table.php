<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventarticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventarticle', function (Blueprint $table) {
            $table->integer('EventArticleID', true);
            $table->integer('EventEventID')->index('have4');
            $table->integer('ArticleArticleID')->nullable()->index('contains2');
            $table->float('EventArticleQty', 10, 0);
            $table->float('EventArticleMax', 10, 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventarticle');
    }
}
