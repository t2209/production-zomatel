<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToRawmatrequireTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rawmatrequire', function (Blueprint $table) {
             
            $table->foreign(['PurchaseOrderServicePOServID'], 'contains')->references(['POServID'])->on('purchaseorderservice')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rawmatrequire', function (Blueprint $table) {
            $table->dropForeign('have3');
            $table->dropForeign('contains');
        });
    }
}
