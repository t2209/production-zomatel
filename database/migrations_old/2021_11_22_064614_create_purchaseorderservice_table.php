<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseorderserviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchaseorderservice', function (Blueprint $table) {
            $table->integer('POServID', true);
            $table->integer('PurchaseOrderProviderPurchaseOrderProviderID')->nullable()->index('have5');
            $table->integer('ServiceServiceID')->nullable()->index('requires');
            $table->date('POServDateNeed')->nullable();
            $table->date('POServDateSend')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchaseorderservice');
    }
}
