<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article', function (Blueprint $table) {
            $table->integer('ArticleID', true);
            $table->string('ArticleName')->nullable();
            $table->string('ArticleUnity')->nullable();
            $table->string('ArticlePurchasePrice')->nullable();
            $table->string('ArticleBarCode')->nullable();
            $table->string('ArticlePhoto')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article');
    }
}
