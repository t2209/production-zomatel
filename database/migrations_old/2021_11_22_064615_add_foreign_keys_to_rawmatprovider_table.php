<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToRawmatproviderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rawmatprovider', function (Blueprint $table) {
            $table->foreign(['RawMaterialRawMaterialID'], 'provided')->references(['RawMaterialID'])->on('rawmaterial')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['ProviderProviderID'], 'belongs')->references(['ProviderID'])->on('provider')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rawmatprovider', function (Blueprint $table) {
            $table->dropForeign('provided');
            $table->dropForeign('belongs');
        });
    }
}
