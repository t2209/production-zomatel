<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRawmatarticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rawmatarticle', function (Blueprint $table) {
            $table->integer('RawMatArticleID', true);
            $table->integer('RawMatArticleCode')->nullable();
            $table->integer('EventEventID')->nullable()->index('composeby');
            $table->integer('ServiceServiceID')->index('compose3');
            $table->integer('RawMaterialRawMaterialID')->nullable()->index('have2');
            $table->float('RawMatArticleQty', 10, 0);
            $table->integer('ArticleArticleID')->index('compose');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rawmatarticle');
    }
}
