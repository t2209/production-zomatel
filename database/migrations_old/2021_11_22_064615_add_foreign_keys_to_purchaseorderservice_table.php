<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToPurchaseorderserviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchaseorderservice', function (Blueprint $table) {
            $table->foreign(['ServiceServiceID'], 'requires')->references(['ServiceID'])->on('service')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['PurchaseOrderProviderPurchaseOrderProviderID'], 'have5')->references(['PurchaseOrderProviderID'])->on('purchaseorderprovider')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchaseorderservice', function (Blueprint $table) {
            $table->dropForeign('requires');
            $table->dropForeign('have5');
        });
    }
}
