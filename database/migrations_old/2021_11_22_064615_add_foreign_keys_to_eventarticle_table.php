<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToEventarticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('eventarticle', function (Blueprint $table) {
            $table->foreign(['EventEventID'], 'have4')->references(['EventID'])->on('event')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['ArticleArticleID'], 'contains2')->references(['ArticleID'])->on('article')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('eventarticle', function (Blueprint $table) {
            $table->dropForeign('have4');
            $table->dropForeign('contains2');
        });
    }
}
