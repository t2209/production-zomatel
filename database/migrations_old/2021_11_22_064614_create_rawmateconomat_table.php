<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRawmateconomatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rawmateconomat', function (Blueprint $table) {
            $table->integer('RawMatEconomatID', true);
            $table->integer('EconomatEconomatID')->index('belongs2');
            $table->integer('RawMaterialRawMaterialID')->index('have6');
            $table->float('RawMatEconomatQty', 10, 0);
            $table->date('RawMatEconomatDate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rawmateconomat');
    }
}
