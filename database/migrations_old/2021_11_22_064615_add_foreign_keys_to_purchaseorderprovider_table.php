<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToPurchaseorderproviderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchaseorderprovider', function (Blueprint $table) {
            $table->foreign(['GrandBookProviderGrandBookProviderID'], 'dispose2')->references(['GrandBookProviderID'])->on('grandbookprovider')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchaseorderprovider', function (Blueprint $table) {
            $table->dropForeign('dispose2');
        });
    }
}
