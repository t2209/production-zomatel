<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToRawmatarticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rawmatarticle', function (Blueprint $table) {
            $table->foreign(['ServiceServiceID'], 'compose3')->references(['ServiceID'])->on('service');
            $table->foreign(['RawMaterialRawMaterialID'], 'have2')->references(['RawMaterialID'])->on('rawmaterial')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['ArticleArticleID'], 'compose')->references(['ArticleID'])->on('article')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['EventEventID'], 'composeby')->references(['EventID'])->on('event');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rawmatarticle', function (Blueprint $table) {
            $table->dropForeign('compose3');
            $table->dropForeign('have2');
            $table->dropForeign('compose');
            $table->dropForeign('composeby');
        });
    }
}
