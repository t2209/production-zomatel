<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleserviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articleservice', function (Blueprint $table) {
            $table->integer('ArticleServiceID', true);
            $table->integer('ServiceServiceID')->index('possesses');
            $table->integer('ArticleArticleID')->index('contains4');
            $table->float('ArticleServiceQty', 10, 0);
            $table->float('ArticleServiceMax', 10, 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articleservice');
    }
}
