<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRawmaterialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rawmaterial', function (Blueprint $table) {
            $table->integer('RawMaterialID', true);
            $table->integer('RawMatFamilyRawMatFamilyID')->nullable()->index('have');
            $table->string('RawMaterialAccountNum')->nullable();
            $table->string('RawMaterialName')->nullable();
            $table->string('RawMaterialUnity')->nullable();
            $table->boolean('RawMaterialForProd');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rawmaterial');
    }
}
