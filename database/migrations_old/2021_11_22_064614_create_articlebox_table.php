<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleboxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articlebox', function (Blueprint $table) {
            $table->integer('ArticleBoxID', true);
            $table->integer('BoxBoxID')->index('inside');
            $table->float('ArticleBoxQty', 10, 0);
            $table->integer('ArticleArticleID')->index('possesses2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articlebox');
    }
}
