<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseorderproviderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchaseorderprovider', function (Blueprint $table) {
            $table->integer('PurchaseOrderProviderID', true);
            $table->integer('GrandBookProviderGrandBookProviderID')->nullable()->index('dispose2');
            $table->date('POProvDateSend')->nullable();
            $table->date('POProvDateReceive')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchaseorderprovider');
    }
}
