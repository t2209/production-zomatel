<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRawmatrequireTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rawmatrequire', function (Blueprint $table) {
            $table->integer('RawMatRequireID', true);
            $table->integer('RawMaterialRawMaterialID')->nullable()->index('have3');
            $table->integer('PurchaseOrderServicePOServID')->index('contains');
            $table->float('RawMatRequireQty', 10, 0);
            $table->float('RawMatReceiveQty', 10, 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rawmatrequire');
    }
}
