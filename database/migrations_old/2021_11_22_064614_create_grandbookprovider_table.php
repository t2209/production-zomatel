<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrandbookproviderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grandbookprovider', function (Blueprint $table) {
            $table->integer('GrandBookProviderID', true);
            $table->float('GrandBookProviderDebit', 10, 0);
            $table->float('GrandBookProviderCredit', 10, 0);
            $table->float('GrandBookProviderReport', 10, 0);
            $table->float('GrandBookProviderSold', 10, 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grandbookprovider');
    }
}
