<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToArticleboxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articlebox', function (Blueprint $table) {
            $table->foreign(['ArticleArticleID'], 'possesses2')->references(['ArticleID'])->on('article')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['BoxBoxID'], 'inside')->references(['BoxID'])->on('box')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articlebox', function (Blueprint $table) {
            $table->dropForeign('possesses2');
            $table->dropForeign('inside');
        });
    }
}
