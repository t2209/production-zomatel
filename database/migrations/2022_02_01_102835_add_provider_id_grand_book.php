<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProviderIdGrandBook extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if (!Schema::hasColumn('grandbookprovider', 'ProviderProviderID')) {
             Schema::table('grandbookprovider', function (Blueprint $table) {
                 $table->integer('ProviderProviderID')->nullable();
            });
            Schema::table('grandbookprovider', function (Blueprint $table) {
                $table->foreign(['ProviderProviderID'], 'belongTo')->references(['ProviderID'])->on('provider')->onUpdate('CASCADE')->onDelete('CASCADE');
           });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('grandbookprovider', 'ProviderProviderID')) {
            Schema::table('grandbookprovider', function (Blueprint $table) {
                $table->dropColumn('ProviderProviderID');
                $table->dropForeign('belongTo');
            });
        }
    }
}
