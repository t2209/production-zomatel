<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeyTicketing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ticketing', function (Blueprint $table) {
            $table->foreign(['FundFundID'], 'isAbout')->references(['FundID'])->on('fund')->onUpdate('CASCADE')->onDelete('CASCADE');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ticketing', function (Blueprint $table) {
            $table->dropForeign('isAbout');
        });
    }
}
