<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleorderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articleorder', function (Blueprint $table) {
            $table->integer('ArticleOrderID',true);
            $table->integer('ArticleServiceArticleServiceID')->index('orderFor');
            $table->integer('OrderOrderID')->index('propertyOf');
            $table->integer('ArticleOrderQty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articleorder');
    }
}
