<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveRawmatrequireRawmaterial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rawmatrequire', function (Blueprint $table) {
            $table->dropForeign('have3');
            $table->dropColumn('RawMaterialRawMaterialID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rawmatrequire', function (Blueprint $table) {
            $table->foreign(['RawMaterialRawMaterialID'], 'have3')->references(['RawMaterialID'])->on('rawmaterial')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }
}
