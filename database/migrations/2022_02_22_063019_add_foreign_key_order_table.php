<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeyOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ordertable', function (Blueprint $table) {
            $table->foreign(['TableTableID'], 'isFor')->references(['TableID'])->on('table')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['OrderOrderID'], 'isFor2')->references(['OrderID'])->on('order')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ordertable', function (Blueprint $table) {
            $table->dropForeign('isFor');
            $table->dropForeign('isFor2');

        });
    }
}
