<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMaterialproviderIDRawmateconomat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rawmateconomat', function (Blueprint $table) {
            $table->integer('RawMatProviderRawMatProviderID')->nullable();
            $table->foreign(['RawMatProviderRawMatProviderID'],'byProvider')->references(['RawMatProviderID'])->on('rawmatprovider')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rawmateconomat', function (Blueprint $table) {
            $table->dropForeign('byProvider');
            $table->dropColumn('RawMatProviderRawMatProviderID');
        });
    }
}
