<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateRawmatrequireRawmaterialprovider extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('rawmatrequire', function (Blueprint $table) {
            $table->integer('RawMatProviderRawMatProviderID')->nullable();
            $table->foreign(['RawMatProviderRawMatProviderID'], 'ownBy')->references(['RawMatProviderID'])->on('rawmatprovider')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rawmatrequire', function (Blueprint $table) {
            $table->dropColumn('RawMatProviderRawMatProviderID');
            $table->dropForeign('ownBy');
        });
    }
}
