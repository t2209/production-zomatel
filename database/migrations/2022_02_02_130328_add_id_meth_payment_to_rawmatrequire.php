<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdMethPaymentToRawmatrequire extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rawmatrequire', function (Blueprint $table) {
            $table->integer('PaymentMethPaymentMethID')->nullable();
            $table->foreign(['PaymentMethPaymentMethID'] , 'MethPaytoRawmat')->references(['PaymentMethID'])->on('payment_methods')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rawmatrequire', function (Blueprint $table) {
            //
        });
    }
}
