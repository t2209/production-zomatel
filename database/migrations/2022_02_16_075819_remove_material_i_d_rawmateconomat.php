<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveMaterialIDRawmateconomat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rawmateconomat', function (Blueprint $table) {
            $table->dropForeign("have6");
            $table->dropColumn('RawMaterialRawMaterialID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rawmateconomat', function (Blueprint $table) {
            //
        });
    }
}
