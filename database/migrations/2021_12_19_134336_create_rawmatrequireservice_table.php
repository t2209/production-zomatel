<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRawmatrequireserviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rawmatrequireservice', function (Blueprint $table) {
            $table->integer('RawMatRequireServiceID', true);
            $table->integer('RawMaterialRawMaterialID')->nullable()->index('haveRMRS1');
            $table->integer('ServiceServiceID')->index('containsRMTS1');
            $table->float('RawMatRequireServiceQtyNeeded', 10, 0);
            // $table->float('RawMatReceiveServiceQty', 10, 0);
            $table->foreign(['RawMaterialRawMaterialID'], 'haveRMRS1')->references(['RawMaterialID'])->on('rawmaterial')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['ServiceServiceID'], 'containsRMTS1')->references(['ServiceID'])->on('service')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->date('RawMatRequireServiceDate')->nullable();
            $table->date('RawMatRequireServiceDateConfirm')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rawmatrequireservice');
    }
}
