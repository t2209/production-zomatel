<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeyOrderArticle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articleorder', function (Blueprint $table) {
            $table->foreign(['OrderOrderID'], 'propertyOf')->references(['OrderID'])->on('order')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['ArticleServiceArticleServiceID'], 'orderFor')->references(['ArticleServiceID'])->on('articleservice')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articeorder', function (Blueprint $table) {
            $table->dropForeign('propertyOf');
            $table->dropForeign('orderFor');
        });
    }
}
