<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeyUserService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('service', 'UserUserID')) {

            Schema::table('service', function (Blueprint $table) {
                $table->integer('UserUserID')->nullable();
                $table->foreign(['UserUserID'], 'ownAccount2')->references(['UserID'])->on('user')->onUpdate('CASCADE')->onDelete('CASCADE');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service', function (Blueprint $table) {
            $table->dropForeign('ownAccount2');
            $table->dropColumn('UserUserID');

        });
    }
}
